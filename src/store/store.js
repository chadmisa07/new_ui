import { createStore, applyMiddleware } from 'redux'

import logger from 'redux-logger'
import promise from 'redux-promise-middleware'
import thunk from 'redux-thunk'
import reducer from '../reducers'
import { loadingBarMiddleware } from 'react-redux-loading-bar'
import throttle from 'lodash/throttle'

import PERSISTED_STATE_KEY from '../constants/keys'

import setAuthorizationToken from '../utils/setAuthorizationToken'

let persistedState = {}
let defaultState = {
  menuInterface: {
    arr:[]
  },
  textResponse: {
    arr:[]
  },
  quickReply: {
    arr:[]
  },
  app: {
    createBotMode: true,
  },
  taskEditor: {
    savingImage: false,
    cartContainer: null,
    checkoutContainer: null,
    deliveryContainer: null,
    operationContainer: null,
    productContainer: null,
    quantityContainer: null,
    bot: null,
    pickup:true,
    delivery:false,
    skip: false,
    editMode: false,
    taskName: "Order Bot",
    taskDescription: "Order Bot Description"
  },
  businessProcesses: {
    bots:[],
    purchases:[]
  }
}

if (localStorage.getItem(PERSISTED_STATE_KEY)){
  persistedState = JSON.parse(localStorage.getItem(PERSISTED_STATE_KEY)) 
} else {
  persistedState = defaultState
}

const middleware = applyMiddleware(promise(), thunk, logger(),loadingBarMiddleware({
      promiseTypeSuffixes: ['REQUEST', 'FULFILLED', 'REJECTED'],
    })
)

if (persistedState!==defaultState){
  setAuthorizationToken(persistedState.auth.access_token)
}

const store = createStore(
  reducer, 
  persistedState,
  middleware
)

store.subscribe(throttle(()=>{
  localStorage.setItem(PERSISTED_STATE_KEY, JSON.stringify(store.getState()))
}), 1000)

export default store
