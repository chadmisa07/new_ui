import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import * as api from '../utils/api'

export function createPersistentMenu(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/text-response/", data)
      .then(function (response) {
        dispatch({type: "PERSISTENT_MENU_CREATED", payload: response.data})
        toastr.success('Success', 'Persistent menu was created')
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to create PersistentMenu')
      })
  }  
}