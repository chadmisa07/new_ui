import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import * as api from '../utils/api'

export function createButtonResponse(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/button-response/", data)
      .then(function (response) {
        toastr.success('Success', 'Button response was created')
        dispatch({type: 'BUTTON_RESPONSE_CREATED', payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating button response')
      })
  }  
}