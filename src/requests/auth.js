import axios from 'axios'
import { getUser } from './user'
import { getAllBots, getFBPages } from './bot'
import setAuthorizationToken from '../utils/setAuthorizationToken'
import * as api from '../utils/api'

export function getToken(username, password){
  return function(dispatch) {
    dispatch({type: "LOGIN_REQUEST" })
    axios.post(api.URL+"o/token/", "grant_type=password&username="+username+"&password="+password, {
      auth: {
        username: "fCqpMV4GJoMDE0qWLj6WmGlIUI6lCT2FsJvZPbdN",
        password: "ssFqkuzblfcT7k4qtqU16kkTXJUcPtenNcAex1xwMFrwl8aKUvzKtpfICGVOW6AFPf2hJvdU7o184XwLdgummmbl5etLhOCcbXGq3uq3p3zJK2RE7XDRilW294Jh6EF7"
      }
    })
    .then(function (response) {
      setAuthorizationToken(response.data.access_token)
      dispatch(getUser(username))
      dispatch(getAllBots())
      dispatch(getFBPages())
      dispatch({type: "LOGIN_REQUEST_FULFILLED", payload: response.data, username: username})
    })
    .catch(function (error) {
      dispatch({type: "LOGIN_REQUEST_REJECTED"})
    })
  }
    
}