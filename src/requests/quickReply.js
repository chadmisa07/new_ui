import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { createJobflow } from './jobFlow'
import * as api from '../utils/api'

export function createQuickReply(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/quick-reply/", data)
      .then(function (response) {
        toastr.success('Success', 'Quick reply was created')
        dispatch({type: "QUICK_REPLY_CREATED", payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating quick reply')
      })
  }  
}

export function createQuickReplyArray(qr, jobFlow) {
  return function(dispatch) {
    axios.post(api.URL+"api/quick-reply/", qr)
      .then(function (response) {
        response.data.forEach(function(value) {
          dispatch({type: "QUICK_REPLY_CREATED", payload: value})
          toastr.success('Success', 'Quick reply was created')    
          jobFlow.quick_replies.push(value.id)
        })
        dispatch(createJobflow(jobFlow))
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating quick reply')
      })
  }  
}