import axios from 'axios'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as api from '../utils/api'

export function createJobflow(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/job-flow/", data)
      .then(function (response) {
        toastr.success('Success', 'Job flow was created. You may now test your bot.')
        dispatch({type:'RESET_BOT'})
        dispatch({type:'RESET_BUTTON_RESPONSE'})
        dispatch({type:'RESET_MENU_INTERFACE'})
        dispatch({type:'RESET_QUICK_REPLY'})
        dispatch({type:'RESET_TEXT_RESPONSE'})
        browserHistory.push('/main')
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating job flow.')
      })
  }  
}