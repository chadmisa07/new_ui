import axios from 'axios'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as api from '../utils/api'

export function createBot(data) {
  return function(dispatch) {
    dispatch({type: "CREATE_BOT_REQUEST" })
    axios.post(api.URL+"api/bot/", data)
      .then(function (response) {
        dispatch({type: "CREATE_BOT_REQUEST_FULFILLED", payload: response.data})
        dispatch({type: "ASSIGN_BOT_FOR_TASK", payload: response.data})
        toastr.success('Success', 'Bot was created')
        browserHistory.push('/bot/edit_bot')
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
        dispatch({type: "CREATE_BOT_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getBots() {
  return function(dispatch) {
    axios.get(api.URL+"api/bot/")
      .then(function (response) {
        dispatch({type:'BOTS_FETCHED', payload: response.data.results})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch bots')
      })
  }
}

export function getAllBots() {
  return function(dispatch) {
    dispatch({type:'BOTS_FETCH_REQUEST'})
    axios.get(api.URL+"api/bot/")
      .then(function(response) {
        dispatch({type:'BOTS_FETCH_REQUEST_FULFILLED', payload: response.data.results})
      })
      .catch(function (error) {
        dispatch({type:'BOTS_FETCH_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to fetch bots')
      })
  }
}

export function getFBPages() {
  return function(dispatch) {
    dispatch({type:'FB_PAGES_FETCH_REQUEST'})
    axios.get(api.URL+"api/fb-page/")
      .then(function (response) {
        dispatch({type:'FB_PAGES_FETCH_REQUEST_FULFILLED', payload: response.data.results})
      })
      .catch(function (error) {
        dispatch({type:'FB_PAGES_FETCH_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to fetch Facebook Pages')
      })
  }
}