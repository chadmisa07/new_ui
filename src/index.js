import React from 'react'
import ReactDOM from 'react-dom'
import Routes from './routes'
import './includes/css/asi.css'
import './includes/css/common.css'
import './includes/css/ie.css'
import './includes/css/style.css'

ReactDOM.render(
	<Routes />, document.getElementById('root')
)
