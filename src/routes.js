import React from 'react'
import { IndexRoute, Route, Router, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import Container from './components/main/previews/container'
import App from './components/app'
import Home from './components/home'
import BotEdit from './components/bot-edit'
import BotCreate from './components/bot-create'
import Login from './components/login'
import Logout from './components/logout'
import FBAuth from './components/fbAuth'
import * as Order from './components/main/taskEditor/order/index'
import * as BotCreation from './components/main/botCreation/index'
import * as BotAdministration from './components/main/botAdministration/index'
import * as BusinessProcess from './components/main/businessProcesses/index'
import * as BotTask from './components/main/botTask/index'
import store from './store/store'
import requireAuth from './utils/requireAuth'
import ReduxToastr from 'react-redux-toastr'
import * as TaskEditorSurvey from './components/main/taskEditor/survey/index'

const Routes = () => {
    return (
      <Provider store={store}>
        <div>
          <Router history={browserHistory}>
            <Route path="/" component={Login} />
            <Route path="/fb-preview" component={Container} />
            <Route path="/fb/*" component={FBAuth} />
            <Route path="/login" component={Login} />
            <Route path="/home" component={Home} />
            <Route path="/bot-create" component={BotCreate} />
            <Route path="/bot-edit" component={BotEdit} />
            <Route path="/logout" component={Logout} />
            <Route path="/order" component={requireAuth(App)} >
              <IndexRoute component={Order.OperationContainer} />
              <Route path="/order/operation" component={Order.OperationContainer} />
              <Route path="/order/product" component={Order.ProductContainer} />
              <Route path="/order/quantity" component={Order.QuantityContainer} />
              <Route path="/order/cart" component={Order.CartContainer} />
              <Route path="/order/delivery" component={Order.DeliveryContainer} />
              <Route path="/order/checkout" component={Order.CheckoutContainer} />
            </Route>
            <Route path="/survey" component={requireAuth(App)} >
              <IndexRoute component={TaskEditorSurvey.SurveyOperation} />
              <Route path="/survey/operation" component={TaskEditorSurvey.SurveyOperation} />
              <Route path="/survey/category" component={TaskEditorSurvey.SurveyCategory} />
              <Route path="/survey/question" component={TaskEditorSurvey.SurveyQuestion} />
            </Route>
            <Route path="/bot" component={App} >
              <IndexRoute component={BotCreation.CreateBotContainer} />
              <Route path="/bot/create" component={BotCreation.CreateBotContainer} />
              <Route path="/bot/create/list" component={BotCreation.BotListContainer} />
              <Route path="/bot/edit" component={BotCreation.EditBotContainer} />
              <Route path="/bot/edit-jobs" component={BotCreation.EditBotJobContainer} />
              <Route path="/bot/survey">
                <Route component={BotCreation.CreateSurveyContainer}>
                  <IndexRoute component={BotCreation.CreateSurvey} />
                  <Route path="/bot/survey/create" component={BotCreation.CreateSurvey} />
                </Route>
              </Route>
            </Route>
           <Route path="/task" component={requireAuth(App)} >
              <IndexRoute component={BotTask.EditBotJobContainer} />
              <Route path="/task/edit-jobs" component={BotTask.EditBotJobContainer} />
              <Route path="/task/survey/create" component={BotTask.CreateSurvey} />                  
              <Route path="/task/survey/edit" component={BotTask.EditSurvey} />
              <Route path="/task/survey/manage" component={BotTask.ManageSurvey} />
              <Route path="/task/survey-settings" component={BotTask.SettingsTab} />
              <Route path="/task/faq" component={BotTask.FAQ} />
            </Route>
            <Route path="/administration" component={requireAuth(App)} >
              <IndexRoute component={BotAdministration.AdministationMainContainer} />
              <Route path="/administration/main" component={BotAdministration.AdministationMainContainer} />
              <Route path="/administration/test" component={BotAdministration.TestAndLaunchContainer} />
            </Route>
            <Route path="/business-processes" component={requireAuth(App)} >
              <IndexRoute component={BusinessProcess.OrderManagementContainer} />
              <Route path="/business-processes/order-management" component={BusinessProcess.OrderManagementContainer} />
              <Route path="/business-processes/survey-management" component={BusinessProcess.SurveyManagementContainer} />
              <Route path="/business-processes/faq-management" component={BusinessProcess.FAQManagementContainer} />
              <Route path="/administration/test" component={BotAdministration.TestAndLaunchContainer} />
            </Route>
            <Route path="*" component={requireAuth(Home)} />
          </Router>
          <ReduxToastr
            timeOut={5000}
            newestOnTop={true}
            preventDuplicates={false}
            position="top-right"
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            progressBar/>
        </div>
      </Provider>
    )
}
export default Routes