import React from 'react'
import { Link } from 'react-router'
import logo from '../includes/img/logo.png'
import logo_small from '../includes/img/logo-small.png'

class Home extends React.Component {
  constructor(props){
    super(props)
    this.state  ={
      sidebar:true
    }
  }

  render() {
    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
          <div className="navbar navbar-fixed-top">
            <div className="navbar-inner">
              <div className="container-fluid">
                <a className="brand tac" href="sched_index_week.html">
                  <img src={logo} className="visible-desktop visible-tablet" alt="NTUITIV" />
                  <img src={logo_small} className="visible-phone" alt="NTUITIV" />
                </a>
                <ul className="nav user_menu nav-items">
                  <li className="hidden-phone hidden-tablet"></li>
                  <li className="dropdown visible-desktop">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="fa fa-user-circle fa-lg icon-blue"></i>&nbsp;&nbsp;
                      Hi, John Appleseed! &nbsp;<i className="fa fa-caret-down icon-blue"></i></a>
                    <ul className="dropdown-menu">
                      <li><a href="login.html">Change Password</a></li>
                      <li><a href="login.html">Logout</a></li>
                    </ul>
                  </li>
                </ul>
                <ul className="nav nav-btns">
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                   <Link to="/bot-create" activeClassName="active">
                    <i className="fa fa-plus-circle icon-blue"></i>
                    <span className="action-btn-label"> Create New Bot</span>
                   </Link>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                    <Link to="/bot-edit" activeClassName="active"><i className="fa fa-pencil icon-blue"></i>
                    <span className="action-btn-label"> Edit Bots</span></Link>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                    <Link to="/home" activeClassName="active"><i className="fa fa-file-code-o icon-blue"></i>
                    <span className="action-btn-label"> Edit Bot Tasks</span></Link>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                    <a href="#"><i className="fa fa-commenting-o icon-blue"></i>
                    <span className="action-btn-label"> Create Survey</span></a>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown visible-phone visible-tablet pull-right">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" data-target=".nav-collapse"><i className="fa fa-caret-down icon-blue"></i></a>
                    <ul className="dropdown-menu">
                      <li><a href="#">Change Password</a></li>
                      <li><a href="login.html">Logout</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div id="contentwrapper">
    <div className="main_content">
      <nav>
        <div className="breadCrumb module">
          <ul>
            <li><a href="index.html"><i className="icon-home"></i></a></li>
            <li>Bot</li>
            <li>Edit Bot Tasks</li>
          </ul>
        </div>
      </nav>

      <div className="row-fluid">
        <div className="span12">
          <h3 className="heading">Edit Bot Tasks</h3>
          <div className="row-fluid">
            <div className="span12">
              <div className="well">
                This is where you will <span className="orange-text">create new tasks or change the ones you created.</span>
                The steps involved are:
                <ol>
                  <li>Choose to create new or select the task to update.</li>
                  <li>Change descriptions, update the flows and contents using the Task Editor.</li>
                  <li>Save the new task and link it your bots.</li>
                </ol>  
                You just gave your bots new job!<br/><br/>
                *Note running bots need to be stopped first to associate the new jobs. 
                Data from old jobs are saved separately. For more information click <a href="#">here</a>.<br/><br/>
                Let's get started!
                <span className="pull-right">
                  <label htmlFor="checkbox">
                    <input type="checkbox" className="checkbox-position" id="checkbox"/> Don't show this again
                  </label>
                </span>
              </div>
            </div>
          </div>
          <div className="row-fluid">
            <div className="span12">
              <div className="well">
                <p className="blue-text">Select which Tasks to Edit or Create New from catalog of available jobs.</p>
                <br/>
                <p>
                  <a href="#newTaskModal" className="btn btn-primary btn-small" data-toggle="modal" data-backdrop="static">
                    Add New
                  </a>
                </p>
                <div className="overflow">
                  <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                    <thead>
                      <tr>
                        <th>Task ID </th>
                        <th>Task Name</th>
                        <th>Description</th>
                        <th>Task Count</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr className="rowlink">
                        <td>123</td>
                        <td>Order Task</td>
                        <td>This is a bot that takes on orders.</td>
                        <td>2</td>
                        <td>
                          <a className="btn btn-primary btn-small">Edit</a>
                          <a className="btn btn-danger btn-small" href="#deleteTaskModal" data-toggle="modal" data-backdrop="static">Delete</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <a onClick={() => this.setState({sidebar:!this.state.sidebar})} className="sidebar_switch on_switch" title="Hide Menu"></a>
  <div className="sidebar">
    <div className="antiScroll">
      <div className="antiscroll-inner">
        <div className="antiscroll-content">
          <div className="sidebar_inner">
            <div className="timer tac">
              <span className="timer-time">14:09:56</span><br/>
              <span className="timer-date">Friday, August 28, 2015</span>
            </div>
          <div id="side_accordion" className="accordion">
            <div className="accordion-group">
              <div className="accordion-heading">
                <a href="#collapseOne" className="accordion-toggle" onClick="location.href='#'">
                  <i className="fa fa-cogs icon-blue"></i>&nbsp;&nbsp;&nbsp;Bot Creation
                </a>
              </div>
            </div>
            <div className="accordion-group">
              <div className="accordion-heading">
                <a href="#collapseOne" className="accordion-toggle" onClick="location.href='#'">
                  <i className="fa fa-briefcase icon-blue"></i>&nbsp;&nbsp;&nbsp;Bot Administration
                </a>
              </div>
            </div>
            <div className="accordion-group">
              <div className="accordion-heading">
                <a href="#collapseEleven" data-parent="#side_accordion" data-toggle="collapse" className="accordion-toggle">
                  <i className="fa fa-bar-chart icon-blue"></i>&nbsp;&nbsp;&nbsp;Business Process
                </a>
              </div>
              <div className="accordion-body collapse" id="collapseEleven">
                <div className="accordion-inner">
                  <ul className="nav nav-list">
                    <li><a href="#">Order Management</a></li>
                    <li><a href="#">Survey Management</a></li>
                    <li><a href="#">FAQ Management</a></li>
                    <li><a href="#">Custom Task</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="push"></div>
          </div> 
          </div>
        </div>
      </div>
    </div>
  </div>

  <div className="modal hide fade" id="newTaskModal">
    <div className="modal-header modal-header-new">
      <button className="close" data-dismiss="modal">×</button>
      <h3>Persona or Task Editor</h3>
    </div>
    <div className="modal-body">
      <div className="row-fluid">
        <div className="span18">
            Please choose a persona or role. <br/>
            <ol>
              <li><a href="">Item 1</a></li>
              <li><a href="">Item 1</a></li>
              <li><a href="">Item 1</a></li>
              <li><a href="">Item 1</a></li>
              <li><a href="">Item 1</a></li>
              <li><a href="">Item 1</a></li>
            </ol>
          <div className="clear-10"></div>
          <div className="row-fluid">
            <div className="btns-action">
              <a href="#newTaskModal" data-toggle="modal" data-backdrop="static" className="btn">Cancel</a>
            </div>
          </div>
          <div className="formSep"></div>
        </div>
      </div>
    </div>
  </div>

  <div className="modal hide fade" id="deleteTaskModal">
    <div className="modal-header modal-header-new">
      <button className="close" data-dismiss="modal">×</button>
      <h3>Delete Task</h3>
    </div>
    <div className="modal-body">
      <div className="row-fluid">
        <div className="span18">
          <p>You are about to delete Survey Task with Task ID 308. Are you sure you want to delete this bot?</p>
          <div className="clear-10"></div>
          <div className="row-fluid">
            <div className="btns-action">
              <a className="btn btn-danger btn-small" href="#deleteTaskModal" data-toggle="modal" data-backdrop="static">Delete</a>
              <a className="btn btn-small" href="#deleteTaskModal" data-toggle="modal" data-backdrop="static">Cancel</a>
            </div>
          </div>
          <div className="formSep"></div>
        </div>
      </div>
    </div>
  </div>
  </div>
        </div>

    )
  }
}

export default Home