import React from 'react'
import { connect } from 'react-redux'
import { Grid, Col } from 'react-bootstrap'
import { browserHistory } from 'react-router'

import { getFacebookData } from './fbRequest'


const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
    fb_pages: state.botAdministration.fb_pages,
  }
}


class FBAuth extends React.Component {
  componentWillMount(){
    if(!this.props.location.query.error){
      this.props.dispatch(getFacebookData(this.props.location.query.code, this.props.user.id, this.props.auth.access_token, this.props.fb_pages))
    }else{
      browserHistory.push("/administration")
    }
  }

  render() {
		return (
      <Grid fluid>
        <div>
          <Col lg={8}>
            <h2>We are communicating with Facebook to connect to your account. Please wait!</h2>
          </Col>
        </div>
      </Grid>
		)
  }
}

export default connect(mapStateToProps)(FBAuth)
