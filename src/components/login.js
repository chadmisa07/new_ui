import React from 'react'
import { connect } from 'react-redux'
import { login } from '../actions/loginActions'
import logo_small from '../includes/img/logo-small.png'
import { toastr } from 'react-redux-toastr'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

class Login extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      usernameInput: "",
      passwordInput: "",
      erorr: ""
    }
  }


  login() {
    let username = this.state.usernameInput
    let password = this.state.passwordInput

    if(!username.replace(/\s/g, '').length){
      toastr.error('Error', 'Please enter a username')
    }else if(!password.replace(/\s/g, '').length){
      toastr.error('Error', 'Please enter a password')
    }else{
      this.props.dispatch(login(username, password))
    }
  }

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.login()
    }
  }

  handleUsernameChange = (event) => {
    this.setState({ usernameInput: event.target.value })
  }

  handlePasswordChange = (event) => {
    this.setState({ passwordInput: event.target.value })
  }

  clearInputs(){
    this.setState({ passwordInput: "" })
    this.setState({ usernameInput: "" })
  }

  render() {
		return (
      <div>
        <div className="login_page">
          <div className="login_box">
           <form>
              <div className="top_b"><img src={logo_small} className="logo" alt="NTUITIV" /></div>
              {
                this.props.auth.error != null? 
                  <div className="alert alert-danger alert-login tac">{this.props.auth.error}</div>
                  :
                  null
              }
              <br/>
              <div className="cnt_b">
                <div className="formRow">
                  <div className="input-prepend">
                    <span className="add-on"><i className="icon-user"></i></span>
                    <input type="text" id="username" name="username" placeholder="Username" onChange={this.handleUsernameChange.bind(this)}/>
                  </div>
                </div>
                <div className="formRow">
                  <div className="input-prepend">
                    <span className="add-on"><i className="icon-lock"></i></span>
                    <input type="password" id="password" name="password" placeholder="Password" 
                     onChange={this.handlePasswordChange.bind(this)} onKeyPress={this.handleKeyPress.bind(this)}/>
                  </div>
                </div>
              </div>
              <div className="btm_b clearfix">
                <div className="pull-right">
                  <button className="btn btn-success" type="button"
                    disabled={this.props.auth.fetching} onClick={this.login.bind(this)}>
                      {this.props.auth.fetching ? 'Signing in...' : 'Sign in'}
                  </button>
                  <button className="btn" type="button" onClick={this.clearInputs.bind(this)}>Clear</button>
                </div>
              </div> 
            </form>
          </div>
        </div>
      </div>
		)
  }
}

export default connect(mapStateToProps)(Login)
