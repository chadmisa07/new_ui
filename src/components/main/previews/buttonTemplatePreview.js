import React from 'react'
import { Row } from 'react-bootstrap'

const ButtonTemplatePreview = (props) => {
  return(
    <div>
      <Row className="font-color-blue margin-bottom-top">
      </Row>
      <div className="chatbox-border">
        <Row className="padding-top-15">
          <div className="text-center pull-left">
            <div className="btn-response-text text-center pull-left">
             {props.survey.question}
            </div>
            {props.answers}
          </div>
        </Row>
      </div>
    </div>
  )
}

export default ButtonTemplatePreview
