import React from 'react'
import { Row } from 'react-bootstrap'

const QuickReplyPreview = (props) => {
  return(
    <div>
      <div className="form-padding chatbox-border">
        <Row>
          <p className="recipient-message-bubble text-center pull-left">
            {props.survey.question}
          </p>
        </Row>
        <Row>
          {props.answers}
        </Row>
      </div>
    </div>
  )
}

export default QuickReplyPreview
