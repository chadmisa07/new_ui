import React from 'react'
import { Row, Thumbnail, Col } from 'react-bootstrap'

const ListTemplatePreview = (props) => {
  return(
    <div>
      <Row className="font-color-blue margin-bottom-top">
      </Row>
      <div className="chatbox-border">
        <Row className="padding-top-15">
          <div>
            <Col lg={12}>
              <Thumbnail src={props.survey.question_image} >
                <div className="listTitle">{props.survey.question}</div>
                  {props.answers}
              </Thumbnail>
            </Col>
          </div>
        </Row>
      </div>
    </div>
  )
}

export default ListTemplatePreview
