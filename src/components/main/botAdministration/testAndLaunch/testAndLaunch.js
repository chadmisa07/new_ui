import React from 'react'

const TestAndLaunch = (props) => {
  return (
     <tr>
      <td className="tac"><a>{props.bot.id}</a></td>
      <td className="tac"><a>{props.bot.name}</a></td>
      <td className="tac">{props.bot.description}</td>
      <td className="tac">
        {
          props.bot.task===null?
            null
          :
            <a>{props.bot.task.name}</a>
        }
      </td>
      <td className="tac">Waiting for deployment</td>
      <td className="tac">
        {
          props.bot.task===null?
            null
          :props.fb_pages.length===0?
            <button type="button" className="btn btn-primary btn-small span12" 
            data-toggle="modal" data-backdrop="static" href="#noFacebookPageModal"
            onClick={props.openModal}>
                Test & Launch
            </button>
          :
          <button type="button" className="btn btn-primary btn-small span12" 
          data-toggle="modal" data-backdrop="static" href="#deployModal"
          onClick={props.openModal}>
              Test & Launch
          </button>
        }
      </td>
    </tr>
  )
}

export default TestAndLaunch