import React from 'react'

const NoFbPageModal = (props) => {
  return (
   <div className="modal hide fade" id="noFacebookPageModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>No Facebook Page Present</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span18">
            <p>There are no Facebook pages linked yet. You need to link a Facebook page for your bot.</p>
            <div className="clear-10"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="btns-action">
          <a href="https://www.facebook.com/v2.8/dialog/oauth?client_id=1038731512904671&
              redirect_uri=https://cbot-tooling.herokuapp.com/fb/&auth_type=rerequest&scope=email,
              public_profile,manage_pages,pages_messaging,pages_messaging_subscriptions" 
              className="btn btn-success btn-small">Connect With Facebook</a>
          <button className="btn btn-small btn-default" data-dismiss="modal">Deploy Later</button>
        </div>
      </div>
    </div>
  )
}

export default NoFbPageModal