import React from 'react'
import * as Common from '../commons/index'
import * as BotAdministration from './index'
import { connect } from 'react-redux'
import * as Request from '../request/request'
import { toastr } from 'react-redux-toastr'
import setAuthorizationToken from '../../../../utils/setAuthorizationToken'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot:  state.bot,
    user: state.user,
    bots: state.botAdministration.bots,
    fb_pages: state.botAdministration.fb_pages.sort(function(a, b){return a.id - b.id}),
    botCount: state.bot.count, 
  }
}

class TestAndLaunchContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      showModal:false,
      selectedFbPage:{},
      selectedBot: "",
      currentPage: 1,
      displayNumber: 3,
      pageNumber: 0, 
      showAdditionalInfo: true,
      sidebar: true,
      date: new Date(),
    }
  }

  componentWillMount() {
    if(this.props.auth.access_token){
      setAuthorizationToken(this.props.auth.access_token)
    }
    this.props.dispatch(Request.getBot(1))
    this.props.dispatch(Request.getFBPages())
  }

  componentDidMount() {
    this.setState({selectedFbPage: this.props.fb_pages[0]})  
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  closeModal() {
    this.setState({ showModal: false });
  }

  openModal(id) {
    this.setState({ selectedBot: id });
    this.setState({ showModal: true });
    this.setState({selectedFbPage: this.props.fb_pages[0]})  
  }

  mockDeploy() {
    toastr.success('Success', 'Bot was successfully deployed')
    this.closeModal()
  }

  deployBot() {
      let selectedFbPage = this.state.selectedFbPage
      let data = {
      id: selectedFbPage.id,
      name: selectedFbPage.name,
      description: "This is a page description",
      page_token: selectedFbPage.page_token,
      page_id: selectedFbPage.page_id,
      messenger_link: "https://m.me/"+selectedFbPage.page_id,
      bot: null,
      created_by: this.props.user.id,
      updated_by: this.props.user.id
    }
    this.setState({selectedFbPage:data})
    this.props.dispatch(Request.deployBot(data, {bot:this.state.selectedBot}, this.props.auth.access_token))
    this.props.dispatch({type:"DEPLOY_BOT", payload: true, overlayText: "Deploying Bot. Please wait."})
  }

  handleFBPageSelection = (e) => {
    this.setState({selectedFbPage: this.props.fb_pages[e.target.value]})
  }

   handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(Request.getBot(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(Request.getBot(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(Request.getBot(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(Request.getBot(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.botCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(Request.getBot(bots.length))
     this.setState({currentPage: bots.length})
  }

  toggleInfo(){
    this.setState({ showAdditionalInfo: !this.state.showAdditionalInfo })
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {

    let pages = this.props.fb_pages
    let deployedBots = []
    for(let i=0;i<pages.length;i++){
      if (pages[i].bot!==null) {
        deployedBots.push(pages[i].bot)
      }
    }

    let allBots = this.props.bots
    let undeployedBots = []
    for(let i=0;i<allBots.length;i++){
      let flag = false
      for (let j=0;j<deployedBots.length;j++) {
        JSON.stringify(allBots[i])
        if (allBots[i].id===deployedBots[j]) {
          flag = true
        }
      }
      if (!flag) {
        undeployedBots.push(allBots[i])
      }
    }

    const bots = undeployedBots.map((bot, index) => {
      return (<BotAdministration.TestAndLaunch fb_pages={this.props.fb_pages} bot={bot} key={index} openModal={this.openModal.bind(this, bot.id)}/>)
    })
    const fb_pages = pages.map((fb_page, index) => {
      return (<option value={index} key={index}>{fb_page.name}</option>)
    })

    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.botCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""} onClick={this.handleTransferPage.bind(this)}>{number}</a>
        )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>
        <div id="myNav" className={this.props.bot.showOverlay? "overlay fullwidth":"overlay clearwidth"}>
          <div className="overlay-content">
            {this.props.bot.overlayText}
          </div>
        </div>
        <div id="contentwrapper">
          <div className="main_content">
            <BotAdministration.Nav />

            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Test & Launch</h3>
                <div className="row-fluid">
                  <div className="span12">
                    <form className="form-horizontal">
                      <BotAdministration.AdditionalInfo toggleInfo={this.toggleInfo.bind(this)} {...this.state}/>
                      <fieldset className="margin-top-20">
                        <BotAdministration.Table bots={bots}
                         renderPageNumbers={renderPageNumbers}
                         currentPage={this.state.currentPage}
                         handleFirst={this.handleFirst.bind(this)}
                         handlePrevious={this.handlePrevious.bind(this)}
                         handleNext={this.handleNext.bind(this)}
                         handleLast={this.handleLast.bind(this)}/>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.SideBar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
        <BotAdministration.NoFbPageModal />
       <BotAdministration.LaunchModal 
          fb_pages={fb_pages}
          showModal={this.state.showModal}
          closeModal={this.closeModal.bind(this)}
          handleFBPageSelection={this.handleFBPageSelection.bind(this)}
          deployBot={this.deployBot.bind(this)}/>
    </div>
    )
  }
}

export default connect(mapStateToProps)(TestAndLaunchContainer)