import React from 'react'

class AdditionalInfo extends React.Component{
  render(){
    return (
      <div className="well margin-top-5">
        {
          this.props.showAdditionalInfo?
          <div>
            <div>
              <p>This is where you will <span className="orange-text">launch and manage bots</span>.</p>
              <p>The Steps involved are:</p>
              <ol>
                <li>Select the bot to test and launch</li>
                <li>Confirm the settings (specific for each platform)</li>
                <li>Link to associated account (ie: Facebook Page)</li>
              </ol>
              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You're ready to test it! If there are no problems, then you can launch it!</p>
            </div>
            <div>
              <span>&nbsp;</span>
              <span className="pull-right"> 
                <label>
                  <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position"/>
                  <span>&nbsp;Hide Additional Info</span>
                </label>
              </span>
            </div>
          </div>:
          <span className="pull-right"> 
            <label>
              <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position"/>
              <span>&nbsp;Show Additional Info</span>
            </label>
          </span>
        }
      </div>
    )
  }
}

export default AdditionalInfo