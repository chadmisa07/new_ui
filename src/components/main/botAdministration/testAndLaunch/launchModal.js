import React from 'react'

const LaunchModal = (props) => {
	return (
		<div className="modal hide fade" id="deployModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Deploy Bot</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <p><b>Select what Facebook page the attach to bot</b></p>
            <select className="span12" onChange={props.handleFBPageSelection} >
              {props.fb_pages}
            </select>
            <div className="clear-10"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="btns-action">
          <button type="button" className="btn btn-success btn-small" onClick={props.deployBot} 
            data-dismiss="modal">Deploy
          </button>
          <button type="button" className="btn btn-small btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
	)
}

LaunchModal.propTypes = {
	showModal: React.PropTypes.bool,
	closeModal: React.PropTypes.func,
	handleOnChange: React.PropTypes.func,
	fb_pages: React.PropTypes.array,
	deployBot: React.PropTypes.func
}

export default LaunchModal