import React from 'react'

const BotTable = (props) =>{
	return(
		<div className="well">
      <p><u><i className="caret margin-top-7"></i><b> Created but not yet launched</b></u></p>
  	  <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
      	<tbody>
         	<tr>
          	<th className="tac"><u>Bot ID</u></th>
          	<th className="tac"><u>Bot Name</u></th>
          	<th className="tac"><u>Description</u></th>
          	<th className="tac">Linked Tasks</th>
          	<th className="tac">Status</th>
          	<th className="tac" width="130">Action</th>
          </tr>
            {props.bots}
        </tbody>
      </table>
        	<div className="pagination">
                {props.renderPageNumbers.length!==0?
                <a href="#" onClick={props.currentPage !== 1 && props.renderPageNumbers.length!==0? 
                props.handleFirst : ""}>&#x27EA;</a>:""}
                  
                {props.currentPage > 1 ? <a href="#" onClick={props.handlePrevious}>&#x27E8;</a> : ""}
                {props.renderPageNumbers}
                {props.currentPage <= props.renderPageNumbers.length + 2 && props.renderPageNumbers.length!== 1 &&
                    props.renderPageNumbers.length!==0?<a href="#" onClick={props.handleNext}>&#x27E9;</a>:""}
                {props.renderPageNumbers.length!==0?
                   <a href="#" onClick={props.currentPage !== props.renderPageNumbers.length + 2 && 
                    props.renderPageNumbers.length !== 1 && props.renderPageNumbers.length!==0? props.handleLast : ""}>
                   &#x27EB;</a>: ""
                }
            </div>
        </div>
	)
}

BotTable.propTypes = {
	bots : React.PropTypes.array,
	currentPage : React.PropTypes.number,
	renderPageNumbers : React.PropTypes.array,
	handleFirst : React.PropTypes.func,
	handlePrevious : React.PropTypes.func,
	handleNext : React.PropTypes.func,
	handleLast : React.PropTypes.func,
}

export default BotTable