import React from 'react'

const Main = (props) =>{

  return (
    <div className="well">
      <p className="orange-text">Your current Bot Status</p>
        <fieldset className="margin-top-20">
          <div className="span12">
            <p><u><i className="caret margin-top-7"></i><b> Launched Bots</b></u></p>
              <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                <thead>
                  <tr>
                    <th className="tac">Bot ID</th>
                    <th className="tac">Bot Name</th>
                    <th className="tac">Bot Description</th>
                    <th className="tac">Linked Task</th>
                    <th className="tac">Status</th>
                    <th className="tac" width="130">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {!props.deployedBotsFinal.length ?
                    <tr><td colSpan="6" className="align-center"><i>No launched bot</i></td></tr> : 
                    props.deployedBots
                  }
                </tbody>
              </table>
            <div className="margin-top-20">
              <p><u><i className="caret margin-top-7"></i><b> Created but not yet launched</b></u></p>
              <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                <thead>
                  <tr>
                    <th className="tac">Bot ID</th>
                    <th className="tac">Bot Name</th>
                    <th className="tac">Bot Description</th>
                    <th className="tac">Linked Task</th>
                    <th className="tac">Status</th>
                    <th className="tac" width="130">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    !props.undeployBotCount.length?
                    <tr>
                      <td colSpan="6" className="tac">No Bot found</td>
                    </tr> : null
                  }
                  {props.undeployedBots}
                </tbody>
              </table>
            </div>
          </div>
        </fieldset>
    </div>
  )
}

Main.propTypes = {
  deployedBotsFinal: React.PropTypes.array,
  undeployedBots: React.PropTypes.array,
  currentPage: React.PropTypes.number,
  pageNumbers: React.PropTypes.array,
  handleFirst: React.PropTypes.func,
  handlePrevious: React.PropTypes.func,
  handleNext: React.PropTypes.func,
  handleLast: React.PropTypes.func,
}

export default Main