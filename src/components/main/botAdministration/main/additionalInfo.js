import React from 'react'

class AdditionalInfo extends React.Component{
  render(){
    return (
      <div className="well margin-top-5">
        {
          this.props.showAdditionalInfo?
          <div>
            <div>
              <p>This is where you will <span className="orange-text">launch and manage bots</span>.</p>
              <p>You can select bots to start, stop or see their detail status by clicking on their names.</p>
              <p>The Dashboard allows you to see quick analysis on the over-all status of your bots.</p>
            </div>
            <div>
              <span>&nbsp;</span>
              <span className="pull-right"> 
                <label>
                  <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position"/>
                  <span>&nbsp;Hide Additional Info</span>
                </label>
              </span>
            </div>
          </div>:
          <span className="pull-right"> 
            <label>
              <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position"/>
              <span>&nbsp;Show Additional Info</span>
            </label>
          </span>
        }
      </div>
    )
  }
}

export default AdditionalInfo