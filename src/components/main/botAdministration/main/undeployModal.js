import React from 'react'

const UndeployModal = (props) => {
  return (
    <div className="modal hide fade" id="unDeployModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Undeploy Bot</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <h5>Are you sure you want to stop this bot?</h5>
            <div className="clear-10"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="btns-action">
          <button type="button" className="btn btn-danger btn-small" onClick={props.stopBot} 
            data-dismiss="modal">Stop
          </button>
          <button type="button" className="btn btn-small btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  )
}

export default UndeployModal