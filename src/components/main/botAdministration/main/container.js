import React from 'react'
import * as Common from '../commons/index'
import * as BotAdministration from './index'
import { connect } from 'react-redux'
import * as Request from '../request/request'
import setAuthorizationToken from '../../../../utils/setAuthorizationToken'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'


const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    botCount: state.bot.count, 
    user: state.user,
    botList: state.bot.bots,
    bots: state.botAdministration.bots,
    fb_pages: state.botAdministration.fb_pages.sort(function(a, b){return a.id - b.id}),
    facebook: state.facebook
  }
}

class AdministationMainContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      showLaunchModal:false,
      showUndeployModal:false,
      selectedFbPage: {},
      selectedBot: "",
      currentPage: 1,
      displayNumber: 3,
      pageNumber: 0, 
      showAdditionalInfo: true,
      showNoFbPageModal: false,
      sidebar: true,
      date: new Date(),
    }
  }

  componentWillMount() {
    if(this.props.auth.access_token){
      setAuthorizationToken(this.props.auth.access_token)
    }
    this.props.dispatch(Request.getBot(1))
    this.props.dispatch(Request.getFBPages())
  }

  componentDidMount() {
    this.setState({selectedFbPage: this.props.fb_pages[0]})  
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  closeLaunchModal() {
    this.setState({ showLaunchModal: false })
  }

  openNoFbPageModal(){
    this.setState({ showNoFbPageModal: true })
  }

  closeNoFbPageModal(){
    this.setState({ showNoFbPageModal: false })
  }

  openLaunchModal(id) {
    this.setState({ selectedBot: id })
    this.setState({ showLaunchModal: true })
    this.setState({selectedFbPage: this.props.fb_pages[0]})  
  }

  closeUndeployModal() {
    this.setState({ showUndeployModal: false })
  }

  openUndeployModal(id) {
    this.setState({ selectedBot: id })
    this.setState({ showUndeployModal: true })
  }

  handleOnChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
  }

  handleFBPageSelection = (e) => {
    this.setState({selectedFbPage:this.props.fb_pages[e.target.value]})
  }

  deployBot() {
    let selectedFbPage = this.state.selectedFbPage
    let data = {
      id: selectedFbPage.id,
      name: selectedFbPage.name,
      description: "This is a page description",
      page_token: selectedFbPage.page_token,
      page_id: selectedFbPage.page_id,
      messenger_link: "https://m.me/"+selectedFbPage.page_id,
      bot: null,
      created_by: this.props.user.id,
      updated_by: this.props.user.id
    }
    this.setState({selectedFbPage:data})
    this.props.dispatch(Request.deployBot(data, {bot:this.state.selectedBot}, this.props.auth.access_token))
    this.closeLaunchModal()
    this.props.dispatch({type:"DEPLOY_BOT", payload: true, overlayText: "Deploying Bot. Please wait."})
  }

  stopBot() {
    this.props.dispatch(Request.stopBot(this.state.selectedBot, this.props.auth.access_token))
    this.closeUndeployModal()
  }

  handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(Request.getBot(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(Request.getBot(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(Request.getBot(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(Request.getBot(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.botCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(Request.getBot(bots.length))
     this.setState({currentPage: bots.length})
  }

  toggleInfo(){
    this.setState({ showAdditionalInfo: !this.state.showAdditionalInfo })
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {

     const fb_pages = this.props.fb_pages.map((fb_page, index) => {
      return (<option value={index} key={index}>{fb_page.name}</option>)
    })

    let pages = this.props.fb_pages

    let deployedBots = []
    for(let i=0;i<pages.length;i++){
      if (pages[i].bot!==null) {
        deployedBots.push(pages[i].bot)
      }
    }

    let allBots = this.props.botList
    let undeployedBots = []
    let deployedBotsFinal = []
    for(let i=0;i<allBots.length;i++){
      let flag = false
      for (let j=0;j<deployedBots.length;j++) {
        JSON.stringify(allBots[i])
        if (allBots[i].id===deployedBots[j]) {
          flag = true
        }
      }
      if (!flag) {
        undeployedBots.push(allBots[i])
      } else {
        deployedBotsFinal.push(allBots[i])
      }
    }

    undeployedBots = undeployedBots.map((bot, index) => {
      return (
        <tr key={index} className='text-center'>
          <td className="tac"><a>{bot.id}</a></td>
          <td className="tac">{bot.name}</td>
          <td className="tac">{bot.description}</td>
          <td className="tac">{bot.task!==null?bot.task.name:""}</td>
          <td className="tac">Waiting for deployment</td>
          <td className="tac">
            {
              bot.task===null?
                null
              :this.props.fb_pages.length===0?
                <button 
                  className="btn btn-primary btn-small span12"
                  data-toggle="modal" data-backdrop="static"
                  onClick={this.openNoFbPageModal.bind(this)}
                  href="#noFacebookPageModal" type="button">
                    Test & Launch
                </button>
                :
                <button className="btn btn-primary btn-small span12"
                  data-toggle="modal" data-backdrop="static" 
                  onClick={this.openLaunchModal.bind(this, bot.id)}
                  href="#deployModal" type="button">
                    Test & Launch
                </button>
            }
          </td>
        </tr>
      )
    })

    let deployedBotList = deployedBotsFinal.map((bot, index) => {
      return (
         <tr key={index} className='text-center'>
          <td className="tac"><a>{bot.id}</a></td>
          <td className="tac">{bot.name}</td>
          <td className="tac">{bot.description}</td>
          <td className="tac">{bot.task!==null?bot.task.name:"None"}</td>
          <td className="tac">Deployed</td>
          <td className="tac">
            <button 
              data-toggle="modal" data-backdrop="static"
              className="btn btn-danger btn-small span12"
              onClick={this.openUndeployModal.bind(this, bot.id)} 
              href="#unDeployModal">
              Stop
            </button>
          </td>
        </tr>
      )
    })

    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.botCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""} onClick={this.handleTransferPage.bind(this)}>{number}</a>
        )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>
        <div id="myNav" className={this.props.bot.showOverlay? "overlay fullwidth":"overlay clearwidth"}>
          <div className="overlay-content">
            {this.props.bot.overlayText}
          </div>
        </div>
        <div id="contentwrapper">
          <div className="main_content">
            <BotAdministration.Nav />

            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Main</h3>
                <div className="row-fluid">
                  <div className="span12">
                    <form className="form-horizontal">
                      <BotAdministration.AdditionalInfo toggleInfo={this.toggleInfo.bind(this)} {...this.state}/>
                      <fieldset className="margin-top-20">
                        <BotAdministration.Main {...this.props}  {...this.state}
                          undeployBotCount={undeployedBots}
                          undeployedBots={undeployedBots}
                          deployedBots={deployedBotList}
                          deployedBotsFinal={deployedBotsFinal}
                          pageNumbers={renderPageNumbers}
                          handleNext={this.handleNext.bind(this)}
                          handleFirst={this.handleFirst.bind(this)}
                          handlePrevious={this.handlePrevious.bind(this)}
                          handleLast={this.handleLast.bind(this)}/>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.SideBar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
        <BotAdministration.NoFbPageModal />
        <BotAdministration.LaunchModal {...this.state} 
          fb_pages={fb_pages}
          closeLaunchModal={this.closeLaunchModal.bind(this)}
          handleOnChange={this.handleOnChange.bind(this)}
          handleFBPageSelection={this.handleFBPageSelection.bind(this)}
          deployBot={this.deployBot.bind(this)}/>
        <BotAdministration.UndeployModal {...this.state} 
          closeUndeployModal={this.closeUndeployModal.bind(this)}
          stopBot={this.stopBot.bind(this)} />
    </div>
    )
  }
}

export default connect(mapStateToProps)(AdministationMainContainer)