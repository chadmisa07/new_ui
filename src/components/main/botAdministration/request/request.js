import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { browserHistory } from 'react-router'
import * as api from '../../../../utils/api'

export function getBots() {
  return function(dispatch) {
    dispatch({type:'BOTS_FETCH_REQUEST'})
    axios.get(api.URL+"api/bot/")
      .then(function(response) {
        dispatch({type:'BOTS_FETCH_REQUEST_FULFILLED', payload: response.data.results})
      })
      .catch(function (error) {
        dispatch({type:'BOTS_FETCH_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to fetch bots')
      })
  }
}

export function getBot(page) {
  return function(dispatch) {
  dispatch({type: "GET_BOT_REQUEST" })
  axios.get(api.URL+"api/bot/?page="+page)
    .then(function (response) {
      dispatch({type: "GET_BOT_REQUEST_FULFILLED", payload: response.data})
      dispatch({type:'BOTS_FETCH_REQUEST_FULFILLED', payload: response.data.results})
      if(response.data.count === 0){
        toastr.info('Information', 'You don\'t have a bot to be deploy.')
      }
    })
    .catch(function (error) {
      toastr.error('Error', 'Failed to fetch bots')
      dispatch({type: "GET_BOT_REQUEST_REJECTED", payload: error})
    })
  }  
}

export function getFBPages() {
  return function(dispatch) {
    dispatch({type:'FB_PAGES_FETCH_REQUEST'})
    axios.get(api.URL+"api/fb-page/")
      .then(function (response) {
        dispatch({type:'FB_PAGES_FETCH_REQUEST_FULFILLED', payload: response.data.results})
      })
      .catch(function (error) {
        dispatch({type:'FB_PAGES_FETCH_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to fetch Facebook Pages')
      })
  }
}

export function deployBot(fb_page, data, access_token) {
  return function(dispatch) {
    dispatch({type:'DEPLOY_BOT_REQUEST'})
    axios.patch(api.URL+"api/fb-page/"+fb_page.id.toString()+"/", data)
      .then(function (response) {
          axios.post(api.URL+"app/update-thread/", {"fb_page":fb_page.id})
            .then(function (response) {
              delete axios.defaults.headers.common['Authorization']
              axios.defaults.headers.common['Authorization'] = 'Bearer ' + fb_page.page_token
              axios.post("https://graph.facebook.com/v2.8/"+fb_page.page_id+"/subscribed_apps")
              .then(function (response) {
                delete axios.defaults.headers.common['Authorization']
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
                toastr.success('Success', 'Bot was successfully deployed')
                dispatch({type:'DEPLOY_BOT_REQUEST_FULFILLED'})
                dispatch(getBots())
                dispatch(getFBPages())
                window.open('https://m.me/' + fb_page.page_id)
                browserHistory.push("/administration/main")
                dispatch({type:"DEPLOY_BOT", payload: false, overlayText: ""})
              })
              .catch(function (error) {
                dispatch({type: "SUBSCRIBE_REQUEST_REJECTED!" })
                toastr.error('Error', 'Failed to subcribe apps')
              })
            })
            .catch(function (error) {
              dispatch({type:'DEPLOY_BOT_REQUEST_REJECTED'})
              toastr.error('Error', 'Unable to deploy the bot')
            })
      })
      .catch(function (error) {
        dispatch({type:'DEPLOY_BOT_REQUEST_REJECTED'})
        toastr.error('Error', 'Unable to deploy the bot')
      })
  }
}

export function unlaunchBot(fb_page, data, access_token) {
  return function(dispatch) {
    dispatch({type:'UNLAUCH_BOT_REQUEST'})
    axios.patch(api.URL+"api/fb-page/"+fb_page.id.toString()+"/", data)
      .then(function (response) {
        toastr.success('Success', 'Bot was successfully stopped')
        dispatch(getBot(1))
        dispatch(getFBPages())
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + fb_page.page_token
        axios.delete("https://graph.facebook.com/v2.8/"+fb_page.page_id+"/subscribed_apps")
          .then(function (response) {
            let data = {
              "setting_type":"CALL_TO_ACTIONS",
              "thread_state":"existing_thread"
            }
            let url = "https://graph.facebook.com/v2.6/me/thread_settings?access_token="+fb_page.page_token
            axios.delete(url, {data:data})
              .then(function (response){
                toastr.success('Success', 'Deleted persistent menu')
              })
              .catch(function (error){
                toastr.error('Error', 'Failed to update persistent menu')
              })

            url = "https://graph.facebook.com/v2.6/me/messenger_profile?access_token="+fb_page.page_token
            data = {
              "fields":[
                "greeting"
              ]
            }

            axios.delete(url, {data:data})
              .then(function (response){
                toastr.success('Success', 'Deleted greeting text')
              })
              .catch(function (error){
                toastr.error('Error', 'Failed to delete greeting text')
              })
          })
          .catch(function (error) {
            toastr.error('Error', 'Failed to unsubcribe apps')
          })
        delete axios.defaults.headers.common['Authorization']
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
        dispatch({type:'UNLAUCH_BOT_REQUEST_FULFILLED'})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to stop the bot')
        dispatch({type:'UNLAUCH_BOT_REQUEST_REJECTED'})
      })
  }
}


export function stopBot(bot, access_token) {
  return function(dispatch) {
    dispatch({type:'STOP_BOT_REQUEST'})
    axios.get(api.URL+"api/fb-page/?bot="+bot.toString())
      .then(function (response) {
       dispatch(unlaunchBot(response.data.results[0], {bot:null}, access_token))
        dispatch({type:'STOP_BOT_REQUEST_FULFILLED'})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to stop the bot')
        dispatch({type:'STOP_BOT_REQUEST_REJECTED'})
      })
  }
}