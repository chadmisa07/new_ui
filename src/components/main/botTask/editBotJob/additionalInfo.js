import React from 'react'

class AdditionalInfo extends React.Component{
  render(){
    return (
      <div className="well margin-top-5">
        <div>
        {
          this.props.showAdditionalInfo?
          <div>
            This is where you will <span className="orange-text">create new tasks or change the ones you created.</span>
            The steps involved are:
            <ol>
              <li>Choose to create new or select the task to update.</li>
              <li>Change descriptions, update the flows and contents using the Task Editor.</li>
              <li>Save the new task and link it your bots.</li>
            </ol>  
            You just gave your bots new job!<br/><br/>
            *Note running bots need to be stopped first to associate the new jobs.
            Data from old jobs are saved separately. For more information click <a href="#">here</a>.<br/><br/>
            Let's get started!
            <span className="pull-right">
              <label htmlFor="checkbox">
                <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position" id="checkbox"/> Hide Additional Info
              </label>
            </span>
          </div>: 
          <span className="pull-right">
            <label htmlFor="checkbox">
              <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position" id="checkbox"/> Show Additional Info
            </label>
          </span>
        }
        </div>
      </div>
    )
  }
}

export default AdditionalInfo