import React from 'react'

const EditJobForm = (props) =>{
  return (
    <div className="well">
      <p className="blue-text">Select which Tasks to Edit or Create New from catalog of available jobs.</p>
      <br/>
      <p>
        <a href="#createNewTaskModal" className="btn btn-primary btn-small" data-toggle="modal" data-backdrop="static">
          <i className="fa fa-plus icon-white"></i>&nbsp;Create New
        </a>
      </p>
      <div className="overflow">
        <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
          <thead>
            <tr>
              <th className="tac">Task ID </th>
              <th className="tac">Task Name</th>
              <th className="tac">Description</th>
              <th className="tac">Task Count</th>
              <th className="tac" width="130">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              !props.taskCount?
              <tr>
                <td colSpan="5" className="tac">No Task found</td>
              </tr> : null
            }
            {props.tasks}
          </tbody>
        </table>
      </div>
      <div className="pagination">
        {props.pageNumbers.length!==0?
          <a href="#" onClick={props.currentPage !== 1 && props.pageNumbers.length!==0? 
            props.handleFirst : ""}>&#x27EA;</a>:""
        }
        {props.currentPage > 1 ? <a href="#" onClick={props.handlePrevious}>&#x27E8;</a> : ""}
        {props.pageNumbers}
        { props.currentPage <= props.pageNumbers.length + 2 && props.pageNumbers.length!== 1 &&
          props.pageNumbers.length!==0?<a href="#" onClick={props.handleNext}>&#x27E9;  </a>:""
        }
        {props.pageNumbers.length!==0?
          <a href="#" onClick={props.currentPage !== props.pageNumbers.length + 2 && 
            props.pageNumbers.length !== 1 && props.pageNumbers.length!==0? props.handleLast : ""}>
            &#x27EB;</a>: ""
        }
      </div>
    </div>
  )
}

EditJobForm.propTypes = {
  currentPage: React.PropTypes.number,
  pageNumbers: React.PropTypes.array,
  handleFirst: React.PropTypes.func,
  handlePrevious: React.PropTypes.func,
  handleNext: React.PropTypes.func,
  handleLast: React.PropTypes.func,
  tasks: React.PropTypes.array,
}

export default EditJobForm