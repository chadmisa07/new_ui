import React from 'react'

const CreateNewModal = (props) => {
  return (
    <div className="modal hide fade" id="createNewTaskModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Persona / Job Editor</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <p className="light-blue-text">Define Persona / Role</p>
            <ol>
              <li>Order Taker</li>
              <li>Info Concierge</li>
              <li>Survey Taker</li>
              <li>Campaign Announcer</li>
              <li>Help Provider</li>
            </ol>
            <div className="formSep"></div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateNewModal