import React from 'react'
import * as Common from '../index'
import * as EditBotTask from './index'
import { connect } from 'react-redux'
import * as Request from '../requests/request'
import { browserHistory } from 'react-router'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'


const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    tasks: state.task,
    data: state.task.data,
    taskCount: state.task.count
  }
}

class EditBotJobContainer extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      showModal: false,
      currentPage: 1,
      displayNumber: 3,
      pageNumber: 0, 
      showDeleteModal: false,
      id: null,
      name: null,
      showAdditionalInfo: true,
      sidebar: true,
      date: new Date(),
    }
  }

  componentDidUpdate(){
    let status = this.props.tasks
    let data = this.props.data

    if(status.menuInterfaceStatus && status.buttonResponseStatus && 
      status.textResponseStatus && status.quickReplyStatus &&
      status.locationInterfaceStatus && status.greetingTextStatus &&
      status.persistentMenuStatus){
      let productWelcomeMessage = data.text_response.find(message=>message.text_type === 34)
      let checkout = data.quick_replies.find(checkout=>checkout.qr_type === 53)
      let askRecept = data.quick_replies.find(receipt=>receipt.qr_type === 54)
      let askEmail = data.quick_replies.find(email=>email.qr_type === 55)
      let emailError = data.text_response.find(error=>error.text_type === 56)
      let provideEmail = data.text_response.find(text=>text.text_type === 57)
      let provideEmailError = data.text_response.find(text=>text.text_type === 58)
      let retryEmail = data.quick_replies.find(text=>text.qr_type === 59)
      
      let productContainer = {
            sidebar: true,
            savingImage: false,
            addMode: true,
            showProductModal: false,
            showPreviewModal: false,
            name: "Add Menu",
            code: "",
            description: "",
            price: 0,
            image_url: "",
            preview_url: "",
            item_url: "",
            add_button: "",
            view_description: "",
            current_index: "",
            previewInterface:{},
            welcomeText: {
              "message": productWelcomeMessage ? productWelcomeMessage.message : "",
              "id": productWelcomeMessage ? productWelcomeMessage.id : "",
              "text_type": 34,
              "created_by": this.props.user.id,
              "updated_by": this.props.user.id,
            },
            menuInterfaces: data.menu_interface.sort(function(a, b){return a.id - b.id}),
            nameError: null,
            codeError: null,
            descriptionError: null,
            priceError: null,
            image_url_error: null,
            item_url_error: null,
            add_button_error: null,
            view_description_error: null,
            invalidUrl: true,
            openWarningModal: false,
            uploadImage: false,
            taskNameError: null,
            taskDescriptionError: null,
          }

          let checkoutContainer ={
          sidebar:true,
          askEmailChoice: data.ask_email,
          taskNameError: null,
          taskDescriptionError: null,
          checkout: {
            text: checkout ? checkout.text : "",
            id: checkout ? checkout.id : "",
            qr_type: 53,
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
            quick_replies:[{
              id: checkout ? checkout.quick_replies[0].id : "",
              title:  "Yes",
              payload:  "c0mm@nd~~proceed_with_checkout",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id,
            },{ 
              id: checkout ? checkout.quick_replies[1].id : "",
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id
            }]
          }, 
          askReceipt: {
            text: askRecept ? askRecept.text : "",
            id: askRecept ? askRecept.id : "",
            qr_type: 54,
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
            quick_replies:[{
              id: askRecept ? askRecept.quick_replies[0].id : "",
              title:  "Yes",
              payload:  "c0mm@nd~~send_email_receipt",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id
            },{ 
              id: askRecept ? askRecept.quick_replies[1].id : "",
              title:  "No",
              payload:  "c0mm@nd~~do_checkout",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id
            }]
          },
          askEmail: {
            text: askEmail ? askEmail.text : "",
            id: askEmail ? askEmail.id : "",
            qr_type: 55,
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
            quick_replies:[{
              id: askEmail ? askEmail.quick_replies[0].id : "",
              title:  "Yes",
              payload:  "c0mm@nd~~wait_email",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id
            },{ 
              id: askEmail ? askEmail.quick_replies[1].id : "",
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id
            }]
          },
          emailError:{
            text_type:  56,
            message: emailError ? emailError.message : "",
            id: emailError ? emailError.id : "",
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
          },
          checkoutError: null,
          askReceiptError: null,
          askEmailError: null,
          emailErrorTextError: null,
          provideEmail: {
            id: provideEmail ? provideEmail.id : "",
            text_type:  57,
            message: provideEmail ? provideEmail.message : "",
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
          },
          provideEmailError: null,
          saveEmail: {
            id: provideEmailError ? provideEmailError.id: "",
            text_type:  58,
            message: provideEmailError ? provideEmailError.message : "",
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
          },
          saveEmailError: null,
          retryEmailError: null,
          retryEmail: {
            id: retryEmail ? retryEmail.id : "",
            text: retryEmail ? retryEmail.text:"",
            qr_type: 59,
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
            quick_replies:[{
              id: retryEmail ? retryEmail.quick_replies[0].id: "",
              title:  "Yes",
              payload:  "c0mm@nd~~wait_email",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id,
            },{ 
              id: retryEmail ? retryEmail.quick_replies[1].id: "",
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: this.props.user.id,
              updated_by: this.props.user.id
            }]
          }
        }

        let responseText = data.text_response.find(text=>text.text_type === 46)
        let orderMore = data.button_response.find(btn=>btn.payload === "/menu")
        let viewCart = data.button_response.find(btn=>btn.button_type === 39)
        let checkoutBtn = data.button_response.find(btn=>btn.payload === "c0mm@nd~~@sk-CheckOut")

        let cartContainer = {
          sidebar: true,
          cartResponseTextError: null,
          cartButtonResponseTextError: null,
          cartOrderButtonError: null,
          cartViewCartButtonError: null,
          cartCheckoutButtonError: null,
          taskNameError: null,
          taskDescriptionError: null,
          responseText:{
            text_type:  46,
            message: responseText ? responseText.message : "",
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
            id: responseText ? responseText.id:""
          },
          buttonResponse: {
            text: data.button_response_message.message,
            id: data.button_response_message.id,
            button_type:  48,
            created_by: this.props.user.id,
            updated_by: this.props.user.id,
            buttons: [{
              title: orderMore ? orderMore.title : "",
              payload: "/menu",
              button_type: 38,
              created_by: this.props.user.id,
              updated_by: this.props.user.id,
              id: orderMore ? orderMore.id : ""
            },{
              button_type: 39,
              url: "https://cbot-api.herokuapp.com/app/view_cart/?id=",
              title: viewCart ? viewCart.title : "",
              webview_height_ratio: 50,
              messenger_extensions: true,  
              fallback_url: "https://cbot-api.herokuapp.com/app/view_cart/?id=",
              id: viewCart ? viewCart.id : ""
            },{
              title: checkoutBtn ? checkoutBtn.title : "",
              payload: "c0mm@nd~~@sk-CheckOut",
              button_type: 38,
              created_by: this.props.user.id,
              updated_by: this.props.user.id,
              id: checkoutBtn ? checkoutBtn.id : ""
            }]
          }
        }

        let deliverContainer = {
          sidebar: true,
          savingImage: false,
          interfaces: data.location_interface.sort(function(a, b){return a.id - b.id}),
          pickUpChecked: data.location_interface.length? true : false,
          preview_url: null,
          skipChecked: !data.ask_delivery && !data.location_interface.length? true : false,
          deliveryChecked: data.ask_delivery,
          locationName: "",
          description: "",
          image_url: "",
          location_url: "",
          contact_info: "",
          visit_page: "",
          call_btn: "",
          current_index: "",
          addMode: true,
          showLocationModal: false,
          showPreviewModal: false,
          previewInterface: null,
          locationNameError: null,
          locationDescriptionError: null,
          location_image_url_error: null,
          location_url_error: null,
          contact_info_error: null,
          visit_page_error: null,
          call_btn_error: null,
          invalidUrl: false,
          openWarningModal: false,
          uploadImage: false,
          taskNameError: null,
          taskDescriptionError: null,
        }

        let qtyResponseText = data.text_response.find(text=>text.text_type === 40)
        let quantity = data.quick_replies.find(text=>text.qr_type === 42)

        let quantityContainer = {
          sidebar: true,
          responseTextError: null,
          quickReplyTextError: null,
          taskNameError: null,
          taskDescriptionError: null,
          responseText: {
            "message": qtyResponseText ? qtyResponseText.message : "",
            "id": qtyResponseText ? qtyResponseText.id : "",
            "text_type": 40,
            "created_by": this.props.user.id,
            "updated_by": this.props.user.id,
          },
          quick_reply: {
            quick_replies:quantity ? quantity.quick_replies.sort(function(a, b){return a.id - b.id}):"",
            text: quantity ? quantity.text : "",
            id: quantity ? quantity.id:"",
            qr_type: 42,
            "created_by": this.props.user.id,
            "updated_by": this.props.user.id,
          }
        }

      this.props.dispatch({ 
        type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
        payload:{
          name: data.taskName,
          description: data.taskDescription
        }
      }) 

      this.props.dispatch({type:"UPDATE_QUANTITY_CONTAINER", payload:quantityContainer})
      this.props.dispatch({type:'UPDATE_DELIVERY_CONTAINER', payload:deliverContainer})
      this.props.dispatch({type:'UPDATE_CART_CONTAINER', payload: cartContainer})
      this.props.dispatch({type:"UPDATE_PRODUCT_CONTAINER", payload:productContainer})
      this.props.dispatch({type:'UPDATE_CHECKOUT_CONTAINER', payload:checkoutContainer})

      this.props.dispatch({type:"SET_MENU_INTERFACE_STATUS", payload: false})
      this.props.dispatch({type:"SET_TEXT_RESPONSE_STATUS", payload: false})
      this.props.dispatch({type:"SET_BUTTON_RESPONSE_STATUS", payload: false})
      this.props.dispatch({type:"SET_QUICK_REPLY_STATUS", payload: false})
      this.props.dispatch({type:"SET_LOCATION_INTERFACE_STATUS", payload: false})
      this.props.dispatch({type:"SET_GREETING_TEXT_STATUS", payload: false})
      this.props.dispatch({type:"SET_PERSISTENT_MENU_STATUS", payload: false})

      browserHistory.push('/order/operation')
    }
  }
  

  closeModal(){
    this.setState({ showModal: false })
  }


  openModal(){
    this.setState({ showModal: true })
  }

  componentWillMount(){
    this.props.dispatch(Request.getJobFlow(1))
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  handleEditTask(taskId){
    this.props.dispatch(Request.getTask(taskId, this.props.user.id))
  }

  handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(Request.getJobFlow(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(Request.getJobFlow(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(Request.getJobFlow(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(Request.getJobFlow(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let tasks = []
    for (let i = 1; i <= Math.ceil(this.props.taskCount / 10); i++) {
      tasks.push(i);
    }
    
    this.props.dispatch(Request.getJobFlow(tasks.length))
    this.setState({currentPage: tasks.length})
  }

  openDeleteModal(id,name){
    this.setState({showDeleteModal:true})
    this.setState({id: id})
    this.setState({name: name})
  }

  closeDeleteModal(){
    this.setState({ showDeleteModal: false })
  }

  deleteTask(id){
    this.props.dispatch(Request.deleteTask(this.state.id))
    this.props.dispatch(Request.getJobFlow(1))
    this.setState({ showDeleteModal: false })
  }

  toggleInfo(){
    this.setState({ showAdditionalInfo: !this.state.showAdditionalInfo })
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    let tasks = this.props.tasks.tasks.map((task, index) => {
      return (
        <tr key={index}>
          <td className="tac">{task.id}</td>
          <td className="tac">{task.name}</td>
          <td className="tac">{task.description}</td>
          <td className="tac">1</td>
          <td className="tac">
            {
              task.menu_interface.length ?
                <button className="btn btn-primary btn-small" 
                  onClick={this.handleEditTask.bind(this,task.id)}>
                    Edit
                </button> : null
            }
            &nbsp;
            <button className="btn btn-danger btn-small pull-right"  data-toggle="modal" 
              data-backdrop="static" href="#deleteTaskModal" 
              onClick={this.openDeleteModal.bind(this,task.id,task.name)}>
                Delete
            </button>
          </td>
        </tr>
      )
    }) 

    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.taskCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""} 
              onClick={this.handleTransferPage.bind(this)}>{number}</a>
        )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>

        <div id="contentwrapper">
          <div className="main_content">
            <EditBotTask.Nav />

          <div className="row-fluid">
            <div className="span12 margin-top-7">
              <h3 className="heading">Edit Bot Tasks</h3>
              <div className="row-fluid">
                <div className="span12">
                  <EditBotTask.AdditionalInfo toggleInfo={this.toggleInfo.bind(this)} {...this.state}/>
                </div>
              </div>
              <div className="row-fluid">
                <div className="span12">
                  <EditBotTask.EditJobForm 
                    {...this.state} {...this.props}
                    taskCount={this.props.tasks.tasks.length}
                    tasks={tasks}
                    handleEditTask={this.handleEditTask.bind(this)}
                    openModal={this.openModal.bind(this)}
                    closeModal={this.closeModal.bind(this)}
                    pageNumbers={renderPageNumbers}
                    handleNext={this.handleNext.bind(this)}
                    handleFirst={this.handleFirst.bind(this)}
                    handlePrevious={this.handlePrevious.bind(this)}
                    handleLast={this.handleLast.bind(this)}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.EditBotTaskSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
        <EditBotTask.DeleteModal {...this.state}
          openDeleteModal={this.openDeleteModal.bind(this)}
          closeDeleteModal={this.closeDeleteModal.bind(this)}
          delete={this.deleteTask.bind(this)}/>
        <EditBotTask.CreateNewModal />
      </div>
    </div>
    )
  }
}

export default connect(mapStateToProps)(EditBotJobContainer)