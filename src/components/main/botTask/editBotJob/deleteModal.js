import React from 'react'

const DeleteModal = (props) => {
  return (
    <div className="modal hide fade" id="deleteTaskModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Delete Task</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span18">
            <p>You are about to delete <span className="red-text">{props.name}</span> with Task ID <span className="red-text">{props.id}</span>. Are you sure you want to delete this bot?</p>
            <div className="clear-10"></div>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="btns-action">
          <a className="btn btn-danger btn-small" data-dismiss="modal"  onClick={props.delete}>Delete</a>
          <a className="btn btn-small" href="#deleteTaskModal" data-dismiss="modal">Cancel</a>
        </div>
      </div>
    </div>
  )
}

export default DeleteModal