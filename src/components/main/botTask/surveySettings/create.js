import React, { Component } from 'react'
import { 
  Tabs, 
  Tab, 
} from 'react-bootstrap'
import { toastr } from 'react-redux-toastr'
import { connect } from 'react-redux'
import SurveyAssignment from './surveyAssignment'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'
import * as Common from '../index'
import Nav from './nav'
import cloudinary from 'cloudinary'
import { 
  createSurvey, 
  createQuestion,
  updateSurveyType,
  createJobFlow,
  getSurveys,
  getSurvey,
  attachSurveysToTask,
  getTasks
} from '../requests/request'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

const mapStateToProps = (state) => {
  let surveys = state.survey.survey
  if (state.createSurvey.tasks.length!==0){
    for (let survey in surveys){
      if (survey.task_flow===state.createSurvey.tasks[0].id){
        survey['forUpdate'] = true
      }
    }
  }
  return {
    task: state.createSurvey.task,
    questions: state.createSurvey.questions,
    survey: state.createSurvey.survey,
    user: state.user.id,
    firstName: state.user.first_name,
    surveys: surveys,
    tasks: state.createSurvey.tasks,
    createSurvey: state.createSurvey,
    surveyCount: state.survey.surveyCount
  }
}

class CreateSurvey extends Component {
  constructor(props) {
    super(props)
    this.state = {
      "sidebar": true,
      "currentPage": 1,
      "date": new Date(),
      "questionModal": false,
      "selected": 1,
      "form":{
        text:"",
        answers:this.newAnswer()
      },
      "questions":props.questions,
      "addMode":true,
      "indexForUpdate": -1,
      "previewStatus": "1",
      "questionType":"yesno",
      "survey":props.survey,
      "ratingAnswers":[{
        "text":"Needs Improvement",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Satisfactory",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Excellent",
        "created_by":props.user,
        "updated_by":props.user
      }],
      "previewQuestion":this.newPreviewQuestion(),
      "task":props.task,
      "uploadingImage":false,
      "surveyTypeSelector":"qr",
      "surveysForUpdate":[],
      "surveys":props.surveys,
      "tasks":props.tasks,
      "selectedTask":"-1",
      "checkAll": false,
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  processImage(image_url){
    try {
      let reader = new FileReader()
      reader.readAsDataURL(image_url)
      reader.onloadend = () => {
        this.uploadImage(reader.result)
      }
    } catch (error) {
      toastr.error('Error', error.toString())
    }
  }

  uploadImage(image) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        let survey = this.state.survey
        survey.image_url = result.secure_url
        this.setState({survey:survey})
        this.setState({uploadingImage:false})
        toastr.success('Success','Successfully uploaded image')
      } else {
        toastr.error("Error", "Unable to upload image")
      }
    })
  }

  newPreviewQuestion(){
    const preview = [{
      "text":"Sample Question",
      "answers":[{
        "text":"Sample Answer 1",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      },{
        "text":"Sample Answer 2",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      }]
    }]
    return preview
  }

  newQuestion(){
    return ({
      text:"",
      answers:this.newAnswer()
    })
  }

  newAnswer(){
    let answers
    try {
      if (this.state.questionType==='yesno'){
        answers = [{"text":"True",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"False",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      } else if (this.state.questionType==='custom'){
        answers = [{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      } else if (this.state.questionType==='rating'){
        answers = this.state.ratingAnswers
      }
    } catch(e) {
      answers = [{
        "text":"True",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      },{
        "text":"False",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      }]
    }
    return answers
  }

  addAnswer(){
    let form = this.state.form
    // form.answers.push(this.newAnswer())
    form.answers.push({
      "text":"",
      "created_by":this.props.user,
      "updated_by":this.props.user 
    })
    this.setState({form:form})
  }

 removeAnswer(index){
    let form = this.state.form
    form.answers.splice(index, 1)
    this.setState({form:form})
  }

  removeQuestion(index){
    let questions = this.state.questions
    questions.splice(index, 1)
    this.setState({questions:questions}) 
    this.setState({previewQuestion: [this.state.questions[this.state.questions.length - 1]]})
  }

  placeUp(index){
    let questions = this.state.questions
    let question = questions[index]
    questions.splice(index, 1)
    questions.splice(index-1, 0, question)
    this.setState({questions:questions}) 
  }

  placeDown(index){
    let questions = this.state.questions
    let question = questions[index]
    questions.splice(index, 1)
    questions.splice(index+1, 0, question)
    this.setState({questions:questions}) 
  }

  printQuestions(){
    // console.log(JSON.stringify(this.state.questions))
    // console.log(JSON.stringify(this.state.survey))
    console.log(JSON.stringify(this.state.surveysForUpdate))
  }

  saveQuestion(){
    if(this.state.addMode){
      let answers = []
      for (var i=0; i < this.state.form.answers.length; i++) {
        answers.push(Object.assign({}, this.state.form.answers[i]))
      }
      let form = {
        text: this.state.form.text,
        answers: answers
      }
      let questions = this.state.questions
      questions.push(form)
      this.setState({questions:questions})
      this.setState({previewQuestion:[form]})
      this.setFormBlank()
      if (this.state.questions.length===10){
        toastr.info('Info','You have reached the maximum number of questions you can create.')
      }
    } else {
      let answers = []
      for (var j=0; j < this.state.form.answers.length; j++) {
        answers.push(Object.assign({}, this.state.form.answers[j]))
      }
      let form = {
        text: this.state.form.text,
        answers: answers
      }
      let questions = this.state.questions
      questions[this.state.indexForUpdate] = form      
      this.setState({questions:questions})      
      this.setState({previewQuestion:[form]})
      this.setFormBlank()
      this.setState({addMode:true, indexForUpdate:-1})      
    }
  }

  setFormBlank(){
    let form = this.newQuestion()
    this.setState({form:form})
  }

  setFormForUpdate(index){
    this.setState({addMode:false})
    this.setState({indexForUpdate:index})
    let form = this.state.questions[index]
    this.setState({form:form})
  }

  handleQuestionChange = (e) => {
    let form = this.state.form
    form.text = e.target.value
    this.setState({form:form})
  }

  handleAnswerChange = (index, e) => {
    let form = this.state.form
    form.answers[index].text = e.target.value
    this.setState({form:form})
  }

  handlePreviewChange = (e) => {
    this.setState({previewStatus:e.target.value})  
    if (e.target.value === '1'){
      this.props.dispatch(updateSurveyType(this.state.survey.id, "qr"))
    } else if (e.target.value === '2'){
      this.props.dispatch(updateSurveyType(this.state.survey.id, "br"))
    }
  }

  handleSelect(key) {
    this.setState({selected:key})
    this.setState({form:this.newQuestion()})
  }

  handleInputChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
    this.setState({form:{}})
    this.setFormBlank()
    this.setState({questions:[],previewQuestion:this.newPreviewQuestion()})
  }

  handleRatingOne = (index, e) => {
    let ratingAnswers = this.state.ratingAnswers
    ratingAnswers[index].text = e.target.value
    this.setState({ratingAnswers:ratingAnswers})
    this.setState({questions:[],previewQuestion:this.newPreviewQuestion()})
  }

  handleSurveyInputChange = (e) => {
    let survey = this.state.survey
    if (e.target.name==="name"){
      survey.name = e.target.value
      this.setState({survey:survey})
    } else if (e.target.name==="description"){
      survey.description = e.target.value
      this.setState({survey:survey})
    } else if (e.target.name==="image_url"){
      this.setState({uploadingImage:true})
      this.processImage(e.target.files[0])
    }
  }

  handleTaskFlowInputChange = (e) => {
    let task = this.state.task
    if (e.target.name==="name"){
      task.name = e.target.value
      this.setState({task:task})
    } else if (e.target.name==="description"){
      task.description = e.target.value
      this.setState({task:task})
    }
  }

  createSurvey = (e) => {
    e.preventDefault()
    let survey = this.state.survey
    survey["created_by"] = this.props.user
    survey["updated_by"] = this.props.user
    this.props.dispatch(createSurvey(survey))
    this.setState({selected:2})
  }

  createQuestion(){
    let questions = this.state.questions
    for (let i=0; i<questions.length;i++){
      questions[i]['survey'] = this.props.survey.id
      questions[i]['order'] = i+1
      questions[i]["created_by"] = this.props.user
      questions[i]["updated_by"] = this.props.user
    }
    this.props.dispatch(createQuestion(questions))
    this.handleSelect(4)
  }

  componentWillMount(){
    // this.props.dispatch(getSurveys())
    this.props.dispatch(getSurvey(1))
    this.props.dispatch(getTasks())
    this.forceUpdate()
  }

  componentWillReceiveProps(nextProps){
    this.setState({tasks:nextProps.tasks})
    this.setState({surveys:nextProps.surveys})
    if (nextProps.tasks.length!==0) {
      if (this.state.selectedTask==='-1'){
        this.setState({selectedTask:nextProps.tasks[0].id.toString()})
        let surveys = nextProps.surveys
        // let flag = true 
        for (let survey of surveys){
          try{
            if (survey.task_flow.toString() === nextProps.tasks[0].id.toString()){
              survey['forUpdate'] = true
            } else {
              survey['forUpdate'] = false
              // flag = false
            }
          } catch (e){
            // Ignore error
          }
        }
        this.setState({surveys:surveys})
      } else if(this.state.selectedTask!=='-1'){
        let surveys = nextProps.surveys
        // let flag = true        
        for (let survey of surveys){
          try{
            if (survey.task_flow.toString() === this.state.selectedTask){
              survey['forUpdate'] = true
            } else {
              survey['forUpdate'] = false
              // flag = false
            }
          } catch (e){
            // Ignore error
          }
        }
        this.setState({surveys:surveys})
      }
    }
  }

  createJobFlow = (e) => {
    e.preventDefault()
    let task = this.state.task
    task["created_by"] = this.props.user
    task["updated_by"] = this.props.user
    task["job_type"] = 60
    this.props.dispatch(createJobFlow(task))
    this.setState({selected:2})
    this.props.dispatch(getSurveys())
    this.props.dispatch(getTasks())
  }

  handleSurveyTypeChange = (e) =>{
    this.setState({surveyTypeSelector:e.target.value})
  }

  handleCheckboxChange = (index, e) => {
    const checked = e.target.checked
    let surveys = this.state.surveys
    surveys[index]['forUpdate'] = checked
    this.setState({surveys:surveys})
  }

  handleCheckAll = (e) => {
    const checked = e.target.checked
    let surveys = this.state.surveys
    for (let i = 0; i < surveys.length; i++){
      surveys[i]['forUpdate'] = checked
    }
    this.setState({surveys:surveys, checkAll:checked})    
  }

  attachToTask(){
    if (this.state.selectedTask==='-1'){
      toastr.error('Error', 'Please select a task')
    } else {
      this.props.dispatch(attachSurveysToTask(this.state.surveys, this.state.selectedTask))
      this.setState({selectedTask: "-1"})
    }
  }

  handleTaskSelectChange = (e) => {
    this.setState({selectedTask:e.target.value})
    let surveys = this.state.surveys
    // let flag = true
      for (let survey of surveys){
        try{
          if (survey.task_flow.toString() === e.target.value){
            survey['forUpdate'] = true
          } else {
            survey['forUpdate'] = false
            // flag = false
          }
        } catch (e){
          // Ignore error
        }
      }
    this.setState({surveys:surveys})
  }

  handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(getSurvey(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(getSurvey(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(getSurvey(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(getSurvey(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.surveyCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(getSurvey(bots.length))
    this.setState({currentPage: bots.length})
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render(){
    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.surveyCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""}
              onClick={this.handleTransferPage.bind(this)}>{number}
            </a>
        )
    })

   const surveys = this.state.surveys.map((survey, index) => {
      return(
        <tr key={index}>
          <td className="tac">
            <input
              className="no-margin position-relative"
              type="checkbox"
              checked={survey.forUpdate}
              onChange={this.handleCheckboxChange.bind(this, index)}/>
          </td>
          <td className="tac">
            {survey.name}
          </td>
          <td className="tac">
            {survey.description}
          </td>
          <td className="tac">
            {survey.updated_at}
          </td>
        </tr>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.firstName}/>

        <div id="contentwrapper">
          <div className="main_content">
            <Nav page={"Survey Settings"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Survey Settings</h3>
                <div className="row-fluid">
                   <div className="span12">
                    <div id="graph" className="row-fluid" >
                      <div className="span12 margin-top-5">
                        <Tabs activeKey={ this.state.selected } onSelect={this.handleSelect.bind(this)} id="survey-details">
                          <Tab eventKey={1} title="Create a Survey Task">
                            <div className="micro-top-padding">
                              <form onSubmit={this.createJobFlow.bind(this)}>
                                <div className="control-group">
                                  <label><b>Name</b></label>
                                  <input type="text" className="span4" 
                                    placeholder="Enter Task Name"
                                    required
                                    name="name"
                                    value={this.state.task.name}
                                    onChange={this.handleTaskFlowInputChange.bind(this)}/>
                                </div>
                                <div className="control-group">
                                  <label><b>Description</b></label>
                                  <textarea className="span4"
                                    placeholder="Enter Task Description"
                                    required
                                    name="description"
                                    value={this.state.task.description}
                                    onChange={this.handleTaskFlowInputChange.bind(this)}></textarea>
                                </div>
                                <div className="control-group">
                                  <span className="span4">
                                    <button className="btn btn-primary btn-small pull-right">
                                      Create Survey <i className="fa fa-check icon-white"></i>
                                    </button>
                                  </span>
                                </div>
                              </form>
                            </div>
                          </Tab>
                          <Tab eventKey={2} title="Assign Survey to Task">
                            <SurveyAssignment 
                              {...this.state} 
                              {...this.props}
                              surveys={surveys} 
                              handleSurveyTypeChange={this.handleSurveyTypeChange.bind(this)} 
                              attachToTask={this.attachToTask.bind(this)}
                              handleTaskSelectChange={this.handleTaskSelectChange.bind(this)}
                              handleCheckAll={this.handleCheckAll.bind(this)}
                              pageNumbers={renderPageNumbers}
                              handleNext={this.handleNext.bind(this)}
                              handleFirst={this.handleFirst.bind(this)}
                              handlePrevious={this.handlePrevious.bind(this)}
                              handleLast={this.handleLast.bind(this)}/>
                          </Tab>              
                        </Tabs>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.EditBotTaskSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
      </div>
    )
  }
}

CreateSurvey.propTypes = {
  surveys: React.PropTypes.array.isRequired,
  tasks: React.PropTypes.array.isRequired,
}

export default connect(mapStateToProps)(CreateSurvey)

// <Tab eventKey={4} title="Assign to Bot">
//   <Row className="micro-top-padding">
//     <Col md={4}>
//       <FormControl componentClass="select" bsSize="small" onChange={this.handlePreviewChange.bind(this)}>
//         <option value={1}>Autobot</option>
//         <option value={2}>Decepticon</option>
//         <option value={3}>BotBot</option>
//         <option value={4}>Bot Tobias</option>
//       </FormControl>
//     </Col>
//   </Row>
//   <Row className="micro-top-padding">
//     <Col md={6}>
//       {surveys}
//     </Col>
//   </Row>
//   <Row className="micro-top-padding">
//     <Col md={6} className="pull-right">
//       <Button bsStyle="primary" bsSize="small" onClick={() => this.handleSelect(3)}>
//         Save{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-save" />                        
//       </Button>
//     </Col>
//   </Row>
// </Tab>