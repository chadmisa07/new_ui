import React from 'react'
import { Modal, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap'
import { Col, Glyphicon, Form } from 'react-bootstrap'

const SurveyModal = (props) => {
  return (
    <Modal show={props.questionModal} onHide={props.closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>Survey</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form horizontal>
          <FormGroup>
            <Col sm={12}>
              <ControlLabel>Question</ControlLabel>
            </Col>
            <Col sm={12}>
              <FormControl 
                componentClass="textarea" 
                placeholder="Question" 
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col sm={12}>
              <ControlLabel>Answers</ControlLabel>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col sm={9}>
              <FormControl type="text" />
            </Col>
            <Col sm={1}>
              <Button bsStyle="danger">
                <Glyphicon glyph="glyphicon glyphicon-remove" />
              </Button>
            </Col>        
          </FormGroup>
          <FormGroup>
            <Col sm={9}>
              <FormControl type="text" />
            </Col>
            <Col sm={1}>
              <Button bsStyle="danger">
                <Glyphicon glyph="glyphicon glyphicon-remove" />
              </Button>
            </Col>
            <Col sm={1}>
              <Button bsStyle="primary">
                <Glyphicon glyph="glyphicon glyphicon-plus" />
              </Button>
            </Col>            
          </FormGroup>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="primary" onClick={props.closeModal}>
          Add
        </Button>
        <Button bsStyle="default" onClick={props.closeModal}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default SurveyModal


