import React from 'react'

const SurveyAssignment = (props) => {
  // console.log(JSON.stringify(props.tasks))
  // console.log(JSON.stringify(props.tasks))
  const tasks = props.tasks.map((task, index) => {
    return(
      <option key={index} value={task.id}>{task.name}</option>
    )
  })
  return (
    <div className="row-fluid span8">
      <div className="row-fluid">
        <h5>Choose the task</h5>
        <select className="span4" onChange={props.handleTaskSelectChange}>
          {tasks}
        </select>
      </div>
      <div className="row-fluid items">
        <table className="table table-bordered table-striped table-overflow">
          <thead>
            <tr>
              <th className="tac">
                <input 
                  type="checkbox"
                  checked={props.checkAll}
                  onChange={props.handleCheckAll}
                  className="no-margin position-relative"/>
              </th>
              <th className="tac">
                Name
              </th>
              <th className="tac">
                Description
              </th>
              <th className="tac">
                Date Modified
              </th>
            </tr>
          </thead>
          <tbody>
            {
              !props.surveys.length?
              <tr>
                <td colSpan="4" className="tac">No Survey found</td>
              </tr> : null
            }
            {props.surveys}
          </tbody>
        </table>
      </div>
      <div className="row-fluid">
        <div className="pagination">
          {props.pageNumbers.length!==0?
            <a href="#" onClick={props.currentPage !== 1 && props.pageNumbers.length!==0? 
              props.handleFirst : ""}>&#x27EA;</a>:""
          }
          {props.currentPage > 1 ? <a href="#" onClick={props.handlePrevious}>&#x27E8;</a> : ""}
          {props.pageNumbers}
          { props.currentPage <= props.pageNumbers.length + 2 && props.pageNumbers.length!== 1 &&
            props.pageNumbers.length!==0?<a href="#" onClick={props.handleNext}>&#x27E9;  </a>:""
          }
          {props.pageNumbers.length!==0?
            <a href="#" onClick={props.currentPage !== props.pageNumbers.length + 2 && 
              props.pageNumbers.length !== 1 && props.pageNumbers.length!==0? props.handleLast : ""}>
              &#x27EB;</a>: ""
          }
        </div>
      </div>
      <div className="row-fluid">
        <button type="button" className="btn btn-primary pull-right"  onClick={props.attachToTask}>
        Assign <i className="fa fa-check"></i></button>
      </div>
    </div>
    
  )
}

export default SurveyAssignment