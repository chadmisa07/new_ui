import React from 'react'
import { Grid, Col, Row, FormControl, Button } from 'react-bootstrap'
import ButtonTemplatePreview from './button'
import GenericTemplatePreview from './generic'
import QuickReplyPreview from './qr'
import ListTemplatePreview from './list'


class Container extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      templateList : ["button template", "list template", "generic template", "quick reply"],
      survey : {
        question: "How are you?",
        question_image: "https://www.sciencealert.com/images/articles/processed/cell-3d-map_1024.jpg",
        answer: [
          {text: "I'm okay",image:"http://kids.nationalgeographic.com/content/dam/kids/photos/games/screen-shots/More%20Games/A-G/babyanimal_open.jpg"},
          {text: "I'm good",image:"http://www.lpzoo.org/sites/default/files/styles/slider/public/circle_redpanda.jpg?itok=Dy61Pr7G"},
          {text: "I'm fine",image:"http://www.drodd.com/images13/animals48.jpg"},
          {text: "I'm fine",image:"http://www.drodd.com/images13/animals48.jpg"},
          {text: "I'm fine",image:"http://www.drodd.com/images13/animals48.jpg"}
        ]
      }
    }
  }

  componentWillMount(){
    this.setState({template: this.state.templateList[0]})
  }

  handleTemplateChange = (event) =>{
    this.setState({template: event.target.value})
  }

  render() {
    const buttonTemplateAnswers = this.state.survey.answer.map(function(answer,index){
      return(
        <div key={index} className="btn-response-button text-center pull-left">{answer.text}</div>
      )
    })

    const genericTemplateAnswers = this.state.survey.answer.map(function(answer,index){
      return(
        <Button key={index} className="fb-button">{answer.text}</Button>
      )
    })

    const quickReplyAnswers = this.state.survey.answer.map(function(answer,index){
      return(
        <p className="quick-reply-btn">
          {answer.text}
        </p>
      )
    })

    const listTemplateyAnswers = this.state.survey.answer.map(function(answer,index){
      return(
        <div>
          <div className="list-template-answer">
            <Row>
              <Col lg={6}>
                <p className="quick-reply-btn">
                  {answer.text}
                </p>
              </Col>
              <Col lg={6}>
                <img className="list-answer-image" src={answer.image}/>
              </Col>
            </Row>
          </div>
          <hr/>
        </div>
      )
    })

    const templates = this.state.templateList.map(function(template,index){
      return(
        <option key={index} value={template}>{template}</option>
      )
    })

		return (
      <Grid fluid>
        <div>
          <Row>&nbsp;</Row>
          <Row>&nbsp;</Row>
          <Row>
            <Col lg={2}>
              <FormControl componentClass="select" placeholder="Platform" 
                onChange={this.handleTemplateChange.bind(this)}>
              {templates}
            </FormControl>
            </Col>
          </Row>
          <Row>
            <Col lg={10}>
              { this.state.template === "button template" ?
                <ButtonTemplatePreview {...this.state} answers={buttonTemplateAnswers}/>:
                this.state.template === "generic template" ? <GenericTemplatePreview {...this.state} answers={genericTemplateAnswers}/> : 
                this.state.template === "quick reply" ? <QuickReplyPreview {...this.state} answers={quickReplyAnswers}/>:
                <ListTemplatePreview {...this.state} answers={listTemplateyAnswers}/> 
              }
            </Col>
          </Row>
        </div>
      </Grid>
		)
  }
}

export default Container
