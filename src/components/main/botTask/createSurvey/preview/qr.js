import React from 'react'

const QuickReply = (props) => {
  const questions = props.previewQuestion.map((question, index) => {
    let answers = question.answers.map((answer, index) => {
      return(
        <span className="quick_reply_btn" key={index}>
          {answer.text}
        </span>
      )
    })
    return(
      <div key={index}>
        <div className="row-fluid">
          <span className="sender_message_bubble">
            {question.text}
          </span>
        </div>
        <div className="row-fluid force-txt-center">
          {answers}
        </div>
      </div>
    )
  })
  return(
    <div className="row-fluid">
      <div className="chatbox-border">
        {questions}
      </div>
    </div>
  )
}

export default QuickReply
