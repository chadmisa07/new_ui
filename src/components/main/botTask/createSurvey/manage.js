import React from 'react'
import * as Common from '../index'
import { connect } from 'react-redux'
import { Button } from 'react-bootstrap'
import * as Request from '../requests/request'
import { browserHistory } from 'react-router'
import DeleteModal from './deleteModal'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'
import Nav from './nav'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    surveys: state.survey.survey,
    surveyCount: state.survey.surveyCount,
  }
}

class ManageSurvey extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      "sidebar": true,
      date: new Date(),
      currentPage: 1,
      "showDeleteModal":false,
      "openDeleteModal":this.openDeleteModal.bind(this),
      "closeDeleteModal":this.closeDeleteModal.bind(this),
      "deleteSurvey":this.deleteSurvey.bind(this),
      "selectedSurvey":{}
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  openDeleteModal(index) {
    this.setState({selectedSurvey:this.props.surveys[index]})
    this.setState({showDeleteModal:true})
  }
  closeDeleteModal() {
    this.setState({showDeleteModal:false})
  }

  componentWillMount(){
    this.props.dispatch(Request.getSurvey(1))
  }

  deleteSurvey(id){
    this.props.dispatch(Request.deleteSurvey(id))
    this.props.dispatch(Request.getSurvey(1))
    this.setState({currentPage: 1})
    this.closeDeleteModal()
  }

  editSurvey(index){
    this.props.dispatch({type:"SET_SURVEY_FOR_EDIT", payload:this.props.surveys[index]})
    browserHistory.push('/task/survey/edit')
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(Request.getSurvey(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(Request.getSurvey(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(Request.getSurvey(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(Request.getSurvey(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.surveyCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(Request.getSurvey(bots.length))
     this.setState({currentPage: bots.length})
  }

  render() {
    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.surveyCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""}
              onClick={this.handleTransferPage.bind(this)}>{number}
            </a>
        )
    })

    const rows = this.props.surveys.map((survey, index) => {
      return (
        <tr key={index}>
          <td className="tac">{survey.id}</td>
          <td className="tac">{survey.name}</td>
          <td className="tac">{survey.description}</td>
          <td className="tac">
            <Button 
              bsStyle="primary" 
              bsSize="xsmall"
              onClick={this.editSurvey.bind(this, index)}
            >
              Edit
            </Button>&nbsp;
            <Button 
              bsStyle="danger" 
              bsSize="xsmall"
              onClick={this.openDeleteModal.bind(this, index)}
            >
              Delete
            </Button>
          </td>
        </tr>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>

        <div id="contentwrapper">
          <div className="main_content">
            <Nav page={"Edit Survey"}/>

            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Edit Survey</h3>
                <div className="row-fluid">
                  <div className="span8">
                    <form className="form-horizontal">
                      <fieldset className="margin-top-20">
                         <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                          <thead>
                            <tr>
                              <th className="tac table-header">Survey ID</th>
                              <th className="tac table-header">Survey Name</th>
                              <th className="tac table-header">Description</th>
                              <th className="tac table-header" width="130">Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                            {
                              !this.props.surveys.length?
                              <tr>
                                <td colSpan="4" className="tac">No Survey found</td>
                              </tr> : null
                            }
                            {rows}
                          </tbody>
                        </table>
                        <div className="pagination">
                          {renderPageNumbers.length!==0?
                            <a href="#" onClick={renderPageNumbers.length!== 1 && renderPageNumbers.length!==0? 
                              this.handleFirst.bind(this) : ""}>&#x27EA;</a>:""
                          }
                          {this.state.currentPage > 1 ? <a href="#" onClick={this.handlePrevious.bind(this)}>&#x27E8;</a> : ""}
                          {renderPageNumbers}
                          { this.state.currentPage <= renderPageNumbers.length + 2 && renderPageNumbers.length!== 1 &&
                            renderPageNumbers.length!==0?<a href="#" onClick={this.handleNext.bind(this)}>&#x27E9;  </a>:""
                          }
                          {renderPageNumbers.length!==0?
                            <a href="#" onClick={this.state.currentPage !== renderPageNumbers.length + 2 && 
                              renderPageNumbers.length !== 1 && renderPageNumbers.length!==0? this.handleLast.bind(this) : ""}>
                              &#x27EB;</a>: ""
                          }
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteModal {...this.state} {...this.props} />
        <Common.EditBotTaskSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
    </div>
    )
  }
}

export default connect(mapStateToProps)(ManageSurvey)