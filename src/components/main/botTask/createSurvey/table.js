import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { Table, Button, Row, Col, Glyphicon } from 'react-bootstrap'

class SurveyTable extends Component{
  render(){
    return (
      <div>
        <Row className="micro-top-padding">
          <Col md={12}>
            <Button 
              bsStyle="warning" 
              bsSize="xsmall"
              className="pull-left"
              onClick={() => browserHistory.push('/bot/survey/create')} > 
              + Create New
            </Button>
          </Col>
        </Row>
        <Row className="micro-top-padding">
          <Col md={12}>
            <Table striped bordered condensed hover >
              <thead>
                <tr>
                  <th>Survey Title</th>
                  <th>Questions</th>
                  <th>Created Date</th>
                  <th>Updated Date</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Income</td>
                  <td>5</td>
                  <td>3/9/2017</td>
                  <td>3/9/2017</td>
                  <td>
                    <Button 
                      bsStyle="info" 
                      bsSize="xsmall" > 
                      <Glyphicon glyph="glyphicon glyphicon-pencil" />{'\u00A0'}Edit
                    </Button>
                    {'\u00A0'}
                    <Button 
                      bsStyle="danger" 
                      bsSize="xsmall" > 
                      <Glyphicon glyph="glyphicon glyphicon-remove" />{'\u00A0'}Delete
                    </Button>
                  </td>
                </tr>
                <tr>
                  <td>Sex</td>
                  <td>10</td>
                  <td>3/10/2017</td>
                  <td>3/10/2017</td>
                  <td>
                    <Button 
                      bsStyle="info" 
                      bsSize="xsmall" > 
                      <Glyphicon glyph="glyphicon glyphicon-pencil" />{'\u00A0'}Edit
                    </Button>
                    {'\u00A0'}
                    <Button 
                      bsStyle="danger" 
                      bsSize="xsmall" > 
                      <Glyphicon glyph="glyphicon glyphicon-remove" />{'\u00A0'}Delete
                    </Button>
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    )
  }
}

export default SurveyTable