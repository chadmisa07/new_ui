import React from 'react'
import {
  Row,
  Col,
  FormControl,
  Button,
  Glyphicon
} from 'react-bootstrap'

const SurveyAssignment = (props) => {
  // console.log(JSON.stringify(props.tasks))
  // console.log(JSON.stringify(props.tasks))
  const tasks = props.tasks.map((task, index) => {
    return(
      <option key={index} value={task.id}>{task.name}</option>
    )
  })
  return (
     <Row className="micro-top-padding">
      <Col md={10}>
        <Col md={5}>
          <h5>Choose the task</h5>
          <FormControl componentClass="select" bsSize="small" onChange={props.handleTaskSelectChange}>
            {tasks}
          </FormControl>
        </Col>
      </Col>
      <Col md={10}>
        <Col md={5}>
          <h5>Choose the surveys you want to attach to the task</h5>
          <FormControl componentClass="select" bsSize="small" onChange={props.handleSurveyTypeChange}>
            <option value={"qr"}>Quick Reply</option>
            <option value={"br"}>Button Template</option>
            <option value={"gt"}>Generic Template</option>
            <option value={"lt"}>List Template</option>
          </FormControl>
        </Col>
      </Col>
      <Col md={10} className="pull-left">
        {props.surveys}
      </Col>
      <Col md={10}>
        <Button 
          bsStyle="primary" 
          bsSize="small"
          onClick={props.attachToTask}
          className="pull-right"
        >
          Finish{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-ok" />                        
        </Button>
      </Col>
    </Row>
  )
}

export default SurveyAssignment