import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { 
  Col, 
  Tabs, 
  Tab, 
  DropdownButton, 
  MenuItem,
} from 'react-bootstrap'
import QR from './preview/qr'
import ButtonResponse from './preview/button'
import { toastr } from 'react-redux-toastr'
import { connect } from 'react-redux'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'
import * as Common from '../index'
import Nav from './nav'

import cloudinary from 'cloudinary'
import { 
  createSurvey, 
  createQuestion,
  updateSurveyType,
  updateSurvey,
  createJobFlow,
  getSurveys,
  attachSurveysToTask,
  getTasks
} from '../requests/request'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

const mapStateToProps = (state) => {
  return {
    task: state.createSurvey.task,
    questions: state.createSurvey.questions,
    survey: state.createSurvey.survey,
    user: state.user.id,
    firstName: state.user.first_name,
    surveys: state.createSurvey.surveys,
    tasks: state.createSurvey.tasks,
    createSurvey: state.createSurvey
  }
}

class CreateSurvey extends Component {
  constructor(props) {
    super(props)
    this.state = {
      "sidebar": true,
      date: new Date(),
      "questionModal": false,
      "selected": 1,
      "form":{
        text:"Were you satisfied with our service?",
        answers:this.newAnswer()
      },
      "questions":props.questions,
      "addMode":true,
      "indexForUpdate": -1,
      "previewStatus": "1",
      "questionType":"yesno",
      "survey":props.survey,
      "threeRatingAnswers":[{
          "text":"Excellent",
          "created_by":props.user,
          "updated_by":props.user
        },{
          "text":"Satisfactory",
          "created_by":props.user,
          "updated_by":props.user
        },{
          "text":"Needs Improvement",
          "created_by":props.user,
          "updated_by":props.user
        }],
      "fiveRatingAnswers":[{
          "text":"Strongly Agree",
          "created_by":props.user,
          "updated_by":props.user
        },{
          "text":"Agree",
          "created_by":props.user,
          "updated_by":props.user
        },{
          "text":"Neutral",
          "created_by":props.user,
          "updated_by":props.user
        },{
          "text":"Disagree",
          "created_by":props.user,
          "updated_by":props.user
        },{
          "text":"Strongly Disagree",
          "created_by":props.user,
          "updated_by":props.user
        }],
      "previewQuestion":[{
        "text":"Were you satisfied with our service?",
        "answers": [{
          "text":"YES"
        },{
          "text":"NO"
        }]
      }],
      "task":props.task,
      "uploadingImage":false,
      "surveyTypeSelector":"qr",
      "surveysForUpdate":[],
      "surveys":props.surveys,
      "tasks":props.tasks,
      "selectedTask":"-1",
      "defaultImage":true,
      "multipleAnswers":[{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }],
        "yesno":[{
          "text":"YES",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"NO",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
    this.props.dispatch({type:"CLEAR_SURVEY"})
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  processImage(image_url){
    try {
      let reader = new FileReader()
      reader.readAsDataURL(image_url)
      reader.onloadend = () => {
        this.uploadImage(reader.result)
      }
    } catch (error) {
      toastr.error('Error', error.toString())
    }
  }

  uploadImage(image) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        let survey = this.state.survey
        survey.image_url = result.secure_url
        this.setState({survey:survey})
        this.setState({uploadingImage:false})
        toastr.success('Success','Successfully uploaded image')
      } else {
        toastr.error("Error", "Unable to upload image")
      }
    })
  }

  newPreviewQuestion(questionType){
    let preview = null
    try{
      if (questionType === 'yesno'){
        preview = [{
          "text":"Were you satisfied with our service?",
          "answers":this.state.yesno
        }]
      } else if (questionType === '3-rating') {
        preview = [{
          "text":"How would you rate our service?",
          "answers":this.state.threeRatingAnswers
        }]      
      } else if (questionType === '5-rating') {
        preview = [{
          "text":"Store hours are convenient for my dining needs",
          "answers":this.state.fiveRatingAnswers
        }]      
      } else if (questionType === 'multipleAnswers') {
        preview = [{
          "text":"Which matters to you most?",
          "answers":[{
            "text":"Service",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Price",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Location",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          }]
        }]      
      } else {
        preview = [{
          "text":"Which matters to you most?",
          "answers":[{
            "text":"Service",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Price",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Location",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          }]
        }]
      }
    } catch (e) {
      return []
    }
    return preview
  }

   newQuestion(){
    return ({
      text:this.state.previewQuestion[0].text,
      answers:this.newAnswer()
    })
  }

  newAnswer(){
    let answers
    try {
      if (this.state.questionType==='yesno'){
        answers = this.state.yesno
      } else if (this.state.questionType==='custom'){
        answers = [{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      } else if (this.state.questionType==='3-rating'){
        answers = this.state.threeRatingAnswers
      } else if (this.state.questionType==='5-rating'){
        answers = this.state.fiveRatingAnswers
      } else if (this.state.questionType==='multiple'){
        answers = this.state.multipleAnswers
      }
    } catch(e) {
      answers = [{
        "text":"True",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      },{
        "text":"False",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      }]
    }
    return answers
  }

  addAnswer(){
    let form = this.state.form
    // form.answers.push(this.newAnswer())
    form.answers.push({
      "text":"",
      "created_by":this.props.user,
      "updated_by":this.props.user 
    })
    this.setState({form:form})
  }

  multipleAddAnswer(){
    let multipleAnswers = this.state.multipleAnswers
    // form.answers.push(this.newAnswer())
    multipleAnswers.push({
      "text":"",
      "created_by":this.props.user,
      "updated_by":this.props.user 
    })
    this.setState({multipleAnswers:multipleAnswers})
  }

  removeAnswer(index){
    let form = this.state.form
    form.answers.splice(index, 1)
    this.setState({form:form})
  }

  handleYesNoInputChange = (e) => {
    if (e.target.name==="positive"){
      let answers = this.state.yesno
      answers[0].text = e.target.value
      this.setState({"yesno":answers})
    } else {
      let answers = this.state.yesno
      answers[1].text = e.target.value
      this.setState({"yesno":answers})
    }
    this.setState({previewQuestion:this.newPreviewQuestion('yesno')})
  }

  multipleRemoveAnswer(index){
    let multipleAnswers = this.state.multipleAnswers
    multipleAnswers.splice(index, 1)
    this.setState({multipleAnswers:multipleAnswers})
  }

  removeQuestion(index){
    let questions = this.state.questions
    questions.splice(index, 1)
    this.setState({questions:questions}) 
    this.setState({previewQuestion: [this.state.questions[this.state.questions.length - 1]]})
  }

  placeUp(index){
    let questions = this.state.questions
    let question = questions[index]
    questions.splice(index, 1)
    questions.splice(index-1, 0, question)
    this.setState({questions:questions}) 
  }

  placeDown(index){
    let questions = this.state.questions
    let question = questions[index]
    questions.splice(index, 1)
    questions.splice(index+1, 0, question)
    this.setState({questions:questions}) 
  }

  printQuestions(){
    // console.log(JSON.stringify(this.state.questions))
    // console.log(JSON.stringify(this.state.survey))
    console.log(JSON.stringify(this.state.surveysForUpdate))
  }

  saveQuestion = (e) => {
    e.preventDefault()
    if(this.state.addMode){
      let answers = []
      for (var i=0; i < this.state.form.answers.length; i++) {
        answers.push(Object.assign({}, this.state.form.answers[i]))
      }
      let form = {
        text: this.state.form.text,
        answers: answers
      }
      let questions = this.state.questions
      questions.push(form)
      this.setState({questions:questions})
      this.setState({previewQuestion:[form]})
      this.setFormBlank()
      if (this.state.questions.length===10){
        toastr.info('Info','You have reached the maximum number of questions you can create.')
      }
    } else {
      let answers = []
      for (var j=0; j < this.state.form.answers.length; j++) {
        answers.push(Object.assign({}, this.state.form.answers[j]))
      }
      let form = {
        text: this.state.form.text,
        answers: answers
      }
      let questions = this.state.questions
      questions[this.state.indexForUpdate] = form      
      this.setState({questions:questions})      
      this.setState({previewQuestion:[form]})
      this.setFormBlank()
      this.setState({addMode:true, indexForUpdate:-1})      
    }
  }

  setFormBlank(){
    let form = {
      text:"",
      answers:this.newAnswer()
    }
    this.setState({form:form})
  }

  setFormForUpdate(index){
    this.setState({addMode:false})
    this.setState({indexForUpdate:index})
    let form = this.state.questions[index]
    this.setState({form:form})
  }

  handleQuestionChange = (e) => {
    let form = this.state.form
    form.text = e.target.value
    this.setState({form:form})
  }

  handleAnswerChange = (index, e) => {
    let form = this.state.form
    form.answers[index].text = e.target.value
    this.setState({form:form})
  }


  multipleHandleAnswerChange = (index, e) => {
    let multipleAnswers = this.state.multipleAnswers
    multipleAnswers[index].text = e.target.value
    this.setState({multipleAnswers:multipleAnswers})
  }

  handlePreviewChange = (e) => {
    this.setState({previewStatus:e.target.value})  
    if (e.target.value === '1'){
      this.props.dispatch(updateSurveyType(this.state.survey.id, "qr"))
    } else if (e.target.value === '2'){
      this.props.dispatch(updateSurveyType(this.state.survey.id, "br"))
    }
  }

  handleSelect(key) {
    this.setState({selected:key})
    this.setState({form:this.newQuestion()})
  }

  handleInputChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
    this.setState({form:{}})
    this.setFormBlank()
    this.setState({questions:[]})
    this.setState({previewQuestion:this.newPreviewQuestion(e.target.value)})    
    if (e.target.value==='multiple'){
      let form = Object.assign({}, this.state.form)
      form.answers = [{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      this.setState({form:form})
    }
    let form = this.state.form
    let q = this.newPreviewQuestion(e.target.value)
    form.text = q[0].text
    this.setState({form:form})
  }

  handleRatingOne = (index, e) => {
    let ratingAnswers = this.state.threeRatingAnswers
    ratingAnswers[index].text = e.target.value
    this.setState({ratingAnswers:ratingAnswers})
    this.setState({questions:[],previewQuestion:this.newPreviewQuestion("3-rating")})
  }

  handleRatingFive = (index, e) => {
    let ratingAnswers = this.state.fiveRatingAnswers
    ratingAnswers[index].text = e.target.value
    this.setState({ratingAnswers:ratingAnswers})
    this.setState({questions:[],previewQuestion:this.newPreviewQuestion("5-rating")})
  }

  handleSurveyInputChange = (e) => {
    let survey = this.state.survey
    if (e.target.name==="name"){
      survey.name = e.target.value
      this.setState({survey:survey})
    } else if (e.target.name==="description"){
      survey.description = e.target.value
      this.setState({survey:survey})
    } else if (e.target.name==="image_url"){
      this.setState({uploadingImage:true})
      this.processImage(e.target.files[0])
    }
  }

  handleTaskFlowInputChange = (e) => {
    let task = this.state.task
    if (e.target.name==="name"){
      task.name = e.target.value
      this.setState({task:task})
    } else if (e.target.name==="description"){
      task.description = e.target.value
      this.setState({task:task})
    }
  }

  createSurvey = (e) => {
    e.preventDefault()
    console.log(JSON.stringify(this.props.survey))
    let survey = this.state.survey
    survey["created_by"] = this.props.user
    survey["updated_by"] = this.props.user
    if (this.state.defaultImage){
      survey.image_url = "https://res.cloudinary.com/dvarqi8oo/image/upload/v1491199516/survey_pnma5p.jpg"
    }
    if (!this.props.survey.created_at){
      delete survey['created_at']
      delete survey['updated_at']
      delete survey['id']
      delete survey['task_flow']
      this.props.dispatch(createSurvey(survey))
    } else {
      this.props.dispatch(updateSurvey(survey))
    }
    this.setState({selected:2})
  }

  createQuestion(){
    let questions = this.state.questions
    for (let i=0; i<questions.length;i++){
      questions[i]['survey'] = this.props.survey.id
      questions[i]['order'] = i+1
      questions[i]["created_by"] = this.props.user
      questions[i]["updated_by"] = this.props.user
    }
    this.props.dispatch(createQuestion(questions))
    this.props.dispatch({type:"CLEAR_SURVEY"})
    // this.handleSelect(4)
    browserHistory.push('/task/survey/manage')
  }

  componentWillMount(){
    this.props.dispatch(getSurveys())
    this.props.dispatch(getTasks())
    this.forceUpdate()
  }

  componentWillReceiveProps(nextProps){
    this.setState({surveys:nextProps.createSurvey.surveys})
    this.setState({tasks:nextProps.createSurvey.tasks})
    this.setState({task:nextProps.createSurvey.task})
    this.setState({survey:nextProps.createSurvey.survey})
    if (nextProps.createSurvey.tasks.length!==0) {
      this.setState({selectedTask:nextProps.createSurvey.tasks[0].id.toString()})  
    }
  }

  // component(nextProps){
  //   this.props.dispatch(getSurveys())
  // }

  createJobFlow = (e) => {
    e.preventDefault()
    let task = this.state.task
    task["created_by"] = this.props.user
    task["updated_by"] = this.props.user
    task["job_type"] = 60
    this.props.dispatch(createJobFlow(task))
    this.setState({selected:5})
    this.props.dispatch(getSurveys())
    this.props.dispatch(getTasks())
  }

  handleSurveyTypeChange = (e) =>{
    this.setState({surveyTypeSelector:e.target.value})
  }

  handleCheckboxChange = (index, e) => {
    const checked = e.target.checked
    let surveys = this.state.surveys
    surveys[index]['forUpdate'] = checked
    this.setState({surveys:surveys})
  }

  attachToTask(){
    if (this.state.selectedTask==='-1'){
      toastr.error('Error', 'Please select a task')
    } else {
      this.props.dispatch(attachSurveysToTask(this.state.surveys, this.state.selectedTask))
      this.props.dispatch({type:"CLEAR_SURVEY"})
      browserHistory.push('/administration')
    }
  }

  handleTaskSelectChange = (e) => {
    this.setState({selectedTask:e.target.value})
  }

  handleDefaultImageChange(){
    this.setState({defaultImage:!this.state.defaultImage})
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render(){
    // console.log(JSON.stringify(this.state.surveys))
    const rows = this.state.questions.map((question, index) => {
      const answers = question.answers.map((answer, index) => {
        return (
          <p key={index}>{"- "+answer.text}</p>
        )
      })
      return (
        <tr key={index}>
          <td>{ index+1 }</td>
          <td>{ question.text }</td>
          <td>
            { answers }
          </td>
          <td className="text-center">
            <DropdownButton dropup title="Edit" id={index} bsStyle="primary" bsSize="xsmall">
              <MenuItem eventKey="1" bsStyle="warning" onClick={this.setFormForUpdate.bind(this, index)}>
                Update
              </MenuItem>
              <MenuItem eventKey="2" bsStyle="danger" onClick={this.removeQuestion.bind(this,index)}>Delete</MenuItem>
              {
                index === 0?
                  null
                  :
                  <MenuItem eventKey="2" bsStyle="danger" onClick={this.placeUp.bind(this, index)}>Go Up</MenuItem>
              }
              {
                index === this.state.questions.length-1?
                  null
                  :                  
                  <MenuItem eventKey="2" bsStyle="danger" onClick={this.placeDown.bind(this, index)}>Go Down</MenuItem>
              }
            </DropdownButton>
          </td>
        </tr>
      )
    })

    const answerSize = this.state.form.answers.length

    const answers = this.state.form.answers.map((answer, index) => {
      return(
        <div className="row-fluid" key={index}>
            <input type="text" id="" className="span7" 
              placeholder={"Answer " + String(index+1)} 
              onChange={this.handleAnswerChange.bind(this, index)} 
              value={answer.text}/>
          &nbsp;
          {
            index > 1 || answerSize  > 2? 
              <button type="button" className="btn btn-danger" onClick={this.removeAnswer.bind(this, index)}>
                <i className="fa fa-remove"></i>
              </button>
            : null
          }
          &nbsp;
          {
            index === answerSize - 1?
             <button type="button" className="btn btn-primary"onClick={this.addAnswer.bind(this)}>
                <i className="fa fa-plus"></i>
              </button>
            : null
          }
          &nbsp;
        </div>
      )
    })

    const multipleAnswerSize = this.state.multipleAnswers.length

    const multipleAnswers = this.state.multipleAnswers.map((answer, index) => {
      return(
        <div className="control-group" key={index}>
          <div className="row-fluid">
            <span className="span4">
              <input type="text" className="span12" 
                placeholder={"Answer " + String(index+1)} 
                onChange={this.multipleHandleAnswerChange.bind(this, index)} 
                value={answer.text}/>
            </span> &nbsp;
            {
              index > 1 || multipleAnswerSize  > 2? 
                <button type="button" className="btn btn-danger" onClick={this.multipleRemoveAnswer.bind(this, index)}>
                  <i className="fa fa-remove"></i>
                </button>
              : null
            }
            &nbsp;
            {
              index === multipleAnswerSize - 1?
                <button type="button" className="btn btn-primary" onClick={this.multipleAddAnswer.bind(this)}>
                  <i className="fa fa-plus"></i>
                </button>
              : null
            }
          </div>
        </div>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.firstName}/>

        <div id="contentwrapper">
          <div className="main_content">
            <Nav page={"Create Survey"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Create Survey</h3>
                <div className="row-fluid">
                   <div className="span12">
                    <div id="graph" className="row-fluid" >
                      <div className="span12 margin-top-5">
                        <Tabs activeKey={ this.state.selected } onSelect={this.handleSelect.bind(this)} id="survey-details">
                          <Tab eventKey={1} title="General" disabled>
                            <form onSubmit={this.createSurvey.bind(this)}>
                                <div className="control-group">
                                  <span className="span12">
                                    <button className="pull-right btn btn-primary btn-small pull-right" 
                                      disabled={this.state.uploadingImage}>Next
                                      <i className="fa fa-arrow-right icon-white"></i>
                                    </button>
                                  </span>
                                </div>
                                <div className="control-group">
                                  <label><b>Name</b></label>
                                  <input type="text" id="" className="span4" placeholder="Enter Name"  
                                     name="name" required onChange={this.handleSurveyInputChange.bind(this)}/>
                                </div>
                                <div className="control-group">
                                  <label><b>Description</b></label>
                                  <textarea name="description" className="span4" placeholder="Enter Description"
                                    required onChange={this.handleSurveyInputChange.bind(this)}>
                                  </textarea>
                                </div>
                                <div className="control-group">
                                  <label>
                                    <input type="checkbox" className="checkbox-position"
                                    checked={this.state.defaultImage} onChange={this.handleDefaultImageChange.bind(this)} />
                                      <b>Use Default Image</b>
                                  </label>
                                </div>
                                {
                                  this.state.defaultImage?
                                    null
                                  :
                                    <div className="control-group">
                                      <label><b>Image</b></label>
                                      <input type="file" id="" accept="image/*" 
                                        name="image_url"
                                        required
                                        onChange={this.handleSurveyInputChange.bind(this)}/>
                                    </div>
                                }
                              </form>
                          </Tab>
                          <Tab eventKey={2} title="Type" disabled>
                            <form>
                              <div className="control-group">
                                <span className="span12">
                                  <button onClick={() => this.handleSelect(1)} 
                                    type="button" 
                                    className="pull-left btn btn-primary btn-small" >
                                    <i className="fa fa-arrow-left icon-white"></i> Previous
                                  </button>
                                  <button onClick={() => this.handleSelect(3)} 
                                    type="button" 
                                    className="pull-right btn btn-primary btn-small pull-right" >
                                    Next <i className="fa fa-arrow-right icon-white"></i>
                                  </button>
                                </span>
                              </div><br/><br/>
                              <div className="control-group">
                                <label>
                                  <input type="checkbox" className="checkbox-position"
                                    name="questionType" 
                                    onChange={this.handleInputChange.bind(this)}
                                    value="yesno"
                                    checked={this.state.questionType==="yesno"?true:false}/>&nbsp;
                                  <span>Yes / No Question and Answers</span>
                                </label>
                              </div>
                              <div className="control-group">
                                <label>
                                  <input type="checkbox" className="checkbox-position"
                                    name="questionType" 
                                    onChange={this.handleInputChange.bind(this)}
                                    value="3-rating"
                                    checked={this.state.questionType==="3-rating"?true:false}/>&nbsp;
                                  <span>3-Rating Scale Survey</span>
                                </label>
                              </div>
                              <div className="control-group">
                                <label>
                                  <input type="checkbox" className="checkbox-position"
                                    name="questionType" 
                                    onChange={this.handleInputChange.bind(this)}
                                    value="5-rating"
                                    checked={this.state.questionType==="5-rating"?true:false}/>&nbsp;
                                  <span>5-Rating Scale Survey</span>
                                </label>
                              </div>
                              <div className="control-group">
                                <label>
                                  <input type="checkbox" className="checkbox-position"
                                    name="questionType" 
                                    onChange={this.handleInputChange.bind(this)}
                                    value="multiple"
                                    checked={this.state.questionType==="multiple"?true:false}/>&nbsp;
                                  <span>Multiple Choice Questions</span>
                                </label>
                              </div>
                              <div className="control-group">
                                <label>
                                  <input type="checkbox" className="checkbox-position"
                                    name="questionType" 
                                    onChange={this.handleInputChange.bind(this)}
                                    value="custom"
                                    checked={this.state.questionType==="custom"?true:false}/>&nbsp;
                                  <span>Custom Type</span>
                                </label>
                              </div>
                              <div className="control-group">
                                {
                                  this.state.questionType==="3-rating"?
                                    <div className="row-fluid">
                                      <h5> For this type of question, please set the 3 choices.</h5>
                                      <div className="control-group">
                                        <input type="text" className="span4"
                                          placeholder="Choice 1"
                                          maxLength="20"
                                          value={this.state.threeRatingAnswers[0].text}
                                          onChange={this.handleRatingOne.bind(this, 0)} />
                                      </div>
                                      <div className="control-group">
                                        <input type="text" className="span4"
                                          placeholder="Choice 2"
                                          maxLength="20"
                                          value={this.state.threeRatingAnswers[1].text}
                                          onChange={this.handleRatingOne.bind(this, 1)}/>
                                      </div>
                                      <div className="control-group">
                                        <input type="text" className="span4"
                                          placeholder="Choice 3"
                                          maxLength="20"
                                          value={this.state.threeRatingAnswers[2].text}
                                          onChange={this.handleRatingOne.bind(this, 2)}/>
                                      </div>
                                    </div>
                                    : 
                                    this.state.questionType==="5-rating"?
                                      <div className="row-fluid">
                                        <h5> For this type of question, please set the 3 choices.</h5>
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="Choice 1"
                                            maxLength="20"
                                            value={this.state.fiveRatingAnswers[0].text}
                                            onChange={this.handleRatingFive.bind(this, 0)} />
                                        </div>
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="Choice 2"
                                            maxLength="20"
                                            value={this.state.fiveRatingAnswers[1].text}
                                            onChange={this.handleRatingFive.bind(this, 1)} />
                                        </div>
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="Choice 3"
                                            maxLength="20"
                                            value={this.state.fiveRatingAnswers[2].text}
                                            onChange={this.handleRatingFive.bind(this, 2)} />
                                        </div>
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="Choice 3"
                                            maxLength="20"
                                            value={this.state.fiveRatingAnswers[3].text}
                                            onChange={this.handleRatingFive.bind(this, 3)} />
                                        </div>
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="Choice 3"
                                            maxLength="20"
                                            value={this.state.fiveRatingAnswers[4].text}
                                            onChange={this.handleRatingFive.bind(this, 4)} />
                                        </div>
                                      </div>
                                    :
                                      this.state.questionType==="multiple"?
                                      <Col md={6}>
                                        <div>
                                          {multipleAnswers}
                                        </div>
                                      </Col>
                                      :
                                      this.state.questionType==="yesno"?
                                      <div className="row-fluid">
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="YES"
                                            maxLength="20"
                                            name="positive"
                                            value={this.state.yesno[0].text}
                                            onChange={this.handleYesNoInputChange.bind(this)}/>
                                        </div>
                                        <div className="control-group">
                                          <input type="text" className="span4"
                                            placeholder="Choice 2"
                                            maxLength="20"
                                            name="negative"
                                            value={this.state.yesno[1].text}
                                            onChange={this.handleYesNoInputChange.bind(this)}/>
                                        </div>
                                      </div>
                                      :
                                      this.state.questionType==="custom"?
                                      <Col md={6}>
                                        <h5><i><b>Custom mode selected. Please proceed to the next page.</b></i></h5>
                                      </Col>
                                      :
                                      null
                                }
                              </div>
                            </form>
                          </Tab>
                          <Tab eventKey={3} title="Content" disabled>
                            <div className="control-group">
                              <span className="span12">
                                <button onClick={() => this.handleSelect(2)} 
                                    type="button" 
                                    className="pull-left btn btn-primary btn-small" >
                                    <i className="fa fa-arrow-left icon-white"></i> Previous
                                </button>
                                <button onClick={this.createQuestion.bind(this)} 
                                  type="button" 
                                  className="pull-right btn btn-primary btn-small pull-right" >
                                  Next <i className="fa fa-arrow-right icon-white"></i>
                                </button>
                              </span>
                            </div><br/><br/>
                            <div className="control-group">
                              <div className="row-fluid">
                                <div className="well span6">
                                  <form onSubmit={this.saveQuestion.bind(this)}>
                                    <div className="row-fluid">
                                      Question
                                    </div>
                                    <div className="row-fluid">
                                      <textarea className="span8" rows="3" 
                                        value={this.state.form.text}
                                        onChange={this.handleQuestionChange.bind(this)}
                                        placeholder="Question" 
                                        required></textarea>&nbsp;&nbsp;
                                      <button 
                                        className={
                                          this.state.addMode?
                                            "btn btn-primary btn-small add-question-btn":"btn btn-danger btn-small add-question-btn"}
                                        disabled={
                                          this.state.questions.length===10
                                          &&this.state.addMode?true:false
                                        }>
                                       <i className={this.state.addMode?"fa fa-plus icon-white":"fa fa-plus icon-white"}></i>
                                       &nbsp;{this.state.addMode?" Add":" Change"}
                                      </button>
                                    </div>
                                    <br/>
                                    <div className="row-fluid">
                                      Answers
                                    </div>
                                     { this.state.questionType==='custom'? answers:null }
                                   </form>
                                </div>
                                <div className="well span6">
                                   { 
                                      this.state.previewStatus === '1'?
                                        <QR {...this.state} /> :
                                      this.state.previewStatus === '2'?
                                        <ButtonResponse {...this.state} /> : null
                                    }
                                </div>
                              </div>
                              <div className="row-fluid">
                                <div className="well span6">
                                  <div className="row-fluid">
                                    Question
                                  </div>
                                  <div className="row-fluid">
                                    <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Question</th>
                                          <th>Answers</th>
                                          <th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        { rows }
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Tab>
                        </Tabs>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.EditBotTaskSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
      </div>
    )
  }
}

CreateSurvey.propTypes = {
  surveys: React.PropTypes.array.isRequired,
  tasks: React.PropTypes.array.isRequired,
}

export default connect(mapStateToProps)(CreateSurvey)


// <Col sm={6} className="micro-top-padding">
//   <FormControl componentClass="select" bsSize="small" onChange={this.handlePreviewChange.bind(this)}>
//     <option value={1}>Quick Reply</option>
//     <option value={2}>Button Template</option>
//     <option value={3}>Generic Template</option>
//     <option value={4}>List Template</option>
//   </FormControl>
// </Col>

// <Tab eventKey={4} title="Assign to Bot">
//   <Row className="micro-top-padding">
//     <Col md={4}>
//       <FormControl componentClass="select" bsSize="small" onChange={this.handlePreviewChange.bind(this)}>
//         <option value={1}>Autobot</option>
//         <option value={2}>Decepticon</option>
//         <option value={3}>BotBot</option>
//         <option value={4}>Bot Tobias</option>
//       </FormControl>
//     </Col>
//   </Row>
//   <Row className="micro-top-padding">
//     <Col md={6}>
//       {surveys}
//     </Col>
//   </Row>
//   <Row className="micro-top-padding">
//     <Col md={6} className="pull-right">
//       <Button bsStyle="primary" bsSize="small" onClick={() => this.handleSelect(3)}>
//         Save{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-save" />                        
//       </Button>
//     </Col>
//   </Row>
// </Tab>