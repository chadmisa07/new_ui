import React from 'react'

const Nav = (props) => {
	return (
		<nav>
	        <div className="breadCrumb module">
	          <ul>
	            <li><a><i className="icon-home"></i></a></li>
	            <li>Edit Bot Task</li>
	            <li>{props.page}</li>
	          </ul>
	        </div>
      	</nav>

	)
}

export default Nav