import React, { Component } from 'react'
import { Grid, Row, Navbar } from 'react-bootstrap'
import { Link } from 'react-router'
import LoadingBar from 'react-redux-loading-bar'

class Header extends Component{
  render(){
    return (
      <div className="header-2">
        <Grid fluid>
          <Row>
            <LoadingBar className="loadingBar"/>
            <Navbar fluid className="create-bot-navbar" defaultExpanded>
              <Navbar.Header >
                <Navbar.Brand>
                  <Link><span className="header-text-left">Chatbot Tooling</span></Link>
                </Navbar.Brand>
              </Navbar.Header>
              <Navbar.Collapse>
                <Navbar.Text pullLeft className="header-margin-left header-border-bottom">
                  <Link to="/task/edit-jobs" 
                  className={this.props.selected==='editBotJob'?"header-active":"header-text-2"}>
                    Edit Bot Tasks
                  </Link>
                </Navbar.Text>
                <Navbar.Text pullLeft className="header-border-bottom">
                  <Link to="/task/survey"><span className={this.props.selected==='createSurvey'?"header-active":"header-text-2"}>Create Survey</span></Link>
                </Navbar.Text>
                <Navbar.Text pullLeft className="header-border-bottom">
                  <Link to="/task/edit-jobs" 
                  className={this.props.selected==='editBotJob'?"header-active":"header-text-2"}>
                    Survey Settings
                  </Link>
                </Navbar.Text>
                <Navbar.Text pullRight className="header-border-bottom">
                  <a className="header-text-right-2" href="/logout" >My Account</a>
                </Navbar.Text>
                <Navbar.Text pullRight className="header-border-bottom">
                  <a className="header-text-right-2" href="/logout" >Help</a>
                </Navbar.Text>
              </Navbar.Collapse>
            </Navbar>
          </Row>
        </Grid>
      </div>
    )
  }
}

export default Header