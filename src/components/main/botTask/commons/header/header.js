import React, { Component } from 'react'
import { Link } from 'react-router'
import LoadingBar from 'react-redux-loading-bar'

class Header extends Component{
  render(){
    return (
      <header>
      <div className="navbar navbar-fixed-top">
        <LoadingBar className="loadingBar"/>
        <div className="navbar-inner">
          <div className="container-fluid">
            <Link className="brand tac">
              <img src={this.props.logo} className="visible-desktop visible-tablet" alt="NTUITIV" />
              <img src={this.props.logo_small} className="visible-phone" alt="NTUITIV" />
            </Link>
            <ul className="nav user_menu nav-items">
              <li className="hidden-phone hidden-tablet"></li>
              <li className="dropdown visible-desktop">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                <i className="fa fa-user-circle fa-lg icon-blue"></i>&nbsp;&nbsp;
                  Hi, {this.props.name}! &nbsp;<i className="fa fa-caret-down icon-blue"></i></a>
                <ul className="dropdown-menu">
                  <li>
                    <a href="https://www.facebook.com/v2.8/dialog/oauth?client_id=1038731512904671&
                      redirect_uri=https://cbot-tooling.herokuapp.com/fb/&auth_type=rerequest&scope=email,
                      public_profile,manage_pages,pages_messaging,pages_messaging_subscriptions">
                        <span className="action-btn-label">Connect with Facebook</span>
                    </a>
                  </li>
                  <li><a href="#">Dashboard</a></li>
                  <li><a href="#">Change Password</a></li>
                  <li><Link to="/logout">Logout</Link></li>
                </ul>
              </li>
            </ul>
            <ul className="nav nav-btns">
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <Link to="/task/edit-jobs" activeClassName="active">
                  <i className="fa fa-tasks icon-blue"></i>
                  <span className="action-btn-label"> Edit Bot Tasks</span>
                </Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <Link to="/task/survey/create" activeClassName="active"><i className="fa fa-commenting-o icon-blue"></i>
                <span className="action-btn-label"> Create Survey</span></Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
               <Link to="/task/survey/manage" activeClassName="active">
                <i className="fa fa-pencil icon-blue"></i>
                <span className="action-btn-label"> Edit Survey</span>
               </Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <Link to="/task/survey-settings" activeClassName="active">
                  <i className="fa fa-cog icon-blue"></i>
                  <span className="action-btn-label"> Survey Settings</span>
                </Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <Link to="/task/faq" activeClassName="active">
                  <i className="fa fa-question-circle-o icon-blue"></i>
                  <span className="action-btn-label"> FAQ</span>
                </Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown visible-phone visible-tablet pull-right">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown" data-target=".nav-collapse"><i className="fa fa-caret-down icon-blue"></i></a>
                <ul className="dropdown-menu">
                  <li><a href="#">Change Password</a></li>
                  <li><Link to="/logout">Logout</Link></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      </header>
    )
  }
}

export default Header