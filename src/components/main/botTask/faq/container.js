import React from 'react'
import { connect } from 'react-redux'
// import { browserHistory } from 'react-router'
import { Tabs, Tab } from 'react-bootstrap'
import { toastr } from 'react-redux-toastr'
import cloudinary from 'cloudinary'
cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'
import * as Common from '../index'
// import validator from 'validator'
import * as Request from '../requests/request'
import DeleteModal from './deleteModal'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    faqs: state.faq.list,
    faqCount: state.faq.faqCount,
    tasks: state.createSurvey.tasks
  }
}

class FAQContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      "sidebar": true,
      "date": new Date(),
      "currentPage": 1,
      "name": "",
      "description": "",
      "created_by": props.user.id,
      "updated_by": props.user.id,
      "defaultImage": true,
      "showDeleteModal":false,
      "faq": {
        "name":"",
        "description":"",
        "faq_type":"Website",
        "payload":"",
        "image_url":"",
        "faq_url":"",
      },
      "faqs":props.faqs,
      "uploadingImage":false,
      "selectedFaq":{},
      "addMode":true,
      "selected":1,
      "openDeleteModal":this.openDeleteModal.bind(this),
      "closeDeleteModal":this.closeDeleteModal.bind(this),
      "deleteFAQ":this.deleteFAQ.bind(this),
      "selectedTask":"-1",
      "checkAll":false,
      "faqType": "url",
      "faqTypes": [{
        "type":"Website"
      },{
        "type":"Contact"
      },{
        "type":"Text"
      },{
        "type":"Audio"
      },{
        "type":"File"
      },{
        "type":"Image"
      },{
        "type":"Video"
      },]
    }
  }

  componentWillMount(){
    this.props.dispatch(Request.getFAQ(1))
    this.props.dispatch(Request.getTasks())    
    this.setState({selectedTask: "-1"})
    this.setState({originalFaq: this.props.faqs})
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
    this.props.dispatch({type: "FAQ_LIST_FETCHED", payload: [], count: this.props.faqCount})
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

   openDeleteModal(index) {
    this.setState({selectedFaq:this.props.faqs[index]})
    this.setState({showDeleteModal:true})
  }

  closeDeleteModal() {
    this.setState({showDeleteModal:false})
  }

  deleteFAQ(id){
    this.props.dispatch(Request.deleteFAQ(id))
    this.closeDeleteModal()
  }

  handleDefaultImageChange(){
    this.setState({defaultImage:!this.state.defaultImage})
  }

  handleFAQInputChange = (e) => {
    let faq = this.state.faq
    // faq.payload = ""
    if (e.target.name==="name"){
      faq.name = e.target.value
      this.setState({faq:faq})
    } else if (e.target.name==="description"){
      faq.description = e.target.value
      this.setState({faq:faq})
    } else if (e.target.name==="payload"){
      faq.payload = e.target.value.toString()
      this.setState({faq:faq})
    } else if (e.target.name==="image_url"){
      this.setState({uploadingImage:true})
      this.processImage(e.target.files[0])
    } else if (e.target.name==="faqType"){
      faq.faq_type = e.target.value
      this.setState({faq:faq})
    }
  }

  processImage(image_url){
    try {
      let reader = new FileReader()
      reader.readAsDataURL(image_url)
      reader.onloadend = () => {
        this.uploadImage(reader.result)
      }
    } catch (error) {
      toastr.error('Error', error.toString())
    }
  }

  uploadImage(image) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        let faq = this.state.faq
        faq.image_url = result.secure_url
        this.setState({faq:faq})
        this.setState({uploadingImage:false})
        toastr.success('Success','Successfully uploaded image')
      } else {
        toastr.error("Error", "Unable to upload image")
      }
    })
  }

  createFAQ = (e) => {
    e.preventDefault()
    let faq = Object.assign({}, this.state.faq)
    faq["created_by"] = this.props.user.id
    faq["updated_by"] = this.props.user.id
    if (this.state.defaultImage || this.state.faq.faq_url===''){
      faq.image_url = "https://res.cloudinary.com/dvarqi8oo/image/upload/v1491295840/faq_lmxyn4.png"
    }
    if (this.state.addMode){
      if (this.state.faq.faq_type==='Contact'){
        if (faq.payload.length!==11){
          toastr.error('Error', "Please provide an 11 digit contact number")
        } else {
          faq.payload = "+" + faq.payload
          this.props.dispatch(Request.createFAQ(faq))
        }
      } else {     
        this.props.dispatch(Request.createFAQ(faq))
        this.setFAQBlank()
      }      
    } else {
      delete faq['created_at']
      delete faq['updated_at']
      delete faq['task_flow']
      this.props.dispatch(Request.updateFAQ(faq))
      this.setState({addMode:true})
      this.setFAQBlank()
    }
  }

  setFAQBlank() {
    let blankFaq = this.state.faq
    blankFaq.name = ""
    blankFaq.description = ""
    blankFaq.image_url = ""
    blankFaq.payload = ""
    this.setState({"faq": blankFaq})
  }

  handleTaskSelectChange = (e) => {
    this.setState({selectedTask:e.target.value})
    this.setState({checkAll:false})
    let faqs = this.props.faqs
    for (let i=0; i<faqs.length;i++){
      try{
        if (faqs[i].task_flow.toString()===e.target.value){
          faqs[i]['forUpdate'] = true
        } else {
          faqs[i]['forUpdate'] = false
        }
      } catch (e) {
        faqs[i]['forUpdate'] = false
      }
    }
    this.props.dispatch({type: "FAQ_LIST_FETCHED", payload: faqs, count: this.props.faqCount})
  }

  handleCheckAll = (e) => {
    const checked = e.target.checked
    this.setState({checkAll:checked})
    let faqs = this.props.faqs
    for (let i = 0; i < faqs.length; i++){
      faqs[i]['forUpdate'] = checked
    }
    this.props.dispatch({type: "FAQ_LIST_FETCHED", payload: faqs, count: this.props.faqCount})
  }

  handleSelect(key) {
    this.setState({selected:key})
    this.props.dispatch(Request.getFAQ(1))
  }

  goToEdit(index){
    let faq = Object.assign({}, this.props.faqs[index])
    if (faq.faq_type==="Contact"){
      faq.payload = faq.payload.substring(1)
    }
    this.setState({faq:faq})
    this.setState({addMode:false})
    this.setState({selected:1})
  }

  handleCheckboxChange = (index, e) => {
    const checked = e.target.checked
    let faqs = this.props.faqs
    faqs[index]['forUpdate'] = checked
    this.setState({faqs:faqs})
  }

  attachToTask(){
    if (this.state.selectedTask==='-1'){
      toastr.error('Error', 'Please select a task')
    } else {
      this.props.dispatch(Request.attachFAQToTask(this.props.faqs, this.state.selectedTask))
      // browserHistory.push('/administration')
    }
  }

  handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(Request.getFAQ(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(Request.getFAQ(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(Request.getFAQ(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(Request.getFAQ(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.faqCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(Request.getFAQ(bots.length))
    this.setState({currentPage: bots.length})
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.faqCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""}
              onClick={this.handleTransferPage.bind(this)}>{number}
            </a>
        )
    })

    const faqTypes = this.state.faqTypes.map((faqType, index) => {
      return (
        <option key={index} value={faqType.type}>{faqType.type}</option>
      )
    })

    const rows = this.props.faqs.map((faq, index) => {
      return (
        <tr key={index}>
          <td className="tac">{faq.id}</td>
          <td className="tac">{faq.name}</td>
          <td className="tac">{faq.description}</td>
          {
            faq.faq_type==='Text'?
              <td>{faq.payload}</td>
              :
            faq.faq_type==='Contact'?
              <td>{faq.payload}</td>
              :
              <td><a target="_blank" href={faq.payload}>{faq.faq_type}</a></td>
          }
          <td className="tac">
            <button type="button"
              className="btn btn-primary btn-small"
              onClick={this.goToEdit.bind(this, index)}>
                Edit
            </button>&nbsp;
            <button type="button"
              className="btn btn-danger btn-small"
              onClick={this.openDeleteModal.bind(this, index)}>
                Delete
            </button>
          </td>
        </tr>
      )
    })

    const tasks = this.props.tasks.map((task, index) => {
      return(
        <option 
          key={index} 
          value={task.id}
        >
          {task.name}
        </option>
      )
    })

    const faqs = this.props.faqs.map((faq, index) => {
      return(
        <tr key={index}>
          <td className="tac">
            <input
              className="no-margin position-relative"
              type="checkbox"
              checked={faq.forUpdate}
              value={faq.name}
              onChange={this.handleCheckboxChange.bind(this, index)}
            />
          </td>
          <td className="tac">
            {faq.name}
          </td>
          <td className="tac">
            {faq.description}
          </td>
          <td className="tac">
            {faq.updated_at}
          </td>
        </tr>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>

        <div id="contentwrapper">
          <div className="main_content">
            <Common.Nav page={"FAQ"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">FAQ</h3>
                <div className="row-fluid">
                   <div className="span12">
                    <div id="graph" className="row-fluid" >
                      <div className="span12 margin-top-5">
                        <Tabs id="faq-details" activeKey={ this.state.selected } onSelect={this.handleSelect.bind(this)}>
                          <Tab eventKey={1} title="Create FAQ Links">
                            <form onSubmit={this.createFAQ.bind(this)}>
                                <div className="control-group">
                                  <label><b>FAQ Name</b></label>
                                  <input type="text" id="" className="span5" 
                                    placeholder="Enter Name"
                                    name="name"
                                    required
                                    value={this.state.faq.name}
                                    onChange={this.handleFAQInputChange.bind(this)}/>
                                </div>
                                <div className="control-group">
                                  <label><b>Description</b></label>
                                  <textarea name="description" className="span5"
                                    placeholder="Enter Description"
                                    required
                                    value={this.state.faq.description}
                                    onChange={this.handleFAQInputChange.bind(this)}>
                                  </textarea>
                                </div>
                                <div className="control-group">
                                  <label><b>FAQ Type</b></label>
                                  <select name="faqType" className="span5"
                                    required
                                    value={this.state.faq.faq_type}
                                    onChange={this.handleFAQInputChange.bind(this)}>
                                      {faqTypes}
                                  </select>
                                </div>
                                <div className="control-group">
                                  <label>
                                    <b> 
                                      {
                                        this.state.faq.faq_type==='Website'? "FAQ Website":
                                        this.state.faq.faq_type==='Contact'? "Contact Number (Country Code + Area Code + Number)": "Payload"
                                      }
                                    </b>
                                  </label>
                                  <input id="" className="span5" 
                                    placeholder={
                                              this.state.faq.faq_type==='Text'? "Enter Text Message":
                                              this.state.faq.faq_type==='Contact'? "Enter Contact Number": "Enter URL"
                                            }
                                    name="payload"
                                    type={
                                        this.state.faq.faq_type==='Text'? "text":
                                        this.state.faq.faq_type==='Contact'? "number": "url"
                                      }
                                    maxLength="630"
                                    required
                                    value={this.state.faq.payload}
                                    onChange={this.handleFAQInputChange.bind(this)}/>
                                </div>
                                <div className="control-group">
                                  <label>
                                    <input type="checkbox" className="checkbox-position"
                                      checked={this.state.defaultImage} onChange={this.handleDefaultImageChange.bind(this)}/>
                                        &nbsp;<b>Use Default Image</b>
                                  </label>
                                </div>
                                {
                                  this.state.defaultImage?
                                    null
                                  :
                                    <div className="control-group">
                                      <label>Image</label>
                                      <input type="file" id="" accept="image/*" 
                                        name="image_url"
                                        required
                                        onChange={this.handleFAQInputChange.bind(this)}/>
                                    </div>
                                }
                                <div className="control-group">
                                  <span className="span5">
                                    <button className="pull-right btn btn-primary btn-small pull-right" 
                                      disabled={this.state.uploadingImage}>{this.state.addMode?"Create":"Update"}
                                    </button>
                                  </span>
                                </div>
                              </form>
                          </Tab>
                          <Tab eventKey={2} title="Manage FAQs">
                            <div className="row-fluid span8">
                              <b>List of created FAQs</b><br/><br/>
                              <table className="table table-bordered table-striped table-overflow">
                                <thead>
                                  <tr>
                                    <th className="tac">FAQ ID</th>
                                    <th className="tac">FAQ Name</th>
                                    <th className="tac">Description</th>
                                    <th className="tac">URL</th>
                                    <th className="tac" width="130"></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {
                                    !this.props.faqs.length?
                                    <tr>
                                      <td colSpan="5" className="tac">No FAQ found</td>
                                    </tr> : null
                                  }
                                  {rows}
                                </tbody>
                              </table>
                            </div>
                            <div className="row-fluid pagination">
                              {renderPageNumbers.length!==0?
                                <a href="#" onClick={renderPageNumbers.length!== 1 && renderPageNumbers.length!==0? 
                                  this.handleFirst.bind(this) : ""}>&#x27EA;</a>:""
                              }
                              {this.state.currentPage > 1 ? <a href="#" onClick={this.handlePrevious.bind(this)}>&#x27E8;</a> : ""}
                              {renderPageNumbers}
                              { this.state.currentPage <= renderPageNumbers.length + 2 && renderPageNumbers.length!== 1 &&
                                renderPageNumbers.length!==0?<a href="#" onClick={this.handleNext.bind(this)}>&#x27E9;  </a>:""
                              }
                              {renderPageNumbers.length!==0?
                                <a href="#" onClick={this.state.currentPage !== renderPageNumbers.length + 2 && 
                                  renderPageNumbers.length !== 1 && renderPageNumbers.length!==0? this.handleLast.bind(this) : ""}>
                                  &#x27EB;</a>: ""
                              }
                            </div>
                          </Tab>
                          <Tab eventKey={3} title="Assign FAQ to Task">
                            <div className="row-fluid micro-top-padding span8">
                              <div className="row-fluid">
                                <h5>Choose the task</h5>
                                <select className="span4" onChange={this.handleTaskSelectChange.bind(this)}>
                                  <option value="-1">Select Task</option>
                                  {tasks}
                                </select>
                              </div>
                              <div className="row-fluid items">
                                <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                                  <thead>
                                    <tr>
                                      <th className="tac">
                                        <input 
                                          type="checkbox"
                                          onChange={this.handleCheckAll.bind(this)}
                                          checked={this.state.checkAll}
                                          value={"yo"}
                                        />
                                      </th>
                                      <th className="tac">
                                        Name
                                      </th>
                                      <th className="tac">
                                        Description
                                      </th>
                                      <th className="tac">
                                        Date Modified
                                      </th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {
                                      !this.props.faqs.length?
                                      <tr>
                                        <td colSpan="4" className="tac">No FAQ found</td>
                                      </tr> : null
                                    }
                                    {faqs}
                                  </tbody>
                                </table>
                              </div>
                              <div className="pagination">
                                {renderPageNumbers.length!==0?
                                  <a href="#" onClick={renderPageNumbers.length!== 1 && renderPageNumbers.length!==0? 
                                    this.handleFirst.bind(this) : ""}>&#x27EA;</a>:""
                                }
                                {this.state.currentPage > 1 ? <a href="#" onClick={this.handlePrevious.bind(this)}>&#x27E8;</a> : ""}
                                {renderPageNumbers}
                                { this.state.currentPage <= renderPageNumbers.length + 2 && renderPageNumbers.length!== 1 &&
                                  renderPageNumbers.length!==0?<a href="#" onClick={this.handleNext.bind(this)}>&#x27E9;  </a>:""
                                }
                                {renderPageNumbers.length!==0?
                                  <a href="#" onClick={this.state.currentPage !== renderPageNumbers.length + 2 && 
                                    renderPageNumbers.length !== 1 && renderPageNumbers.length!==0? this.handleLast.bind(this) : ""}>
                                    &#x27EB;</a>: ""
                                }
                              </div>
                              <div className="row-fluid micro-top-padding">
                                <button type="button" 
                                  className="btn btn-primary btn-small pull-right" 
                                  onClick={this.attachToTask.bind(this)}>
                                   Assign <i className="fa fa-check"></i></button>
                              </div>
                            </div>
                          </Tab>
                        </Tabs>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <DeleteModal {...this.state} {...this.props} />
        <Common.EditBotTaskSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
      </div>
    )
  }
}

export default connect(mapStateToProps)(FAQContainer)