import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { 
  Row, 
  Col, 
  FormGroup, 
  ControlLabel, 
  FormControl, 
  Checkbox,
  Table, 
  Button, 
  Glyphicon, 
  Tabs, 
  Tab, 
  Form, 
  DropdownButton, 
  MenuItem,
  Radio
} from 'react-bootstrap'
import QR from './preview/qr'
import ButtonResponse from './preview/button'
import { toastr } from 'react-redux-toastr'
import { connect } from 'react-redux'

import cloudinary from 'cloudinary'
import { 
  createSurvey, 
  createQuestion,
  updateSurveyType,
  createJobFlow,
  getSurveys,
  attachSurveysToTask,
  getTasks
} from '../requests/request'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

const mapStateToProps = (state) => {
  return {
    task: state.createSurvey.task,
    questions: state.createSurvey.questions,
    survey: state.createSurvey.survey,
    user: state.user.id,
    surveys: state.createSurvey.surveys,
    tasks: state.createSurvey.tasks,
    createSurvey: state.createSurvey
  }
}

class CreateSurvey extends Component {
  constructor(props) {
    super(props)
    this.state = {
      "questionModal": false,
      "selected": 1,
      "form":{
        text:"",
        answers:this.newAnswer()
      },
      "questions":props.questions,
      "addMode":true,
      "indexForUpdate": -1,
      "previewStatus": "1",
      "questionType":"yesno",
      "survey":props.survey,
      "threeRatingAnswers":[{
        "text":"Needs Improvement",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Satisfactory",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Excellent",
        "created_by":props.user,
        "updated_by":props.user
      }],
      "fiveRatingAnswers":[{
        "text":"Strongly Disagree",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Disagree",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Neutral",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Agree",
        "created_by":props.user,
        "updated_by":props.user
      },{
        "text":"Strongly Agree",
        "created_by":props.user,
        "updated_by":props.user
      }],
      "previewQuestion":this.newPreviewQuestion("yesno"),
      "task":props.task,
      "uploadingImage":false,
      "surveyTypeSelector":"qr",
      "surveysForUpdate":[],
      "surveys":props.surveys,
      "tasks":props.tasks,
      "selectedTask":"-1",
      "defaultImage":true,
      "multipleAnswers":[{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
    }
  }

  processImage(image_url){
    try {
      let reader = new FileReader()
      reader.readAsDataURL(image_url)
      reader.onloadend = () => {
        this.uploadImage(reader.result)
      }
    } catch (error) {
      toastr.error('Error', error.toString())
    }
  }

  uploadImage(image) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        let survey = this.state.survey
        survey.image_url = result.secure_url
        this.setState({survey:survey})
        this.setState({uploadingImage:false})
        toastr.success('Success','Successfully uploaded image')
      } else {
        toastr.error("Error", "Unable to upload image")
      }
    })
  }

  newPreviewQuestion(questionType){
    let preview = null
    try{
      if (questionType === 'yesno'){
        preview = [{
          "text":"Were you satisfied with our service?",
          "answers":[{
            "text":"YES",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"NO",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          }]
        }]
      } else if (questionType === '3-rating') {
        preview = [{
          "text":"How would you rate our service?",
          "answers":this.state.threeRatingAnswers
        }]      
      } else if (questionType === '5-rating') {
        preview = [{
          "text":"Store hours are convenient for my dining needs",
          "answers":this.state.fiveRatingAnswers
        }]      
      } else if (questionType === 'multipleAnswers') {
        preview = [{
          "text":"Which matters to you most?",
          "answers":[{
            "text":"Service",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Price",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Location",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          }]
        }]      
      } else {
        preview = [{
          "text":"Which matters to you most?",
          "answers":[{
            "text":"Service",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Price",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          },{
            "text":"Location",
            "created_by":this.props.user,
            "updated_by":this.props.user  
          }]
        }]
      }
    } catch (e) {
      return []
    }
    return preview
  }

  newQuestion(){
    return ({
      text:"",
      answers:this.newAnswer()
    })
  }

  newAnswer(){
    let answers
    try {
      if (this.state.questionType==='yesno'){
        answers = [{"text":"YES",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"NO",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      } else if (this.state.questionType==='custom'){
        answers = [{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      } else if (this.state.questionType==='3-rating'){
        answers = this.state.threeRatingAnswers
      } else if (this.state.questionType==='5-rating'){
        answers = this.state.fiveRatingAnswers
      } else if (this.state.questionType==='multiple'){
        answers = this.state.multipleAnswers
      }
    } catch(e) {
      answers = [{
        "text":"True",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      },{
        "text":"False",
        "created_by":this.props.user,
        "updated_by":this.props.user  
      }]
    }
    return answers
  }

  addAnswer(){
    let form = this.state.form
    // form.answers.push(this.newAnswer())
    form.answers.push({
      "text":"",
      "created_by":this.props.user,
      "updated_by":this.props.user 
    })
    this.setState({form:form})
  }

  multipleAddAnswer(){
    let multipleAnswers = this.state.multipleAnswers
    // form.answers.push(this.newAnswer())
    multipleAnswers.push({
      "text":"",
      "created_by":this.props.user,
      "updated_by":this.props.user 
    })
    this.setState({multipleAnswers:multipleAnswers})
  }

  removeAnswer(index){
    let form = this.state.form
    form.answers.splice(index, 1)
    this.setState({form:form})
  }

  multipleRemoveAnswer(index){
    let multipleAnswers = this.state.multipleAnswers
    multipleAnswers.splice(index, 1)
    this.setState({multipleAnswers:multipleAnswers})
  }

  removeQuestion(index){
    let questions = this.state.questions
    questions.splice(index, 1)
    this.setState({questions:questions}) 
    this.setState({previewQuestion: [this.state.questions[this.state.questions.length - 1]]})
  }

  placeUp(index){
    let questions = this.state.questions
    let question = questions[index]
    questions.splice(index, 1)
    questions.splice(index-1, 0, question)
    this.setState({questions:questions}) 
  }

  placeDown(index){
    let questions = this.state.questions
    let question = questions[index]
    questions.splice(index, 1)
    questions.splice(index+1, 0, question)
    this.setState({questions:questions}) 
  }

  printQuestions(){
    // console.log(JSON.stringify(this.state.questions))
    // console.log(JSON.stringify(this.state.survey))
    console.log(JSON.stringify(this.state.surveysForUpdate))
  }

  saveQuestion = (e) => {
    e.preventDefault()
    if(this.state.addMode){
      let answers = []
      for (var i=0; i < this.state.form.answers.length; i++) {
        answers.push(Object.assign({}, this.state.form.answers[i]))
      }
      let form = {
        text: this.state.form.text,
        answers: answers
      }
      let questions = this.state.questions
      questions.push(form)
      this.setState({questions:questions})
      this.setState({previewQuestion:[form]})
      this.setFormBlank()
      if (this.state.questions.length===10){
        toastr.info('Info','You have reached the maximum number of questions you can create.')
      }
    } else {
      let answers = []
      for (var j=0; j < this.state.form.answers.length; j++) {
        answers.push(Object.assign({}, this.state.form.answers[j]))
      }
      let form = {
        text: this.state.form.text,
        answers: answers
      }
      let questions = this.state.questions
      questions[this.state.indexForUpdate] = form      
      this.setState({questions:questions})      
      this.setState({previewQuestion:[form]})
      this.setFormBlank()
      this.setState({addMode:true, indexForUpdate:-1})      
    }
  }

  setFormBlank(){
    let form = this.newQuestion()
    this.setState({form:form})
  }

  setFormForUpdate(index){
    this.setState({addMode:false})
    this.setState({indexForUpdate:index})
    let form = this.state.questions[index]
    this.setState({form:form})
  }

  handleQuestionChange = (e) => {
    let form = this.state.form
    form.text = e.target.value
    this.setState({form:form})
  }

  handleAnswerChange = (index, e) => {
    let form = this.state.form
    form.answers[index].text = e.target.value
    this.setState({form:form})
  }


  multipleHandleAnswerChange = (index, e) => {
    let multipleAnswers = this.state.multipleAnswers
    multipleAnswers[index].text = e.target.value
    this.setState({multipleAnswers:multipleAnswers})
  }

  handlePreviewChange = (e) => {
    this.setState({previewStatus:e.target.value})  
    if (e.target.value === '1'){
      this.props.dispatch(updateSurveyType(this.state.survey.id, "qr"))
    } else if (e.target.value === '2'){
      this.props.dispatch(updateSurveyType(this.state.survey.id, "br"))
    }
  }

  handleSelect(key) {
    this.setState({selected:key})
    this.setState({form:this.newQuestion()})
  }

  handleInputChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
    this.setState({form:{}})
    this.setFormBlank()
    this.setState({questions:[]})
    this.setState({previewQuestion:this.newPreviewQuestion(e.target.value)})
    if (e.target.value==='multiple'){
      let form = Object.assign({}, this.state.form)
      form.answers = [{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        },{
          "text":"",
          "created_by":this.props.user,
          "updated_by":this.props.user  
        }]
      this.setState({form:form})
    }
  }

  handleRatingOne = (index, e) => {
    let ratingAnswers = this.state.threeRatingAnswers
    ratingAnswers[index].text = e.target.value
    this.setState({ratingAnswers:ratingAnswers})
    this.setState({questions:[],previewQuestion:this.newPreviewQuestion("3-rating")})
  }

  handleRatingFive = (index, e) => {
    let ratingAnswers = this.state.fiveRatingAnswers
    ratingAnswers[index].text = e.target.value
    this.setState({ratingAnswers:ratingAnswers})
    this.setState({questions:[],previewQuestion:this.newPreviewQuestion("5-rating")})
  }

  handleSurveyInputChange = (e) => {
    let survey = this.state.survey
    if (e.target.name==="name"){
      survey.name = e.target.value
      this.setState({survey:survey})
    } else if (e.target.name==="description"){
      survey.description = e.target.value
      this.setState({survey:survey})
    } else if (e.target.name==="image_url"){
      this.setState({uploadingImage:true})
      this.processImage(e.target.files[0])
    }
  }

  handleTaskFlowInputChange = (e) => {
    let task = this.state.task
    if (e.target.name==="name"){
      task.name = e.target.value
      this.setState({task:task})
    } else if (e.target.name==="description"){
      task.description = e.target.value
      this.setState({task:task})
    }
  }

  createSurvey = (e) => {
    e.preventDefault()
    let survey = this.state.survey
    survey["created_by"] = this.props.user
    survey["updated_by"] = this.props.user
    if (this.state.defaultImage){
      survey.image_url = "https://res.cloudinary.com/dvarqi8oo/image/upload/v1491199516/survey_pnma5p.jpg"
    }
    this.props.dispatch(createSurvey(survey))
    this.setState({selected:2})
  }

  createQuestion(){
    let questions = this.state.questions
    for (let i=0; i<questions.length;i++){
      questions[i]['survey'] = this.props.survey.id
      questions[i]['order'] = i+1
      questions[i]["created_by"] = this.props.user
      questions[i]["updated_by"] = this.props.user
    }
    this.props.dispatch(createQuestion(questions))
    // this.handleSelect(4)
    browserHistory.push('/task/survey/manage')
  }

  componentWillMount(){
    this.props.dispatch(getSurveys())
    this.props.dispatch(getTasks())
    this.forceUpdate()
  }

  componentWillReceiveProps(nextProps){
    this.setState({surveys:nextProps.createSurvey.surveys})
    this.setState({tasks:nextProps.createSurvey.tasks})
    this.setState({task:nextProps.createSurvey.task})
    this.setState({survey:nextProps.createSurvey.survey})
    if (nextProps.createSurvey.tasks.length!==0) {
      this.setState({selectedTask:nextProps.createSurvey.tasks[0].id.toString()})  
    }
  }

  // component(nextProps){
  //   this.props.dispatch(getSurveys())
  // }

  createJobFlow = (e) => {
    e.preventDefault()
    let task = this.state.task
    task["created_by"] = this.props.user
    task["updated_by"] = this.props.user
    task["job_type"] = 60
    this.props.dispatch(createJobFlow(task))
    this.setState({selected:5})
    this.props.dispatch(getSurveys())
    this.props.dispatch(getTasks())
  }

  handleSurveyTypeChange = (e) =>{
    this.setState({surveyTypeSelector:e.target.value})
  }

  handleCheckboxChange = (index, e) => {
    const checked = e.target.checked
    let surveys = this.state.surveys
    surveys[index]['forUpdate'] = checked
    this.setState({surveys:surveys})
  }

  attachToTask(){
    if (this.state.selectedTask==='-1'){
      toastr.error('Error', 'Please select a task')
    } else {
      this.props.dispatch(attachSurveysToTask(this.state.surveys, this.state.selectedTask))
      browserHistory.push('/administration')
    }
  }

  handleTaskSelectChange = (e) => {
    this.setState({selectedTask:e.target.value})
  }

  handleDefaultImageChange(){
    this.setState({defaultImage:!this.state.defaultImage})
  }

  render(){
    // console.log(JSON.stringify(this.state.surveys))
    const rows = this.state.questions.map((question, index) => {
      const answers = question.answers.map((answer, index) => {
        return (
          <p key={index}>{"- "+answer.text}</p>
        )
      })
      return (
        <tr key={index}>
          <td className="tac">{ index+1 }</td>
          <td className="tac">{ question.text }</td>
          <td className="tac">
            { answers }
          </td>
          <td className="tac">
            <DropdownButton dropup title="Edit" id={index} bsStyle="primary" bsSize="xsmall">
              <MenuItem eventKey="1" bsStyle="warning" onClick={this.setFormForUpdate.bind(this, index)}>
                Update
              </MenuItem>
              <MenuItem eventKey="2" bsStyle="danger" onClick={this.removeQuestion.bind(this,index)}>Delete</MenuItem>
              {
                index === 0?
                  null
                  :
                  <MenuItem eventKey="2" bsStyle="danger" onClick={this.placeUp.bind(this, index)}>Go Up</MenuItem>
              }
              {
                index === this.state.questions.length-1?
                  null
                  :                  
                  <MenuItem eventKey="2" bsStyle="danger" onClick={this.placeDown.bind(this, index)}>Go Down</MenuItem>
              }
            </DropdownButton>
          </td>
        </tr>
      )
    })

    const answerSize = this.state.form.answers.length

    const answers = this.state.form.answers.map((answer, index) => {
      return(
        <FormGroup bsSize="small" key={index}>
          <Col sm={9}>
            <FormControl 
              type="text" 
              placeholder={"Answer " + String(index+1)} 
              onChange={this.handleAnswerChange.bind(this, index)} 
              value={answer.text}
            />
          </Col>
          {
            index > 1 || answerSize  > 2? 
              <Col sm={1} className="micro-top-padding">
                <Button bsStyle="danger" bsSize="xsmall" onClick={this.removeAnswer.bind(this, index)}>
                  <Glyphicon glyph="glyphicon glyphicon-remove" />
                </Button>
              </Col>
              :
              null
          }
          {
            index === answerSize - 1?
              <Col sm={1} className="micro-top-padding">
                <Button bsStyle="primary" bsSize="xsmall" onClick={this.addAnswer.bind(this)}>
                  <Glyphicon glyph="glyphicon glyphicon-plus" />
                </Button>
              </Col>
              :
              null
          }
        </FormGroup>
      )
    })

    const multipleAnswerSize = this.state.multipleAnswers.length

    const multipleAnswers = this.state.multipleAnswers.map((answer, index) => {
      return(
        <FormGroup bsSize="small" key={index}>
          <Col sm={9}>
            <FormControl 
              type="text" 
              placeholder={"Answer " + String(index+1)} 
              onChange={this.multipleHandleAnswerChange.bind(this, index)} 
              value={answer.text}
            />
          </Col>
          {
            index > 1 || multipleAnswerSize  > 2? 
              <Col sm={1} className="micro-top-padding">
                <Button bsStyle="danger" bsSize="xsmall" onClick={this.multipleRemoveAnswer.bind(this, index)}>
                  <Glyphicon glyph="glyphicon glyphicon-remove" />
                </Button>
              </Col>
              :
              null
          }
          {
            index === multipleAnswerSize - 1?
              <Col sm={1} className="micro-top-padding">
                <Button bsStyle="primary" bsSize="xsmall" onClick={this.multipleAddAnswer.bind(this)}>
                  <Glyphicon glyph="glyphicon glyphicon-plus" />
                </Button>
              </Col>
              :
              null
          }
        </FormGroup>
      )
    })

    return (
      <div>
        <Row className="micro-top-padding">
          <Col md={12}>
            <Tabs activeKey={ this.state.selected } onSelect={this.handleSelect.bind(this)} id="survey-details">
              <Tab eventKey={1} title="General" disabled>
                <div className="micro-top-padding">
                  <Form onSubmit={this.createSurvey.bind(this)}>
                      <Col md={12}>
                        <div className="pull-right">
                          <Button 
                            disabled={this.state.uploadingImage} bsStyle="primary" bsSize="xsmall" type="submit">
                            Next{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-arrow-right" />
                          </Button>
                        </div>
                      </Col>
                      <Col md={6}>
                        <FormGroup bsSize="small">
                          <ControlLabel>Name</ControlLabel>
                          <FormControl
                            bsSize="small"
                            type="text"
                            placeholder="Enter Name"
                            name="name"
                            required
                            onChange={this.handleSurveyInputChange.bind(this)}
                          />
                        </FormGroup>
                        <FormGroup bsSize="small">
                          <ControlLabel>Description</ControlLabel>
                          <FormControl
                            componentClass="textarea"
                            placeholder="Enter Description"
                            name="description"
                            required
                            onChange={this.handleSurveyInputChange.bind(this)}
                          />
                        </FormGroup>
                        <Checkbox checked={this.state.defaultImage} onChange={this.handleDefaultImageChange.bind(this)}>
                          Use Default Image
                        </Checkbox>
                        { 
                          this.state.defaultImage?
                            null
                            :
                            <FormGroup bsSize="small">
                              <ControlLabel>Image</ControlLabel>
                              <FormControl
                                type="file"
                                label="Image"
                                name="image_url"
                                required
                                onChange={this.handleSurveyInputChange.bind(this)}
                              />
                            </FormGroup>
                        }
                    </Col>
                  </Form>
                </div>
              </Tab>
              <Tab eventKey={2} title="Type" disabled>
                <div className="micro-top-padding">
                  <Row className="padding-left-4">
                    <Col sm={12}>
                      <Button 
                        bsStyle="primary" 
                        className="pull-right" 
                        bsSize="xsmall" onClick={() => this.handleSelect(3)}
                      >
                        Next{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-arrow-right" />
                      </Button>
                    </Col>
                    <Col md={6}>
                      <h5>
                        Please choose the type of survey you want.
                      </h5>
                      <Form>
                        <FormGroup bsSize="small">
                          <Radio 
                            name="questionType" 
                            onChange={this.handleInputChange.bind(this)}
                            value="yesno"
                            checked={this.state.questionType==="yesno"?true:false}
                          >
                            Yes / No Question and Answers
                          </Radio>
                          <Radio 
                            name="questionType" 
                            onChange={this.handleInputChange.bind(this)}
                            value="3-rating"
                            checked={this.state.questionType==="3-rating"?true:false}
                          >
                            3-Rating Scale Survey
                          </Radio>
                          <Radio 
                            name="questionType" 
                            onChange={this.handleInputChange.bind(this)}
                            value="5-rating"
                            checked={this.state.questionType==="5-rating"?true:false}
                          >
                            5-Rating Scale Survey
                          </Radio>
                          <Radio 
                            name="questionType" 
                            onChange={this.handleInputChange.bind(this)}
                            value="multiple"
                            checked={this.state.questionType==="multiple"?true:false}
                          >
                            Multile Choice Questions
                          </Radio>
                          <Radio 
                            name="questionType" 
                            onChange={this.handleInputChange.bind(this)}
                            value="custom"
                            checked={this.state.questionType==="custom"?true:false}
                          >
                            Custom Type
                          </Radio>
                        </FormGroup>
                      </Form>
                    </Col>
                  </Row>
                  {
                    this.state.questionType==="3-rating"?
                      <Row className="padding-left-4">
                        <Col md={6}>
                          <h5>
                            For this type of question, please set the 3 choices.
                          </h5>
                          <Form>
                            <FormGroup bsSize="small">
                              <FormControl
                                type="text"
                                placeholder="Choice 1"
                                maxLength="20"
                                value={this.state.threeRatingAnswers[0].text}
                                onChange={this.handleRatingOne.bind(this, 0)}
                              />
                            </FormGroup>
                            <FormGroup bsSize="small">
                              <FormControl
                                type="text"
                                placeholder="Choice 2"
                                maxLength="20"
                                value={this.state.threeRatingAnswers[1].text}
                                onChange={this.handleRatingOne.bind(this, 1)}
                              />
                            </FormGroup>
                            <FormGroup bsSize="small">
                              <FormControl
                                type="text"
                                placeholder="Choice 3"
                                maxLength="20"
                                value={this.state.threeRatingAnswers[2].text}
                                onChange={this.handleRatingOne.bind(this, 2)}
                              />
                            </FormGroup>
                          </Form>
                        </Col>
                      </Row>
                      : 
                      this.state.questionType==="5-rating"?
                        <Row className="padding-left-4">
                          <Col md={6}>
                            <h5>
                              For this type of question, please set the 3 choices.
                            </h5>
                            <Form>
                              <FormGroup bsSize="small">
                                <FormControl
                                  type="text"
                                  placeholder="Choice 1"
                                  maxLength="20"
                                  value={this.state.fiveRatingAnswers[0].text}
                                  onChange={this.handleRatingFive.bind(this, 0)}
                                />
                              </FormGroup>
                              <FormGroup bsSize="small">
                                <FormControl
                                  type="text"
                                  placeholder="Choice 2"
                                  maxLength="20"
                                  value={this.state.fiveRatingAnswers[1].text}
                                  onChange={this.handleRatingFive.bind(this, 1)}
                                />
                              </FormGroup>
                              <FormGroup bsSize="small">
                                <FormControl
                                  type="text"
                                  placeholder="Choice 3"
                                  maxLength="20"
                                  value={this.state.fiveRatingAnswers[2].text}
                                  onChange={this.handleRatingFive.bind(this, 2)}
                                />
                              </FormGroup>
                              <FormGroup bsSize="small">
                                <FormControl
                                  type="text"
                                  placeholder="Choice 4"
                                  maxLength="20"
                                  value={this.state.fiveRatingAnswers[3].text}
                                  onChange={this.handleRatingFive.bind(this, 3)}
                                />
                              </FormGroup>
                              <FormGroup bsSize="small">
                                <FormControl
                                  type="text"
                                  placeholder="Choice 5"
                                  maxLength="20"
                                  value={this.state.fiveRatingAnswers[4].text}
                                  onChange={this.handleRatingFive.bind(this, 4)}
                                />
                              </FormGroup>
                            </Form>
                          </Col>
                        </Row>
                      :
                        this.state.questionType==="multiple"?
                        <Col md={6}>
                          <Form>
                            {multipleAnswers}
                          </Form>
                        </Col>
                        :
                        null
                  }
                </div>
              </Tab>
              <Tab eventKey={3} title="Content" disabled>
                <Row className="padding-left-4 micro-top-padding">
                  <Col sm={12} className="pull-right padding-bottom-5">
                    <Button bsStyle="primary" className="pull-right" bsSize="xsmall" onClick={this.createQuestion.bind(this)}>
                      Next{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-arrow-right" />                        
                    </Button>
                  </Col>
                  <Col md={6}>
                    <Row className="padding-left-4">
                      <Col md={12} className="gray-border">
                        <Form horizontal onSubmit={this.saveQuestion.bind(this)}>
                          <FormGroup bsSize="small">
                            <Col sm={12}>
                              <ControlLabel>Question</ControlLabel>
                            </Col>
                            <Col sm={10}>
                              <FormControl 
                                name="question"
                                value={this.state.form.text}
                                onChange={this.handleQuestionChange.bind(this)}
                                componentClass="textarea" 
                                placeholder="Question" 
                                required
                              />
                            </Col>
                            <Col sm={2}>
                              <Button 
                                bsStyle={this.state.addMode?"primary":"warning"}
                                bsSize="xsmall" 
                                type="submit"
                                disabled={
                                  this.state.questions.length===10
                                  &&this.state.addMode?true:false
                                }
                              >
                                <Glyphicon 
                                  glyph={this.state.addMode?"glyphicon glyphicon-plus":"glyphicon glyphicon-pencil"} 
                                /> {this.state.addMode?" Add":" Change"}
                              </Button>
                            </Col>
                          </FormGroup>
                        <div className="overflow-hidden">
                          <FormGroup bsSize="small">
                            <Col sm={12}>
                              <ControlLabel>Answers</ControlLabel>
                            </Col>
                          </FormGroup>
                          { this.state.questionType==='custom'? answers:null }
                        </div>
                        </Form>
                      </Col>
                    </Row>
                    <Row className="padding-left-4">
                      <div className="overflow-hidden-table gray-border">
                        <Col md={12} >
                          <h5>Questions</h5>
                          <Table striped bordered condensed hover >
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Question</th>
                                <th>Answers</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>                        
                              { rows }
                            </tbody>
                          </Table>
                        </Col>
                      </div>
                    </Row>
                  </Col>
                  <Col md={6} className="gray-border">
                    <Row>
                      <Col md={12}>
                        <div className="overflow-hidden-preview">
                          { 
                            this.state.previewStatus === '1'?
                              <QR {...this.state} /> :
                            this.state.previewStatus === '2'?
                              <ButtonResponse {...this.state} /> : null
                          }
                        </div>
                      </Col>
                    </Row>                    
                  </Col>
                </Row>
              </Tab>              
            </Tabs>
          </Col>    
        </Row>
      </div>
    )
  }
}

CreateSurvey.propTypes = {
  surveys: React.PropTypes.array.isRequired,
  tasks: React.PropTypes.array.isRequired,
}

export default connect(mapStateToProps)(CreateSurvey)


// <Col sm={6} className="micro-top-padding">
//   <FormControl componentClass="select" bsSize="small" onChange={this.handlePreviewChange.bind(this)}>
//     <option value={1}>Quick Reply</option>
//     <option value={2}>Button Template</option>
//     <option value={3}>Generic Template</option>
//     <option value={4}>List Template</option>
//   </FormControl>
// </Col>

// <Tab eventKey={4} title="Assign to Bot">
//   <Row className="micro-top-padding">
//     <Col md={4}>
//       <FormControl componentClass="select" bsSize="small" onChange={this.handlePreviewChange.bind(this)}>
//         <option value={1}>Autobot</option>
//         <option value={2}>Decepticon</option>
//         <option value={3}>BotBot</option>
//         <option value={4}>Bot Tobias</option>
//       </FormControl>
//     </Col>
//   </Row>
//   <Row className="micro-top-padding">
//     <Col md={6}>
//       {surveys}
//     </Col>
//   </Row>
//   <Row className="micro-top-padding">
//     <Col md={6} className="pull-right">
//       <Button bsStyle="primary" bsSize="small" onClick={() => this.handleSelect(3)}>
//         Save{'\u00A0'}<Glyphicon glyph="glyphicon glyphicon-save" />                        
//       </Button>
//     </Col>
//   </Row>
// </Tab>