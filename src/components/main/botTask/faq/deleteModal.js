import React from 'react'
import { Button, Modal } from 'react-bootstrap'

const DeleteModal = (props) => {
  return (
     <Modal show={props.showDeleteModal} onHide={props.closeDeleteModal} className="clear-padding-right">
     <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Warning</h3>
      </div>
      <Modal.Body>
        <span>
          <p>
          You are about to delete <span className="delete-warning">{props.selectedFaq.name}</span>
          {" "}with the ID <span className="delete-warning">{props.selectedFaq.id}</span>. <br/>
          Are you sure you want to delete this FAQ link?
          </p>
        </span>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="danger" bsSize="xsmall" onClick={props.deleteFAQ.bind(this, props.selectedFaq.id)}>Delete</Button>
        <Button bsSize="xsmall" onClick={props.closeDeleteModal}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default DeleteModal