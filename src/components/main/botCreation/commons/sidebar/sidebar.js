import React, { Component } from 'react'
import { Link } from 'react-router'

class CreateBotSidebar extends Component{
  render(){
    return (
      <div>
        <a className="sidebar_switch on_switch" onClick={this.props.sidebarClick} title="Hide Menu"></a>
        <div className="sidebar">
          <div className="antiScroll">
            <div className="antiscroll-inner">
              <div className="antiscroll-content">
                <div className="sidebar_inner">
                  <div className="timer tac">
                    <span className="timer-time">{this.props.date.toLocaleTimeString()}</span><br/>
                    <span className="timer-date">
                      {this.props.date.toLocaleString("en-US", {month:"long"}) +" "+ 
                        this.props.date.toLocaleString("en-US", {day:"2-digit"}) + ", "+
                        this.props.date.toLocaleString("en-US", {year:"numeric"})}
                    </span>
                  </div>
                <div id="side_accordion" className="accordion">
                  <div className="accordion-group">
                    <div className="accordion-heading active">
                      <Link to="/bot/create" className="accordion-toggle background-gray">
                        <i className="fa fa-cogs icon-blue"></i>&nbsp;&nbsp;&nbsp;Bot Creation
                      </Link>
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading active">
                      <Link to="/task/edit-jobs" className="accordion-toggle">
                        <i className="fa fa-tasks icon-blue"></i>&nbsp;&nbsp;&nbsp;Manage Bot Task
                      </Link>
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      <Link to="/administration/main" className="accordion-toggle">
                        <i className="fa fa-window-restore icon-blue"></i>&nbsp;&nbsp;&nbsp;Bot Administration
                      </Link>
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      <Link to="/business-processes/order-management/" className="accordion-toggle">
                        <i className="fa fa-bar-chart icon-blue"></i>&nbsp;&nbsp;&nbsp;Business Process
                      </Link>
                    </div>
                  </div>
                  <div className="push"></div>
                </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
     </div>
    )
  }
}

export default CreateBotSidebar