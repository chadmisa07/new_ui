import React from 'react'

const CreateBotForm = (props) => {
  return (
    <div className="span12">
      <div className="well">
        <form className="form-horizontal">
          <fieldset>
            <div className="control-group">
              <label className="control-label"><b>Platform</b></label>
              <div className="controls">
                <select>
                  <option>Facebook</option>
                </select>
              </div>
            </div>
            <div className="control-group">
              <label className="control-label"><b>Enter Name</b></label>
              <div className={props.nameError + " controls"}>
                <input type="text" name="botName" className="span5" placeholder="Enter Name" 
                  value={props.name} onChange={props.handleInputChange}/>
              </div>
            </div>
            <div className="control-group">
              <label className="control-label "><b>Enter Description</b></label>
              <div className={props.descriptionError + " controls"}>
                <textarea value={props.description} type="text" name="botDescription" className="span5 form-control" 
                  placeholder="Enter Description" onChange={props.handleInputChange}>
                </textarea>
              </div>
            </div>            
            <div className="control-group">
              <label className="control-label width-100"></label>
              <div className="controls">
                <span className="span5">
                  <button type="button" className="btn btn-primary btn-small pull-right" onClick={props.createBot}>Create Bot</button>
                </span>
              </div>
            </div>
          </fieldset>
        </form> 
      </div>  
    </div>
  )
}

CreateBotForm.propTypes = {
  handleInputChange: React.PropTypes.func.isRequired,
  createBot : React.PropTypes.func.isRequired
}

export default CreateBotForm

