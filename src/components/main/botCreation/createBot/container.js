import React from 'react'
import * as Common from '../index'
import * as CreateBot from './index'
import { connect } from 'react-redux'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'
import * as Request from '../requests/request'
import { toastr } from 'react-redux-toastr'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
  }
}

class CreateBotContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      platform:32,
      name: "",
      description: "",
      created_by: props.user.id,
      updated_by: props.user.id,
      showAdditionalInfo: true,
      sidebar:true,
      date: new Date(),
    }
  }

  componentWillMount(){
    this.props.dispatch({type:"EDIT_MODE", payload:false})
    this.props.dispatch({type:"RESET_TASK_EDITOR"})
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  createBot(){
   if(!this.state.name){
      if(!this.state.name.replace(/\s/g, '').length){
        this.setState({nameError: "f_error"})
        toastr.error("Error","Please enter a bot name.")
      }else{
        this.setState({nameError: null})
      }
    }else if(!this.state.description){
      if(!this.state.description.replace(/\s/g, '').length){
        this.setState({descriptionError: "f_error"})
        toastr.error("Error","Please enter a bot description.")
      }else{
        this.setState({descriptionError: null})
      }
    }else{
      this.props.dispatch(Request.createBot(this.state))
    }
  }

  handleInputChange = (e) => {
    if(e.target.name === "botName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({nameError: "f_error"})
      }else{
        this.setState({nameError: null})
      }
      this.setState({ name: e.target.value })
    }else{
       if(!e.target.value.replace(/\s/g, '').length){
        this.setState({descriptionError: "f_error"})
      }else{
        this.setState({descriptionError: null})
      }
      this.setState({ description: e.target.value })
    }
  }

  handlePlatformChange = (event) => {
    this.setState({ platform: event.target.value })
  }

  toggleInfo(){
    this.setState({ showAdditionalInfo: !this.state.showAdditionalInfo })
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>
        <div id="contentwrapper">
          <div className="main_content">
            <CreateBot.Nav page={"Create Bot"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Create New Bot</h3>
                <div className="row-fluid">
                  <CreateBot.AdditionalInfo toggleInfo={this.toggleInfo.bind(this)} {...this.state}/>
                </div>
                <div className="row-fluid">
                  <CreateBot.CreateBotForm {...this.state}
                    createBot={this.createBot.bind(this)}
                    handleInputChange={this.handleInputChange.bind(this)}
                    />
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.CreateBotSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
      </div>
    )
  }
}

export default connect(mapStateToProps)(CreateBotContainer)


