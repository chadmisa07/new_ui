import React from 'react'
import * as Common from '../index'
import BotList from './botList'
import * as CreateBot from './index'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    creating: false,
  }
}

class BotListContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      showAdditionalInfo: true,
      sidebar: true,
      date: new Date(),
    }
  }
  
  componentWillMount(){
    if(this.props.bot.success !== true){
      browserHistory.push('/bot/create')
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  toggleInfo(){
    this.setState({ showAdditionalInfo: !this.state.showAdditionalInfo })
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  goToTaskEditor(){
    browserHistory.push("/order/operation")
  }

  render() {
    return (
      
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small}/>
        <div id="contentwrapper">
          <div className="main_content">
            <CreateBot.Nav page={"Task Template List"} />
            <div className="row-fluid">
              <div className="span12">
                <h3 className="heading">Task Template List</h3>
                <div className="row-fluid">
                  <CreateBot.AdditionalInfo toggleInfo={this.toggleInfo.bind(this)} {...this.state}/>
                </div>
                <div className="row-fluid">
                  <BotList goToTaskEditor={this.goToTaskEditor.bind(this)}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.CreateBotSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
      </div>
    )
  }
}

export default connect(mapStateToProps)(BotListContainer)