import React from 'react'

class AdditionalInfo extends React.Component{
  render(){
    return (
      <div className="span12 margin-top-5">
        <div className="well">
          <div>
          {
            this.props.showAdditionalInfo?
            <div>
              This is where you will <span className="orange-text">create a new bot</span>.<br/><br/>
              The Steps involved are:
              <ol>
                <li>Name and describe your bot.</li>
                <li>Select what the Bot does to give it its "task". (ie: Take Orders, Do Survey, etc.)</li>
                <li>Edit the menu texts, flow and responses to fit to your needs.</li>
              </ol>
              <p>Then you're ready to test and launchi it!</p>
            <span>Let's get started!</span>
            <span className="pull-right">
              <label htmlFor="checkbox">
                <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position" id="checkbox"/> Hide Additional Info
              </label>
            </span>
            </div>: 
            <span className="pull-right">
              <label htmlFor="checkbox">
                <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position" id="checkbox"/> Show Additional Info
              </label>
            </span>
          }
            
          </div>
          <div>
          </div>
        </div>
      </div>
    )
  }
}

export default AdditionalInfo