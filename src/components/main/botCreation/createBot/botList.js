import React from 'react'
import { Table } from 'react-bootstrap'
import { bdivserHistory, Link } from 'react-router'

class BotList extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      showModal: false,
      sidebar: true
    }
  }

  openModal(){
    this.setState({showModal: true})
  }

  closeModal(){
    this.setState({showModal: false})
  }

  choose(){
    this.setState({showModal: false})
    bdivserHistory.push('/order/operation')
  }

  render(){
    return (
      <div className="well">
          <div className="row-fluid">
            <div className="create-form-header"> 
              <span className="light-blue-text">Let's assign a task to this bot, choose from predefined selection and edit it</span>.
            </div>
          </div>
          <div className="row-fluid">
            <div className="create-form-note">
              Note: We created predefined task templates for you to use and save as your own. 
            </div>
          </div>
          <div className="row-fluid">
              <div className="margin-left-20">You can add more to the collection of tasks below using the Edit Bot Jobs menu.  
                <a href="#"> Learn more.</a><br/><br/></div>
          </div>
          <div>
            <div className="predefined-template-text"><span className="caret margin-top-7"></span>
              &nbsp;Predifined Templates
            </div>
          </div>
          <div>
            <Table hover className="predefined-template-table">
              <tbody>
                <tr>
                  <th className="text-center table-header"><u>Task Name</u></th>
                  <th className="text-center table-header"><u>Description</u></th>
                  <th className="text-center table-header"><u>Business Process</u></th>
                  <th className="text-center table-header"></th>
                </tr>
                <tr>
                  <td className="text-center">
                    <a data-toggle="modal" data-backdrop="static" href="#selecteTemplateModal">Order  Taker 01</a>
                  </td>
                  <td className="text-center">Show product menu, takes order.</td>
                  <td className="text-center">Order Mgt</td>
                  <td className="text-center"><Link to="/order/operation">Select</Link></td>
                </tr>
                <tr>
                  <td className="text-center"><a>Ticketing Office</a></td>
                  <td className="text-center">Takes Ticket Reservations Order</td>
                  <td className="text-center">Order Mgt</td>
                  <td className="text-center"><a href="#">Select</a></td>
                </tr>
                <tr>
                  <td className="text-center"><a>FAQ Handler</a></td>
                  <td className="text-center">Info conceirge & FAQ server</td>
                  <td className="text-center">FAQ Mgt</td>
                  <td className="text-center"><a href="#">Select</a></td>
                </tr>
                <tr>
                  <td className="text-center"><a>Survey Taker 01</a></td>
                  <td className="text-center">Ask multiple choice Q&A for survey</td>
                  <td className="text-center">Survey Mgt</td>
                  <td className="text-center"><a href="#">Select</a></td>
                </tr>
              </tbody>
            </Table>
          </div>
          <div></div>
          <div></div><br/>
          <div className="row-fluid">
            <div className="user-created-collection-text">
              <span className="caret margin-top-7"></span>&nbsp;User Created Collection
            </div>
          </div>
          <div>
            <Table hover className="user-created-collection-table">
              <tbody>
                <tr>
                  <th className="text-center table-header"><u>Task Name</u></th>
                  <th className="text-center table-header"><u>Description</u></th>
                  <th className="text-center table-header"><u>Business Process</u></th>
                  <th className="text-center table-header"></th>
                </tr>
                <tr>
                  <td>- none -</td>
                  <td className="text-center"></td>
                  <td className="text-center"></td>
                  <td className="text-center"></td>
                </tr>
              </tbody>
            </Table>
          </div>

        <div className="modal hide fade" id="selecteTemplateModal">
          <div className="modal-header modal-header-new">
            <button className="close" data-dismiss="modal">×</button>
            <h3>View Task</h3>
          </div>
          <div className="modal-body">
            <div className="row-fluid">
              <div className="span12">
                <form className="form-horizontal">
                  <fieldset>
                    <div className="control-group formSep">
                      <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                        <tbody>
                          <tr className="rowlink">
                            <td><b>Task ID:</b></td>
                            <td><b>s101</b></td>
                          </tr>
                          <tr className="rowlink">
                            <td><b>Task Name:</b></td>
                            <td><b>Order Taker 001</b></td>
                          </tr>
                          <tr className="rowlink">
                            <td><b>Description:</b></td>
                            <td><b>For presenting users with products and taking orders to deliver or have it picked up.</b></td>
                          </tr>
                        </tbody>
                      </table><br/>
                      <div className="row-fluid">
                        <p><b>Task Flow</b></p>
                        <ol>
                          <li>Displays operation menu items to user: Choose Product, View Cart, Checkout.</li>
                          <li>User selects a product to view and order.</li>
                          <li>When the user choosed to Order (or Buy) the bot gets the quantity.</li>
                          <li>Product is added to cart and user can order again or proceed to check out.</li>
                          <li>User can proceed to checkout items in cart.</li>
                          <li>Transaction details is displayed and finished.</li>
                            Optional: User can provide an Email and transaction record is sent by email.
                          <li>When the product is shipped, auto notification is sent to the user.</li>
                          <li>When delivery or pickup has been confirmed, a "Thank you" note is sent to the user.</li>
                        </ol><br/>
                        <p>Sample screen flow, click <a>here</a>.</p>
                        <span className="span3">
                          <button onClick={this.props.goToTaskEditor} type="button" data-dismiss="modal" className="btn btn-primary btn-small">
                            Choose This
                          </button>
                        </span>
                        <span className="span3">
                          <button type="button" data-dismiss="modal" 
                            className="btn btn-danger btn-small">Close Window
                          </button>
                        </span>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default BotList

