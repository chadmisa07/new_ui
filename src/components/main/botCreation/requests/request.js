import axios from 'axios'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as api from '../../../../utils/api'

export function createBot(data) {
  return function(dispatch) {
    console.log(JSON.stringify(data))
    dispatch({type: "CREATE_BOT_REQUEST" })
    axios.post(api.URL+"api/bot/", data)
      .then(function (response) {
        dispatch({type: "CREATE_BOT_REQUEST_FULFILLED", payload: response.data})
        dispatch({type: "ASSIGN_BOT_FOR_TASK", payload: response.data})
        toastr.success('Success', 'Bot was created')
        browserHistory.push('/bot/create/list')
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
        dispatch({type: "CREATE_BOT_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getBot(page) {
  return function(dispatch) {
  dispatch({type: "GET_BOT_REQUEST" })
  axios.get(api.URL+"api/bot/?page="+page)
    .then(function (response) {
      dispatch({type: "GET_BOT_REQUEST_FULFILLED", payload: response.data})
      // if(response.data.count === 0){
      //   toastr.error('Error', 'Please create a bot')
      // }
    })
    .catch(function (error) {
      toastr.error('Error', 'Failed to fetch bots')
      dispatch({type: "GET_BOT_REQUEST_REJECTED", payload: error})
    })
  }  
}

export function getJobFlow(id) {
  return function(dispatch) {
    dispatch({type: "GET_JOB_FLOW_REQUEST" })
    axios.get(api.URL+"api/job-flow/?page="+id)
      .then(function (response) {
        dispatch({type: "GET_JOB_FLOW_REQUEST_FULFILLED", payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Failed to fetch tasks')
        dispatch({type: "GET_JOB_FLOW_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function deleteBot(id) {
  return function(dispatch) {
    dispatch({type: "DELETE_BOT_REQUEST" })
    axios.delete(api.URL+"api/bot/"+id+"/")
      .then(function (response) {
        dispatch({type: "DELETE_BOT_REQUEST_FULFILLED", payload: response.data})
        toastr.success('Success', 'Bots successfully deleted')
        dispatch(getBot(1))
      })
      .catch(function (error) {
        toastr.error('Error', 'Failed to delete bot')
        dispatch({type: "DELETE_BOT_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getTask(taskId, userId){
let persistentMenuId
let text_response = []
let button_response = []
let menu_interface = []
let quick_replies = []
let location_interface = []
let greeting_text = []
let persistent_menu = []
let ask_delivery
let ask_pickup
let ask_email
let name
let taskName
let taskDescription
let button_response_message = {}

  return function(dispatch){
    dispatch({type:"GET_TASK_REQUEST"})
    axios.get(api.URL+"api/bot/?task="+taskId)
    .then(function(response){
      // console.log(JSON.stringify(response.data))
      taskId = response.data.results[0].task.id
      ask_email = response.data.results[0].task.ask_email
      ask_pickup = response.data.results[0].task.ask_pickup
      ask_delivery = response.data.results[0].task.ask_delivery
      taskName = response.data.results[0].task.name
      taskDescription = response.data.results[0].task.description
      name = response.data.results[0].name

      if(response.data.results[0].task.menu_interface.length){
        let ctr = response.data.results[0].task.menu_interface.length
        response.data.results[0].task.menu_interface.forEach(function(value, index) {
            axios.get(api.URL+"api/menu-interface/"+value+"/")
            .then(function(responseMenuInterface){
              menu_interface.push(responseMenuInterface.data)
              menu_interface[menu_interface.length - 1].productId = responseMenuInterface.data.product

              axios.get(api.URL+"api/product-item/"+responseMenuInterface.data.product+"/")
              .then(function (responseProductItem){
                let interfaces = menu_interface.find(x=>x.productId === responseMenuInterface.data.product)
                menu_interface[menu_interface.indexOf(interfaces)].product = responseProductItem.data
                ctr--
                if(ctr === 0 ){
                  dispatch({type:"SET_MENU_INTERFACE_STATUS", payload: true})
                }
              })
              .catch(function (error){
                dispatch({type:"GET_TASK_PRODUCT_REQUEST_REJECTED", payload: error})
              })
            })
            .catch(function (error){
              toastr.error('Error', 'Failed to fetch menu interface')
              dispatch({type:"SET_MENU_INTERFACE_STATUS", payload: false})
              dispatch({type:"GET_TASK_REQUEST_REJECTED"})
            })
        })
      }else{
        dispatch({type:"SET_MENU_INTERFACE_STATUS", payload: true})
      }

      if(response.data.results[0].task.text_response.length){
      let ctr = response.data.results[0].task.text_response.length
        response.data.results[0].task.text_response.forEach(function(value, index) {
            axios.get(api.URL+"api/text-response/"+value+"/")
            .then(function(response){
              text_response.push(response.data)
              text_response.sort()
              ctr--
              if(ctr === 0){
                dispatch({type:"SET_TEXT_RESPONSE_STATUS", payload: true})
              }
            })
            .catch(function (error){
              toastr.error('Error', 'Failed to fetch text response')
              dispatch({type:"SET_TEXT_RESPONSE_STATUS", payload: false})
              dispatch({type:"GET_TASK_REQUEST_REJECTED"})
            })
        })
      }else{
        dispatch({type:"SET_TEXT_RESPONSE_STATUS", payload: true})
      }

      if(response.data.results[0].task.button_responses.length){
        let ctr = response.data.results[0].task.button_responses.length
        response.data.results[0].task.button_responses.forEach(function(value, index) {
            axios.get(api.URL+"api/button-response/"+value+"/")
            .then(function(response){
               button_response_message.message = response.data.text
               button_response_message.id = response.data.id
               response.data.buttons.map(button => {
                  return (button_response.push(button))
                })
               ctr--
               if(ctr === 0){
                 dispatch({type:"SET_BUTTON_RESPONSE_STATUS", payload: true})
               }
            })
            .catch(function (error){
              toastr.error('Error', 'Failed to fetch button response')
              dispatch({type:"SET_BUTTON_RESPONSE_STATUS", payload: false})
              dispatch({type:"GET_TASK_REQUEST_REJECTED"})
            })
        })
      }else{
        dispatch({type:"SET_BUTTON_RESPONSE_STATUS", payload: true})
      }

      if(response.data.results[0].task.quick_replies.length){
        let ctr = response.data.results[0].task.quick_replies.length
         response.data.results[0].task.quick_replies.forEach(function(value, index) {
            axios.get(api.URL+"api/quick-reply/"+value+"/")
            .then(function(response){
              quick_replies.push(response.data)
              ctr--
              if(ctr === 0){
                dispatch({type:"SET_QUICK_REPLY_STATUS", payload: true})
              }
            })
            .catch(function (error){
              toastr.error('Error', 'Failed to fetch quick replies')
              dispatch({type:"SET_QUICK_REPLY_STATUS", payload: false})
              dispatch({type:"GET_TASK_REQUEST_REJECTED"})
            })
        })
      }else{
        dispatch({type:"SET_QUICK_REPLY_STATUS", payload: true})
      }

      if(response.data.results[0].task.location_interface.length){
        let ctr = response.data.results[0].task.location_interface.length
         response.data.results[0].task.location_interface.forEach(function(value, index) {
            axios.get(api.URL+"api/location-interface/"+value+"/")
            .then(function(responseLocationInterface){
              location_interface.push(responseLocationInterface.data)
              location_interface[location_interface.length - 1].locationId = responseLocationInterface.data.location

              axios.get(api.URL+"api/pickup-location/"+responseLocationInterface.data.location+"/")
              .then(function (responseLocation){
                let interfaces = location_interface.find(x=>x.locationId === responseLocationInterface.data.location)
                location_interface[location_interface.indexOf(interfaces)].location = responseLocation.data
                ctr--
                if(ctr === 0){
                  dispatch({type:"SET_LOCATION_INTERFACE_STATUS", payload: true})
                }
              })
              .catch(function (error){
                dispatch({type:"GET_TASK_LOCATION_REQUEST_REJECTED", payload: error})
              })
            })
            .catch(function (error){
              toastr.error('Error', 'Failed to fetch location interface')
              dispatch({type:"SET_LOCATION_INTERFACE_STATUS", payload: false})
              dispatch({type:"GET_TASK_REQUEST_REJECTED"})
            })
        })
      }else{
        dispatch({type:"SET_LOCATION_INTERFACE_STATUS", payload: true})
      }

      axios.get(api.URL+"api/greeting-text/?job_flow="+taskId)
      .then(function(response){
        response.data.results.map(text =>{
          return(
            greeting_text.push(text)
          )
        })
        dispatch({type:"SET_GREETING_TEXT_STATUS", payload: true})
      })
      .catch(function(error){
        toastr.error('Error', 'Failed to fetch greeting text')
        dispatch({type:"SET_GREETING_TEXT_STATUS", payload: false})
        dispatch({type:"GET_TASK_REQUEST_REJECTED"})
      })

      axios.get(api.URL+"api/persistent-menu/?job_flow="+taskId)
      .then(function(response){
        persistentMenuId = response.data.results[0].id
        response.data.results[0].buttons.map(button => {
          return (
            persistent_menu.push(button)
          )
        })

        let data = {
            persistentMenuId: persistentMenuId,
            id: taskId,
            ask_email: ask_email,
            ask_pickup: ask_pickup,
            ask_delivery: ask_delivery,
            taskName: taskName,
            taskDescription: taskDescription,
            menu_interface: menu_interface, 
            location_interface: location_interface,
            text_response: text_response,
            quick_replies: quick_replies,
            button_response: button_response,
            button_response_message: button_response_message,
            name: name,
            greeting_text: greeting_text,
            persistent_menu: persistent_menu
        }
        
        dispatch({type:"EDIT_MODE", payload:true})
        dispatch({type:"SET_PERSISTENT_MENU_STATUS", payload: true})
        dispatch({type: "GET_TASK_REQUEST_FULFILLED", payload: response.data, data: data})
      })
      .catch(function (error){
        toastr.error('Error', 'Failed to fetch greeting text')
        dispatch({type:"GET_TASK_PERSISTENT_MENU_REQUEST_REJECTED", payload: error})
        dispatch({type:"SET_PERSISTENT_MENU_STATUS", payload: false})
        dispatch({type:"GET_TASK_REQUEST_REJECTED"})
        browserHistory.push("/bot/edit-jobs")
      })
    })
    .catch(function (error){
      toastr.error('Error', error)
      dispatch({type:"GET_TASK_REQUEST_REJECTED", payload: error})
      browserHistory.push("/bot/edit-jobs")
    })
  }
}

export function updateBot(id, task, data) {
  return function(dispatch) {
    dispatch({type: "UPDATE_BOT_REQUEST" })
    axios.put(api.URL+"api/bot/"+id+"/" , data)
      .then(function (response) {
        dispatch({type: "UPDATE_BOT_REQUEST_FULFILLED", payload: response.data})
        toastr.success('Success', 'Bot successfully updated') 
        if(task !== 0){
          let taskData = {bot:id,task:task}
          axios.post(api.URL+"survey/update-bot-task/", taskData)
          .then(function (response){
            toastr.success('Success', 'Successfully updated bot task') 
            dispatch(getBot(1))
          })
          .catch(function (error){
            toastr.error('Error', 'Failed to update bot task') 
          })
        }else{
          dispatch(getBot(1))
        }
      })
      .catch(function (error) {
        toastr.error('Error', 'Failed to update bot')
        dispatch({type: "UPDATE_BOT_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function deleteTask(id){
  return function(dispatch){
    dispatch({type:"DELETE_TASK_REQUEST"})
    axios.delete(api.URL+"api/job-flow/"+id+"/")
    .then(function (response){
      dispatch({type:"DELETE_TASK_REQUEST_FULFILLED"})
      toastr.success('Success','Task successfully deleted')
      dispatch(getJobFlow(1))
    })
    .catch(function (error){
      dispatch({type:"DELETE_TASK_REQUEST_REJECTED"})
      toastr.error('Error', 'Failed to delete task')
    })
  }
}


export function createSurvey(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/survey/", data)
      .then(function (response) {
        dispatch({type: "SURVEY_CREATED", payload: response.data})
        toastr.success('Success', 'Survey was created')
        dispatch(getSurveys())
        dispatch(getTasks())
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
      })
  }  
}

export function createQuestion(questions) {
  return function(dispatch) {
    for (let question of questions){
      let answers = question.answers
      axios.post(api.URL+"api/question/", question)
        .then(createAnswers.bind(null, answers))
        .catch(function (error) {
          toastr.error('Error', 'Unable to create a question')
        })
    }
  }  
}

function createAnswers(answers, response) {
  for (let answer of answers){
    answer["question"] = response.data.id
  }
  axios.post(api.URL+"api/answer/", answers)
    .then(function (response) {
      toastr.success('Success', 'Successfully created the question')
    })
    .catch(function (error) {
      toastr.error('Error', 'Unable to create answers')
    })
}

export function updateSurveyType(survey, type) {
  return function(dispatch) {
    axios.patch(api.URL+"api/survey/"+survey+"/", {"survey_type": type})
      .then(function (response) {
        toastr.success('Success', 'Successfully updated the survey')
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to update the survey type')
      })
  }
}

export function createJobFlow(data) {
  return function(dispatch) {
    axios.post(api.URL+"api/job-flow/", data)
      .then(function (response) {
        toastr.success('Success', 'Successfully created task')
        dispatch({type: "TASK_CREATED", payload: response.data})
        dispatch(getSurveys())
        dispatch(getTasks())
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to create task')
      })
  }
}

export function getSurveys(){
  return function(dispatch) {
    axios.get(api.URL+"api/survey/")
      .then(function (response) {
        dispatch({type: "FETCHED_SURVEYS", payload: response.data.results})
      })
      .catch(function (error) {
      })
  }
}

export function getTasks(){
  return function(dispatch) {
    axios.get(api.URL+"api/job-flow/")
      .then(function (response) {
        dispatch({type: "FETCHED_TASKS", payload: response.data.results})
      })
      .catch(function (error) {
      })
  }
}

export function attachSurveysToTask(surveys, task){
  return function(dispatch) {
    for (let survey of surveys){
      try{
        if (survey.forUpdate){
          axios.patch(api.URL+"api/survey/"+String(survey.id)+"/", {"task_flow":task})
            .then(function (response) {
              toastr.success('Success','Successfully attached survey to task')
            })
            .catch(function (error) {
              toastr.error('Error','Unable to attach survey to task')
            })
        } else {
          axios.patch(api.URL+"api/survey/"+String(survey.id)+"/", {"task_flow":null})
            .then(function (response) {
              toastr.success('Success','Successfully attached survey to task')
            })
            .catch(function (error) {
              toastr.error('Error','Unable to attach survey to task')
            })
        }
      } catch(e) {
        axios.patch(api.URL+"api/survey/"+String(survey.id)+"/", {"task_flow":null})
          .then(function (response) {
            toastr.success('Success','Successfully attached survey to task')
          })
          .catch(function (error) {
            toastr.error('Error','Unable to attach survey to task')
          })
      }
    }
    dispatch({type:"CLEAR_SURVEY"})
  }
}