import React from 'react'

const DeleteModal = (props) => {
  return (
     <div className="modal hide fade" id="deleteBotModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Delete Bot</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="control-group">
            <p>You are about to delete <span className="red-text">{props.name}</span>, are you sure you want to delete this bot?</p><br/>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="btns-action">
          <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={props.delete}>Delete</button>
          <button type="button" className="btn" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  )
}

export default DeleteModal