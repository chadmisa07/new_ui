import React from 'react'

const EditModal = (props) => {
  return (
    <div className="modal hide fade" id="editBotModal">
      <div className="modal-header modal-header-new">
        <button className="close" data-dismiss="modal">×</button>
        <h3>Update Bot</h3>
      </div>
    <div className="modal-body">
      <div className="row-fluid">
        <div className="span12">
          <fieldset>
            <div className="control-group">
              <div className="control-group">
                <label className="">Bot ID</label>
                <input type="text" id="" className="span12" placeholder="Bot ID"  disabled="true" 
                  value={props.id +"~~"+ typeof(props.taskName)}/>
              </div>
              <div className="control-group">
                <label className="">Bot Name</label>
                <input type="text" className="span12" placeholder="Bot Name" 
                  value={props.name} onChange={props.handleNameChange} />
              </div>
              <div className="control-group">
                <label className="">Bot Description</label>
                <input type="text" className="span12" placeholder="Bot Description" 
                  value={props.description} onChange={props.handleDescriptionChange}/>
              </div>
              <div className="control-group">
                <label className="">Choose Task</label>
                <select value={props.taskId} className="span12" onChange={props.taskHandlerChange}>
                  {props.tasks}
                </select>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
    <div className="modal-footer">
      <div className="btns-action">
        <button type="button" data-dismiss="modal" className="btn btn-primary btn-small" 
          onClick={props.update}>Update</button>
        <button className="btn btn-danger btn-small" data-dismiss="modal"
          onClick={props.closeEditModal}>Close</button>
      </div>
    </div>
  </div>
  )
}

EditModal.propTypes = {
  id: React.PropTypes.number,
  name: React.PropTypes.string,
  description: React.PropTypes.string,
  handleNameChange: React.PropTypes.func,
  handleDescriptionChange: React.PropTypes.func,
  update: React.PropTypes.func,
  closeEditModal: React.PropTypes.func
}

export default EditModal