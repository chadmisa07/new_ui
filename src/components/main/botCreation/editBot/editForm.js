import React from 'react'

const EditBotTable = (props) =>{
    return (
      <div className="span12">
        <div className="well">
        <p className="blue-text">Select which bot to edit by pressing the Edit button beside it.</p>
        <br/>
        <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
          <thead>
            <tr>
              <th className="tac">Bot ID</th>
              <th className="tac">Bot Name</th>
              <th className="tac">Bot Description</th>
              <th className="tac">Jobs Defined</th>
              <th className="tac" width="130">Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              !props.bots.length?
              <tr>
                <td colSpan="5" className="tac">No Bot found</td>
              </tr> : null
            }
            {props.botlist}
          </tbody>
        </table>
        </div>
        <div className="pagination">
          {props.pageNumbers.length!==0?
            <a href="#" onClick={props.currentPage !== 1 && props.pageNumbers.length!==0? 
              props.handleFirst : ""}>&#x27EA;</a>:""
          }
          {props.currentPage > 1 ? <a href="#" onClick={props.handlePrevious}>&#x27E8;</a> : ""}
          {props.pageNumbers}
          { props.currentPage <= props.pageNumbers.length + 2 && props.pageNumbers.length!== 1 &&
            props.pageNumbers.length!==0?<a href="#" onClick={props.handleNext}>&#x27E9;  </a>:""
          }
          {props.pageNumbers.length!==0?
            <a href="#" onClick={props.currentPage !== props.pageNumbers.length + 2 && 
              props.pageNumbers.length !== 1 && props.pageNumbers.length!==0? props.handleLast : ""}>
              &#x27EB;</a>: ""
          }
          
        </div>
      </div>
    )
}


EditBotTable.propTypes = {
  currentPage: React.PropTypes.number,
  pageNumbers: React.PropTypes.array,
  handleFirst: React.PropTypes.func,
  handlePrevious: React.PropTypes.func,
  handleNext: React.PropTypes.func,
  handleLast: React.PropTypes.func,
  botlist : React.PropTypes.array,
}

export default EditBotTable