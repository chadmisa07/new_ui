import React from 'react'

const Nav = (props) => {
	return (
		<nav>
	        <div className="breadCrumb module">
	          <ul>
	            <li><a href="index.html"><i className="icon-home"></i></a></li>
	            <li>Bot Creation</li>
	            <li>Edit Bots</li>
	          </ul>
	        </div>
     	 </nav>

	)
}

export default Nav