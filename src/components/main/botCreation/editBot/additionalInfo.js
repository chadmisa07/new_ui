import React from 'react'

class AdditionalInfo extends React.Component{
  render(){
    return (
        <div className="well margin-top-5">
        {
            this.props.showAdditionalInfo?
          <div>
            This is where you will <span className="orange-text">edit or delete bots that you created, as well as change its tasks</span>.<br/><br/>
            The Steps involved are:
            <ol>
              <li>Select which bot to edit.</li>
              <li>You can change its descriptions or change the linked tasks.</li>
              <li>Save as new bot and relaunch it!</li>
            </ol>
            <span>Let's get started!</span>
            <span className="pull-right">
              <label htmlFor="checkbox">
                <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position" id="checkbox"/> Hide Additional Info
              </label>
            </span>
          </div>:
          <span className="pull-right">
            <label htmlFor="checkbox">
              <input type="checkbox" onClick={this.props.toggleInfo} className="checkbox-position" id="checkbox"/> Show Additional Info
            </label>
          </span>
          }
        </div>
    )
  }
}

export default AdditionalInfo