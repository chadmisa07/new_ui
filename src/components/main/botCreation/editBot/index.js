import EditBotTable from './editForm'
import AdditionalInfo from './additionalInfo'
import DeleteModal from './deleteModal'
import EditModal from './editModal'
import Nav from './nav'

export{
	EditBotTable,
	AdditionalInfo,
	DeleteModal,
	EditModal,
	Nav
}