import React from 'react'
import * as Common from '../index'
import * as EditBot from './index'
import { connect } from 'react-redux'
import * as Request from '../requests/request'
import { toastr } from 'react-redux-toastr'
import logo from '../../../../includes/img/logo.png'
import logo_small from '../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bots: state.bot.bots,
    botCount: state.bot.count, 
    user: state.user,
    tasks: state.task,
  }
}

class EditBotContainer extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      showEditModal: false,
      showDeleteModal: false,
      id: 0,
      name: "",
      description: "",
      platform: null,
      taskName: "",
      currentPage: 1,
      displayNumber: 3,
      pageNumber: 0, 
      nameError: null,
      descriptionError: null,
      showAdditionalInfo: true,
      currentTask: 0,
      sidebar: true,
      date: new Date(),
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  componentWillMount(){
    this.props.dispatch(Request.getBot(1))
    this.props.dispatch(Request.getJobFlow(1))
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }


  closeEditModal(){
    this.setState({ showEditModal: false })
  }

  closeDeleteModal(){
    this.setState({ showDeleteModal: false })
  }

  openEditModal(index){
    this.setState({showEditModal: true})
    this.setState({id: this.props.bots[index].id})
    this.setState({name: this.props.bots[index].name})
    this.setState({description: this.props.bots[index].description})
    this.setState({platform: this.props.bots[index].platform})
    this.setState({taskName: this.props.bots[index].task.name})
    this.setState({taskId: this.props.bots[index].task.id})
    this.setState({currentTask: 0})
  }

  openDeleteModal(index){
    this.setState({showDeleteModal: true})
    this.setState({id: this.props.bots[index].id})
    this.setState({name: this.props.bots[index].name})
  }

  update(){
    if(!this.state.name.replace(/\s/g, '').length){
      toastr.error("Error","Please fill everything.")
    }else if(!this.state.description.replace(/\s/g, '').length) {
      toastr.error("Error","Please fill everything.")
    }else{
      let data = {
                name:this.state.name,
                description:this.state.description,
                platform:this.state.platform,
               }
      this.props.dispatch(Request.updateBot(this.state.id,this.state.currentTask,data))
      this.setState({ showEditModal: false })
    }
    
  }

  delete(){
    this.props.dispatch(Request.deleteBot(this.state.id))
    this.setState({ showDeleteModal: false })
  }

  handleNameChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({nameError:"error"})
    }else{
      this.setState({nameError:null})
    }
    this.setState({name: event.target.value})
  }

  handleDescriptionChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({descriptionError:"error"})
    }else{
      this.setState({descriptionError:null})
    }
    this.setState({description: event.target.value})
  }

  handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(Request.getBot(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(Request.getBot(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(Request.getBot(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(Request.getBot(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.botCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(Request.getBot(bots.length))
     this.setState({currentPage: bots.length})
  }

  toggleInfo(){
    this.setState({ showAdditionalInfo: !this.state.showAdditionalInfo })
  }

  taskHandlerChange = (e) => {
    this.setState({currentTask: e.target.value})
    this.setState({taskId:e.target.value})
  }

  render() {
     let tasks = this.props.tasks.tasks.map((task, index) => {
      return (
        <option key={task.id} value={task.id}>{task.name}</option>
      )
    }) 

    let bots = this.props.bots

    let botlist = bots.map((bot, index) => {
      return (
        <tr key={index}>
          <td className="tac">{bot.id}</td>
          <td className="tac">{bot.name}</td>
          <td className="tac">{bot.description}</td>
          <td className="tac">{!bot.task ? "" : bot.task.name}</td>
          <td className="tac">
            <button className="btn btn-primary btn-small" data-toggle="modal" data-backdrop="static"
              onClick={this.openEditModal.bind(this,index)} href="#editBotModal">
                Edit</button>
            <button className="btn btn-danger btn-small " data-toggle="modal" 
              data-backdrop="static" href="#deleteBotModal" onClick={this.openDeleteModal.bind(this,index)}>
                Delete</button> 
          </td>
        </tr>
      )
    })

    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.botCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""}
              onClick={this.handleTransferPage.bind(this)}>{number}
            </a>
        )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>

        <div id="contentwrapper">
          <div className="main_content">
            <EditBot.Nav />

            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Edit Bots</h3>
                <div className="row-fluid">
                  <div className="span12">
                    <form className="form-horizontal">
                      <EditBot.AdditionalInfo toggleInfo={this.toggleInfo.bind(this)} {...this.state}/>
                      <fieldset className="margin-top-20">
                        <EditBot.EditBotTable {...this.state} {...this.props}
                           botlist={botlist}
                           pageNumbers={renderPageNumbers}
                           handleNext={this.handleNext.bind(this)}
                           handleFirst={this.handleFirst.bind(this)}
                           handlePrevious={this.handlePrevious.bind(this)}
                           handleLast={this.handleLast.bind(this)}/>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <Common.CreateBotSidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
      <EditBot.EditModal {...this.state}
        closeEditModal={this.closeEditModal.bind(this)}
        update={this.update.bind(this)}
        handleNameChange={this.handleNameChange.bind(this)}
        handleDescriptionChange={this.handleDescriptionChange.bind(this)}
        nameError={this.state.nameError}
        descriptionError={this.state.descriptionError}
        tasks={tasks}
        taskHandlerChange={this.taskHandlerChange.bind(this)}/>
      <EditBot.DeleteModal {...this.state}
        openDeleteModal={this.openDeleteModal.bind(this)}
        closeDeleteModal={this.closeDeleteModal.bind(this)}
        delete={this.delete.bind(this)}/>
    </div>
    )
  }
}

export default connect(mapStateToProps)(EditBotContainer)