import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import * as api from '../../../../../utils/api'

export function getBots() {
  return function(dispatch) {
    axios.get(api.URL+"api/bot/")
      .then(function(response) {
        dispatch({type:'FETCHED_BOTS_BUSINESS_PROCESSES', payload: response.data.results})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch bots')
      })
  }
}

export function getPurchases(id) {
  return function(dispatch) {
    axios.get(api.URL+"api/user-purchase/?bot="+id)
      .then(function(response) {
        dispatch({type:'FETCHED_PURCHASES_BUSINESS_PROCESSES', payload: response.data.results})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch purchases')
      })
  }
}

export function updateStatus(purchaseId, data, currentBot) {
  return function(dispatch) {
    dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST" })
    axios.patch(api.URL+"api/user-purchase/"+purchaseId+"/", {status:data})
      .then(function (response) {
        toastr.success('Success', 'Purchase status successfully updated.')
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_FULFILLED", payload: response.data})
        dispatch(getPurchases(currentBot))
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to update purchase status')
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_REJECTED", payload: error})
      })
  }  
}