import React from 'react'

const PurchasesTable = (props) => {
  return (
    <div className="span8 well">
      <table className="table table-bordered table-overflow">
        <thead>
          <tr>
            <th className="tac">Question</th>
            <th className="tac">Answers</th>
            <th className="tac" width="100">Total</th>
            <th className="tac" width="100">Percentage</th>
          </tr>
        </thead>
        <tbody>
          {!props.survey.length ?
            <tr><td colSpan="4" className="align-center"><i>No data found</i></td></tr> : 
            null
          }
         {props.survey}
        </tbody>
      </table>
    </div>
  )
}

export default PurchasesTable


