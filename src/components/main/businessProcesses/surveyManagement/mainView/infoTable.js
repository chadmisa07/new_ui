import React from 'react'

const InfoTable = (props) => {
  return (
    <div className="well span7">
      <div className="row-fluid">
       <span className="span3 margin-top-7">Choose Survey:</span>
          <div className="span7">
            <select className="span8" onChange={props.botChange}>
              <option value="0">Select Survey</option>
              {props.options}
            </select>
          </div>
        </div>
      <div className="row-fluid">
        <span className="span12">
          <table className="table table-bordered table-overflow">
            <tbody>
              <tr>
                <td><b>Platform:</b>&nbsp; Facebook</td>
                <td><b>Date Started:</b>&nbsp; 03/29/2017</td>
              </tr>
              <tr>
                <td><b>Status:</b>&nbsp;</td>
                <td><a href="#">View Bot Details</a></td>
              </tr>
            </tbody>
          </table>
        </span>
      </div>
    </div>
)
}

export default InfoTable


