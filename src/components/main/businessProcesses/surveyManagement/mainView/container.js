import React from 'react'
import * as Common from '../../commons/index'
import { connect } from 'react-redux'
import { updateStatus } from './request'
import * as Request from '../request/request'
import InfoTable from './infoTable'
import SurveyTable from './surveyTable'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
    bots: state.businessProcesses.bots,
    purchases: state.businessProcesses.purchases,
    survey: state.survey
  }
}

class SurveyManagementContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      purchase: null,
      status: "Received",
      currentSurvey: 0,
      sidebar: true,
      date: new Date(),
    }
  }

  componentWillMount() {
    this.props.dispatch(Request.getSurvey())
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  botChange = (e) => {
    this.setState({selectedBot:e.target.value})
    this.props.dispatch(Request.getSurveyResult(e.target.value))
    this.setState({currentBot: e.target.value})
  }

  componentWillUnmount() {
    this.props.dispatch({type:'RESET_SURVEY'})
    clearInterval(this.timerID);
  }

  handleChange = (event) => {
    this.setState({status: event.target.value})
  }

  update(){
    let purchaseId = this.state.purchase.id
    let status = this.state.status
    this.props.dispatch(updateStatus(purchaseId, status,this.state.currentBot))
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {

   let options = this.props.survey.survey.map((item, index) => {
      return(
        <option key={index} value={item.id}>{item.name}</option>
      )
    })

    let survey = this.props.survey.result.map((survey, index) =>{
    let total = 0
    let answer = survey.answers.map((answer,index) =>{
      total += answer.count
        return(
            <tr key={index}>
              <td >{answer.text}</td>
            </tr>
        )
      })
    let totalAnswer = survey.answers.map((answer,index) =>{
        return(
            <tr key={index}>
              <td className="tac">{answer.count}</td>
            </tr>
        )
      })

    let totalPercentage= survey.answers.map((answer,index) =>{
        return(
            <tr key={index}>
              <td className="tac">{!answer.count?"0.00":(answer.count / total * 100).toFixed(2) + "%"}</td>
            </tr>
        )
      })
      return (
        <tr key={index}>
          <td>{survey.question}</td>
          <td>
            <table className="table table-bordered table-overflow">
              <tbody>
                {answer}
                <tr>
                  <td>Total</td>
                </tr>
              </tbody>
            </table>
          </td>
          <td>
            <table className="table table-bordered table-overflow">
              <tbody>
                {totalAnswer}
                <tr>
                  <td className="tac">{total}</td>
                </tr>
              </tbody>
            </table>
          </td>
          <td>
            <table className="table table-bordered table-overflow">
              <tbody>
                {totalPercentage}
                <tr>
                  <td className="tac">{total?"100.00":"0.00"}</td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>
        <Common.SideBar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
        <div id="contentwrapper">
          <div className="main_content">
            <Common.Nav page={"Survey Management"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Survey Management</h3>
                <div className="row-fluid">
                  <div className="margin-top-5">
                    <p>This is the main view of your processes.</p>
                  </div>
                </div>
                <div className="row-fluid">
                  <InfoTable botChange={this.botChange.bind(this)} options={options}/>
                </div>
                <div className="row-fluid">
                  <SurveyTable {...this.props} {...this.state}
                    survey={survey}/>
                </div>
                <div className="row-fluid">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(SurveyManagementContainer)