import axios from 'axios'
// import { browserHistory } from 'react-router'
// import { toastr } from 'react-redux-toastr'

export function getSurveyResult(id){
  return function(dispatch){
    if(id !== "0"){
      dispatch({type: "GET_SURVEY_RESULT_REQUEST"})
      axios.get("https://cbot-api.herokuapp.com/survey/result/?survey=" + id)
      .then(function (response){
        dispatch({type: "GET_SURVEY_RESULT_REQUEST_FULFILLED", payload: response.data.result})
      })
      .catch(function (error){
        dispatch({type: "GET_SURVEY_RESULT_REQUEST_REJECTED", payload: error})
      })
    }
  }
}

export function getSurvey(){
  return function(dispatch){
    dispatch({type: "GET_SURVEY_REQUEST"})
    axios.get("https://cbot-api.herokuapp.com/api/survey/")
    .then(function (response){
      dispatch({type: "GET_SURVEY_REQUEST_FULFILLED", payload: response.data.results})
    })
    .catch(function (error){
      dispatch({type: "GET_SURVEY_REQUEST_REJECTED", payload: error})
    })
  }
}