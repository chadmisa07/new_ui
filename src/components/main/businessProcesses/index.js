import OrderManagementContainer from './orderManagement/mainView/container'
import SurveyManagementContainer from './surveyManagement/mainView/container'
import FAQManagementContainer from './faqManagement/mainView/container'

export{
	OrderManagementContainer,
	SurveyManagementContainer,
	FAQManagementContainer
}