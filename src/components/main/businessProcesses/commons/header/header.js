import React, { Component } from 'react'
import { Link } from 'react-router'
import LoadingBar from 'react-redux-loading-bar'

class Header extends Component{
  render(){
    return (
      <header>
      <div className="navbar navbar-fixed-top">
        <LoadingBar className="loadingBar"/>
        <div className="navbar-inner">
          <div className="container-fluid">
            <Link className="brand tac">
              <img src={this.props.logo} className="visible-desktop visible-tablet" alt="NTUITIV" />
              <img src={this.props.logo_small} className="visible-phone" alt="NTUITIV" />
            </Link>
            <ul className="nav user_menu nav-items">
              <li className="hidden-phone hidden-tablet"></li>
              <li className="dropdown visible-desktop">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                <i className="fa fa-user-circle fa-lg icon-blue"></i>&nbsp;&nbsp;
                  Hi, {this.props.name}! &nbsp;<i className="fa fa-caret-down icon-blue"></i></a>
                <ul className="dropdown-menu">
                  <li>
                    <a href="https://www.facebook.com/v2.8/dialog/oauth?client_id=1038731512904671&
                      redirect_uri=https://cbot-tooling.herokuapp.com/fb/&auth_type=rerequest&scope=email,
                      public_profile,manage_pages,pages_messaging,pages_messaging_subscriptions">
                        <span className="action-btn-label">Connect with Facebook</span>
                    </a>
                  </li>
                  <li><Link to="">Dashboard</Link></li>
                  <li><Link to="">Change Password</Link></li>
                  <li><Link to="/logout">Logout</Link></li>
                </ul>
              </li>
            </ul>
            <ul className="nav nav-btns">
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
               <Link to="" activeClassName="active">
                <i className="fa fa-plus-circle icon-blue"></i>
                <span className="action-btn-label"> Dashboard</span>
               </Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <Link to="/business-processes/" activeClassName="active">
                  <i className="fa fa-pencil icon-blue"></i>
                  <span className="action-btn-label"> Main View</span>
                </Link>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <Link to="" activeClassName="active">
                  <i className="fa fa-cog icon-blue"></i>
                  <span className="action-btn-label"> Settings</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
      </header>
    )
  }
}

export default Header