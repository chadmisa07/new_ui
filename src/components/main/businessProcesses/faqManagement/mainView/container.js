import React from 'react'
import * as Common from '../../commons/index'
import { connect } from 'react-redux'
import { getFAQ } from '../request/request'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
    bots: state.businessProcesses.bots,
    purchases: state.businessProcesses.purchases,
    faqs: state.faq.list,
    faqCount: state.faq.faqCount,
  }
}

class FAQManagementContainer extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      sidebar: true,
      date: new Date(),
      currentPage: 1,
    }
  }

  componentWillMount() {
    this.props.dispatch(getFAQ(1))
  }  

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

    handleTransferPage = (event) => {
    this.setState({currentPage: Number(event.target.id)})
    this.props.dispatch(getFAQ(event.target.id))
  }

  handleNext = () =>{
    let nextNumber = Number(this.state.currentPage) + 1
    this.props.dispatch(getFAQ(nextNumber))
    this.setState({currentPage: nextNumber})
  }

  handlePrevious = () =>{
    let previousNumber = Number(this.state.currentPage) - 1
    this.props.dispatch(getFAQ(previousNumber))
    this.setState({currentPage: previousNumber})
  }

  handleFirst = () =>{
    this.props.dispatch(getFAQ(1))
    this.setState({currentPage: 1})
  }

  handleLast = () =>{
    let bots = []
    for (let i = 1; i <= Math.ceil(this.props.faqCount / 10); i++) {
      bots.push(i);
    }
    
    this.props.dispatch(getFAQ(bots.length))
    this.setState({currentPage: bots.length})
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    const pageNumbers = []
    for (let i = 1; i <= Math.ceil(this.props.faqCount / 10); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.filter(number => {
      return number < this.state.currentPage + 3 && number > this.state.currentPage -1
    }).map((number, index) => {
          return (
            <a key={number} id={number} className={number===this.state.currentPage?"active":""}
              onClick={this.handleTransferPage.bind(this)}>{number}
            </a>
        )
    })

    const faqs = this.props.faqs.map((faq, index) => {
      return (
        <tr key={index}>
          <td className="tac">{faq.id}</td>
          <td className="tac">{faq.name}</td>
          <td className="tac">{faq.description}</td>
          <td className="tac">{faq.faq_type}</td>
          <td className="tac">{faq.clicks}</td>
        </tr>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>
        <Common.SideBar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
        <div id="contentwrapper">
          <div className="main_content">
            <Common.Nav page={"FAQ Management"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">FAQ Management</h3>
                <div className="row-fluid">
                  <div className="margin-top-5">
                    <p>These are the current clicks of your FAQs</p>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span8">
                    <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                      <thead>
                        <tr>
                          <th className="tac table-header">ID</th>
                          <th className="tac table-header">Name</th>
                          <th className="tac table-header">Description</th>
                          <th className="tac table-header">Type</th>
                          <th className="tac table-header">Clicks</th>
                        </tr>
                      </thead>
                      <tbody> 
                        {!this.props.faqs.length ?
                          <tr><td colSpan="5" className="align-center"><i>No data found</i></td></tr> : 
                          null
                        }
                        {faqs}
                      </tbody>
                    </table>
                    <div className="row-fluid pagination">
                      {renderPageNumbers.length!==0?
                        <a href="#" onClick={renderPageNumbers.length!== 1 && renderPageNumbers.length!==0? 
                          this.handleFirst.bind(this) : ""}>&#x27EA;</a>:""
                      }
                      {this.state.currentPage > 1 ? <a href="#" onClick={this.handlePrevious.bind(this)}>&#x27E8;</a> : ""}
                      {renderPageNumbers}
                      { this.state.currentPage <= renderPageNumbers.length + 2 && renderPageNumbers.length!== 1 &&
                        renderPageNumbers.length!==0?<a href="#" onClick={this.handleNext.bind(this)}>&#x27E9;  </a>:""
                      }
                      {renderPageNumbers.length!==0?
                        <a href="#" onClick={this.state.currentPage !== renderPageNumbers.length + 2 && 
                          renderPageNumbers.length !== 1 && renderPageNumbers.length!==0? this.handleLast.bind(this) : ""}>
                          &#x27EB;</a>: ""
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(FAQManagementContainer)