import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import * as api from '../../../../../utils/api'

export function updateStatus(purchaseId, data) {
  console.log(data)
  return function(dispatch) {
    dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST" })
    axios.post(api.URL+"api/user-purchase/"+purchaseId+"/", data)
      .then(function (response) {
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_FULFILLED", payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_REJECTED", payload: error})
      })
  }  
}

export function getFAQ(page) {
  return function(dispatch) {
    axios.get(api.URL+"api/faq/?page="+page)
      .then(function (response) {
        let results = response.data.results
        for (let result of results){
          result['forUpdate'] = false
        }
        dispatch({type:"FAQ_LIST_FETCHED", payload:results, count: response.data.count})
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to fetch FAQ')
      })
  }
}