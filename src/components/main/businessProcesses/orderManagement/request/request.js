import axios from 'axios'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as api from '../../../../../utils/api'

export function updateStatus(purchaseId, data) {
  console.log(data)
  return function(dispatch) {
    dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST" })
    axios.post(api.URL+"api/user-purchase/"+purchaseId+"/", data)
      .then(function (response) {
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_FULFILLED", payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
        dispatch({type: "UPDATE_PURCHASE_STATUS_REQUEST_REJECTED", payload: error})
      })
  }  
}