import React from 'react'
import * as Common from '../../commons/index'
import { connect } from 'react-redux'
import { getBots, getPurchases, updateStatus } from './request'
import InfoTable from './infoTable'
import { Link } from 'react-router'
import PurchasesTable from './purchasesTable'
import PurchaseModal from './purchaseModal'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
    bots: state.businessProcesses.bots,
    purchases: state.businessProcesses.purchases
  }
}

class OrderManagementContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      showPurchaseModal: false,
      purchase: null,
      status: "Received",
      currentBot: 0,
      sidebar: true,
      date: new Date(),
    }
  }

  componentWillMount() {
    this.props.dispatch(getBots())
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  botChange = (e) => {
    this.setState({selectedBot:e.target.value})
    this.props.dispatch(getPurchases(e.target.value))
    this.setState({currentBot: e.target.value})
  }

  componentWillUnmount() {
    this.props.dispatch({type:'RESET_BUSINESS_PROCESSES'})
    clearInterval(this.timerID);
  }

  openPurchaseModal(purchase) {
    this.setState({purchase:purchase})
    this.setState({currentStatus: purchase.status})
    this.setState({showPurchaseModal:true})
  }

  closePurchaseModal() {
    this.setState({showPurchaseModal:false})
  }

  handleChange = (event) => {
    this.setState({status: event.target.value})
    this.setState({currentStatus: event.target.value})
  }

  update(){
    let purchaseId = this.state.purchase.id
    let status = this.state.status
    this.props.dispatch(updateStatus(purchaseId, status,this.state.currentBot))
    this.closePurchaseModal()
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {

    let options = this.props.bots.map((item, index) => {
      return(
        <option key={index} value={item.id}>{item.name}</option>
      )
    })

    let purchases = this.props.purchases.map((item, index) => {
      let items = item.items.map((item, index) =>{
        return (
          <p key={index}>{item.product.name + " - " + item.quantity}</p>
        )
      })
      return (
        <tr key={index}>
          <td><Link href="#showDetailsModal" data-toggle="modal" data-backdrop="static" 
                onClick={this.openPurchaseModal.bind(this, item)}>{item.id}
              </Link>
          </td>
          <td>{item.user.first_name + " " + item.user.last_name}</td>
          <td>{items}</td>
          <td>{item.created_at}</td>
        </tr>
      )
    })

    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} name={this.props.user.first_name}/>
        <Common.SideBar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
        <div id="contentwrapper">
          <div className="main_content">
            <Common.Nav page={"Order Management"}/>
            <div className="row-fluid">
              <div className="span12 margin-top-7">
                <h3 className="heading">Order Management</h3>
                <div className="row-fluid">
                  <div className="margin-top-5">
                    <p>This is the main view of your processes.</p>
                  </div>
                </div>
                <div className="row-fluid">
                  <InfoTable botChange={this.botChange.bind(this)} options={options}/>
                </div>
                <div className="row-fluid">
                  <PurchasesTable {...this.props}
                    purchases={purchases} 
                    openPurchaseModal={this.openPurchaseModal.bind(this)}/>
                </div>
                <div className="row-fluid">
                  <PurchaseModal {...this.state} {...this.props}
                    closePurchaseModal={this.closePurchaseModal.bind(this)}
                    handleChange={this.handleChange.bind(this)} 
                    update={this.update.bind(this)}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(OrderManagementContainer)