import React from 'react'

const PurchaseModal = (props) => {
  let user = {}
  let bot = {}
  let items 
  let created_at
  let total = 0.0
  let statusArray = ["Received","Confirmed","Shipped","Delivered"]

  try {
    user = props.purchase.user
    bot = props.purchase.bot
    created_at = props.purchase.created_at
    items = props.purchase.items.map((item, index) => {
      total += (item.product.price * item.quantity)
      return(
        <div key={index}>
          {item.quantity + " pcs. " + item.product.name + " ₱" + (item.product.price * item.quantity)}
        </div>
      )
    })
  } catch(error) {
  }

  let status = statusArray.map((status, index) => {
      return (
        <option key={index} value={status}>{status}</option>
      )
    })

  return (
    <div className="modal hide fade" id="showDetailsModal">
      <div className="modal-header modal-header-new" >
        <button className="close" data-dismiss="modal">×</button>
        <h3>Purchase Details</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <form className="form-horizontal">
              <fieldset>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Status:</b></p>
                  </div>
                  <div className="span3">
                    <select value={props.currentStatus} onChange={props.handleChange} 
                      className="span10 margin-top-neg-6">
                        {status}
                    </select>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Bot:</b></p>
                  </div>
                  <div className="span5">
                    <p className="pull-left"><b>{bot.name}</b></p>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Description:</b></p>
                  </div>
                  <div className="span5">
                    <p className="pull-left"><b>{bot.description}</b></p>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Buyer:</b></p>
                  </div>
                  <div className="span5">
                    <p className="pull-left"><b>{user.first_name+" "+user.last_name}</b></p>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Items:</b></p>
                  </div>
                  <div className="span5">
                    <div className="pull-left">
                      <b>{items}</b>
                    </div>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Total Cost:</b></p>
                  </div>
                  <div className="span5">
                    <p className="pull-left"><b>{"₱ "+total}</b></p>
                  </div>
                </div>
                <div className="row-fluid">
                  <div className="span4">
                    <p className="pull-right"><b>Purchase Date:</b></p>
                  </div>
                  <div className="span5">
                  <div className="pull-left"><b>{created_at}</b></div>
                  </div>
                </div>
                <br/>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="btns-action">
          <button type="button" onClick={props.update} className="btn btn-primary" 
            data-dismiss="modal">Update</button>
          <button type="button" className="btn" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  )
}


PurchaseModal.propTypes = {
  // purchase : React.PropTypes.object,
  // showPurchaseModal : React.PropTypes.bool,
  // handleChange : React.PropTypes.func,
  // closePurchaseModal : React.PropTypes.func,
  // update: React.PropTypes.func,
}

export default PurchaseModal


