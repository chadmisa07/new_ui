import React from 'react'

const PurchasesTable = (props) => {
  return (
    <div className="span8 well">
      <table className="table table-bordered table-overflow">
        <thead>
          <tr>
            <th>Purchase ID</th>
            <th>Name</th>
            <th>Items</th>
            <th>Purchase Date</th>
          </tr>
        </thead>
        <tbody>
          {!props.purchases.length ?
            <tr><td colSpan="4" className="align-center"><i>No data found</i></td></tr> : 
            null
          }
         {props.purchases}
        </tbody>
      </table>
    </div>
    
  )
}

export default PurchasesTable


