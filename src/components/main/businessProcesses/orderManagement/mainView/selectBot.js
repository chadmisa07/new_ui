import React from 'react'
import { Form, FormGroup, Col, FormControl } from 'react-bootstrap'

const SelectBot = (props) => {
  return (
    <Form horizontal>
      <FormGroup>
        <Col sm={4}>
          <h5>Choose Bot:</h5>
        </Col>
        <Col sm={7}>
          <FormControl componentClass="select" 
            placeholder="Select Bot"
            onChange={props.botChange} >
            <option>Select Bot</option>
            {props.options}
          </FormControl>
        </Col>
      </FormGroup>
    </Form>
  )
}

export default SelectBot