import React from 'react'

const FBPreview = (props) => {
  return (
    <div>
      <div className="row-fluid">
        <span className="font-color-blue">What your bot looks like when run</span>
      </div>
      <div className="well clear-background">
        <div className="row-fluid">
          <div className="span12">
            <div className="row-fluid">
              <span className="sender-message-bubble pull-right">Add to Cart</span>
            </div>
            <div className="row-fluid">
              <span className="sender_message_bubble pull-left">{props.responseText.message}</span>
            </div>
            <div className="row-fluid">
              <span className="sender_message_bubble pull-left">{props.quick_reply.text}</span>
            </div>
            <div className="row-fluid force-txt-center">
              {props.fb_qr_btn}
            </div>
          </div>
        </div> 
      </div> 
    </div>
  )
}

FBPreview.propTypes = {
  responseText: React.PropTypes.object.isRequired,
  quick_reply: React.PropTypes.object.isRequired,
  fb_qr_btn: React.PropTypes.array.isRequired,
}

export default FBPreview


