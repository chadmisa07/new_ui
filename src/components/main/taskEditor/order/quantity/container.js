import React from 'react'
import { connect } from 'react-redux'
import { toastr } from 'react-redux-toastr'
import * as Common from '../common/index'
import * as Quantity from './index'
import { browserHistory } from 'react-router'
import * as Request from '../request/request'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    processName: "Order Management",
    taskEditor: state.taskEditor,
    data: state.task.data,
    taskMeta : state.task,
  }
}

let count = 3

class QuantityContainer extends React.Component {

  constructor(props) {
    super(props)
    if (!this.props.taskEditor.editMode){
      if (props.taskEditor.quantityContainer===null){
        this.state = {
          sidebar: true,
          responseTextError: null,
          quickReplyTextError: null,
          taskNameError: null,
          taskDescriptionError: null,
          responseText: {
            "message": "Great! How many would you like?",
            "text_type": 40,
            "created_by": props.user.id,
            "updated_by": props.user.id,
          },
          quick_reply: {
            quick_replies:[
              this.newQuickReplyButton(1), 
              this.newQuickReplyButton(2),
              this.newQuickReplyButton(3),
            ],
            text: "Please choose from among the options below.",
            qr_type: 42,
            "created_by": props.user.id,
            "updated_by": props.user.id,
          }
        }
      } else {
        this.state = props.taskEditor.quantityContainer
      }
    }else{
      this.state = props.taskEditor.quantityContainer
    }
  }

  newQuickReplyButton(index) {
    return ({
      id:"",
      title: index,
      payload: "order_here",
      reply_type: 44,
      created_by: this.props.user.id,
      updated_by: this.props.user.id,
    })
  }

  componentDidMount() {
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  updateStore() {
    this.props.dispatch({type:"UPDATE_QUANTITY_CONTAINER", payload:this.state})
    this.props.dispatch({ 
      type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
      payload:{
        name: this.state.taskName,
        description: this.state.taskDescription
      }
    })
  }

  componentWillMount(){
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    this.setState({buttonTextError: null})
    if(!this.state.responseText.message.replace(/\s/g, '').length){
      this.setState({responseTextError:"f_error"})
    } else if (!this.state.quick_reply.text.replace(/\s/g, '').length){
      this.setState({quickReplyTextError:"f_error"})
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  taskNameError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_NAME_ERROR",
        payload: {
          taskNameError: error,
        }
    })
  }

  taskDescriptionError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_DESCRIPTION_ERROR",
        payload: {
          taskDescriptionError: error
        }
    })
  }

  handleInputChange = (e) => {
    if(e.target.name === "taskName"){
       if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskNameError:"f_error"})
        this.taskNameError("error")
      } else {
        this.setState({taskNameError:null})
        this.taskNameError(null)
      }
      this.setState({taskName: e.target.value})
    }
    else if(e.target.name === "taskDescription"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskDescriptionError:"f_error"})
        this.taskDescriptionError("f_error")
      } else {
        this.setState({taskDescriptionError:null})
        this.taskDescriptionError(null)
      }
      this.setState({taskDescription: e.target.value})
    }
    else if(e.target.name === "responseText"){
      if(!event.target.value.replace(/\s/g, '').length){
        this.setState({responseTextError:"f_error"})
      } else {
        this.setState({responseTextError:null})
      }
      this.setState({
        responseText: {
          message: event.target.value,
          id:this.state.responseText.id,
          "text_type": 40,
          "created_by": this.props.user.id,
          "updated_by": this.props.user.id
        }
      })
    }
    else{
      if(!event.target.value.replace(/\s/g, '').length){
        this.setState({quickReplyTextError:"f_error"})
      } else {
        this.setState({quickReplyTextError:null})
      }
      let quick_reply = this.state.quick_reply
      quick_reply.text = event.target.value
      this.setState({
        quick_reply: quick_reply
      })
    }
  }

  handleTextResponseChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({responseTextError:"f_error"})
    } else {
      this.setState({responseTextError:null})
    }
    this.setState({
      responseText: {
        message: event.target.value,
        id:this.state.responseText.id,
        "text_type": 40,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      }
    })
  }

  handleMenuSelectionTextChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({quickReplyTextError:"f_error"})
    } else {
      this.setState({quickReplyTextError:null})
    }
    let quick_reply = this.state.quick_reply
    quick_reply.text = event.target.value
    this.setState({
      quick_reply: quick_reply
    })
  }

  handleTaskNameChange = (event) => {
    this.setState({taskName:event.target.value})
  }

  handleTaskDescriptionChange = (event) => {
    this.setState({taskDescription:event.target.value})
  }

  handleQuickReplyTextChange = (index, event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({buttonTextError: "f_error"})
    }else{
      this.setState({buttonTextError: null})
    }
    let quick_reply = this.state.quick_reply
    quick_reply.quick_replies[index].title = event.target.value
    this.setState({quick_reply:quick_reply})
  }

  addNewQRButton() {
    let quick_reply = this.state.quick_reply
    let quick_replies = this.state.quick_reply.quick_replies
    quick_replies.push(this.newQuickReplyButton(Number(count+1)))
    quick_reply.quick_replies = quick_replies
    this.setState({quick_reply:quick_reply})
    count++
  }

  removeQRButton = (index, event) => {
    let quick_reply = this.state.quick_reply
    let quick_replies = this.state.quick_reply.quick_replies
    quick_replies.splice(index, 1)
    quick_reply.quick_replies = quick_replies
    this.setState({quick_reply:quick_reply})
    count--
  }

  save() {
   if(this.state.taskName && this.state.taskDescription){
      if(this.props.taskEditor.editMode){
        this.updateStore()
        this.props.dispatch(Request.updateTask(this.props.taskEditor))
      }else{
        this.props.dispatch(Request.saveTask(this.props.taskEditor))
      }
    } else {
      toastr.error('Error', 'Please fill everything')
    }
  }

  previous(){
    if(this.state.responseTextError === "f_error" ||
       this.state.quickReplyTextError === "f_error" || 
       this.state.buttonTextError === "f_error"){
        toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/product")
    }
  }

  next(){
    if(this.state.responseTextError === "f_error" || 
       this.state.quickReplyTextError === "f_error" || 
       this.state.buttonTextError === "f_error"){
        toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/cart")
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {

    const qr_buttons = this.state.quick_reply.quick_replies.map((quick_reply, index) => {
      return (
        <Quantity.QuantityButton 
          index={index} 
          key={index}
          count={this.state.quick_reply.quick_replies.length}
          quick_reply={quick_reply}
          handleQuickReplyTextChange={this.handleQuickReplyTextChange.bind(this, index)}
          addNewQRButton={this.addNewQRButton.bind(this)}
          removeQRButton={this.removeQRButton.bind(this, index)}
          buttonTextError={this.state.buttonTextError} />
      )
    })

    const fb_qr_btn = this.state.quick_reply.quick_replies.map((quick_reply, index) => {
      return (
        <Quantity.ButtonPreview 
          title={quick_reply.title} 
          key={index}
          index={index} />
      )
    })

    return (
  
 <div id="contentwrapper" className={this.state.sidebar?"":"sidebar_hidden"}>
  <div className="main_content">
    <Common.Nav parent={'Task Editor'} page={'Order Quantity'}/>
    <Common.Header logo={logo} logo_small={logo_small} 
      fname={this.props.user.first_name} {...this.state} {...this.props} save={this.save.bind(this)}/>
    <div className="row-fluid">
      <div className="span12">
        <h3 className="heading">Order Quantity</h3>
        <div className="row-fluid">
          <div className="span12">
            <div className="row-fluid">
              <button className="pull-right btn btn-primary btn-small" onClick={this.next.bind(this)}>Next &nbsp;
                  <i className="fa fa-arrow-right icon-white"></i>
              </button>
              <button className="pull-right margin-right-5 btn btn-primary btn-small" onClick={this.previous.bind(this)}>
                <i className="fa fa-arrow-left icon-white"></i>&nbsp;Previous
              </button>
            </div>
            <form className="form-horizontal">
              <fieldset className="margin-top-20">
                <div>
                  <div className="well margin-top-neg-20">
                    <Common.TaskMeta {...this.props} {...this.state}
                          handleInputChange={this.handleInputChange.bind(this)} />
                  </div>
                   
                  <div className="row-fluid">
                    <div className="span6">
                      <Quantity.Instruction />
                      <div className="well">
                        <Quantity.QuantityForm {...this.state}
                          handleTextResponseChange={this.handleTextResponseChange.bind(this)}
                          handleMenuSelectionTextChange={this.handleMenuSelectionTextChange.bind(this)} />
                        <div className="row-fluid">
                          <span>We have prepared 3 choices as 1, 2, and 3. You can edit them or delete 2 of them.</span>
                        </div>
                        <br/>
                        {qr_buttons}
                      </div>
                    </div>
                    <div className="span6 margin-top-25">
                      <Quantity.FBPreview fb_qr_btn={fb_qr_btn} {...this.state} />
                    </div>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
 </div>
 <Common.Sidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)}/>
 </div>
          
    )
  }
}

export default connect(mapStateToProps)(QuantityContainer)