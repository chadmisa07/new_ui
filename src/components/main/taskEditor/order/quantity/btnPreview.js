import React from 'react'

const ButtonPreview = (props) => {
  return (
    <p className="circle" key={props.index}>
      {props.title}
    </p>
  )
}

ButtonPreview.propTypes = {
	index: React.PropTypes.number.isRequired,
}

export default ButtonPreview
