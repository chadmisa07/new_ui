import React from 'react'

const QuantityForm = (props) => {
  return (
    <div>
      <div className="control-group">
        <label className="control-label width-190"><b>Response Text</b></label>
        <div className={props.responseTextError + " controls margin-left-200"}>
          <input type="text" id="" className="span11" 
            value={props.responseText.message}
            onChange={props.handleTextResponseChange} 
            name="responseText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Menu Selection Text</b></label>
        <div className={props.quickReplyTextError + " controls margin-left-200"}>
          <input type="text" id="" className="span11" 
            value={props.quick_reply.text}
            onChange={props.handleMenuSelectionTextChange} 
            name="menuSelectionText"/>
        </div>
      </div>
    </div>
  )
}

QuantityForm.propTypes = {
  responseTextError: React.PropTypes.string,
  responseText: React.PropTypes.object.isRequired,
  handleTextResponseChange: React.PropTypes.func.isRequired,
  quickReplyTextError: React.PropTypes.string,
  quick_reply: React.PropTypes.object.isRequired,
  handleMenuSelectionTextChange: React.PropTypes.func.isRequired
}

export default QuantityForm



