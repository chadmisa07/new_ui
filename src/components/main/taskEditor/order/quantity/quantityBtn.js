import React from 'react'

const QuantityButton = (props) => {
  return (
    <div className="row-fluid">
      <div className="span1"></div>
      <div className="span3">
        { 
          (props.index===0&&props.count===1) || props.index===props.count-1?
          <button type="button" className="btn btn-primary btn-small"
            key={props.index} onClick={props.addNewQRButton} disabled={props.count===5}>
            <i className="fa fa-plus icon-white"></i> Add Option
          </button>
          : null 
        }
      </div>
      <div className="span5">
        <div className={props.buttonTextError + " span10"}>
          <input type="number" className="span12" key={props.index} 
            onChange={props.handleQuickReplyTextChange.bind(this)} value={props.quick_reply.title}/>
        </div>
        <div className="span2">
          <button type="button" className="btn btn-danger btn-small" onClick={props.removeQRButton} disabled={props.count===1}>
            <i className="fa fa-plus icon-white"></i>
          </button>
        </div>
      </div>
    </div>
  )
}

QuantityButton.propTypes = {
  index: React.PropTypes.number.isRequired,
  count: React.PropTypes.number.isRequired,
  addNewQRButton: React.PropTypes.func.isRequired,
  handleQuickReplyTextChange: React.PropTypes.func.isRequired,
  quick_reply: React.PropTypes.object.isRequired,
  removeQRButton: React.PropTypes.func.isRequired,
}

export default QuantityButton
