import React from 'react'
import { Link } from 'react-router'
import { toastr } from 'react-redux-toastr'
import LoadingBar from 'react-redux-loading-bar'

const Header = (props) => {
  function showError(){
    if(props.taskEditor.taskNameError === "error"){
      toastr.error('Error', 'Please enter a task name')
    }else if(props.taskEditor.taskDescriptionError === "error"){
      toastr.error('Error', 'Please enter a task description')
    }else{
      toastr.error('Error', 'Please fill everything')
    }
  }
  return (
    <header>
      <div className="navbar navbar-fixed-top">
        <LoadingBar className="loadingBar"/>
        <div className="navbar-inner">
          <div className="container-fluid">
            <Link to="/bot/create"className="brand tac">
              <img src={props.logo} className="visible-desktop visible-tablet" alt=""/>
              <img src={props.logo_small} className="visible-phone" alt=""/>
            </Link>
            <ul className="nav user_menu nav-items">
              <li className="hidden-phone hidden-tablet"></li>
              <li className="dropdown visible-desktop">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-user-circle fa-lg icon-blue"></i>&nbsp;&nbsp;
                    Hi, {props.fname} &nbsp;<i className="fa fa-caret-down icon-blue">
                  </i>
                </a>
                <ul className="dropdown-menu">
                  <li><a href="#">Change Password</a></li>
                  <li><Link to="/logout">Logout</Link></li>
                </ul>
              </li>
            </ul>
            <ul className="nav nav-btns">
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <a href="#">
                  {
                    props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                    props.changeEmailError !== "f_error" && props.buttonTextError !== "f_error" &&
                    props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                    props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                    props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                    props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                    props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                    props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                    props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                    props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                    props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                    props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                    props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                    props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                    props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                    props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                    props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                    props.menuInterfaces !== 0 && props.count !==0 && 
                    props.locationInterfaces !== 0 && props.locationCount !== 0 &&
                    props.taskEditor.taskNameError !== "f_error" && props.taskEditor.taskDescriptionError !== "f_error"?
                     <span className="action-btn-label" onClick={props.save}><i className="fa fa-save icon-blue"></i>&nbsp; Save</span>
                    :
                     <span className="action-btn-label" onClick={showError.bind(this)}><i className="fa fa-save icon-blue"></i>&nbsp; Save</span>
                  }
               </a>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <a href="#">
                  <i className="fa fa-close icon-blue"></i>
                  <span className="action-btn-label"> Close</span>
                </a>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <a href="#">
                  <i className="fa fa-upload icon-blue"></i>
                  <span className="action-btn-label"> Import</span>
                </a>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown">
                <a href="#">
                  <i className="fa fa-download icon-blue"></i>
                  <span className="action-btn-label"> Export</span>
                </a>
              </li>
              <li className="divider-vertical visible-phone visible-tablet"></li>
              <li className="dropdown visible-phone visible-tablet pull-right">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown" data-target=".nav-collapse">
                  <i className="fa fa-caret-down icon-blue"></i>
                </a>
                <ul className="dropdown-menu">
                  <li><a href="#">Change Password</a></li>
                  <li><a href="#">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header