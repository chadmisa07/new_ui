import React from 'react'
import { Link } from 'react-router'
import { toastr } from 'react-redux-toastr'

const SideBar = (props) => {
  function showError(){
    console.log(props.locationCount)
    if(props.count === 0){
      toastr.error('Error', 'Please add a product')
    }
    else if(props.locationCount === 0){
      toastr.error('Error', 'Please add a location')
    }
    else{
      toastr.error('Error', 'Please fill everything')
    }
  }
  return(
    <div>
      <a onClick={props.sidebarClick} className="sidebar_switch on_switch" title="Hide Menu"></a>
        <div className="sidebar">
          <div className="antiScroll">
            <div className="antiscroll-inner">
            <div className="antiscroll-content">
              <div className="sidebar_inner">
                <div className="timer clear-background tac">
                  <span className="timer-time">Task Editor</span><br/>
                </div>
                <div id="side_accordion" className="accordion">
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      {
                        props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                        props.changeEmailError !== "f_error" && props.locationCount !== 0 &&
                        props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                        props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                        props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                        props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                        props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                        props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                        props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                        props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                        props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                        props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                        props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                        props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                        props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                        props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                        props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                        props.menuInterfaces !== 0 && props.count !==0 && 
                        props.locationInterfaces !== 0 && props.buttonTextError !== "f_error"?
                        <Link to="/order/operation"
                          className="accordion-toggle" activeClassName="background-gray">1. Main Menu</Link>: 
                        <Link className="accordion-toggle" activeClassName="background-gray"
                          onClick={showError.bind(this)}>1. Main Menu</Link>
                      }
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      {
                        props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                        props.changeEmailError !== "f_error" && props.locationCount !== 0 &&
                        props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                        props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                        props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                        props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                        props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                        props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                        props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                        props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                        props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                        props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                        props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                        props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                        props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                        props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                        props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                        props.menuInterfaces !== 0 && props.count !==0 && 
                        props.locationInterfaces !== 0 && props.buttonTextError !== "f_error"?
                        <Link to="/order/product"
                          className="accordion-toggle" activeClassName="background-gray">2. Product Choices</Link>: 
                        <Link className="accordion-toggle" activeClassName="background-gray"
                          onClick={showError.bind(this)}>2. Product Choices</Link>
                      }
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      {
                        props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                        props.changeEmailError !== "f_error" && props.locationCount !== 0 &&
                        props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                        props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                        props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                        props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                        props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                        props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                        props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                        props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                        props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                        props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                        props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                        props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                        props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                        props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                        props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                        props.menuInterfaces !== 0 && props.count !==0 && 
                        props.locationInterfaces !== 0 && props.buttonTextError !== "f_error"?
                        <Link to="/order/quantity"
                          className="accordion-toggle" activeClassName="background-gray">3. Order Quantity</Link>: 
                        <Link className="accordion-toggle" activeClassName="background-gray"
                          onClick={showError.bind(this)}>3. Order Quantity</Link>
                      }
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      {
                        props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                        props.changeEmailError !== "f_error" && props.locationCount !== 0 &&
                        props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                        props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                        props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                        props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                        props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                        props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                        props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                        props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                        props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                        props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                        props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                        props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                        props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                        props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                        props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                        props.menuInterfaces !== 0 && props.count !==0 && 
                        props.locationInterfaces !== 0 && props.buttonTextError !== "f_error"?
                        <Link to="/order/cart"
                          className="accordion-toggle" activeClassName="background-gray">4. Cart</Link>: 
                        <Link className="accordion-toggle" activeClassName="background-gray"
                          onClick={showError.bind(this)}>4. Cart</Link>
                      }
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      {
                        props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                        props.changeEmailError !== "f_error" && props.locationCount !== 0 &&
                        props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                        props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                        props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                        props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                        props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                        props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                        props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                        props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                        props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                        props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                        props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                        props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                        props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                        props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                        props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                        props.menuInterfaces !== 0 && props.count !==0 && 
                        props.locationInterfaces !== 0 && props.buttonTextError !== "f_error"?
                        <Link to="/order/delivery"
                          className="accordion-toggle" activeClassName="background-gray">5. Delivery / Pick-up</Link>: 
                        <Link className="accordion-toggle" activeClassName="background-gray"
                          onClick={showError.bind(this)}>5. Delivery / Pick-up</Link>
                      }
                    </div>
                  </div>
                  <div className="accordion-group">
                    <div className="accordion-heading">
                      {
                        props.welcomeTextError !== "f_error"  && props.viewProductError !== "f_error" && 
                        props.changeEmailError !== "f_error" && props.locationCount !== 0 &&
                        props.viewPickupLocationError !== "f_error" && props.nameError !== "f_error" &&
                        props.codeError !== "f_error" && props.descriptionError !== "f_error" && 
                        props.priceError !== "f_error" && props.item_url_error !== "f_error" && 
                        props.image_url_error!== "f_error" && props.add_button_error !== "f_error" &&
                        props.view_description_error !== "f_error" && props.locationNameError !== "f_error" && 
                        props.locationDescriptionError !== "f_error" && props.location_image_url_error !== "f_error" && 
                        props.location_url_error !== "f_error" && props.contact_info_error !== "f_error" &&
                        props.visit_page_error !== "f_error" && props.call_btn_error !== "f_error" && 
                        props.responseTextError !== "f_error" && props.quickReplyTextError !== "f_error" && 
                        props.cartResponseTextError !== "f_error" && props.cartButtonResponseTextError !== "f_error" && 
                        props.cartOrderButtonError !== "f_error" && props.cartViewCartButtonError !== "f_error" && 
                        props.cartCheckoutButtonError !== "f_error" && props.checkoutError !== "f_error" && 
                        props.askReceiptError !== "f_error" && props.askEmailError !== "f_error" && 
                        props.emailErrorTextError !== "f_error" && props.retryEmailError !== "f_error" && 
                        props.saveEmailError !== "f_error" && props.provideEmailError !== "f_error" && 
                        props.menuInterfaces !== 0 && props.count !==0 && 
                        props.locationInterfaces !== 0 && props.buttonTextError !== "f_error"?
                        <Link to="/order/checkout"
                          className="accordion-toggle" activeClassName="background-gray">6. Checkout</Link>: 
                        <Link className="accordion-toggle" activeClassName="background-gray"
                          onClick={showError.bind(this)}>6. Checkout</Link>
                      }
                    </div>
                  </div>
                  <div className="push"></div>
                </div> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SideBar