import React from 'react'

const TaskMeta = (props) => {
  return (
    <table className="table table-bordered">
      <tbody>
        <tr>
          <td><b>Task Name</b></td>
          <td className="three-fourth">
            <div className={props.taskEditor.taskNameError}>
              <input type="text" value={props.taskName}
                  onChange={props.handleInputChange}
                  name="taskName" className="span12"/>
            </div>
          </td>
        </tr>
        <tr>
          <td><b>Process</b></td>
          <td><b>Order Management</b></td>
        </tr>
        <tr>
          <td><b>Task Description</b></td>
          <td>
            <div className={props.taskEditor.taskDescriptionError}>
              <input type="text" 
                value={props.taskDescription}
                onChange={props.handleInputChange}
                name="taskDescription"
                className="span12"/>
            </div>
          </td>
        </tr>
        <tr>
          <td><b>Link to Bot</b></td>
          <td>
            <b>{!props.taskEditor.editMode ? 
                  !props.bot.bot?
                  "":props.bot.bot.name : 
                  !props.data.name?
                  "":props.data.name}
            </b>
          </td>
        </tr>
      </tbody>
    </table>
  )
}

export default TaskMeta
  