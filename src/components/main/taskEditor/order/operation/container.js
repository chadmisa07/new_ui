import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import * as Common from '../common/index'
import * as Operation from './index'
import * as Request from '../request/request'
import { toastr } from 'react-redux-toastr'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    job: state.job,
    taskEditor: state.taskEditor,
    processName: "Order Management",
    data: state.task.data,
    taskMeta : state.task,
  }
}

class OperationContainer extends React.Component{

  constructor(props) {
    super(props)
    if (!this.props.taskEditor.editMode){
      if (props.taskEditor.operationContainer===null){
        this.state = {
          welcomeText: "Welcome! Please select what you want to do.",
          welcomeTextId: -1,
          viewProduct: "View Products",
          changeEmail: "Change Email",
          viewPickupLocation: "View Pickup Location",
          changeEmailRemove: false,
          viewPickupLocationRemove: false,
          welcomeTextError: null,
          viewProductError: null,
          changeEmailError: null,
          viewPickupLocationError: null,
          errorModal: false,
          taskNameError: null,
          taskDescriptionError: null,
          handleInputChange: this.handleInputChange.bind(this),
          sidebar: true,
        }
      } else if (props.taskEditor.operationContainer){
        let data = props.taskEditor.operationContainer
        let changeEmail = data.persistentMenu.buttons.length>=3 && data.persistentMenu.buttons[2].title!=="View Pickup Location"?data.persistentMenu.buttons[2].title:"Change Email"
        let viewPickupLocation = data.persistentMenu.buttons.length>=4?data.persistentMenu.buttons[3].title:"View Pickup Location"
        this.state = {
          welcomeText: data.greetingText.text,
          viewProduct: data.persistentMenu.buttons[0].title,
          changeEmail: changeEmail,
          viewPickupLocation: viewPickupLocation,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal,
          taskNameError: data.taskNameError,
          taskDescriptionError: data.taskDescriptionError,
          handleInputChange: this.handleInputChange.bind(this),
          sidebar: true,
        }
      } else {
        let data = props.taskEditor.operationContainer
        this.state = {
          welcomeText: data.welcomeText,
          viewProduct: data.viewProduct,
          changeEmail: data.changeEmail,
          viewPickupLocation: data.viewPickupLocation,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal,
          taskNameError: data.taskNameError,
          taskDescriptionError: data.taskDescriptionError,
          handleInputChange: this.handleInputChange.bind(this),
          sidebar: true,
        }
      }
    } else {
      if (props.taskEditor.operationContainer===null){
        let data = this.props.data
        let pickUpText = data.persistent_menu.find(menu=>menu.payload === "/pickup~~locs")
        let changeEmailText = data.persistent_menu.find(menu=>menu.payload === "c0mm@nd~~change_email")
        let viewProductText = data.persistent_menu.find(menu=>menu.payload === "/menu")
        this.state = {
          welcomeText: data.greeting_text[0].text,
          welcomeTextId: data.greeting_text[0].id,
          viewProduct: viewProductText ? viewProductText.title: "View Product",
          viewProductId: viewProductText ? viewProductText.id: "",
          viewPickupLocation : pickUpText ? pickUpText.title: "View Pickup Location",
          viewPickupLocationId : pickUpText ? pickUpText.id : "",
          changeEmail: changeEmailText ? changeEmailText.title : "Change Email",
          changeEmailId : changeEmailText ? changeEmailText.id: "",
          changeEmailRemove: changeEmailText ? false : true,
          viewPickupLocationRemove: pickUpText ? false: true,
          welcomeTextError: null,
          viewProductError: null,
          changeEmailError: null,
          viewPickupLocationError: null,
          errorModal: false,
          taskNameError: null,
          taskDescriptionError: null,
          handleInputChange: this.handleInputChange.bind(this),
          sidebar: true,
        }
      }else if (props.taskEditor.operationContainer){
        let data = props.taskEditor.operationContainer
        let pickUpText = data.persistentMenu.buttons.find(menu=>menu.payload === "/pickup~~locs")
        let changeEmailText = data.persistentMenu.buttons.find(menu=>menu.payload === "c0mm@nd~~change_email")
        let viewProductText = data.persistentMenu.buttons.find(menu=>menu.payload === "/menu")
        let changeEmail = changeEmailText ? changeEmailText.title: "Change Email"
        let changeEmailId = changeEmailText ? changeEmailText.id: ""
        let viewPickupLocation = pickUpText ? pickUpText.title : "View Pickup Location"
        let viewPickupLocationId = pickUpText ? pickUpText.id : ""
        this.state = {
          welcomeText: data.greetingText.text,
          welcomeTextId: data.greetingText.id,
          viewProduct: viewProductText ? viewProductText.title : "View Product",
          viewProductId: viewProductText ? viewProductText.id : "",
          changeEmail: changeEmail,
          changeEmailId: changeEmailId,
          viewPickupLocation: viewPickupLocation,
          viewPickupLocationId: viewPickupLocationId,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal,
          taskNameError: data.taskNameError,
          taskDescriptionError: data.taskDescriptionError,
          handleInputChange: this.handleInputChange.bind(this),
          sidebar: true,
        }
      }else {
        let data = props.taskEditor.operationContainer
        this.state = {
          welcomeText: data.welcomeText,
          welcomeTextId: data.greetingText.id,
          viewProduct: data.viewProduct,
          viewProductId: data.persistentMenu.buttons[0].id,
          changeEmail: data.changeEmail,
          changeEmailId: data.changeEmailId,
          viewPickupLocation: data.viewPickupLocation,
          viewPickupLocationId: data.viewPickupLocationId,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal,
          taskNameError: data.taskNameError,
          taskDescriptionError: data.taskDescriptionError,
          handleInputChange: this.handleInputChange.bind(this),
          sidebar: true,
        }
      }
    }
    // this.handleInputChange = this.handleInputChange.bind(this)
  }

  toggleChangeEmailRemove() {
    this.setState({changeEmailRemove:!this.state.changeEmailRemove})
    this.setState({changeEmailError: null})
    this.setState({changeEmail: "Change Email"})
  }

  toggleViewPickupLocationRemove() {
    let result = this.props.taskEditor.pickup
    this.setState({viewPickupLocationRemove:result})
    if (result){
      this.props.dispatch({type:'PICKUP_MODE_OFF', payload:result})
    } else {
      this.props.dispatch({type:'PICKUP_MODE_ON', payload:result})
    }
    this.setState({viewPickupLocationError: null})
    this.setState({viewPickupLocation: "View Pick-up Location"})
  }

  componentDidMount(){
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    this.updateStore()
  }

  jsonifyGreetingText() {
    return({
      "text": this.state.welcomeText,
      "id": this.state.welcomeTextId,
      "job_flow": this.props.data.id,
      "created_by": this.props.user.id,
      "updated_by": this.props.user.id
    })
  }

  jsonifyPersistentMenu() {
    return ({
      id: this.props.data.persistentMenuId,
      changeEmailRemove: this.state.changeEmailRemove,
      viewPickupLocationRemove: this.state.viewPickupLocationRemove,
      "buttons":[{
        "title": this.state.viewProduct,
        "id": this.state.viewProductId,
        "payload": "/menu",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      },{
        "title": this.state.changeEmail,
        "id": this.state.changeEmailId,
        "payload": "c0mm@nd~~change_email",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      },{
        "title": this.state.viewPickupLocation,
        "id": this.state.viewPickupLocationId,
        "payload": "/pickup~~locs",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      }],
      "job_flow": this.props.data.id,
      "created_by": this.props.user.id,
      "updated_by": this.props.user.id,
    })
  }

  updateStore(){
    let persistentMenu = this.jsonifyPersistentMenu()

    if(this.state.changeEmailRemove && this.state.viewPickupLocationRemove){
      persistentMenu.buttons.splice(1,2)
    } else if (this.state.changeEmailRemove){
      persistentMenu.buttons.splice(1,1)
      if(this.props.taskEditor.deliveryContainer && !this.props.taskEditor.deliveryContainer.pickUpChecked){
        persistentMenu.buttons.splice(1,1)
      }
    } else if (this.state.viewPickupLocationRemove){
      persistentMenu.buttons.splice(2,1)
    }  
    // console.log(JSON.stringify(persistentMenu))
    this.props.dispatch({
      type:'UPDATE_OPERATION_CONTAINER', 
      payload:{
        id: this.props.data.id,
        persistentMenu: persistentMenu,
        greetingText: this.jsonifyGreetingText(),
        changeEmailRemove: this.state.changeEmailRemove,
        viewPickupLocationRemove: this.state.viewPickupLocationRemove,
        welcomeTextError: this.state.welcomeTextError,
        viewProductError: this.state.viewProductError,
        changeEmailError: this.state.changeEmailError,
        viewPickupLocationError: this.state.viewPickupLocationError,
        errorModal:  this.state.errorModal
      }
    })
    this.props.dispatch({ 
      type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
      payload:{
        name: this.state.taskName,
        description: this.state.taskDescription
      }
    })
    // console.log(JSON.stringify(persistentMenu))
  }

  componentWillMount() {
    let data = this.props.data
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(this.props.taskEditor.editMode && this.props.taskEditor.operationContainer===null){
      this.setState({welcomeTextId: data.greeting_text[0].id})
      let viewPickupLocation = data.persistent_menu.find(menu=>menu.payload === "/pickup~~locs")

      if(viewPickupLocation){
        this.props.dispatch({type:'PICKUP_MODE_ON', payload: false})
      }else{
        this.props.dispatch({type:'PICKUP_MODE_OFF', payload: true})
      }
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  // handleTaskNameChange = (event) => {
  //   this.setState({taskName:event.target.value})
  // }

  // handleTaskDescriptionChange = (event) => {
  //  this.setState({taskDescription:event.target.value})
  // }

  taskNameError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_NAME_ERROR",
        payload: {
          taskNameError: error,
        }
    })
  }

  taskDescriptionError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_DESCRIPTION_ERROR",
        payload: {
          taskDescriptionError: error
        }
    })
  }

  handleInputChange = (e) => {
    if(e.target.name === "taskName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskNameError:"f_error"})
        this.setState({error: "f_error"})
        this.taskNameError("f_error")
      } else {
        this.setState({taskNameError:null})
        this.taskNameError(null)
      }
    }
    else if(e.target.name === "taskDescription"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskDescriptionError:"f_error"})
        this.setState({error: "f_error"})
        this.taskDescriptionError("f_error")
      } else {
        this.setState({taskDescriptionError:null})
        this.taskDescriptionError(null)
      }
    }
    else if(e.target.name === "welcomeText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({welcomeTextError:"f_error"})
        this.setState({errorModal: "f_error"})
      } else {
        this.setState({welcomeTextError:null})
      }
    }
    else if(e.target.name === "viewProduct"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({viewProductError:"f_error"})
        this.setState({error: "f_error"})
      } else {
        this.setState({viewProductError:null})
      }
    }
    else if(e.target.name === "changeEmail"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({changeEmailError:"f_error"})
        this.setState({error: "f_error"})
      } else {
        this.setState({changeEmailError:null})
      }
    } else {
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({viewPickupLocationError:"f_error"})
        this.setState({error: "f_error"})
      } else {
        this.setState({viewPickupLocationError:null})
      }
    }
    this.setState({[e.target.name]: e.target.value})
  }

  save() {
    if(this.state.taskName && this.state.taskDescription){
      if(this.props.taskEditor.editMode){
        this.updateStore()
        this.props.dispatch(Request.updateTask(this.props.taskEditor))
      }else{
        this.props.dispatch(Request.saveTask(this.props.taskEditor))
      }
    } else {
      toastr.error('Error', 'Please fill everything')
    }
  }

  next(){
    if(this.state.viewPickupLocationError === "f_error" || 
       this.state.changeEmailError === "f_error" || 
       this.state.viewProductError === "f_error" || 
       this.state.welcomeTextError === "f_error"){
        toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/product")
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    return (
      <div id="contentwrapper" className={this.state.sidebar?"":"sidebar_hidden"}>
        <div className="main_content">
         <Common.Nav parent={'Task Editor'} page={'Main Menu'}/>
         <Common.Header logo={logo} logo_small={logo_small} 
            fname={this.props.user.first_name} {...this.state} {...this.props} save={this.save.bind(this)}/>
          <div className="row-fluid">
            <div className="span12">
              <h3 className="heading">Main Menu</h3>
              <div className="row-fluid">
                <div className="span12">
                <div className="row-fluid">
                    <button className="pull-right btn btn-primary btn-small" onClick={this.next.bind(this)}>Next &nbsp;
                      <i className="fa fa-arrow-right icon-white"></i>
                    </button>
                  </div>
                  <form className="form-horizontal">
                    <fieldset className="margin-top-20">
                      <div>
                        <div className="well margin-top-neg-20">
                          <Common.TaskMeta {...this.props} {...this.state}
                            handleInputChange={this.handleInputChange.bind(this)}/>
                        </div>
                        <div className="row-fluid">
                          <div className="span6">
                          <Operation.Instruction/>
                            <div className="well">
                              <Operation.EditInterface {...this.state} 
                                toggleChangeEmailRemove={this.toggleChangeEmailRemove.bind(this)}
                                toggleViewPickupLocationRemove={this.toggleViewPickupLocationRemove.bind(this)}/>
                            </div>
                          </div>
                          <div className="span6 margin-top-25">
                            <div className="row-fluid">
                              <span className="font-color-blue">What your bot looks like when run</span>
                            </div>
                            <div className="well clear-background">
                               <Operation.Preview {...this.state} />
                            </div> 
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Common.Sidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)} />
      </div>
    )
  }
}

export default connect(mapStateToProps)(OperationContainer)


