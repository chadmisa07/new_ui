import React from 'react'

const Instruction = (props) => {
  return (
    <div>
      <div className="row-fluid">
        <span>This is the main operation menu for your user. It will be displayed in standard menu form. Learn <a>more</a>.</span>
      </div>
      <div className="row-fluid">
        <span>Please edit the welcome message and the menu button title texts.</span>
      </div>
    </div>
  )
}

export default Instruction