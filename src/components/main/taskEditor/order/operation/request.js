import axios from 'axios'
import { toastr } from 'react-redux-toastr'

export function createPersistentMenu(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/persistent-menu/", data)
      .then(function (response) {
        dispatch({type: "PERSISTENT_MENU_CREATED", payload: response.data})
        toastr.success('Success', 'Persistent menu was created')
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to create persistent menu')
      })
  }  
}

export function createGreetingText(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/greeting-text/", data)
      .then(function (response) {
        dispatch({type: "GREETING_TEXT_CREATED", payload: response.data})
        toastr.success('Success', 'Greeting text was created')
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to create greeting text')
      })
  }  
}