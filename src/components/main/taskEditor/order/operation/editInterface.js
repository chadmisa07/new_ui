import React from 'react'
// import { Row, Form, FormGroup, Col, FormControl, ControlLabel, Glyphicon, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'


// const ViewProductTooltip = (
//   <Tooltip id="tooltip"><strong>View Product Button Text</strong></Tooltip>
// );

// const ChangeEmailTooltip = (
//   <Tooltip id="tooltip"><strong>Change Email Button Text</strong></Tooltip>
// );

// const PickupLocationTooltip = (
//   <Tooltip id="tooltip"><strong>View Pickup Location Button Text</strong></Tooltip>
// );

// const WelcomeTextTooltip = (
//   <Tooltip id="tooltip"><strong>Welcome Text</strong></Tooltip>
// );

const EditInterface = (props) => {
  return (
    <div>
      <div className="control-group">
        <label className="control-label width-190"><b>Welcome Text</b></label>
        <div className={props.welcomeTextError + " controls margin-left-200"}>
         <textarea rows="4" className="span9"  
          onChange={props.handleInputChange} 
          name="welcomeText"
          value={props.welcomeText}
          placeholder="Welcome! Please select what you want to do."></textarea>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>View Product Button</b></label>
        <div className={props.viewProductError + " controls margin-left-200"}>
          <input type="text" id="" className="span9"
            onChange={props.handleInputChange}
            value={props.viewProduct}
            placeholder="View Product"
            name="viewProduct"
            maxLength={30} />
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Change Email Button</b></label>
        <div className={props.changeEmailError + " controls margin-left-200"}>
          <span className="span9">
            {
              !props.changeEmailRemove?
                <input type="text" id="" className="span12"
                  onChange={props.handleInputChange}
                  value={props.changeEmail}
                  placeholder="Change Email"
                  name="changeEmail"
                  maxLength={30}/>
              :
                null
            }
          </span>
          <span className="span2">
            <button type="button"
              onClick={props.toggleChangeEmailRemove}
              className={props.changeEmailRemove?"btn btn-success btn-small":"btn btn-danger btn-small"} >
              { 
                !props.changeEmailRemove?
                  <i className="fa fa-remove"></i>
                  :
                  <i className="fa fa-plus"></i>
              }
            </button>
          </span>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>View Pickup Location Button</b></label>
        <div className={props.viewPickupLocationError + " controls margin-left-200"}>
          <span className="span9">
            {
              !props.viewPickupLocationRemove?
                <input type="text" id="" className="span12"
                  onChange={props.handleInputChange}
                  value={props.viewPickupLocation}
                  placeholder="View Pick-up Location"
                  name="viewPickupLocation"
                  maxLength={30} />
              :
                null
            }
          </span>
          <span className="span2">
            <button type="button"
              onClick={props.toggleViewPickupLocationRemove}
              className={props.viewPickupLocationRemove?"btn btn-success btn-small":"btn btn-danger btn-small"} >
              { 
                !props.viewPickupLocationRemove?
                  <i className="fa fa-remove"></i>
                  :
                  <i className="fa fa-plus"></i>
              }
            </button>
          </span>
        </div>
      </div>
    </div>
  )
}

EditInterface.propTypes = {
  handleInputChange: React.PropTypes.func.isRequired,
  changeEmailError: React.PropTypes.string,
  changeEmail: React.PropTypes.string.isRequired,
  toggleChangeEmailRemove: React.PropTypes.func.isRequired,
  changeEmailRemove: React.PropTypes.bool.isRequired,
  viewPickupLocationError: React.PropTypes.string,
  viewPickupLocation: React.PropTypes.string.isRequired,
  toggleViewPickupLocationRemove: React.PropTypes.func.isRequired,
  viewPickupLocationRemove: React.PropTypes.bool.isRequired,
  welcomeTextError: React.PropTypes.string,
  welcomeText: React.PropTypes.string.isRequired,
  viewProductError: React.PropTypes.string,

  viewProduct: React.PropTypes.string.isRequired,
}

export default EditInterface





// <Form horizontal>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           
//         </Col>
//         <Col sm={8}>
//           <FormControl componentClass="textarea" placeholder="Welcome! Please select what you want to do." 
//             onChange={props.handleWelcomeTextChange} value={props.welcomeText} />
//         </Col>
//       </FormGroup>      
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Product Button
//         </Col>
//         <Col sm={8}>

//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Cart Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="View Cart"
//             onChange={props.handleViewCartChange}
//             value={props.viewCart}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           Change Email Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="Change Email"
//             onChange={props.handleChangeEmailChange}
//             value={props.changeEmail}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Pick-up Location Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="View Pick-up Location"
//             onChange={props.handleViewPickupLocationChange}
//             value={props.viewPickupLocation}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//     </Form>