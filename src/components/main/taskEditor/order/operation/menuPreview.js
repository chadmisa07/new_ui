import React from 'react'

const Preview = (props) => {
  return (
    <div>
      <div className="row-fluid tac">
        <b>{props.welcomeText}</b>
      </div>
      <br/>
      <div className="row-fluid">
        <div className="span3"></div>
        <div className="span6">
          <div className="gray-box container-fluid">
            <div className="span12">
              <div className="row-fluid">
                <div className="gray-bot-border persistent-menu-text-padding">
                  <span className="persistent-menu-blue-text">
                    Menu
                  </span>
                </div>
              </div>
              <div className="row-fluid">
                <div className="gray-bot-border persistent-menu-text-padding">
                  <span className="persistent-menu-blue-text">
                    {props.viewProduct}
                  </span>
                </div>
              </div>
                {
                  !props.changeEmailRemove?
                    <div className="row-fluid ">
                      <div className="gray-bot-border persistent-menu-text-padding">
                        <span className="persistent-menu-blue-text">
                          {props.changeEmail}
                        </span>
                      </div>
                    </div>: null
                }
                {
                  !props.viewPickupLocationRemove?
                    <div className="row-fluid">
                      <div className="gray-bot-border persistent-menu-text-padding">
                        <span className="persistent-menu-blue-text">
                          {props.viewPickupLocation}
                        </span>
                      </div>
                    </div>:null
                }
            </div>
          </div>
        </div>
      </div>
    </div> 
  )
}

Preview.propTypes = {
  viewProduct: React.PropTypes.string.isRequired,
  viewPickupLocationRemove: React.PropTypes.bool.isRequired,
  viewPickupLocation: React.PropTypes.string.isRequired,
  changeEmailRemove: React.PropTypes.bool.isRequired,
  changeEmail: React.PropTypes.string.isRequired,
}

export default Preview