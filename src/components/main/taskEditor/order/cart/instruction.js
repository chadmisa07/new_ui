import React from 'react'

const Instruction = (props) => {
  return (
    <div >
      <div className="row-fluid">
        <span>Once the quantity is defined, the user can view the cart, proceed to checkout or order again.</span>
      </div>
      <div className="row-fluid">
        <span>Please edit the response text and button menu text. Learn <a>more</a>.</span>
      </div>
    </div>
  )
}

export default Instruction
