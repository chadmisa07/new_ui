import React from 'react'

const FBPreview = (props) => {
  return (
    <div>
      <div className="row-fluid">
        <span className="font-color-blue">What your bot looks like when run</span>
      </div>
      <div className="well clear-background">
        <div className="row-fluid">
          <div className="span3"></div>
          <div className="span6">
            <div className="container-fluid">
              <div className="span12">
                <div className="row-fluid">
                  <span className="sender_message_bubble">{props.responseText.message}</span>
                </div>
                <div className="row-fluid">
                  <div className="span7">
                    <div className="btn-response-text text-center pull-left">{props.buttonResponse.text}</div>
                    <div className="btn-response-button text-center pull-left">{props.buttonResponse.buttons[0].title}</div>
                    <div className="btn-response-button text-center pull-left">{props.buttonResponse.buttons[1].title}</div>
                    <div className="btn-response-button text-center pull-left">{props.buttonResponse.buttons[2].title}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </div> 
    </div>
  )
}

FBPreview.propTypes = {
  responseText: React.PropTypes.object.isRequired,
  buttonResponse: React.PropTypes.object.isRequired
}

export default FBPreview