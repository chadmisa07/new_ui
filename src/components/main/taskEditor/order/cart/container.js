import React from 'react'
import { connect } from 'react-redux'
import { toastr } from 'react-redux-toastr'
import * as Common from '../common/index'
import * as Cart from './index'
import * as Request from '../request/request'
import { browserHistory } from 'react-router'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface,
    taskEditor: state.taskEditor,
    processName: "Order Management",
    data: state.task.data,
    taskMeta : state.task,
  }
}

class CartContainer extends React.Component {

  constructor(props) {
    super(props)
    if(!this.props.taskEditor.editMode){
      if(props.taskEditor.cartContainer===null){
        this.state = {
          sidebar: true,
          cartResponseTextError: null,
          cartButtonResponseTextError: null,
          cartOrderButtonError: null,
          cartViewCartButtonError: null,
          cartCheckoutButtonError: null,
          responseText:{
            text_type:  46,
            message: "Item added",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          buttonResponse: {
            text: "What do you want to do next?",
            button_type:  48,
            created_by: props.user.id,
            updated_by: props.user.id,
            buttons: [{
              title: "Order More",
              payload: "/menu",
              button_type: 38,
              created_by: props.user.id,
              updated_by: props.user.id
            },{
              button_type: 39,
              url: "https://cbot-api.herokuapp.com/app/view_cart/?id=",
              title: "View Cart",
              webview_height_ratio: 50,
              messenger_extensions: true,  
              fallback_url: "https://cbot-api.herokuapp.com/app/view_cart/?id="
            },{
              title: "Checkout",
              payload: "c0mm@nd~~@sk-CheckOut",
              button_type: 38,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          }
        }
      } else {
        this.state = props.taskEditor.cartContainer
      }
    }else{
      this.state = props.taskEditor.cartContainer
    }
  }

  updateStore(){
    this.props.dispatch({type:'UPDATE_CART_CONTAINER', payload:this.state})
    
    this.props.dispatch({ 
      type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
      payload:{
        name: this.state.taskName,
        description: this.state.taskDescription
      }
    })
  }

  componentWillMount(){
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(!this.state.responseText.message.replace(/\s/g, '').length){
      this.setState({cartResponseTextError: "error"})
    }else if(!this.state.buttonResponse.text.replace(/\s/g, '').length){
      this.setState({cartButtonResponseTextError: "error"})
    }else if(!this.state.buttonResponse.buttons[0].title.replace(/\s/g, '').length){
      this.setState({cartOrderButtonError: "error"})
    }else if(!this.state.buttonResponse.buttons[1].title.replace(/\s/g, '').length){
      this.setState({cartViewCartButtonError: "error"})
    }else if(!this.state.buttonResponse.buttons[2].title.replace(/\s/g, '').length){
      this.setState({cartCheckoutButtonError: "error"})
    }
  }
  
  componentDidMount() {
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  taskNameError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_NAME_ERROR",
        payload: {
          taskNameError: error,
        }
    })
  }

  taskDescriptionError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_DESCRIPTION_ERROR",
        payload: {
          taskDescriptionError: error
        }
    })
  }

  handleInputChange = (e) => {
    if(e.target.name === "taskName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskNameError:"f_error"})
        this.taskNameError("f_error")
      } else {
        this.setState({taskNameError:null})
        this.taskNameError(null)
      }
      this.setState({taskName: e.target.value})
    } 
    else if (e.target.name === "taskDescription"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskDescriptionError:"f_error"})
        this.taskDescriptionError("f_error")
      } else {
        this.setState({taskDescriptionError:null})
        this.taskDescriptionError(null)
      }
      this.setState({taskDescription: e.target.value})
    } 
    else if(e.target.name === "responseText"){
      if(!e.target.value.replace(/\s/g, '').length){
          this.setState({cartResponseTextError:"f_error"})
      } else {
        this.setState({cartResponseTextError:null})
      }
      let responseText = this.state.responseText
      responseText.message = e.target.value
      this.setState({responseText: responseText})
    }
    else if(e.target.name === "menuSelectionText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({cartButtonResponseTextError:"f_error"})
      } else {
        this.setState({cartButtonResponseTextError:null})
      }
      let newButtonResponse = this.state.buttonResponse
      newButtonResponse.text = e.target.value
      this.setState({buttonResponse: newButtonResponse})
    }
    else if(e.target.name === "orderMoreText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({cartOrderButtonError:"f_error"})
      } else {
        this.setState({cartOrderButtonError:null})
      }
      let buttonResponse = this.state.buttonResponse
      buttonResponse.buttons[0].title = e.target.value
      this.setState({ buttonResponse: buttonResponse })
    }
    else if(e.target.name === "viewCartText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({cartViewCartButtonError:"f_error"})
      } else {
        this.setState({cartViewCartButtonError:null})
      }
      let buttonResponse = this.state.buttonResponse
      buttonResponse.buttons[1].title = e.target.value
      this.setState({ buttonResponse: buttonResponse })
    } else {
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({cartCheckoutButtonError:"f_error"})
      } else {
        this.setState({cartCheckoutButtonError:null})
      }
      let buttonResponse = this.state.buttonResponse
      buttonResponse.buttons[2].title = e.target.value
      this.setState({ buttonResponse: buttonResponse })
    }
  }

  save() {
    if(this.state.taskName && this.state.taskDescription){
      if(this.props.taskEditor.editMode){
        this.updateStore()
        this.props.dispatch(Request.updateTask(this.props.taskEditor))
      }else{
        this.props.dispatch(Request.saveTask(this.props.taskEditor))
      }
    } else {
      toastr.error('Error', 'Please fill everything')
    }
  }

  previous(){
    if(this.state.cartCheckoutButtonError === "f_error" || this.state.cartViewCartButtonError === "f_error" ||
      this.state.cartOrderButtonError === "f_error" || this.state.cartButtonResponseTextError === "f_error" ||
      this.state.cartResponseTextError === "f_error"){
      toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/quantity")
    }
  }

  next(){
    if(this.state.cartCheckoutButtonError === "f_error" || this.state.cartViewCartButtonError === "f_error" ||
      this.state.cartOrderButtonError === "f_error" || this.state.cartButtonResponseTextError === "f_error" ||
      this.state.cartResponseTextError === "f_error"){
      toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/delivery")
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    return (
       <div id="contentwrapper" className={this.state.sidebar?"":"sidebar_hidden"}>
        <div className="main_content">
          <Common.Nav parent={'Task Editor'} page={'Cart'}/>
          <Common.Header logo={logo} logo_small={logo_small} 
            fname={this.props.user.first_name} {...this.state} {...this.props} save={this.save.bind(this)}/>
            <div className="row-fluid">
              <h3 className="heading">Cart</h3>
              <div className="row-fluid">
                <div className="span12">
                  <div className="row-fluid">
                    <button className="pull-right btn btn-primary btn-small" onClick={this.next.bind(this)}>Next &nbsp;
                        <i className="fa fa-arrow-right icon-white"></i>
                    </button>
                    <button className="pull-right margin-right-5 btn btn-primary btn-small" onClick={this.previous.bind(this)}>
                      <i className="fa fa-arrow-left icon-white"></i>&nbsp;
                        Previous
                    </button>
                  </div>
                  <form className="form-horizontal">
                    <fieldset className="margin-top-20">
                      <div>
                        <div className="well margin-top-neg-20">
                          <Common.TaskMeta {...this.props} {...this.state}
                              handleInputChange={this.handleInputChange.bind(this)} />
                        </div>
                        <div className="row-fluid">
                          <div className="span6">
                            <Cart.Instruction />
                            <div className="well">
                             <Cart.CartForm {...this.state}
                              handleInputChange={this.handleInputChange.bind(this)}/>
                            </div>
                          </div>
                          <div className="span6 margin-top-25">
                            <Cart.FBPreview {...this.state} />
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
         </div>
        <Common.Sidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)} />
      </div>
    )
  }
}

export default connect(mapStateToProps)(CartContainer)