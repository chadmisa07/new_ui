import React from 'react'

const CartForm = (props) => {
  return (
    <div>
      <div className="control-group">
        <label className="control-label width-190"><b>Response Text</b></label>
        <div className={props.cartResponseTextError + " controls margin-left-200"}>
          <input type="text" id="" className="span9" 
            value={props.responseText.message}
            onChange={props.handleInputChange} 
            name="responseText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Menu Selection Text</b></label>
        <div className={props.cartButtonResponseTextError + " controls margin-left-200"}>
          <input type="text" id="" className="span9" 
            value={props.buttonResponse.text}
            onChange={props.handleInputChange}
            name="menuSelectionText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Order More Button</b></label>
        <div className={props.cartOrderButtonError + " controls margin-left-200"}>
          <input type="text" id="" className="span9" 
            value={props.buttonResponse.buttons[0].title}
            onChange={props.handleInputChange}
            name="orderMoreText" />
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>View Cart Button</b></label>
        <div className={props.cartViewCartButtonError + " controls margin-left-200"}>
          <input type="text" id="" className="span9" 
            value={props.buttonResponse.buttons[1].title}
            onChange={props.handleInputChange}
            name="viewCartText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Checkout Button</b></label>
        <div className={props.cartCheckoutButtonError + " controls margin-left-200"}>
          <input type="text" id="" className="span9" 
            value={props.buttonResponse.buttons[2].title}
            onChange={props.handleInputChange}
            name="checkoutText"/>
        </div>
      </div>
    </div>
  )
}

CartForm.propTypes = {
  cartResponseTextError: React.PropTypes.string,
  responseText: React.PropTypes.object.isRequired,
  cartButtonResponseTextError: React.PropTypes.string,
  buttonResponse: React.PropTypes.object.isRequired,
  cartOrderButtonError: React.PropTypes.string,
  cartViewCartButtonError: React.PropTypes.string,
  cartCheckoutButtonError: React.PropTypes.string,
  handleInputChange: React.PropTypes.func.isRequired
}

export default CartForm