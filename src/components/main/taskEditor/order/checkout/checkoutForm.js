import React from 'react'

const EmailQuestions = (props) => {
  return (
    <div>
      <div className="control-group">
        <label className="control-label width-190"><b>Inquire Text</b></label>
        <div className={props.askReceiptError + " controls margin-left-200"}>
          <input type="text" id="" className="span11"
            onChange={props.handleInputChange}
            value={props.askReceipt.text} 
            name="askReceiptText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Input Email Text</b></label>
        <div className={props.askEmailError + " controls margin-left-200"}>
          <input type="text" id="" className="span11"
            onChange={props.handleInputChange}
            value={props.askEmail.text}
            name="askEmailText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Provide Email Text</b></label>
        <div className={props.provideEmailError + " controls margin-left-200"}>
          <input type="text" id="" className="span11"
            onChange={props.handleInputChange}
            value={props.provideEmail.message}
            name="provideEmailText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Email Error Text</b></label>
        <div className={props.emailErrorTextError + " controls margin-left-200"}>
          <textarea rows="2" className="span11"
            onChange={props.handleInputChange}
            value={props.emailError.message}
            name="emailErrorText"></textarea>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Retry Input Email</b></label>
        <div className={props.retryEmailError + " controls margin-left-200"}>
          <input type="text" id="" className="span11"
            onChange={props.handleInputChange}
            value={props.retryEmail.text}
            name="retryEmailText"/>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Email Saved Text</b></label>
        <div className={props.saveEmailError + " controls margin-left-200"}>
          <input type="text" id="" className="span11"
            onChange={props.handleInputChange}
            value={props.saveEmail.message}
            name="saveEmailText"/>
        </div>
      </div>
    </div>
  )
}

EmailQuestions.propTypes = {
  askReceiptError: React.PropTypes.string,
  askReceipt: React.PropTypes.object.isRequired,
  askEmailError: React.PropTypes.string,
  askEmail: React.PropTypes.object.isRequired,
  provideEmailError: React.PropTypes.string,
  provideEmail: React.PropTypes.object.isRequired,
  emailErrorTextError: React.PropTypes.string,
  emailError: React.PropTypes.object.isRequired,
  retryEmailError: React.PropTypes.string,
  retryEmail: React.PropTypes.object.isRequired,
  saveEmailError: React.PropTypes.string,
  saveEmail: React.PropTypes.object.isRequired
}


const CheckoutForm = (props) => {
  return (
    <div>
      <div className="control-group">
        <label className="control-label width-190"><b>Checkout Response Text</b></label>
        <div className={props.checkoutError + " controls margin-left-200"}>
          <textarea rows="2" className="span11"
            onChange={props.handleInputChange}
            value={props.checkout.text} 
            name="checkoutText"></textarea>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"><b>Do you want email?</b></label>
        <div className="controls margin-left-200">
          <label className="margin-top-7">
            <input type="checkbox" className="checkbox-position"
              checked={!props.askEmailChoice}
              onChange={props.handleSkipCheck}/>&nbsp;Skip asking for Email address
          </label>
        </div>
      </div>
      <div className="control-group">
        <label className="control-label width-190"></label>
        <div className="controls margin-left-200">
          <label>
            <input type="checkbox" className="checkbox-position"
              checked={props.askEmailChoice}
              onChange={props.handleAskEmailCheck} />&nbsp;Ask for Email address
          </label>
        </div>
      </div>
      { props.askEmailChoice? <EmailQuestions {...props} /> : null }
    </div>
  )
}

CheckoutForm.propTypes = {
  checkoutError: React.PropTypes.string,
  checkout: React.PropTypes.object.isRequired,
  askEmailChoice: React.PropTypes.bool.isRequired,
}

export default CheckoutForm







