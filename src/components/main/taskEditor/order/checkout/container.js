import React from 'react'
import { connect } from 'react-redux'
import * as Common from '../common/index'
import * as Checkout from './index'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as Request from '../request/request'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface,
    taskEditor: state.taskEditor,
    processName: "Order Management",
    data: state.task.data,
    taskMeta : state.task,
  }
}

class CheckoutContainer extends React.Component{

  constructor(props) {
    super(props)
    if (!this.props.taskEditor.editMode){
      if (props.taskEditor.checkoutContainer===null) {
         this.state = {
          askEmailChoice:true,
          sidebar: true,
          checkout: {
            text: "Proceed with checkout?",
            qr_type: 53,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~proceed_with_checkout",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id,
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          }, 
          askReceipt: {
            text: "Do you want an email receipt?",
            qr_type: 54,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~send_email_receipt",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~do_checkout",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          },
          askEmail: {
            text: "Receiving a receipt requires your email. Provide your email?",
            qr_type: 55,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~wait_email",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          },
          emailError:{
            text_type:  56,
            message: "I'm sorry please input a valid email address.",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          checkoutError: null,
          askReceiptError: null,
          askEmailError: null,
          emailErrorTextError: null,
          provideEmail: {
            text_type:  57,
            message: "Please provide your email address",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          provideEmailError: null,
          saveEmail: {
            text_type:  58,
            message: "Email successfully saved",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          saveEmailError: null,
          retryEmailError: null,
          retryEmail: {
            text: "Your email is invalid. Do you want to try again?",
            qr_type: 59,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~wait_email",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id,
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          }
        }
      } else {
        this.state = props.taskEditor.checkoutContainer
      }
    }else{
      this.state = props.taskEditor.checkoutContainer
    } 
  }

  updateStore(){
    this.props.dispatch({type:'UPDATE_CHECKOUT_CONTAINER', payload:this.state})

    this.props.dispatch({ 
      type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
      payload:{
        name: this.state.taskName,
        description: this.state.taskDescription
      }
    })
  }

  componentDidMount() {
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    this.updateStore()
  }

  componentWillMount(){
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(!this.state.checkout.text.replace(/\s/g, '').length){
      this.setState({checkoutError:"error"})
    } else if (!this.state.askReceipt.text.replace(/\s/g, '').length){
      this.setState({askReceiptError:"error"})
    } else if (!this.state.askEmail.text.replace(/\s/g, '').length){
      this.setState({askEmailError:"error"})
    } else if (!this.state.emailError.message.replace(/\s/g, '').length){
      this.setState({emailErrorTextError:"error"})
    }
  }
  
  componentWillUnmount(){
    this.updateStore()
  }

  handleAskEmailCheck(){
    this.setState({askEmailChoice:true})
    this.updateStore()
  }

  handleSkipCheck(){
    this.setState({askEmailChoice:false})
    this.updateStore()
  }

  taskNameError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_NAME_ERROR",
        payload: {
          taskNameError: error,
        }
    })
  }

  taskDescriptionError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_DESCRIPTION_ERROR",
        payload: {
          taskDescriptionError: error
        }
    })
  }

  handleInputChange = (e) => {
    if(e.target.name === "taskName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskNameError:"f_error"})
        this.taskNameError("f_error")
      } else {
        this.setState({taskNameError:null})
        this.taskNameError(null)
      }
      this.setState({taskName: e.target.value})
    }
    else if(e.target.name === "taskDescription"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskDescriptionError:"f_error"})
        this.taskDescriptionError("f_error")
      } else {
        this.setState({taskDescriptionError:null})
        this.taskDescriptionError(null)
      }
      this.setState({taskDescription: e.target.value})
    }
    else if(e.target.name === "checkoutText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({checkoutError:"f_error"})
      } else {
        this.setState({checkoutError:null})
      }
      let checkout = this.state.checkout
      checkout.text = e.target.value
      this.setState({ checkout: checkout })
    }
    else if(e.target.name === "askReceiptText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({askReceiptError:"f_error"})
      } else {
        this.setState({askReceiptError:null})
      }
      let askReceipt = this.state.askReceipt
      askReceipt.text = e.target.value
      this.setState({ askReceipt: askReceipt })
    }
    else if(e.target.name === "askEmailText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({askEmailError:"f_error"})
      } else {
        this.setState({askEmailError:null})
      }
      let askEmail = this.state.askEmail
      askEmail.text = e.target.value
      this.setState({ askEmail: askEmail })
    }
    else if(e.target.name === "provideEmailText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({provideEmailError:"f_error"})
      } else {
        this.setState({provideEmailError:null})
      }
      let provideEmail = this.state.provideEmail
      provideEmail.message = e.target.value
      this.setState({provideEmail: provideEmail})
    }
    else if(e.target.name === "emailErrorText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({emailErrorTextError:"f_error"})
      } else {
        this.setState({emailErrorTextError:null})
      }
      let emailError = this.state.emailError
      emailError.message = e.target.value
      this.setState({ emailError: emailError })
    }
    else if(e.target.name === "retryEmailText"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({retryEmailError:"f_error"})
      } else {
        this.setState({retryEmailError:null})
      }
      let retryEmail = this.state.retryEmail
      retryEmail.text = e.target.value
      this.setState({ retryEmail: retryEmail })
    } else {
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({saveEmailError:"f_error"})
      } else {
        this.setState({saveEmailError:null})
      }
      let saveEmail = this.state.saveEmail
      saveEmail.message = e.target.value
      this.setState({saveEmail: saveEmail})
    }
  }

  save() {
    if(this.state.taskName && this.state.taskDescription){
      if(this.props.taskEditor.editMode){
        this.updateStore()
        this.props.dispatch(Request.updateTask(this.props.taskEditor))
      }else{
        this.props.dispatch(Request.saveTask(this.props.taskEditor))
      }
    } else {
      toastr.error('Error', 'Please fill everything')
    }
  }

  previous(){
    if(this.state.saveEmailError === "f_error" || this.state.provideEmailError === "f_error" ||
      this.state.retryEmailError === "f_error" || this.state.emailErrorTextError === "f_error" ||
      this.state.askEmailError === "f_error" || this.state.askReceiptError === "f_error" ||
      this.state.checkoutError === "f_error"){
        toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/delivery")
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
        <Common.Header logo={logo} logo_small={logo_small} 
          fname={this.props.user.first_name} {...this.state} {...this.props} save={this.save.bind(this)}/>
        <div id="contentwrapper">
          <div className="main_content">
            <Common.Nav parent={'Task Editor'} page={'Checkout'}/>
            <div className="row-fluid">
              <div className="span12">
                <h3 className="heading">Checkout</h3>
                <div className="row-fluid">
                  <div className="span12">
                    <div className="row-fluid">
                      <button className="pull-right btn btn-success btn-small">Finish &nbsp;
                          <i className="fa fa-arrow-right icon-white"></i>
                      </button>
                      <button className="pull-right margin-right-5 btn btn-primary btn-small" onClick={this.previous.bind(this)}>
                        <i className="fa fa-arrow-left icon-white"></i>&nbsp;Previous
                      </button>
                    </div>
                    <form className="form-horizontal">
                      <fieldset className="margin-top-20">
                        <div>
                          <div className="well margin-top-neg-20">
                            <Common.TaskMeta {...this.props} {...this.state}
                              handleInputChange={this.handleInputChange.bind(this)}/>
                          </div>
                          <div className="row-fluid">
                            <div className="span6">
                              <Checkout.Instruction/>
                              <div className="well">
                                <Checkout.CheckoutForm {...this.state}
                                  handleInputChange={this.handleInputChange.bind(this)}
                                  handleAskEmailCheck={this.handleAskEmailCheck.bind(this)}
                                  handleSkipCheck={this.handleSkipCheck.bind(this)}/>
                              </div>
                            </div>
                            <div className="span6 margin-top-25">
                              <Checkout.FBPreview {...this.state} /> 
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <Common.Sidebar {...this.state} sidebarClick={this.sidebarClick.bind(this)} />
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(CheckoutContainer)