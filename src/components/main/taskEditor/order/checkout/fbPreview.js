import React from 'react'

const EmailQuestions = (props) => {
  return (
    <div>
      <div className="row-fluid">
        <span className="sender_message_bubble pull-left"> { props.askReceipt.text }</span>
      </div>
      <div className="row-fluid force-txt-center">
        <span className="quick_reply_btn ">Yes</span>
        <span className="quick_reply_btn ">No</span>
      </div>
      <div className="row-fluid">
        <span className="sender_message_bubble pull-left">
         { props.askEmail.text }
        </span>
      </div>
      <div className="row-fluid force-txt-center">
        <span className="quick_reply_btn ">Yes</span>
        <span className="quick_reply_btn ">No</span>
      </div>
      <div className="row-fluid">
        <span className="sender_message_bubble pull-left">{props.provideEmail.message}</span>
      </div>
      <div className="row-fluid">
        <span className="sender-message-bubble pull-right">sample@mail</span>
      </div>
      <div className="row-fluid">
        <span className="sender_message_bubble pull-left">{ props.emailError.message }</span>
      </div>
      <div className="row-fluid">
        <span className="sender_message_bubble pull-left">{props.retryEmail.text}</span>
      </div>
      <div className="row-fluid force-txt-center">
        <span className="quick_reply_btn ">Yes</span>
        <span className="quick_reply_btn ">No</span>
      </div>
      <div className="row-fluid">
        <span className="sender-message-bubble pull-right">sample@gmail.com</span>
      </div>
      <div className="row-fluid">
        <span className="sender_message_bubble pull-left">{props.saveEmail.message}</span>
      </div>
    </div>
  )
}

EmailQuestions.propTypes = {
  saveEmail: React.PropTypes.object.isRequired,
  retryEmail: React.PropTypes.object.isRequired,
  emailError: React.PropTypes.object.isRequired,
  provideEmail: React.PropTypes.object.isRequired,
  askEmail: React.PropTypes.object.isRequired,
  askReceipt: React.PropTypes.object.isRequired
}


const FBPreview = (props) => {
  return (
    <div className="margin-top-neg-35">
      <div className="row-fluid">
        <span className="font-color-blue">What your bot looks like when run!</span>
      </div>
      <div className="well clear-background">
        <div className="row-fluid">
          <div className="span12">
            <div className="row-fluid">
              <span className="sender-message-bubble pull-right">Checkout</span>
            </div>
            <div className="row-fluid">
              <span className="sender_message_bubble pull-left">{ props.checkout.text }</span>
            </div>
            <div className="row-fluid force-txt-center">
              <span className="quick_reply_btn ">Yes</span>
              <span className="quick_reply_btn ">No</span>
            </div>
            { props.askEmailChoice? <EmailQuestions {...props} /> : null }
          </div>
        </div>
      </div>
    </div>
  )
}
FBPreview.propTypes = {
checkout: React.PropTypes.object.isRequired,
askEmailChoice: React.PropTypes.bool.isRequired
}

export default FBPreview