import React from 'react'
import { Modal } from 'react-bootstrap'

const LocationModal = (props) => {
  return (
    <Modal show={props.showLocationModal} onHide={props.closeLocationModal} backdrop={false} className="clear-padding-right">
      <div className="modal-header modal-header-new">
        <h3>Add Location</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <form className="form-horizontal">
              <fieldset>
                <div className="control-group">
                  <div className={props.locationNameError}>
                    <label><b>Location Name</b></label>
                    <input type="text" className="span12" placeholder="Location Name" 
                      maxLength={80} 
                      name="locationName" 
                      onChange={props.handleInputChange}
                      value={props.locationName}/>
                    </div>
                </div>
                <div className="control-group">
                  <div className={props.locationDescriptionError}>
                    <label><b>Description</b></label>
                    <input type="text" className="span12" placeholder="Description" 
                      maxLength={80} 
                      name="description" 
                      onChange={props.handleInputChange}
                      value={props.description} />
                    </div>
                </div>
                <div className="control-group">
                  <div className={props.location_image_url_error}>
                    <label><b>Image</b></label>
                      <input type="file" accept="image/*" 
                        name="image_url" 
                        onChange={props.handleImageChange}/>
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.location_url_error}>
                    <label><b>Location URL</b></label>
                    <input type="text" className="span12" placeholder="Location URL" 
                      name="location_url" 
                      onChange={props.handleInputChange}
                      value={props.location_url}/>
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.contact_info_error}>
                    <label><b>Contact Info</b></label>
                    <input type="number" className="span12" placeholder="Contact Info" 
                      name="contact_info" 
                      onChange={props.handleInputChange}
                      value={props.contact_info} 
                      onInput={(e)=>{ 
                        e.target.value = e.target.value.toString().slice(0,11)
                      }}
                      min={0}/>
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.visit_page_error}>
                    <label><b>Visit Page Button</b></label>
                    <input type="text" className="span12" placeholder="Visit Page Button" 
                      maxLength={20} 
                      name="visit_page" 
                      onChange={props.handleInputChange}
                      value={props.visit_page} />
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.call_btn_error}>
                    <label><b>Call Button</b></label>
                    <input type="text" className="span12" placeholder="Call Button" 
                      maxLength={20} 
                      name="call_btn" 
                      onChange={props.handleInputChange}
                      value={props.call_btn}/>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="control-group">
          <div className="pull-right">
            <button type="button" className="btn btn-primary btn-small" 
              onClick={props.saveMenuInterface} disabled={props.savingImage}> {props.savingImage?"Saving":"Save"}</button>
            <button type="button" className="btn btn-danger btn-small" 
              onClick={props.deleteInterface} disabled={props.savingImage}>Delete</button>
            <button type="button" className="btn btn-default btn-small" data-dismiss="modal"
              onClick={props.closeLocationModal} disabled={props.savingImage}>Close</button>
          </div>
        </div>
      </div>
    </Modal>
  )
}
LocationModal.propTypes = {
  saveMenuInterface: React.PropTypes.func.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  closeLocationModal: React.PropTypes.func.isRequired,
  savingImage: React.PropTypes.bool.isRequired,
  call_btn_error: React.PropTypes.string,
  handleInputChange: React.PropTypes.func.isRequired,
  call_btn: React.PropTypes.string.isRequired,
  visit_page_error: React.PropTypes.string,
  visit_page: React.PropTypes.string.isRequired,
  contact_info_error: React.PropTypes.string,
  contact_info: React.PropTypes.string.isRequired,
  location_url_error: React.PropTypes.string,
  location_url: React.PropTypes.string.isRequired,
  location_image_url_error: React.PropTypes.string,
  handleImageChange: React.PropTypes.func.isRequired,
  locationDescriptionError: React.PropTypes.string,
  description: React.PropTypes.string.isRequired,
  locationNameError: React.PropTypes.string,
  locationName: React.PropTypes.string.isRequired
}

export default LocationModal


