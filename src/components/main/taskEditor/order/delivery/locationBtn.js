import React from 'react'
import { Link } from 'react-router'

const LocationButton = (props) => {
  return (
    <div className="span3">
      <div className="black-box text-center product-btn">
        <button type="button" className="btn btn-primary btn-small" onClick={props.openLocationModal.bind(this)}
          data-toggle="modal" data-backdrop="static" href="#showLocationModal">
          <i className="fa fa-edit icon-white"></i>&nbsp;Edit
        </button>
        <div className="padding-top-15">
          <button type="button" className="btn btn-danger btn-small" onClick={props.deleteInterface.bind(this)}>
            <i className="fa fa-remove icon-white"></i>
          </button>
        </div>
        <div className="padding-top-15">
          {"Location " + (props.index+1).toString()}
        </div>
      </div>
      <div className="padding-top-15 text-center">
        {
          props.location.location.name===""? 
            <p>-</p>
            :
            <Link href="#" onClick={props.openPreviewModal}>
              {props.location.location.name}
            </Link>
        }
      </div>
    </div>
  )
}

LocationButton.propTypes = {
  openLocationModal: React.PropTypes.func.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  openPreviewModal: React.PropTypes.func.isRequired,
  index: React.PropTypes.number.isRequired
}

export default LocationButton
