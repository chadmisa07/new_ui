import React from 'react'
import { Button, Modal } from 'react-bootstrap'

const WarningModal = (props) => {
  return (
     <Modal show={props.openWarningModal} backdrop={false} onHide={props.closeWarningModal}>
      <Modal.Header>
        <Modal.Title><span className="red-text">Warning!</span></Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <span>There are entries that are blank, proceed anyway?</span>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.saveMenuInterface} bsStyle="primary" disabled={props.savingImage}>Yes</Button>
        <Button onClick={props.closeWarningModal} disabled={props.savingImage}>Cancel</Button>
      </Modal.Footer>
    </Modal>
  )
}

export default WarningModal