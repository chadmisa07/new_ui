import React from 'react'
import { Thumbnail, ButtonGroup } from 'react-bootstrap'

const Location = (props) => {
  return (
    <div>
      <Thumbnail src={props.previewInterface.location.image_url} 
        className="fb-button">
        <h3 className="fb-title">{props.previewInterface.location.name}</h3>
        <p className="fb-description">
          {props.previewInterface.location.description}
        </p>
        <p className="product-url">{props.previewInterface.location.location_url}</p>
          <ButtonGroup vertical block>
            <button className="fb-button btn btn-default span12">{props.previewInterface.visit_page}</button>
            <button className="fb-button btn btn-default span12">{props.previewInterface.call_btn}</button>
            <button className="fb-button btn btn-default span12">Share</button>
          </ButtonGroup>
      </Thumbnail>
    </div>
  )
}

Location.propTypes = {
  previewInterface: React.PropTypes.object
}

export default Location