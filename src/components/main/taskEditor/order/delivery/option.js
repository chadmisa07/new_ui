import React from 'react'

const DeliveryOption = (props) => {
  return (
    <div className="control-group">
      <div className="margin-left-20">
        <label className="margin-top-7">
          <input type="checkbox" className="checkbox-position" 
            checked={props.skipChecked}
            onChange={props.handleSkipCheck}/>&nbsp;Skip this
        </label>
        <label className="margin-top-7">
          <input type="checkbox" className="checkbox-position"
            checked={props.pickUpChecked}
            onChange={props.handlePickupCheck}/>&nbsp;Pick-up only
        </label>
        <label className="margin-top-7">
          <input type="checkbox" className="checkbox-position"
            checked={props.deliveryChecked}
            onChange={props.handleDeliveryCheck}/>&nbsp;Delivery only
        </label>
      </div>
    </div>
  )
}

DeliveryOption.propTypes = {
  skipChecked: React.PropTypes.bool.isRequired,
  pickUpChecked: React.PropTypes.bool.isRequired,
  deliveryChecked: React.PropTypes.bool.isRequired,
  handleSkipCheck: React.PropTypes.func.isRequired,
  handlePickupCheck: React.PropTypes.func.isRequired,
  handleDeliveryCheck: React.PropTypes.func.isRequired
}

export default DeliveryOption

