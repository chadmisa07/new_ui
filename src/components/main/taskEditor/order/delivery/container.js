import React from 'react'
import { connect } from 'react-redux'
import { toastr } from 'react-redux-toastr'
import { browserHistory } from 'react-router'
import * as Common from '../common/index'
import * as Delivery from './index'
import validator from 'validator'
import update from 'react-addons-update'
import * as Request from '../request/request'
import cloudinary from 'cloudinary'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface,
    taskEditor: state.taskEditor,
    processName: "Order Management",
    data: state.task.data,
    taskMeta : state.task,
  }
}

let count = 2
let previewImage = null
let current_index = -1

class DeliveryContainer extends React.Component {

  constructor(props) {
    super(props)
    if(!this.props.taskEditor.editMode){
      if (props.taskEditor.deliveryContainer===null){
        this.state = {
          savingImage: false,
          interfaces:[
            this.newLocationInterface(), 
            this.newLocationInterface()
          ],
          pickUpChecked: true,
          preview_url: null,
          skipChecked: false,
          deliveryChecked: false,
          locationName: "",
          description: "",
          image_url: "",
          location_url: "",
          contact_info: "",
          visit_page: "",
          call_btn: "",
          current_index: "",
          addMode: true,
          showLocationModal: false,
          showPreviewModal: false,
          previewInterface: null,
          locationNameError: null,
          locationDescriptionError: null,
          location_image_url_error: null,
          location_url_error: null,
          contact_info_error: null,
          visit_page_error: null,
          call_btn_error: null,
          invalidUrl: false,
          uploadImage: false,
          sidebar: true,
        }
      } else {
        this.state = props.taskEditor.deliveryContainer
      }
    } else {
      this.state = props.taskEditor.deliveryContainer
    }
  }

  updateStore(){
    this.props.dispatch({type:'UPDATE_DELIVERY_CONTAINER', payload:this.state})
    
    this.props.dispatch({ 
      type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
      payload:{
        name: this.state.taskName,
        description: this.state.taskDescription
      }
    })
  }

  componentWillMount(){
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(this.props.taskEditor.editMode){
      count = this.props.data.location_interface.length
    }else{
      count = this.state.interfaces.length
    }
  }

  componentDidMount() {
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  uncheckAll(){
    count = 0
    this.setState({
      pickUpChecked: false,
      skipChecked: false,
      deliveryChecked: false
    })
  }

  handleImageChange = (event) => {
    this.setState({image_url:event.target.files[0]})
    this.setState({location_image_url_error: null})
    this.setState({uploadImage:true})
  }

  mapItem(location){
    this.setState({
      locationName: location.location.name,
      description: location.location.description,
      image_url: location.location.image_url,
      location_url: location.location.location_url,
      contact_info: location.location.contact_info,
      visit_page: location.visit_page,
      call_btn: location.call_btn,
    })
  }

  openLocationModal(location, index) {
    current_index = index
    this.setState({addMode:false, current_index:index})
    this.setState({button_id:location.id})
    this.setState({location_id:location.location.id})
    this.mapItem(location)
    this.setState({ showLocationModal: true })
    this.setState({image_url:location.location.image_url})
  }

  closeLocationModal() {
    this.setState({ showLocationModal: false })
    this.setState({locationNameError: null})
    this.setState({locationDescriptionError: null})
    this.setState({location_image_url_error: null})
    this.setState({location_url_error: null})
    this.setState({contact_info_error: null})
    this.setState({visit_page_error: null})
    this.setState({call_btn_error: null})
    this.setState({invalidUrl: false})
  }

  openPreviewModal(location) {
    // try {
    //   let reader = new FileReader();
    //   reader.readAsDataURL(location.location.preview_url)
    //   reader.onloadend = () => {
    //     previewImage = reader.result
        this.setState({ previewInterface: location })
        this.setState({ showPreviewModal: true })
    //   }
    // } catch (error) {
    //   this.setState({savingImage:false})
    // }
  }

  closePreviewModal() {
    this.setState({ showPreviewModal: false })
  }

  addLocationButton() {
    if (count < 10) {
      let interfaces = this.state.interfaces
      interfaces.splice(count, 0, this.newLocationInterface())
      this.setState({interfaces:interfaces})
      count++
    } else {
      toastr.info('Alert', 'Unable to add more location. \nMaximum of 10 locations reached.')
    }
  }

  saveMenuInterface() {
    if(!this.state.locationName.replace(/\s/g, '').length){
      this.setState({locationNameError: "f_error"})
      toastr.error('Error','Please enter a location name')
    }
    else if(!this.state.description.replace(/\s/g, '').length){
      this.setState({locationDescriptionError: "f_error"})
      toastr.error('Error','Please enter a description')
    }
    else if(!this.state.image_url){
      toastr.error('Error', 'Please select an image')
      this.setState({location_image_url_error: "f_error"})
    }
    else if(this.state.invalidUrl === true || !this.state.location_url.replace(/\s/g, '').length){
      this.setState({location_url_error: "f_error"})
      toastr.error('Error','Please enter a valid url')
    }
    else if(!this.state.contact_info.replace(/\s/g, '').length || this.state.contact_info.replace(/\s/g, '').length < 11){
      this.setState({contact_info_error: "f_error"})
      toastr.error('Error','Please enter the contact information correctly.')
    }
    else if(!this.state.visit_page.replace(/\s/g, '').length){
      this.setState({visit_page_error: "f_error"})
      toastr.error('Error','Please enter a visit page text')
    }
    else if(!this.state.call_btn.replace(/\s/g, '').length){
      this.setState({call_btn_error: "f_error"})
      toastr.error('Error','Please enter a call button text')
    }
    else{
      if(!this.state.uploadImage && this.state.image_url){
        this.setState({savingImage:true})
        let interfaces = this.state.interfaces
        interfaces[current_index] = this.getUserInput()
        this.addLocation(interfaces)
      }else{
        this.setState({savingImage:true})
        let interfaces = this.state.interfaces
        interfaces[current_index] = this.getUserInput()
        this.setState({uploadImage:true})
        try {
          this.setState({location_image_url_error: null})
          let reader = new FileReader()
          reader.readAsDataURL(interfaces[current_index].location.image_url)
          reader.onloadend = () => {
            this.uploadImage(reader.result, interfaces)
          }
        } catch (error) {
          if(!this.state.image_url){
            toastr.error('Error', 'Please select an image')
            this.setState({location_image_url_error: "f_error"})
          }
          this.setState({savingImage:false})
          this.setState({uploadImage:false})
        }
      }
    }
  }

  addLocation(interfaces){
    this.setState({interfaces:interfaces})
    this.closeLocationModal()
    this.setState({savingImage:false})
    this.setState({uploadImage:false})
    this.updateStore()
  }

  uploadImage(image, interfaces) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        interfaces[current_index].location.image_url = result.secure_url
        this.setState({interfaces:interfaces})
        this.closeLocationModal()
        this.setState({savingImage:false})
        this.setState({uploadImage:false})
        this.updateStore()
      } else {
        toastr.error("Error", "Unable to upload image")
        this.closeLocationModal()
        this.setState({savingImage:false})
        this.setState({uploadImage:false})
      }
    })
  }

  deleteInterface() {
    let interfaces = this.state.interfaces
    interfaces.splice(this.state.current_index, 1)
    this.setState({ interfaces: interfaces })
    this.setState({ addMode: true })
    count--
    this.closeLocationModal()
    toastr.success('Success', 'Successfully deleted interface')
  }

  deleteInterfaceAtIndex(index) {
    let interfaces = this.state.interfaces
    interfaces.splice(index, 1)
    this.setState({ interfaces: interfaces })
    count--
    toastr.success('Success', 'Successfully deleted interface')
  }

  validation = (event, field) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({field:"f_error"})
      alert(field)
    } else {
      this.setState({locationNameError:null})
    }
  }

  handleTaskNameChange = (event) => {
    this.setState({taskName:event.target.value})
  }

  handleTaskDescriptionChange = (event) => {
    this.setState({taskDescription:event.target.value})
  }

  taskNameError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_NAME_ERROR",
        payload: {
          taskNameError: error,
        }
    })
  }

  taskDescriptionError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_DESCRIPTION_ERROR",
        payload: {
          taskDescriptionError: error
        }
    })
  }

  handleInputChange = (e) => {
    if(e.target.name === "taskName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskNameError:"f_error"})
        this.taskNameError("f_error")
      } else {
        this.setState({taskNameError:null})
        this.taskNameError(null)
      }
      this.setState({taskName: e.target.value})
    }else if(e.target.name === "taskDescription"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskDescriptionError:"f_error"})
        this.taskDescriptionError("f_error")
      } else {
        this.setState({taskDescriptionError:null})
        this.taskDescriptionError(null)
      }
      this.setState({taskDescription: e.target.value})
    }else if(e.target.name === "locationName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({locationNameError:"f_error"})
      } else {
        this.setState({locationNameError:null})
      }
    }else if(e.target.name === "description"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({locationDescriptionError:"f_error"})
      } else {
        this.setState({locationDescriptionError:null})
      }
    }else if(e.target.name === "image_url"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({location_image_url_error:"f_error"})
      } else {
        this.setState({location_image_url_error:null})
        this.setState({image_url: e.target.files[0]})
      }
    }else if(e.target.name === "location_url"){
      if(validator.isURL(e.target.value)){
        this.setState({invalidUrl: false})
        this.setState({location_url_error:null})
      }else{
         this.setState({invalidUrl: true})
         this.setState({location_url_error:"f_error"})
      }
    }else if(e.target.name === "contact_info"){
      if(!e.target.value.replace(/\s/g, '').length || e.target.value.replace(/\s/g, '').length < 11){
        this.setState({contact_info_error:"f_error"})
      } else {
        this.setState({contact_info_error:null})
      }
    }else if(e.target.name === "visit_page"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({visit_page_error:"f_error"})
      } else {
        this.setState({visit_page_error:null})
      }
    }
    else{
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({call_btn_error:"f_error"})
      } else {
        this.setState({call_btn_error:null})
      }
    }
    this.setState({[e.target.name]:e.target.value})
  }

  getUserInput() {
    const userId = this.props.user.id
    return ({
      location: {
        "id": this.state.location_id,
        "name": this.state.locationName,
        "description": this.state.description,
        "image_url": this.state.image_url,
        "preview_url": this.state.image_url,
        "location_url": this.state.location_url,
        "contact_info": this.state.contact_info,
        "created_by": userId,
        "updated_by": userId
      },
      visit_page: this.state.visit_page,
      call_btn: this.state.call_btn,
      id: this.state.button_id,
      created_by: userId,
      updated_by: userId
    })
  }

  handlePickupCheck(){
    this.uncheckAll()
    let data
    let viewPickupLocation = this.props.taskEditor.operationContainer.persistentMenu.buttons.find(x=>x.payload === "/pickup~~locs")
    if(this.props.taskEditor.operationContainer.viewCartRemove && this.props.taskEditor.operationContainer.changeEmailRemove){
      if(!viewPickupLocation){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[1,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
      }
    } else if (this.props.taskEditor.operationContainer.viewCartRemove){
      if(!viewPickupLocation){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
      }
    } else if(this.props.taskEditor.operationContainer.changeEmailRemove){
      if(!viewPickupLocation){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
      }
    } else {
      if(!viewPickupLocation){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[3,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
      }
    }
    this.setState({pickUpChecked:true})
    if(!viewPickupLocation){
     this.props.dispatch({type:'UPDATE_OPERATION_CONTAINER', payload: data})
    }
    this.props.dispatch({type:"PICKUP_MODE_ON", payload:true})
    this.updateStore()
  }

  handleSkipCheck(){
    this.uncheckAll()
    let data 
    if(!this.props.taskEditor.delivery){
      if(this.props.taskEditor.operationContainer.changeEmailRemove){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[1,1]]}}})
      } else {
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}})
      }
      console.log(JSON.stringify(data))
      this.setState({interfaces: []})
      this.props.dispatch({type:'UPDATE_OPERATION_CONTAINER', payload: data})
      this.updateStore()
    }
    this.setState({skipChecked:true})
    this.props.dispatch({type:"SKIP_MODE_ON", payload:true})
  }

  handleDeliveryCheck(){
    this.uncheckAll()
    let data 
    if(!this.props.taskEditor.skip){
      if(this.props.taskEditor.operationContainer.changeEmailRemove){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[1,1]]}}})
      } else {
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}})
      }
      console.log(JSON.stringify(data))
      this.setState({interfaces: []})
      this.props.dispatch({type:'UPDATE_OPERATION_CONTAINER', payload: data})
      this.updateStore()
    }
    
    this.setState({deliveryChecked:true})
    this.props.dispatch({type:"DELIVERY_MODE_ON", payload:true})
  }

  newLocationInterface() {
    const userId = this.props.user.id
    return ({
      location: {
        "name": "",
        "id": "",
        "description": "",
        "image_url": "",
        "preview_url": "",
        "location_url": "",
        "contact_info": "",
        "created_by": userId,
        "updated_by": userId
      },
      created: userId,
      update_by: userId,
      visit_page: "",
      call_btn: "",
      "id": ""
    })
  }

  save() {
    if(this.props.taskEditor.editMode){
      this.updateStore()
      this.props.dispatch(Request.updateTask(this.props.taskEditor))
    } else {
      this.props.dispatch(Request.saveTask(this.props.taskEditor))
    }
  }

  next(){
    if(this.state.pickUpChecked && count === 0){
      toastr.error('Error', 'Please add a location')
    }else if(this.state.interfaces.find(x=>x.location.name === "")){
      toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/checkout")
    }
  }

  previous(){
    if(this.state.pickUpChecked && count === 0){
      toastr.error('Error', 'Please add a location')
    }else if(this.state.interfaces.find(x=>x.location.name === "")){
      toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/cart")
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {

    const locationBtns = this.state.interfaces.map((locationInterface, index) => {
      return (
        <Delivery.LocationButton index={index} location={locationInterface} key={index}
          openLocationModal={this.openLocationModal.bind(this, locationInterface, index)}
          openPreviewModal={this.openPreviewModal.bind(this, locationInterface)}
          deleteInterface={this.deleteInterfaceAtIndex.bind(this, index)} />
      )
    })

    return (
       <div id="contentwrapper" className={this.state.sidebar?"":"sidebar_hidden"}>
        <div className="main_content">
          <Common.Nav parent={'Task Editor'} page={'Delivery / Pick-up'}/>
          <Common.Header logo={logo} logo_small={logo_small} 
            fname={this.props.user.first_name} {...this.state} {...this.props} save={this.save.bind(this)}/>
          <div className="row-fluid">
            <h3 className="heading">Delivery / Pick-up</h3>
              <div className="span12">
                <div className="row-fluid">
                  <button className="pull-right btn btn-primary btn-small">Next &nbsp;
                      <i className="fa fa-arrow-right icon-white"></i>
                  </button>
                  <button className="pull-right margin-right-5 btn btn-primary btn-small">
                    <i className="fa fa-arrow-left icon-white"></i>&nbsp;Previous
                  </button>
                </div>
              <form className="form-horizontal">
                <fieldset className="margin-top-20">
                  <div>
                    <div className="well margin-top-neg-20">
                      <Common.TaskMeta {...this.props} {...this.state}
                        handleInputChange={this.handleInputChange.bind(this)}/>
                    </div>
                    <div className="row-fluid">
                      <Delivery.Instruction />
                    </div><br/>
                    <div className="row-fluid">
                     <b>Choose order fulfillment options:</b>
                    </div>
                    <div className="row-fluid">
                      <Delivery.DeliveryOption {...this.state}
                        handlePickupCheck={this.handlePickupCheck.bind(this)}
                        handleSkipCheck={this.handleSkipCheck.bind(this)}
                        handleDeliveryCheck={this.handleDeliveryCheck.bind(this)} />
                    </div>
                    <div className="row-fluid">
                      { 
                        this.state.pickUpChecked? 
                          <p className="pull-left"><b>Pickup Locations</b></p> 
                        :
                          null
                      }
                    </div>
                    <div className="row-fluid little-padding-top little-padding-left items">
                      <div className="span11">
                         {
                            this.state.pickUpChecked?
                            <div>
                              {locationBtns}
                              <div className="span3">
                                <div className="black-box text-center product-btn">
                                  <button type="button" className="btn btn-primary btn-small" 
                                    onClick={this.addLocationButton.bind(this)} disabled={count>=10}>
                                    <i className="fa fa-plus icon-white"></i>&nbsp;Add
                                  </button>
                                </div>
                              </div>
                            </div>
                            : null
                          }
                      </div>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
        <Common.Sidebar {...this.state} 
          locationInterfaces={this.state.interfaces.find(x=>x.location.name === "") ? 0 : 1} 
          locationCount={this.state.pickUpChecked ? count : this.state.skipChecked ? 1 : this.state.deliveryChecked ? 1 : 0}
          sidebarClick={this.sidebarClick.bind(this)}/>
        <Delivery.LocationModal {...this.state}
          closeLocationModal={this.closeLocationModal.bind(this)}
          saveMenuInterface={this.saveMenuInterface.bind(this)}
          handleInputChange={this.handleInputChange.bind(this)}
          deleteInterface={this.deleteInterface.bind(this)}
          handleImageChange={this.handleImageChange.bind(this)} />
        <Delivery.PreviewModal {...this.state} previewImage={previewImage}
          closePreviewModal={this.closePreviewModal.bind(this)} />
      </div>
    )
  }
}

export default connect(mapStateToProps)(DeliveryContainer)