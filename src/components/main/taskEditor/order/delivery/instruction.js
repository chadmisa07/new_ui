import React from 'react'

const Instruction = (props) => {
  return (
    <div>
      <div className="row-fluid">
        <span>Prior to checkout you can ask the users for delivery or pickup options.</span>
      </div>
      <div className="row-fluid">
        <span>You can chose to skip this if you do not have plans to fulfill the order this way.</span>
      </div>
      <div className="row-fluid">
        <span>
          (Such as you plan to send the product via email or other form of fulfillment). Learn <a>more</a>.
        </span>
      </div>
    </div>
  )
}

export default Instruction
