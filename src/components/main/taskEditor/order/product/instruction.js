import React from 'react'

const Instruction = (props) => {
  return (
    <div>
      <div className="row-fluid">
        <span>This is where you will show the users product choices.</span>
      </div>
      <div className="row-fluid">
        <span>The products will be displayed from left to right in carousel form. Learn <a>more</a>.</span>
      </div>
      <div className="row-fluid">
        <span>
          You will need to enter the product information for each choice. We have prepared 2 products for you to edit.
        </span>
      </div>
    </div>
  )
}

export default Instruction
