import React from 'react'
import { Thumbnail } from 'react-bootstrap'



const MenuInterface = (props) => {

  return (
    <div>
      <Thumbnail src={props.previewInterface.product.image_url} 
        className="fb-button">
        <h3 className="fb-title">{props.previewInterface.product.name}</h3>
        <p className="fb-description">
          {"₱ " + props.previewInterface.product.price + " " + props.previewInterface.product.description}
        </p>
        <p className="product-url">{props.previewInterface.product.item_url}</p>
          <div className="btn-group-vertical btn-block">
            <button className="fb-button btn btn-default span12">{props.previewInterface.add_button}</button>
            <button className="fb-button btn btn-default span12">{props.previewInterface.view_description}</button>
          </div>
      </Thumbnail>
    </div>
  )
}
MenuInterface.propTypes = {
  previewInterface: React.PropTypes.object.isRequired
}

export default MenuInterface