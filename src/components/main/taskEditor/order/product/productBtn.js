import React from 'react'
import { Link } from 'react-router'

const ProductButton = (props) => {
  return (
    <div className="span3">
      <div className="black-box text-center product-btn">
        <button type="button" className="btn btn-primary btn-small"
          data-toggle="modal" data-backdrop="static"
          onClick={props.openProductModal}>
          <i className="fa fa-edit icon-white"></i>&nbsp;Edit
        </button>
        <div className="padding-top-15">
          <button type="button" className="btn btn-danger btn-small" onClick={props.deleteInterface}>
            <i className="fa fa-remove icon-white"></i>
          </button>
        </div>
        <div className="padding-top-15">
          {"Product " + (props.index+1).toString()}
        </div>
      </div>
      <div className="padding-top-15 text-center">
        {
          props.menuInterface.product.name===""? 
            <p>-</p>
            :
            <Link href="#" onClick={props.openPreviewModal}>
            {props.menuInterface.product.name}
          </Link>
        }
      </div>
    </div>
  )
}

ProductButton.propTypes = {
  openProductModal: React.PropTypes.func.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  index: React.PropTypes.number.isRequired,
  openPreviewModal: React.PropTypes.func.isRequired
}

export default ProductButton
