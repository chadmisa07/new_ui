import React from 'react'
import { Modal } from 'react-bootstrap'

const ProductModal = (props) => {
  return (
    <Modal show={props.showProductModal} onHide={props.closeProductModal} backdrop={false} className="clear-padding-right">
      <div className="modal-header modal-header-new" >
        <h3>Add Product Menu</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <form className="form-horizontal">
              <fieldset>
                <div className="control-group">
                  <div className={props.nameError}>
                    <label className=""><b>Product Name</b></label>
                    <input type="text" id="" className="span12" placeholder="Product Name" maxLength={80}
                      value={props.name}
                      onChange={props.handleInputChange}
                      name="name" />
                    </div>
                </div>
                <div className="control-group">
                  <label className=""><b>Product Code</b></label>
                  <input type="text" id="" className="span12" placeholder="Product Code" 
                    value={props.code}
                    onChange={props.handleInputChange}
                    name="code"/>
                </div>
                <div className="control-group">
                  <div className={props.descriptionError}>
                    <label className=""><b>Product Description</b></label>
                    <input type="text" id="" className="span12" placeholder="Describe your product here" maxLength={80 - props.descriptionLength}
                      value={props.description}
                      onChange={props.handleInputChange}
                      name="description"/>
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.priceError}>
                    <label className=""><b>Price</b></label>
                    <input type="number" id="" className="span12" placeholder="0.00" min="1"
                      value={props.price}
                      onChange={props.handleInputChange}
                      name="price"/>
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.image_url_error}>
                    <label className=""><b>Image</b></label>
                      <input type="file" id="" accept="image/*" 
                        value={props.image}
                        onChange={props.handleInputChange}
                        name="image_url" />
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.item_url_error}>
                    <label className=""><b>Product URL</b></label>
                    <input type="text" id="" className="span12" placeholder="Enter detail product URL to view in View More"
                      value={props.item_url}
                      onChange={props.handleInputChange} 
                      name="item_url"  />
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.add_button_error}>
                    <label className=""><b>"Add to Cart" Button Text</b></label>
                    <input type="text" id="" className="span12" placeholder="Add to cart" maxLength={20} 
                      value={props.add_button}
                      onChange={props.handleInputChange}
                      name="add_button"/>
                  </div>
                </div>
                <div className="control-group">
                  <div className={props.view_description_error}>
                    <label className=""><b>"View More" Button Text. This jumps to the product URL defined above.</b></label>
                    <input type="text" id="" className="span12" placeholder="View More" maxLength={20}
                      value={props.view_description}
                      onChange={props.handleInputChange}
                      name="view_description" />
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
     <div className="modal-footer">
       <div className="control-group">
          <div className="pull-right">
            <button type="button" className="btn btn-primary"
              onClick={props.saveMenuInterface} disabled={props.savingImage}>
                {props.savingImage?"Saving":"Save"}
            </button>
            <button type="button" className="btn btn-danger" 
              onClick={props.deleteInterface} disabled={props.savingImage}>
                Delete
            </button>
            <button type="button" className="btn btn-default" data-dismiss="modal"
              onClick={props.closeProductModal.bind(this)} disabled={props.savingImage}>
                Close
            </button>
          </div>
        </div>
     </div>
    </Modal>
  )
}

ProductModal.propTypes = {
  handleInputChange: React.PropTypes.func.isRequired,
  closeProductModal: React.PropTypes.func,
  savingImage: React.PropTypes.bool.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  saveMenuInterface: React.PropTypes.func.isRequired,
  view_description_error: React.PropTypes.string,
  view_description: React.PropTypes.string.isRequired,
  handleViewDescriptionChange: React.PropTypes.func,
  add_button_error: React.PropTypes.string,
  add_button: React.PropTypes.string.isRequired,
  handleAddBtnChange: React.PropTypes.func,
  item_url_error: React.PropTypes.string,
  item_url: React.PropTypes.string.isRequired,
  handleItemChange: React.PropTypes.func,
  image_url_error: React.PropTypes.string,
  handleImageChange: React.PropTypes.func,
  priceError: React.PropTypes.string,
  handlePriceChange: React.PropTypes.func,
  descriptionError: React.PropTypes.string,
  description: React.PropTypes.string.isRequired,
  handleDescriptionChange: React.PropTypes.func,
  codeError: React.PropTypes.string,
  code: React.PropTypes.string.isRequired,
  handleCodeChange: React.PropTypes.func,
  nameError: React.PropTypes.string,
  name: React.PropTypes.string.isRequired,
  handleNameChange: React.PropTypes.func,
  showProductModal: React.PropTypes.bool.isRequired,
}

export default ProductModal


