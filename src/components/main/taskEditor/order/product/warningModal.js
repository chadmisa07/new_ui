import React from 'react'
import { Modal } from 'react-bootstrap'

const WarningModal = (props) => {
  return (
     <Modal show={props.openWarningModal} backdrop={false} onHide={props.closeWarningModal} className="clear-padding-right">
      <div className="modal-header modal-header-new" >
          <button className="close" data-dismiss="modal" onClick={props.closeWarningModal.bind(this)}>×</button>
          <h3>Warning</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <form className="form-horizontal">
              <fieldset>
                <span>There are entries that are blank, proceed anyway?</span>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <button onClick={props.closeWarningModal} className="btn btn-default btn-small pull-right"disabled={props.savingImage}>Cancel</button>
        <button onClick={props.saveMenuInterface} className="btn btn-primary btn-small pull-right" disabled={props.savingImage}>Yes</button>
      </div>

    </Modal>
  )
}

export default WarningModal