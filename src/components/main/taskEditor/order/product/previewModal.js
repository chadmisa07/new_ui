import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import MenuInterface from './menuInterface'

const PreviewModal = (props) => {
  return (
    <Modal show={props.showPreviewModal} onHide={props.closePreviewModal.bind(this)} className="clear-padding-right">
      <div className="modal-header modal-header-new" >
          <button className="close" data-dismiss="modal" onClick={props.closePreviewModal.bind(this)}>×</button>
          <h3>Template Preview</h3>
      </div>
      <div className="modal-body">
        <div className="row-fluid">
          <div className="span12">
            <form className="form-horizontal">
              <fieldset>
                <div className="row-fluid">
                  <MenuInterface {...props} previewImage={props.previewImage} />
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <div className="row-fluid pull-right">
          <Button onClick={props.closePreviewModal.bind(this)}>
            Close
          </Button>
        </div>
      </div>
    </Modal>
  )
}

PreviewModal.propTypes = {
  showPreviewModal: React.PropTypes.bool.isRequired,
  closePreviewModal: React.PropTypes.func.isRequired,
}

export default PreviewModal


