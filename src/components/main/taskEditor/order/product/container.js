import React from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as Common from '../common/index'
import * as Product from './index'
import validator from 'validator'
import * as Request from '../request/request'
import cloudinary from 'cloudinary'
import logo from '../../../../../includes/img/logo.png'
import logo_small from '../../../../../includes/img/logo-small.png'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

let count = 2
let previewImage = null
let current_index = -1

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    app: state.app,
    taskEditor: state.taskEditor,
    processName: "Order Management",
    data: state.task.data,
    taskMeta : state.task,
  }
}

class ProductContainer extends React.Component{

  constructor(props) {
    super(props)
    if(!this.props.taskEditor.editMode){
      if (props.app.createBotMode){
        if(props.taskEditor.productContainer===null) {
          this.state = {
            display: "display-none",
            fade: "",
            ariaHidden: "true",
            sidebar: true,
            savingImage: false,
            addMode: true,
            showProductModal: false,
            showPreviewModal: false,
            name: "Add Menu",
            code: "",
            description: "",
            price: 0,
            image_url: "",
            preview_url: "",
            item_url: "",
            add_button: "",
            view_description: "",
            current_index: "",
            previewInterface:{},
            welcomeText: {
              "message": "We always have the best deals.",
              "text_type": 34,
              "created_by": props.user.id,
              "updated_by": props.user.id,
            },
            menuInterfaces: [this.newMenuInterface(), this.newMenuInterface()],
            nameError: null,
            codeError: null,
            descriptionError: null,
            priceError: null,
            image_url_error: null,
            item_url_error: null,
            add_button_error: null,
            view_description_error: null,
            invalidUrl: false,
            openWarningModal: false,
            uploadImage: false,
            taskNameError: null,
            taskDescriptionError: null,
          }
        } else {
          this.state = props.taskEditor.productContainer
          count = props.taskEditor.productContainer.menuInterfaces.length
        }
      }
    } else {
        this.state = props.taskEditor.productContainer
        count = props.taskEditor.productContainer.menuInterfaces.length
    }
  }

  updateStore() {
    this.props.dispatch({type:"UPDATE_PRODUCT_CONTAINER", payload:this.state})
    this.props.dispatch({ 
      type: "UPDATE_TASK_NAME_AND_DESCRIPTION", 
      payload:{
        name: this.state.taskName,
        description: this.state.taskDescription
      }
    })
  }

  componentDidMount() {
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  componentWillMount(){
    this.setState({taskName: this.props.taskEditor.taskName})
    this.setState({taskDescription: this.props.taskEditor.taskDescription})
    if(this.props.taskEditor.editMode && this.props.taskEditor.productContainer===null){
      count = this.props.data.menu_interface.length
    }else{
      count = this.state.menuInterfaces.length
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  handleTaskNameChange = (event) => {
    this.setState({taskName:event.target.value})
  }

  handleTaskDescriptionChange = (event) => {
    this.setState({taskDescription:event.target.value})
  }

  taskNameError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_NAME_ERROR",
        payload: {
          taskNameError: error,
        }
    })
  }

  taskDescriptionError(error){
    this.props.dispatch({
      type: "UPDATE_TASK_DESCRIPTION_ERROR",
        payload: {
          taskDescriptionError: error
        }
    })
  }

  handleInputChange = (e) =>{
    if(e.target.name === "taskName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskNameError:"f_error"})
        this.taskNameError("f_error")
      } else {
        this.setState({taskNameError:null})
        this.taskNameError(null)
      }
      this.setState({taskName: e.target.value})
    }
    else if(e.target.name === "taskDescription"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({taskDescriptionError:"f_error"})
        this.taskDescriptionError("f_error")
      } else {
        this.setState({taskDescriptionError:null})
        this.taskDescriptionError(null)
      }
      this.setState({taskDescription: e.target.value})
    }
    else if(e.target.name === "name"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({nameError:"f_error"})
      } else {
        this.setState({nameError:null})
      }
      this.setState({ name: e.target.value })
    }
    else if(e.target.name === "code"){
      this.setState({ code: e.target.value })
    }
    else if(e.target.name === "description"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({descriptionError:"f_error"})
      } else {
        this.setState({descriptionError:null})
      }
      this.setState({ description: e.target.value })
    }
    else if(e.target.name === "price"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({priceError:"f_error"})
      } else {
        this.setState({priceError:null})
      }
      this.setState({ price: Number(e.target.value) })
    }
    else if(e.target.name === "image_url"){
      this.setState({image_url:e.target.files[0]})
      this.setState({image_url_error: null})
      this.setState({uploadImage: true})
    }
    else if(e.target.name === "item_url"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({invalidUrl: true})
        this.setState({item_url_error:null})
      } else if(!validator.isURL(e.target.value)){
        this.setState({invalidUrl: false})
        this.setState({item_url_error:"f_error"})
      } else {
        this.setState({invalidUrl: true})
        this.setState({item_url_error:null})
      }
      this.setState({ item_url: e.target.value })
    }
    else if(e.target.name === "add_button"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({add_button_error:"f_error"})
      } else {
        this.setState({add_button_error:null})
      }
      this.setState({ add_button: e.target.value })
    }
    else{
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({view_description_error:"f_error"})
      } else {
        this.setState({view_description_error:null})
      }
      this.setState({ view_description: e.target.value })
    }
  }

  mapItem(menuInterface){
    this.setState({
      name: menuInterface.product.name,
      code: menuInterface.product.code,
      description: menuInterface.product.description,
      price: menuInterface.product.price,
      image_url: menuInterface.product.image_url,
      item_url: menuInterface.product.item_url,
      add_button: menuInterface.add_button,
      view_description: menuInterface.view_description,
    })
  }

  newMenuInterface(){
    const userId = this.props.user.id
    return {
      "product": {
        "name": "",
        "code": "",
        "description": "",
        "price": 1,
        "image_url": "",
        "preview_url": "",
        "item_url": "",
        "created_by": userId,
        "updated_by": userId
      },
      "id": "",
      "add_button": "",
      "view_description": "",
      "index": count,
      "created_by": userId,
      "updated_by": userId
    }
  }

  openProductModal(menuInterface, index) {
    current_index = index
    this.setState({display: "display-block"})
    this.setState({fade: "in"})
    this.setState({ariaHidden: "false"})
    this.setState({image_url:menuInterface.product.image_url})
    this.setState({addMode:false, current_index:index})
    this.mapItem(menuInterface)
    this.setState({button_id:menuInterface.id})
    this.setState({product_id:menuInterface.product.id})
    this.setState({ showProductModal: true })
    this.setState({invalidUrl: true})
    this.setState({savingImage:false})
  }

  closeWarningModal(){
    this.setState({openWarningModal: false})
  }

  closeProductModal() {
    this.setState({display: "display-none"})
    this.setState({ariaHidden: "true"})
    this.setState({fade: ""})
    this.setState({showProductModal: false })
    this.setState({nameError: null})
    this.setState({codeError: null})
    this.setState({descriptionError: null})
    this.setState({priceError: null})
    this.setState({image_url_error: null})
    this.setState({item_url_error: null})
    this.setState({add_button_error: null})
    this.setState({view_description_error: null})
    this.setState({invalidUrl: false})
    this.setState({savingImage: false})
    this.setState({image_url: ""})
  }

  openPreviewModal(menuInterface) {
    this.setState({ previewInterface: menuInterface })
    this.setState({ showPreviewModal: true })
  }

  closePreviewModal() {
    this.setState({ showPreviewModal: false })
  }

  addProductButton() {
    if (count < 10) {
      let menuInterfaces = this.state.menuInterfaces
      menuInterfaces.splice(count, 0, this.newMenuInterface())
      this.setState({menuInterfaces:menuInterfaces})
      count++
    } else {
      toastr.info('Alert', 'Unable to add more product. \nMaximum of 10 products reached.')
    }
  }

  saveMenuInterface() {
    if(!this.state.name){
      this.setState({nameError: "f_error"})
      toastr.error('Error', 'Please enter a Product Name')
    }
    // else if(!this.state.code){
    //   this.setState({codeError: "error"})
    //   toastr.error('Error', 'Please enter a Product Code')
    // }
    else if(!this.state.description){
      this.setState({descriptionError: "f_error"})
      toastr.error('Error', 'Please enter a Product Description')
    }
    else if(!this.state.price){
      this.setState({priceError: "f_error"})
      toastr.error('Error', 'Please enter a Product Price')
    }
    else if(!this.state.image_url){
      this.setState({image_url_error: "f_error"})
      toastr.error('Error', 'Please select an image')
    }
    else if(this.state.invalidUrl !== true){
      this.setState({item_url_error: "f_error"})
      toastr.error('Error', 'Please enter a valid Url')
    }
    else if(!this.state.add_button){
      this.setState({add_button_error: "f_error"})
      toastr.error('Error', 'Please enter a Button Text')
    }
    else if(!this.state.view_description){
      this.setState({view_description_error: "f_error"})
      toastr.error('Error', 'Please enter a Button Text')
    }
    else{
      if((!this.state.code || !this.state.item_url) && this.state.openWarningModal === false){
        this.setState({openWarningModal: true})
      }else if(!this.state.uploadImage && this.state.image_url){
        this.setState({openWarningModal: false})
        this.setState({savingImage:true})
        let menuInterfaces = this.state.menuInterfaces
        menuInterfaces[current_index] = this.getUserInput()
        this.setState({image_url_error: null})
        this.addProduct(menuInterfaces)
      }else{
        this.setState({openWarningModal: false})
        this.setState({savingImage:true})
        this.setState({uploadImage:true})
        let menuInterfaces = this.state.menuInterfaces
        menuInterfaces[current_index] = this.getUserInput()
        this.setState({image_url_error: null})
        try {
          let reader = new FileReader()
          reader.readAsDataURL(menuInterfaces[current_index].product.image_url)
          reader.onloadend = () => {
            this.uploadImage(reader.result, menuInterfaces)
          }
        } catch (error) {
          this.setState({image_url_error: "f_error"})
          toastr.error('Error', 'Please select an image')
          this.setState({savingImage:false})
          this.setState({openWarningModal: false})
          this.setState({uploadImage:false})
        }
      }
    }
  }

  addProduct(menuInterfaces){
    this.setState({menuInterfaces:menuInterfaces})
    this.closeProductModal()
    this.closeWarningModal()
    this.setState({savingImage:false})
    this.setState({uploadImage:false})
    this.updateStore()
  }

  uploadImage(image, menuInterfaces) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        menuInterfaces[current_index].product.image_url = result.secure_url
        this.setState({menuInterfaces:menuInterfaces})
        this.closeProductModal()
        this.closeWarningModal()
        this.setState({savingImage:false})
        this.setState({uploadImage:false})
        this.updateStore()
        this.setState({display: "display-none"})
        this.setState({ariaHidden: "true"})
        this.setState({fade: ""})
      } else {
        toastr.error("Error", "Unable to upload image")
        this.closeProductModal()
        this.closeWarningModal()
        this.setState({savingImage:false})
        this.setState({uploadImage:false})
      }
    })
  }

  deleteInterface() {
    let menuInterfaces = this.state.menuInterfaces
    menuInterfaces.splice(this.state.current_index, 1)
    this.setState({ menuInterfaces: menuInterfaces })
    this.setState({ addMode: true })
    count--
    this.closeProductModal()
    toastr.success('Success', 'Successfully deleted interface')
  }

  deleteInterfaceAtIndex(index) {
    let menuInterfaces = this.state.menuInterfaces
    menuInterfaces.splice(index, 1)
    this.setState({ menuInterfaces: menuInterfaces })
    count--
    toastr.success('Success', 'Successfully deleted interface')
  }

  getUserInput() {
    const userId = this.props.user.id
    return {
      "add_button": this.state.add_button,
      "id": this.state.button_id,
      "view_description": this.state.view_description,
      "created_by": userId,
      "updated_by": userId,
      "index": count,
      "product": {
        "name": this.state.name,
        "id": this.state.product_id,
        "code": this.state.code,
        "description": this.state.description,
        "price": this.state.price,
        "image_url": this.state.image_url,
        "item_url": this.state.item_url,
        // "item_url": this.state.item_url.indexOf("https") > 0 ? this.state.item_url : "https://"+this.state.item_url ,
        "preview_url": this.state.image_url,
        "created_by": userId,
        "updated_by": userId
      }
    }
  }

  save() {
    if(this.state.taskName && this.state.taskDescription){
      if(this.props.taskEditor.editMode){
        this.updateStore()
        this.props.dispatch(Request.updateTask(this.props.taskEditor))
      }else{
        this.props.dispatch(Request.saveTask(this.props.taskEditor))
      }
    } else {
      toastr.error('Error', 'Please fill everything')
    }
  }

  next(){
    if(count === 0){
      toastr.error('Error', 'Please add a product')
    }else if(this.state.menuInterfaces.find(x=>x.product.name === "")){
      toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/quantity")
    }
  }

  previous(){
    if(count === 0){
      toastr.error('Error', 'Please add a product')
    }else if(this.state.menuInterfaces.find(x=>x.product.name === "")){
      toastr.error('Error', 'Please fill everything')
    }else{
      browserHistory.push("/order/operation")
    }
  }

  sidebarClick(){
    this.setState({sidebar:!this.state.sidebar})
  }

  render() {
    const productButtons = this.state.menuInterfaces.map((menuInterface, index) => {
      return (
        <Product.ProductButton key={index} index={index} menuInterface={menuInterface}
          openProductModal={this.openProductModal.bind(this, menuInterface, index)}
          openPreviewModal={this.openPreviewModal.bind(this, menuInterface)}
          deleteInterface={this.deleteInterfaceAtIndex.bind(this, index)} />
      )
    }) 

    let priceString = "₱ - "+this.state.price+" "

    return (
      <div id="contentwrapper" className={this.state.sidebar?"":"sidebar_hidden"}>
        <div className="main_content">
          <Common.Nav parent={'Task Editor'} page={'Product Choices'}/>
          <Common.Header logo={logo} logo_small={logo_small} 
            fname={this.props.user.first_name} {...this.state} {...this.props} save={this.save.bind(this)}/>
          <div className="row-fluid">
            <div className="span12">
              <h3 className="heading">Product Choices</h3>
              <div className="row-fluid">
                <div className="span12">
                  <div className="row-fluid">
                    <button className="pull-right btn btn-primary btn-small" onClick={this.next.bind(this)}>Next &nbsp;
                        <i className="fa fa-arrow-right icon-white"></i>
                    </button>
                    <button className="pull-right margin-right-5 btn btn-primary btn-small" onClick={this.previous.bind(this)}>
                      <i className="fa fa-arrow-left icon-white"></i>&nbsp;Previous
                    </button>
                  </div>
                  <form className="form-horizontal">
                    <fieldset className="margin-top-20">
                      <div>
                        <div className="well margin-top-neg-20">
                          <Common.TaskMeta {...this.props} {...this.state}
                            handleInputChange={this.handleInputChange.bind(this)}/>
                        </div>
                        <div className="row-fluid">
                          <Product.Instruction/>
                        </div>
                        <div className="row-fluid little-padding-top little-padding-left items">
                          <div className="span11">
                            {productButtons}
                            <div className="span3">
                              <div className="black-box text-center product-btn">
                                <button type="button" className="btn btn-primary btn-small" 
                                onClick={this.addProductButton.bind(this)} disabled={count>=10}>
                                  <i className="fa fa-plus icon-white"></i>&nbsp;Add
                                </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Product.ProductModal {...this.state}
          handleInputChange={this.handleInputChange.bind(this)}
          closeProductModal={this.closeProductModal.bind(this)} 
          saveMenuInterface={this.saveMenuInterface.bind(this)}
          deleteInterface={this.deleteInterface.bind(this)}
          descriptionLength={priceString.length} />
        <Common.Sidebar {...this.state} 
          menuInterfaces={this.state.menuInterfaces.find(x=>x.product.name === "") ? 0 : 1} 
          count={count} 
          sidebarClick={this.sidebarClick.bind(this)} />
        <Product.PreviewModal {...this.state} previewImage={previewImage}
            closePreviewModal={this.closePreviewModal.bind(this)} />
        <Product.WarningModal {...this.state}
          saveMenuInterface={this.saveMenuInterface.bind(this)}
          closeWarningModal={this.closeWarningModal.bind(this)}/>
      </div>
    )
  }
}

export default connect(mapStateToProps)(ProductContainer)