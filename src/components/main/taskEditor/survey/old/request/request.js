import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import cloudinary from 'cloudinary'
import { browserHistory } from 'react-router'
import update from 'react-addons-update'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

function showError(errorData){
  for(let name in errorData) {
    if(name){
      let error = errorData[name];
      toastr.error('Error', name.toUpperCase()+": "+error)
      console.log('Error', name.toUpperCase()+": "+error)
    }
  }
}

export function createProduct(data) {
  return function(dispatch) {
    cloudinary.uploader.upload(data.product.image_url, function(result) { 
      data.product.image_url = result.url
      axios.post("https://cbot-api.herokuapp.com/api/product-item/", data.product)
        .then(function (response) {
          toastr.success('Success', 'Product was created')
          data.product = response.data.id
          dispatch(createMenuInterface(data))
        })
        .catch(function (error) {
          showError(error.response.data)
        })
    })
  }  
}

export function createMenuInterface(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/menu-interface/", data)
      .then(function (response) {
        toastr.success('Success', 'Interface was created')
        dispatch({type: "MENU_INTERFACE_CREATED", payload: response.data})
      })
      .catch(function (error) {
        showError(error.response.data)
      })
  }  
}

export function createTextResponse(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/text-response/", data)
      .then(function (response) {
        dispatch({type: "TEXT_RESPONSE_CREATED", payload: response.data})
        toastr.success('Success', 'Welcome message was created')
      })
      .catch(function (error) {
        showError(error.response.data)
      })
  }  
}

export function createPersistentMenu(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/persistent-menu/", data)
      .then(function (response) {
        dispatch({type: "PERSISTENT_MENU_CREATED", payload: response.data})
        toastr.success('Success', 'Persistent menu was created')
      })
      .catch(function (error) {
        showError(error.response.data)
      })
  }  
}

export function createGreetingText(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/greeting-text/", data)
      .then(function (response) {
        dispatch({type: "GREETING_TEXT_CREATED", payload: response.data})
        toastr.success('Success', 'Greeting text was created')
      })
      .catch(function (error) {
        alert(JSON.stringify(error.response.data))
      })
  }  
}

export function createQuickReply(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/quick-reply/", data)
      .then(function (response) {
        toastr.success('Success', 'Quick reply was created')
        dispatch({type: "QUICK_REPLY_CREATED", payload: response.data})
      })
      .catch(function (error) {
        showError(error.response.data)
      })
  }  
}

export function createQuickReplyArray(qr) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/quick-reply/", qr)
      .then(function (response) {
        response.data.forEach(function(value) {
          dispatch({type: "QUICK_REPLY_CREATED", payload: value})
          toastr.success('Success', 'Quick reply was created')
        })
      })
      .catch(function (error) {
        showError(error.response.data)
      })
  }  
}

export function createJobflow(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/job-flow/", data)
      .then(function (response) {
        toastr.success('Success', 'Job flow was created. You may now test your bot.')
        dispatch({type:'RESET_BOT'})
        dispatch({type:'RESET_BUTTON_RESPONSE'})
        dispatch({type:'RESET_MENU_INTERFACE'})
        dispatch({type:'RESET_QUICK_REPLY'})
        dispatch({type:'RESET_TEXT_RESPONSE'})
        browserHistory.push('/main')
      })
      .catch(function (error) {
        showError(error.response.data)
      })
  }  
}

export function createButtonResponse(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/button-response/", data)
      .then(function (response) {
        toastr.success('Success', 'Button response was created')
        dispatch({type: 'BUTTON_RESPONSE_CREATED', payload: response.data})
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating button response')
      })
  }  
}

export function saveTask(data) {
  // console.log(JSON.stringify(data))
  if(data.operationContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/operation/')
        }
  }
  else if(data.productContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/product/')
        }
  }
  else if(data.quantityContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/quantity/')
        }
  }
  else if(data.cartContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/cart/')
        }
  }
  else if(data.deliveryContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/delivery/')
        }
  }
  else if(data.checkoutContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/checkout/')
        }
  } else {
    let locationInterface = data.deliveryContainer.interfaces.find(x=>x.location.name === "")
    let productInterface = data.productContainer.menuInterfaces.find(x=>x.product.name === "")
    if(!data.productContainer.menuInterfaces.length){
        return function(dispatch) {
          toastr.error('Error', 'Please add a product.')
          browserHistory.push('/order/product/')
        }
    }
    else if(data.deliveryContainer.pickUpChecked === true && !data.deliveryContainer.interfaces.length){
      return function(dispatch) {
        toastr.error('Error', 'Please add a pickup location.')
        browserHistory.push('/order/delivery/')
      }
    }
    else if(data.deliveryContainer.pickUpChecked === true && locationInterface){
      return function(dispatch) {
        toastr.error('Error', 'Please fill everything.')
        browserHistory.push('/order/delivery/')
      }
    }
    else if(productInterface){
      return function(dispatch) {
        toastr.error('Error', 'Please fill everything.')
        browserHistory.push('/order/product/')
      }
    }
    else{
      let updatedData 
      if(!data.deliveryContainer.pickUpChecked){
        if(data.operationContainer.viewCartRemove && data.operationContainer.changeEmailRemove){
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[1,1]]}}}})
        } else if (data.operationContainer.viewCartRemove){
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}}})
        } else if(data.operationContainer.changeEmailRemove){
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}}})
        } else {
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[3,1]]}}}})
        }
      } else {
       updatedData = data
      }
      return function(dispatch) {
        dispatch({type: "SAVE_BOT_REQUEST"})
        axios.post("https://cbot-api.herokuapp.com/app/job-flow/", updatedData)
          .then(function (response) {
              let task = response.data.id
              let greetingText = data.operationContainer.greetingText
              greetingText.job_flow = task
              axios.post("https://cbot-api.herokuapp.com/api/greeting-text/", greetingText)
                .then(function (response) {
                    toastr.success('Success', 'Greeting text was created')
                    let persistentMenu = updatedData.operationContainer.persistentMenu
                    persistentMenu.job_flow = task
                    axios.post("https://cbot-api.herokuapp.com/api/persistent-menu/", persistentMenu)
                      .then(function (response) {
                        toastr.success('Success', 'Persistent menu was created')
                        dispatch({type:'RESET_TASK_EDITOR'})
                        dispatch({type: "SAVE_BOT_REQUEST_FULFILLED"})
                      })
                      .catch(function (error) {
                        toastr.error('Error', JSON.stringify(error))
                      })
                })
                .catch(function (error) {
                  toastr.error('Error', 'Unable to create greeting text')
                  dispatch({type: "SAVE_BOT_REQUEST_REJECTED"})
                })
              try {
                toastr.success('Success', 'Task flow was created')
                browserHistory.push('/administration/main') 
              } catch (error) {
                alert(error)
                // console.log(JSON.stringify(error))
                dispatch({type: "SAVE_BOT_REQUEST_REJECTED"})
              }
          })
          .catch(function (error) {
            showError(error.response.data)
            dispatch({type: "SAVE_BOT_REQUEST_REJECTED"})
          })
      }
    }
  }
}

export function updateTask(data) {
  // console.log(JSON.stringify(data))
  if(data.operationContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/operation/')
        }
  }
  else if(data.productContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/product/')
        }
  }
  else if(data.quantityContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/quantity/')
        }
  }
  else if(data.cartContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/cart/')
        }
  }
  else if(data.deliveryContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/delivery/')
        }
  }
  else if(data.checkoutContainer === null){
    return function(dispatch) {
          toastr.error('Error', 'Please fill everything.')
          browserHistory.push('/order/checkout/')
        }
  } else {
    let locationInterface = data.deliveryContainer.interfaces.find(x=>x.location.name === "")
    let productInterface = data.productContainer.menuInterfaces.find(x=>x.product.name === "")
    if(!data.productContainer.menuInterfaces.length){
        return function(dispatch) {
          toastr.error('Error', 'Please add a product.')
          browserHistory.push('/order/product/')
        }
    }
    else if(data.pickup === true && !data.deliveryContainer.interfaces.length){
      return function(dispatch) {
        toastr.error('Error', 'Please add a pickup location.')
        browserHistory.push('/order/delivery/')
      }
    }
    else if(data.deliveryContainer.pickUpChecked === true && locationInterface){
      return function(dispatch) {
        toastr.error('Error', 'Please fill everything.')
        browserHistory.push('/order/delivery/')
      }
    }
    else if(productInterface){
      return function(dispatch) {
        toastr.error('Error', 'Please fill everything.')
        browserHistory.push('/order/product/')
      }
    }
    else{
      let updatedData 
      if(!data.deliveryContainer.pickUpChecked){
        if(data.operationContainer.viewCartRemove && data.operationContainer.changeEmailRemove){
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[1,1]]}}}})
        } else if (data.operationContainer.viewCartRemove){
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}}})
        } else if(data.operationContainer.changeEmailRemove){
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}}})
        } else {
          updatedData = update(data, {"operationContainer": {"persistentMenu": {"buttons" : {$splice: [[3,1]]}}}})
        }
      } else {
       updatedData = data
      }
      return function(dispatch) {
        dispatch({type: "UPDATE_BOT_REQUEST"})
        axios.post("https://cbot-api.herokuapp.com/app/update-please/", updatedData)
          .then(function (response) {
              // console.log(JSON.stringify(data.operationContainer.greetingText))
              let greetingText = data.operationContainer.greetingText
              axios.patch("https://cbot-api.herokuapp.com/api/greeting-text/"+greetingText.id+"/", greetingText)
                .then(function (response) {
                    toastr.success('Success', 'Greeting text was created')
                    let persistentMenu = updatedData.operationContainer.persistentMenu
                    // console.log(persistentMenu)
                    axios.delete("https://cbot-api.herokuapp.com/api/persistent-menu/"+persistentMenu.id+"/")
                    .then(function (response){
                      axios.post("https://cbot-api.herokuapp.com/api/persistent-menu/", persistentMenu)
                      .then(function (response) {
                        toastr.success('Success', 'Persistent menu was created')
                        dispatch({type:'RESET_TASK_EDITOR'})
                        dispatch({type: "UPDATE_BOT_REQUEST_FULFILLED"})
                      })
                      .catch(function (error) {
                        toastr.error('Error', JSON.stringify(error))
                      })
                    })
                    .catch(function (error){
                      toastr.error('Error', 'Unable to delete persistent menu')
                      dispatch({type: "UPDATE_BOT_REQUEST_REJECTED"})
                    })
                })
                .catch(function (error) {
                  toastr.error('Error', 'Unable to create greeting text')
                  dispatch({type: "UPDATE_BOT_REQUEST_REJECTED"})
                })
              try {
                toastr.success('Success', 'Task flow was created')
                browserHistory.push('/administration/main') 
              } catch (error) {
                alert(error)
                // console.log(JSON.stringify(error))
                dispatch({type: "SAVE_BOT_REQUEST_REJECTED"})
              }
          })
          .catch(function (error) {
            showError(error.response.data)
            dispatch({type: "SAVE_BOT_REQUEST_REJECTED"})
          })
      }
    }
  }
}
