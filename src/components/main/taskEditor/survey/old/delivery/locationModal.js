import React from 'react'
import { Modal, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap'

const LocationModal = (props) => {
  return (
    <Modal show={props.showLocationModal} onHide={props.closeLocationModal}>
      <Modal.Header closeButton>
        <Modal.Title>Add Location</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <FormGroup validationState={props.locationNameError}>
            <ControlLabel>Location Name</ControlLabel>
            <FormControl 
              type="text" 
              placeholder="Location Name" 
              maxLength={80} 
              name="locationName" 
              onChange={props.handleInputChange}
              value={props.locationName} />
          </FormGroup>
          <FormGroup validationState={props.locationDescriptionError}>
            <ControlLabel>Description</ControlLabel>
            <FormControl 
              type="text" 
              placeholder="Location Description" 
              maxLength={80} 
              name="description" 
              onChange={props.handleInputChange}
              value={props.description} />
          </FormGroup>
          <FormGroup validationState={props.location_image_url_error}>
            <ControlLabel>Image</ControlLabel>
            <FormControl 
              type="file" 
              placeholder="Upload photo" 
              name="image_url" 
              onChange={props.handleImageChange} />
          </FormGroup>
          <FormGroup validationState={props.location_url_error}>
            <ControlLabel>Location URL</ControlLabel>
            <FormControl 
              type="url" 
              placeholder="Location URL" 
              name="location_url" 
              onChange={props.handleInputChange}
              value={props.location_url} />
          </FormGroup>
          <FormGroup validationState={props.contact_info_error} >
            <ControlLabel>Contact Info</ControlLabel>
            <FormControl 
              type="number" 
              placeholder="Contact Info" 
              name="contact_info" 
              onChange={props.handleInputChange}
              value={props.contact_info} 
              onInput={(e)=>{ 
                e.target.value = e.target.value.toString().slice(0,11)
              }}
              min={0}/>
          </FormGroup>
          <FormGroup validationState={props.visit_page_error}>
            <ControlLabel>Visit Page Button</ControlLabel>
            <FormControl 
              type="text" 
              placeholder="Visit Page Button" 
              maxLength={20} 
              name="visit_page" 
              onChange={props.handleInputChange}
              value={props.visit_page} />
          </FormGroup>
          <FormGroup validationState={props.call_btn_error}>
            <ControlLabel>Call Button</ControlLabel>
            <FormControl 
              type="text" 
              placeholder="Call Button" 
              maxLength={20} 
              name="call_btn" 
              onChange={props.handleInputChange}
              value={props.call_btn} />
          </FormGroup>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="primary" onClick={props.saveMenuInterface} disabled={props.savingImage}>
          {props.savingImage?"Saving":"Save"}
        </Button>
        <Button bsStyle="danger" onClick={props.deleteInterface} disabled={props.savingImage}>
          Delete
        </Button>
        <Button bsStyle="default" onClick={props.closeLocationModal} disabled={props.savingImage}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
LocationModal.propTypes = {
  saveMenuInterface: React.PropTypes.func.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  closeLocationModal: React.PropTypes.func.isRequired,
  savingImage: React.PropTypes.bool.isRequired,
  call_btn_error: React.PropTypes.string,
  handleInputChange: React.PropTypes.func.isRequired,
  call_btn: React.PropTypes.string.isRequired,
  visit_page_error: React.PropTypes.string,
  visit_page: React.PropTypes.string.isRequired,
  contact_info_error: React.PropTypes.string,
  contact_info: React.PropTypes.string.isRequired,
  location_url_error: React.PropTypes.string,
  location_url: React.PropTypes.string.isRequired,
  location_image_url_error: React.PropTypes.string,
  handleImageChange: React.PropTypes.func.isRequired,
  locationDescriptionError: React.PropTypes.string,
  description: React.PropTypes.string.isRequired,
  locationNameError: React.PropTypes.string,
  locationName: React.PropTypes.string.isRequired
}

export default LocationModal


