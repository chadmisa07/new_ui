import React from 'react'
import { Link } from 'react-router'

const Instruction = (props) => {
  return (
    <div className="little-padding-left">
      <p>
        Prior to checkout you can ask the users for delivery or pickup options.
      </p>
      <p>
        You can chose to skip this if you do not have plans to fulfill the order this way.
      </p>
      <p>
        (Such as you plan to send the product via email or other form of fulfillment). Learn <Link to="">more</Link>.
      </p>
    </div>
  )
}

export default Instruction
