import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import Location from './pickupLocation'

const PreviewModal = (props) => {
  return (
    <Modal show={props.showPreviewModal} onHide={props.closePreviewModal.bind(this)}>
      <Modal.Header closeButton>
        <Modal.Title>Template Preview</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Location {...props} />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.closePreviewModal.bind(this)}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

PreviewModal.propTypes = {
  showPreviewModal: React.PropTypes.bool.isRequired,
  closePreviewModal: React.PropTypes.func.isRequired,
}

export default PreviewModal


