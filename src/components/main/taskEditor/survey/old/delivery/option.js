import React from 'react'
import { Form, Checkbox, FormGroup, Col } from 'react-bootstrap'

const DeliveryOption = (props) => {
  return (
    <Form horizontal className="little-padding-left">
      <p><b>Choose order fulfillment options:</b></p>
      <FormGroup>
        <Col smOffset={2} sm={10}>
          <Checkbox 
            checked={props.skipChecked}
            onChange={props.handleSkipCheck}>
              Skip This
          </Checkbox>
          <Checkbox 
            checked={props.pickUpChecked}
            onChange={props.handlePickupCheck}>
              Pick-up Only
          </Checkbox>
          <Checkbox 
            checked={props.deliveryChecked}
            onChange={props.handleDeliveryCheck}>
              Delivery Only
          </Checkbox>
        </Col>
      </FormGroup>
    </Form>
  )
}

DeliveryOption.propTypes = {
  skipChecked: React.PropTypes.bool.isRequired,
  pickUpChecked: React.PropTypes.bool.isRequired,
  deliveryChecked: React.PropTypes.bool.isRequired,
  handleSkipCheck: React.PropTypes.func.isRequired,
  handlePickupCheck: React.PropTypes.func.isRequired,
  handleDeliveryCheck: React.PropTypes.func.isRequired
}

export default DeliveryOption

