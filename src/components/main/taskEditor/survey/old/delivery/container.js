import React from 'react'
import { connect } from 'react-redux'
import { Grid, Row, Col, Button, Glyphicon } from 'react-bootstrap'
import { toastr } from 'react-redux-toastr'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import Instruction from './instruction'
import DeliveryOption from './option'
import LocationButton from './locationBtn'
import validator from 'validator'
import update from 'react-addons-update'
import { saveTask, updateTask } from '../request/request'
import LocationModal from './locationModal'
import PreviewModal from './previewModal'

import cloudinary from 'cloudinary'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface,
    taskEditor: state.taskEditor,
    taskName: "Order Task",
    processName: "Order Management",
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
    botLink: "Bot Name here (automatically filled) with the Bot created if launched at creation.",
    data: state.task.data
  }
}

let count = 2
let previewImage = null
let current_index = -1

class DeliveryContainer extends React.Component {

  constructor(props) {
    super(props)
    if(!this.props.taskEditor.editMode){
      if (props.taskEditor.deliveryContainer===null){
        this.state = {
          savingImage: false,
          interfaces:[
            this.newLocationInterface(), 
            this.newLocationInterface()
          ],
          pickUpChecked: true,
          preview_url: null,
          skipChecked: false,
          deliveryChecked: false,
          locationName: "",
          description: "",
          image_url: "",
          location_url: "",
          contact_info: "",
          visit_page: "",
          call_btn: "",
          current_index: "",
          addMode: true,
          showLocationModal: false,
          showPreviewModal: false,
          previewInterface: null,
          locationNameError: null,
          locationDescriptionError: null,
          location_image_url_error: null,
          location_url_error: null,
          contact_info_error: null,
          visit_page_error: null,
          call_btn_error: null,
          invalidUrl: false
        }
      } else {
        this.state = props.taskEditor.deliveryContainer
      }
    } else {
      this.state = props.taskEditor.deliveryContainer
    }
  }

  updateStore(){
    this.props.dispatch({type:'UPDATE_DELIVERY_CONTAINER', payload:this.state})
  }

  componentWillMount()
  {
    if(this.props.taskEditor.editMode){
      count = this.props.data.location_interface.length
    }
  }

  componentDidMount() {
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  uncheckAll(){
    this.setState({
      pickUpChecked: false,
      skipChecked: false,
      deliveryChecked: false
    })
  }

  handleImageChange = (event) => {
    this.setState({image_url:event.target.files[0]})
  }

  mapItem(location){
    this.setState({
      locationName: location.location.name,
      description: location.location.description,
      image_url: location.location.image_url,
      location_url: location.location.location_url,
      contact_info: location.location.contact_info,
      visit_page: location.visit_page,
      call_btn: location.call_btn,
    })
  }

  openLocationModal(location, index) {
    current_index = index
    this.setState({addMode:false, current_index:index})
    this.setState({button_id:location.id})
    this.setState({location_id:location.location.id})
    this.mapItem(location)
    this.setState({ showLocationModal: true })
    this.setState({image_url:""})
  }

  closeLocationModal() {
    this.setState({ showLocationModal: false })
    this.setState({locationNameError: null})
    this.setState({locationDescriptionError: null})
    this.setState({location_image_url_error: null})
    this.setState({location_url_error: null})
    this.setState({contact_info_error: null})
    this.setState({visit_page_error: null})
    this.setState({call_btn_error: null})
    this.setState({invalidUrl: false})
  }

  openPreviewModal(location) {
    // try {
    //   let reader = new FileReader();
    //   reader.readAsDataURL(location.location.preview_url)
    //   reader.onloadend = () => {
    //     previewImage = reader.result
        this.setState({ previewInterface: location })
        this.setState({ showPreviewModal: true })
    //   }
    // } catch (error) {
    //   this.setState({savingImage:false})
    // }
  }

  closePreviewModal() {
    this.setState({ showPreviewModal: false })
  }

  addLocationButton() {
    if (count < 10) {
      let interfaces = this.state.interfaces
      interfaces.splice(count, 0, this.newLocationInterface())
      this.setState({interfaces:interfaces})
      count++
    } else {
      toastr.info('Alert', 'Unable to add more location. \nMaximum of 10 locations reached.')
    }
  }

  saveMenuInterface() {
    if(!this.state.locationName.replace(/\s/g, '').length){
      this.setState({locationNameError: "error"})
      toastr.error('Error','Please enter a location name')
    }
    else if(!this.state.description.replace(/\s/g, '').length){
      this.setState({locationDescriptionError: "error"})
      toastr.error('Error','Please enter a description')
    }
    else if(!this.state.image_url){
      toastr.error('Error', 'Please select an image')
      this.setState({location_image_url_error: "error"})
    }
    else if(this.state.invalidUrl === true || !this.state.location_url.replace(/\s/g, '').length){
      this.setState({location_url_error: "error"})
      toastr.error('Error','Please enter a valid url')
    }
    else if(!this.state.contact_info.replace(/\s/g, '').length || this.state.contact_info.replace(/\s/g, '').length < 11){
      this.setState({contact_info_error: "error"})
      toastr.error('Error','Please enter the contact information correctly.')
    }
    else if(!this.state.visit_page.replace(/\s/g, '').length){
      this.setState({visit_page_error: "error"})
      toastr.error('Error','Please enter a visit page text')
    }
    else if(!this.state.call_btn.replace(/\s/g, '').length){
      this.setState({call_btn_error: "error"})
      toastr.error('Error','Please enter a call button text')
    }
    else{
      this.setState({savingImage:true})
      let interfaces = this.state.interfaces
      interfaces[current_index] = this.getUserInput()
      try {
        this.setState({location_image_url_error: null})
        let reader = new FileReader()
        reader.readAsDataURL(interfaces[current_index].location.image_url)
        reader.onloadend = () => {
          this.uploadImage(reader.result, interfaces)
        }
      } catch (error) {
        if(!this.state.image_url){
          toastr.error('Error', 'Please select an image')
          this.setState({location_image_url_error: "error"})
        }
        this.setState({savingImage:false})

      }
    }
  }

  uploadImage(image, interfaces) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        interfaces[current_index].location.image_url = result.url
        this.setState({interfaces:interfaces})
        this.closeLocationModal()
        this.setState({savingImage:false})
        this.updateStore()
      } else {
        toastr.error("Error", "Unable to upload image")
        this.closeLocationModal()
        this.setState({savingImage:false})
      }
    })
  }

  deleteInterface() {
    let interfaces = this.state.interfaces
    interfaces.splice(this.state.current_index, 1)
    this.setState({ interfaces: interfaces })
    this.setState({ addMode: true })
    count--
    this.closeLocationModal()
    toastr.success('Success', 'Successfully deleted interface')
  }

  deleteInterfaceAtIndex(index) {
    let interfaces = this.state.interfaces
    interfaces.splice(index, 1)
    this.setState({ interfaces: interfaces })
    count--
    toastr.success('Success', 'Successfully deleted interface')
  }

  validation = (event, field) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({field:"error"})
      alert(field)
    } else {
      this.setState({locationNameError:null})
    }
  }

  handleInputChange = (e) => {
    if(e.target.name === "locationName"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({locationNameError:"error"})
      } else {
        this.setState({locationNameError:null})
      }
    }else if(e.target.name === "description"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({locationDescriptionError:"error"})
      } else {
        this.setState({locationDescriptionError:null})
      }
    }else if(e.target.name === "image_url"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({location_image_url_error:"error"})
      } else {
        this.setState({location_image_url_error:null})
        this.setState({image_url: e.target.files[0]})
      }
    }else if(e.target.name === "location_url"){
      if(validator.isURL(e.target.value)){
        this.setState({invalidUrl: false})
        this.setState({location_url_error:null})
      }else{
         this.setState({invalidUrl: true})
         this.setState({location_url_error:"error"})
      }
    }else if(e.target.name === "contact_info"){
      if(!e.target.value.replace(/\s/g, '').length || e.target.value.replace(/\s/g, '').length < 11){
        this.setState({contact_info_error:"error"})
      } else {
        this.setState({contact_info_error:null})
      }
    }else if(e.target.name === "visit_page"){
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({visit_page_error:"error"})
      } else {
        this.setState({visit_page_error:null})
      }
    }
    else{
      if(!e.target.value.replace(/\s/g, '').length){
        this.setState({call_btn_error:"error"})
      } else {
        this.setState({call_btn_error:null})
      }
    }
    this.setState({[e.target.name]:e.target.value})
  }

  getUserInput() {
    const userId = this.props.user.id
    return ({
      location: {
        "id": this.state.location_id,
        "name": this.state.locationName,
        "description": this.state.description,
        "image_url": this.state.image_url,
        "preview_url": this.state.image_url,
        "location_url": this.state.location_url,
        "contact_info": this.state.contact_info,
        "created_by": userId,
        "updated_by": userId
      },
      visit_page: this.state.visit_page,
      call_btn: this.state.call_btn,
      id: this.state.button_id,
      created_by: userId,
      updated_by: userId
    })
  }

  handlePickupCheck(){
    this.uncheckAll()
    let data
    if(this.props.taskEditor.operationContainer.viewCartRemove && this.props.taskEditor.operationContainer.changeEmailRemove){
      data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[1,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
    } else if (this.props.taskEditor.operationContainer.viewCartRemove){
      data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
    } else if(this.props.taskEditor.operationContainer.changeEmailRemove){
      data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
    } else {
      data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[3,0,{"title":"View Pickup Location","payload":"/pickup~~locs","button_type":38,"created_by":this.props.user.id,"updated_by":this.props.user.id}]]}}})
    }
    this.setState({pickUpChecked:true})
    this.props.dispatch({type:'UPDATE_OPERATION_CONTAINER', payload: data})
    this.props.dispatch({type:"PICKUP_MODE_ON", payload:true})
    this.updateStore()
  }

  handleSkipCheck(){
    this.uncheckAll()
    let data 
    if(!this.props.taskEditor.delivery){
      if(this.props.taskEditor.operationContainer.viewCartRemove && this.props.taskEditor.operationContainer.changeEmailRemove){
      data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[1,1]]}}})
      } else if (this.props.taskEditor.operationContainer.viewCartRemove){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}})
      } else if(this.props.taskEditor.operationContainer.changeEmailRemove){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}})
      } else {
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[3,1]]}}})
      }
      this.setState({interfaces: []})
      this.props.dispatch({type:'UPDATE_OPERATION_CONTAINER', payload: data})
      this.updateStore()
    }
    this.setState({skipChecked:true})
    this.props.dispatch({type:"SKIP_MODE_ON", payload:true})
  }

  handleDeliveryCheck(){
    this.uncheckAll()
    let data 
    if(!this.props.taskEditor.skip){
      if(this.props.taskEditor.operationContainer.viewCartRemove && this.props.taskEditor.operationContainer.changeEmailRemove){
      data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[1,1]]}}})
      } else if (this.props.taskEditor.operationContainer.viewCartRemove){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}})
      } else if(this.props.taskEditor.operationContainer.changeEmailRemove){
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[2,1]]}}})
      } else {
        data = update(this.props.taskEditor.operationContainer, {"persistentMenu": {"buttons" : {$splice: [[3,1]]}}})
      }
      this.setState({interfaces: []})
      this.props.dispatch({type:'UPDATE_OPERATION_CONTAINER', payload: data})
      this.updateStore()
    }
    
    this.setState({deliveryChecked:true})
    this.props.dispatch({type:"DELIVERY_MODE_ON", payload:true})
  }

  newLocationInterface() {
    const userId = this.props.user.id
    return ({
      location: {
        "name": "",
        "id": "",
        "description": "",
        "image_url": "",
        "preview_url": "",
        "location_url": "",
        "contact_info": "",
        "created_by": userId,
        "updated_by": userId
      },
      created: userId,
      update_by: userId,
      visit_page: "",
      call_btn: "",
      "id": ""
    })
  }

  save() {
    if(this.props.taskEditor.editMode){
      this.updateStore()
      this.props.dispatch(updateTask(this.props.taskEditor))
    } else {
      this.props.dispatch(saveTask(this.props.taskEditor))
    }
  }

  render() {

    const locationBtns = this.state.interfaces.map((locationInterface, index) => {
      return (
        <LocationButton index={index} location={locationInterface} key={index}
          openLocationModal={this.openLocationModal.bind(this, locationInterface, index)}
          openPreviewModal={this.openPreviewModal.bind(this, locationInterface)}
          deleteInterface={this.deleteInterfaceAtIndex.bind(this, index)} />
      )
    })

    return (
      <Grid fluid>
        <Row>
          <Header save={this.save.bind(this)} {...this.props} {...this.state}/>
        </Row>
        <Row  className="task-editor-form">
          <Sidebar selected={'delivery'} {...this.state}/>
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} />
              </Col>
            </Row>
            <Row>
              <Col lg={12} md={6}>
                <Instruction />
              </Col>
            </Row>
            <Row className="form-padding">
              <Col md={5}>
                <DeliveryOption {...this.state}
                  handlePickupCheck={this.handlePickupCheck.bind(this)}
                  handleSkipCheck={this.handleSkipCheck.bind(this)}
                  handleDeliveryCheck={this.handleDeliveryCheck.bind(this)} />
              </Col>
            </Row>
            <Row className="form-padding little-padding-left">
              <Col md={12}>
                { 
                  this.state.pickUpChecked? 
                    <p className="pull-left"><b>Pickup Locations</b></p> 
                    :
                    null
                }
              </Col>
            </Row>
            {
              this.state.pickUpChecked?
                <Row className="little-padding-top little-padding-left">
                  {locationBtns}
                  <Col md={2}>
                    <div className="max-height black-box text-center product-btn ">
                      <Button bsStyle="primary" bsSize="small" onClick={this.addLocationButton.bind(this)}
                        disabled={count>=10}>
                        <Glyphicon glyph="glyphicon glyphicon-plus"/> Add
                      </Button>
                      <p className="padding-top-45"></p>
                    </div>
                  </Col>
                </Row>
                :
                null
              }
          </div>
          <LocationModal {...this.state}
            closeLocationModal={this.closeLocationModal.bind(this)}
            saveMenuInterface={this.saveMenuInterface.bind(this)}
            handleInputChange={this.handleInputChange.bind(this)}
            deleteInterface={this.deleteInterface.bind(this)}
            handleImageChange={this.handleImageChange.bind(this)} />
          <PreviewModal {...this.state} previewImage={previewImage}
            closePreviewModal={this.closePreviewModal.bind(this)} />
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(DeliveryContainer)