import React from 'react'
import { Col, Button, Glyphicon } from 'react-bootstrap'
import { Link } from 'react-router'

const LocationButton = (props) => {
  return (
    <Col md={2}>
      <div className="black-box text-center product-btn">
        <Button bsStyle="primary" bsSize="small" onClick={props.openLocationModal.bind(this)}>
          <Glyphicon glyph="glyphicon glyphicon-edit"/> Edit
        </Button>
        <div className="padding-top-15">
          <Button bsStyle="danger" bsSize="small" onClick={props.deleteInterface.bind(this)}>
            <Glyphicon glyph="glyphicon glyphicon-remove"/>
          </Button>
        </div>
        <p className="padding-top-15">{"Location " + (props.index+1).toString()}</p>
      </div>
      <div className="padding-top-15 text-center">
        {
          props.location.location.name===""? 
            <p>-</p>
            :
            <Link onClick={props.openPreviewModal}>
              {props.location.location.name}
            </Link>
        }
      </div>
    </Col>
  )
}

LocationButton.propTypes = {
  openLocationModal: React.PropTypes.func.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  openPreviewModal: React.PropTypes.func.isRequired,
  index: React.PropTypes.number.isRequired
}

export default LocationButton
