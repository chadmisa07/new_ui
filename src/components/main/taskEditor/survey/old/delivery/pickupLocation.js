import React from 'react'
import { Button, Thumbnail, ButtonGroup } from 'react-bootstrap'

const Location = (props) => {
  return (
    <div>
      <Thumbnail src={props.previewInterface.location.image_url} 
        className="fb-button">
        <h3 className="fb-title">{props.previewInterface.location.name}</h3>
        <p className="fb-description">
          {props.previewInterface.location.description}
        </p>
        <p className="product-url">{props.previewInterface.location.location_url}</p>
          <ButtonGroup vertical block>
            <Button className="fb-button">{props.previewInterface.visit_page}</Button>
            <Button className="fb-button">{props.previewInterface.call_btn}</Button>
            <Button className="fb-button">Share</Button>
          </ButtonGroup>
      </Thumbnail>
    </div>
  )
}

Location.propTypes = {
  previewInterface: React.PropTypes.object
}

export default Location