import React from 'react'
import { Form, FormGroup, Col, FormControl, ControlLabel } from 'react-bootstrap'

const QuantityForm = (props) => {
  return (
    <Form horizontal>
      <FormGroup controlId="formHorizontalEmail" validationState={props.responseTextError}>
        <Col componentClass={ControlLabel} sm={3}>
          Response Text
        </Col>
        <Col sm={9}>
          <FormControl type="text" placeholder="Great! How many would you like?" 
            value={props.responseText.message}
            onChange={props.handleTextResponseChange} />
        </Col>
      </FormGroup>

      <FormGroup controlId="formHorizontalPassword" validationState={props.quickReplyTextError}>
        <Col componentClass={ControlLabel} sm={3}>
          Menu Selection Text
        </Col>
        <Col sm={9}>
          <FormControl type="text" placeholder="Please choose or enter number"
            value={props.quick_reply.text}
            onChange={props.handleMenuSelectionTextChange} />
        </Col>
      </FormGroup>
    </Form>
  )
}

QuantityForm.propTypes = {
  responseTextError: React.PropTypes.string,
  responseText: React.PropTypes.object.isRequired,
  handleTextResponseChange: React.PropTypes.func.isRequired,
  quickReplyTextError: React.PropTypes.string,
  quick_reply: React.PropTypes.object.isRequired,
  handleMenuSelectionTextChange: React.PropTypes.func.isRequired
}

export default QuantityForm



