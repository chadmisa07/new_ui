import React from 'react'
import { Row, Col } from 'react-bootstrap'

const FBPreview = (props) => {
  return (
    <div className="form-padding chatbox-border">
      <Row>
        <p className="sender-message-bubble text-center pull-right">
          Add to Cart
        </p>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          {props.responseText.message}
        </p>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          {props.quick_reply.text}
        </p>
      </Row>
      <Row className="text-center">
        <Col lg={12}>
          {props.fb_qr_btn}
        </Col>
      </Row>
    </div>
  )
}

FBPreview.propTypes = {
  responseText: React.PropTypes.object.isRequired,
  quick_reply: React.PropTypes.object.isRequired,
  fb_qr_btn: React.PropTypes.array.isRequired,
}

export default FBPreview


