import React from 'react'
import { Form, Button, FormControl, Col, ControlLabel, Glyphicon } from 'react-bootstrap'


const QuantityButton = (props) => {
  return (
    <Form inline className="btn-padding">
      <Col componentClass={ControlLabel} sm={3}>{ 
          (props.index===0&&props.count===1) || props.index===props.count-1?
          <Button bsStyle="primary" bsSize="small" key={props.index}
            onClick={props.addNewQRButton} disabled={props.count===5}>
            <Glyphicon glyph="glyphicon glyphicon-plus" />{'\u00A0'} Add Option
          </Button>
          : null 
        }
      </Col>
      <Col sm={9}>
        <FormControl type="number" key={props.index} 
          onChange={props.handleQuickReplyTextChange.bind(this)} value={props.quick_reply.title} /> {' '}
        <Button bsStyle="danger" bsSize="small" onClick={props.removeQRButton}
          disabled={props.count===1}>
          <span className="glyphicon glyphicon-remove"></span>
        </Button>
      </Col>
    </Form>
  )
}

QuantityButton.propTypes = {
  index: React.PropTypes.number.isRequired,
  count: React.PropTypes.number.isRequired,
  addNewQRButton: React.PropTypes.func.isRequired,
  handleQuickReplyTextChange: React.PropTypes.func.isRequired,
  quick_reply: React.PropTypes.object.isRequired,
  removeQRButton: React.PropTypes.func.isRequired,
}

export default QuantityButton




      // <Col componentClass={ControlLabel} sm={3}>
      //   <Button bsStyle="primary" bsSize="small" key={props.index} >
      //     <Glyphicon glyph="glyphicon glyphicon-plus" />{'\u00A0'} Add Product
      //   </Button>
      // </Col>
      // <Col sm={9}>
      //   <FormControl type="number" /> {' '}
      //   <Button bsStyle="danger" bsSize="small">
      //     <span className="glyphicon glyphicon-remove"></span>
      //   </Button>
      // </Col>