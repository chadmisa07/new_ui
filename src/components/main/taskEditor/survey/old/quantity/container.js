import React from 'react'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import Instruction from './instruction'
import QuantityForm from './quantityForm'
import FBPreview from './fbPreview'
import QuantityButton from './quantityBtn'
import ButtonPreview from './btnPreview'

import { saveTask, updateTask } from '../request/request'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    taskName: "Order Task",
    processName: "Order Management",
    taskEditor: state.taskEditor,
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
    botLink: "Bot Name here (automatically filled) with the Bot created if launched at creation.",
    data: state.task.data
  }
}

let count = 3

class QuantityContainer extends React.Component {

  constructor(props) {
    super(props)
    if (!this.props.taskEditor.editMode){
      if (props.taskEditor.quantityContainer===null){
        this.state = {
          responseTextError: null,
          quickReplyTextError: null,
          responseText: {
            "message": "Great! How many would you like?",
            "text_type": 40,
            "created_by": props.user.id,
            "updated_by": props.user.id,
          },
          quick_reply: {
            quick_replies:[
              this.newQuickReplyButton(1), 
              this.newQuickReplyButton(2),
              this.newQuickReplyButton(3),
            ],
            text: "Please choose from among the options below.",
            qr_type: 42,
            "created_by": props.user.id,
            "updated_by": props.user.id,
          }
        }
      } else {
        this.state = props.taskEditor.quantityContainer
      }
    }else{
      this.state = props.taskEditor.quantityContainer
    }
  }

  newQuickReplyButton(index) {
    return ({
      id:"",
      title: index,
      payload: "order_here",
      reply_type: 44,
      created_by: this.props.user.id,
      updated_by: this.props.user.id,
    })
  }

  componentDidMount() {
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  updateStore() {
    this.props.dispatch({type:"UPDATE_QUANTITY_CONTAINER", payload:this.state})
  }

  componentWillMount(){
    if(!this.state.responseText.message.replace(/\s/g, '').length){
      this.setState({responseTextError:"error"})
    } else if (!this.state.quick_reply.text.replace(/\s/g, '').length){
      this.setState({quickReplyTextError:"error"})
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  handleTextResponseChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({responseTextError:"error"})
    } else {
      this.setState({responseTextError:null})
    }
    this.setState({
      responseText: {
        message: event.target.value,
        id:this.state.responseText.id,
        "text_type": 40,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      }
    })
  }

  handleMenuSelectionTextChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({quickReplyTextError:"error"})
    } else {
      this.setState({quickReplyTextError:null})
    }
    let quick_reply = this.state.quick_reply
    quick_reply.text = event.target.value
    this.setState({
      quick_reply: quick_reply
    })
  }

  handleQuickReplyTextChange = (index, event) => {
    let quick_reply = this.state.quick_reply
    quick_reply.quick_replies[index].title = event.target.value
    this.setState({quick_reply:quick_reply})
  }

  addNewQRButton() {
    let quick_reply = this.state.quick_reply
    let quick_replies = this.state.quick_reply.quick_replies
    quick_replies.push(this.newQuickReplyButton(count+1))
    quick_reply.quick_replies = quick_replies
    this.setState({quick_reply:quick_reply})
    count++
  }

  removeQRButton = (index, event) => {
    let quick_reply = this.state.quick_reply
    let quick_replies = this.state.quick_reply.quick_replies
    quick_replies.splice(index, 1)
    quick_reply.quick_replies = quick_replies
    this.setState({quick_reply:quick_reply})
    count--
  }

  save() {
    if(this.props.taskEditor.editMode){
      this.updateStore()
      this.props.dispatch(updateTask(this.props.taskEditor))
    } else {
      this.props.dispatch(saveTask(this.props.taskEditor))
    }
  }

  render() {

    const qr_buttons = this.state.quick_reply.quick_replies.map((quick_reply, index) => {
      return (
        <QuantityButton 
          index={index} 
          key={index}
          count={this.state.quick_reply.quick_replies.length}
          quick_reply={quick_reply}
          handleQuickReplyTextChange={this.handleQuickReplyTextChange.bind(this, index)}
          addNewQRButton={this.addNewQRButton.bind(this)}
          removeQRButton={this.removeQRButton.bind(this, index)} />
      )
    })

    const fb_qr_btn = this.state.quick_reply.quick_replies.map((quick_reply, index) => {
      return (
        <ButtonPreview 
          title={quick_reply.title} 
          key={index}
          index={index} />
      )
    })

    return (
      <Grid fluid>
        <Row>
          <Header save={this.save.bind(this)} {...this.props}/>
        </Row>
        <Row  className="task-editor-form">
          <Sidebar selected={'quantity'} {...this.state}/>
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} />
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Instruction />
              </Col>
            </Row>
            <Row className="form-padding">
              <Col md={6}>
                <Row>
                  <Col md={12}>
                    <Col md={12}>
                      <QuantityForm {...this.state}
                        handleTextResponseChange={this.handleTextResponseChange.bind(this)}
                        handleMenuSelectionTextChange={this.handleMenuSelectionTextChange.bind(this)} />
                    </Col>
                    <Col md={12}>
                      <p className="form-padding">
                        We have prepared 3 choices as 1, 2, and 3.  You can edit them or delete 2 of them.
                      </p>
                      {qr_buttons}
                    </Col>
                  </Col>
                </Row>
              </Col>
              <Col md={6} className="form-padding left-padding-20">
                <FBPreview fb_qr_btn={fb_qr_btn} {...this.state} />
              </Col>
            </Row>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(QuantityContainer)