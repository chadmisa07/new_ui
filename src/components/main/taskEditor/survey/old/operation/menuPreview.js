import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'

const Preview = (props) => {
  return (
    <Grid className="gray-box" fluid>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-preview-text"><b>Menu</b></span>
        </Col>
      </Row>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-blue-text">View Categories</span>
        </Col>
      </Row>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-blue-text">Help</span>
        </Col>
      </Row>
    </Grid>
  )
}

export default Preview