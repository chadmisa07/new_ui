import React from 'react'

import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import Instruction from './instruction'
import EditInterface from './editInterface'
import Preview from './menuPreview'
import { saveTask,updateTask } from '../request/request'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    job: state.job,
    taskEditor: state.taskEditor,
    taskName: "Order Task",
    processName: "Order Management",
    data: state.task.data,
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
    botLink: "Bot Name here (automatically filled) with the Bot created if launched at creation."
  }
}

class OperationContainer extends React.Component{

  constructor(props) {
    super(props)
    if (!this.props.taskEditor.editMode){
      if (props.taskEditor.operationContainer===null){
        this.state = {
          welcomeText: "Welcome! Please select what you want to do.",
          welcomeTextId: -1,
          viewProduct: "View Products",
          viewCart: "View Cart",
          changeEmail: "Change Email",
          viewPickupLocation: "View Pickup Location",
          changeEmailRemove: false,
          viewPickupLocationRemove: false,
          viewCartRemove: false,
          welcomeTextError: null,
          viewProductError: null,
          viewCartError: null,
          changeEmailError: null,
          viewPickupLocationError: null,
          errorModal: false,
        }
      } else if (props.taskEditor.operationContainer){
        let data = props.taskEditor.operationContainer
        let viewCart = data.persistentMenu.buttons.length>=2 && data.persistentMenu.buttons[1].title !== "Change Email"?data.persistentMenu.buttons[1].title:"View Cart"
        let changeEmail = data.persistentMenu.buttons.length>=3 && data.persistentMenu.buttons[2].title!=="View Pickup Location"?data.persistentMenu.buttons[2].title:"Change Email"
        let viewPickupLocation = data.persistentMenu.buttons.length>=4?data.persistentMenu.buttons[3].title:"View Pickup Location"
        this.state = {
          welcomeText: data.greetingText.text,
          viewProduct: data.persistentMenu.buttons[0].title,
          viewCart: viewCart,
          changeEmail: changeEmail,
          viewPickupLocation: viewPickupLocation,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          viewCartRemove: data.viewCartRemove,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          viewCartError: data.viewCartError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal
        }
      } else {
        let data = props.taskEditor.operationContainer
        this.state = {
          welcomeText: data.welcomeText,
          viewProduct: data.viewProduct,
          viewCart: data.viewCart,
          changeEmail: data.changeEmail,
          viewPickupLocation: data.viewPickupLocation,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          viewCartRemove: data.viewCartRemove,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          viewCartError: data.viewCartError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal
        }
      }
    } else {
      if (props.taskEditor.operationContainer===null){
        let data = this.props.data
        let pickUpText = data.persistent_menu.find(menu=>menu.payload === "/pickup~~locs")
        let changeEmailText = data.persistent_menu.find(menu=>menu.payload === "c0mm@nd~~change_email")
        let viewCartText = data.persistent_menu.find(menu=>menu.payload === "$EE-C@rT")
        let viewProductText = data.persistent_menu.find(menu=>menu.payload === "/menu")
        this.state = {
          welcomeText: data.greeting_text[0].text,
          welcomeTextId: data.greeting_text[0].id,
          viewProduct: viewProductText ? viewProductText.title: "View Product",
          viewProductId: viewProductText ? viewProductText.id: "",
          viewPickupLocation : pickUpText ? pickUpText.title: "View Pickup Location",
          viewPickupLocationId : pickUpText ? pickUpText.id : "",
          changeEmail: changeEmailText ? changeEmailText.title : "Change Email",
          changeEmailId : changeEmailText ? changeEmailText.id: "",
          viewCart: viewCartText ? viewCartText.title : "View Cart",
          viewCartId : viewCartText ? viewCartText.id : "",
          changeEmailRemove: changeEmailText ? false : true,
          viewPickupLocationRemove: pickUpText ? false: true,
          viewCartRemove: viewCartText ? false : true,
          welcomeTextError: null,
          viewProductError: null,
          viewCartError: null,
          changeEmailError: null,
          viewPickupLocationError: null,
          errorModal: false,
        }
      }else if (props.taskEditor.operationContainer){
        let data = props.taskEditor.operationContainer
        let pickUpText = data.persistentMenu.buttons.find(menu=>menu.payload === "/pickup~~locs")
        let changeEmailText = data.persistentMenu.buttons.find(menu=>menu.payload === "c0mm@nd~~change_email")
        let viewCartText = data.persistentMenu.buttons.find(menu=>menu.payload === "$EE-C@rT")
        let viewProductText = data.persistentMenu.buttons.find(menu=>menu.payload === "/menu")
        let viewCart = viewCartText ? viewCartText.title : "View Cart"
        let viewCartId = viewCartText ? viewCartText.id : ""
        let changeEmail = changeEmailText ? changeEmailText.title: "Change Email"
        let changeEmailId = changeEmailText ? changeEmailText.id: ""
        let viewPickupLocation = pickUpText ? pickUpText.title : "View Pickup Location"
        let viewPickupLocationId = pickUpText ? pickUpText.id : ""
        this.state = {
          welcomeText: data.greetingText.text,
          welcomeTextId: data.greetingText.id,
          viewProduct: viewProductText ? viewProductText.title : "View Product",
          viewProductId: viewProductText ? viewProductText.id : "",
          viewCart: viewCart,
          viewCartId: viewCartId,
          changeEmail: changeEmail,
          changeEmailId: changeEmailId,
          viewPickupLocation: viewPickupLocation,
          viewPickupLocationId: viewPickupLocationId,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          viewCartRemove: data.viewCartRemove,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          viewCartError: data.viewCartError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal
        }
      }else {
        let data = props.taskEditor.operationContainer
        this.state = {
          welcomeText: data.welcomeText,
          welcomeTextId: data.greetingText.id,
          viewProduct: data.viewProduct,
          viewProductId: data.persistentMenu.buttons[0].id,
          viewCart: data.viewCart,
          viewCartId: data.viewCartId,
          changeEmail: data.changeEmail,
          changeEmailId: data.changeEmailId,
          viewPickupLocation: data.viewPickupLocation,
          viewPickupLocationId: data.viewPickupLocationId,
          changeEmailRemove: data.changeEmailRemove,
          viewPickupLocationRemove: !props.taskEditor.pickup,
          viewCartRemove: data.viewCartRemove,
          welcomeTextError: data.welcomeTextError,
          viewProductError: data.viewProductError,
          viewCartError: data.viewCartError,
          changeEmailError: data.changeEmailError,
          viewPickupLocationError: data.viewPickupLocationError,
          errorModal:  data.errorModal
        }
      }
       
    }
  }

  toggleChangeEmailRemove() {
    this.setState({changeEmailRemove:!this.state.changeEmailRemove})
  }

  toggleViewPickupLocationRemove() {
    let result = this.props.taskEditor.pickup
    this.setState({viewPickupLocationRemove:result})
    if (result){
      this.props.dispatch({type:'PICKUP_MODE_OFF', payload:result})
    } else {
      this.props.dispatch({type:'PICKUP_MODE_ON', payload:result})
    }
  }

  componentDidMount(){
    this.updateStore()
  }

  toggleViewCartRemove() {
    this.setState({viewCartRemove:!this.state.viewCartRemove})
  }

  jsonifyGreetingText() {
    return({
      "text": this.state.welcomeText,
      "id": this.state.welcomeTextId,
      "job_flow": this.props.data.id,
      "created_by": this.props.user.id,
      "updated_by": this.props.user.id
    })
  }

  jsonifyPersistentMenu() {
    return ({
      id: this.props.data.persistentMenuId,
      changeEmailRemove: this.state.changeEmailRemove,
      viewPickupLocationRemove: this.state.viewPickupLocationRemove,
      "buttons":[{
        "title": this.state.viewProduct,
        "id": this.state.viewProductId,
        "payload": "/menu",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      },{
        "title": this.state.viewCart,
        "id": this.state.viewCartId,
        "payload": "$EE-C@rT",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      },{
        "title": this.state.changeEmail,
        "id": this.state.changeEmailId,
        "payload": "c0mm@nd~~change_email",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      },{
        "title": this.state.viewPickupLocation,
        "id": this.state.viewPickupLocationId,
        "payload": "/pickup~~locs",
        "button_type": 38,
        "created_by": this.props.user.id,
        "updated_by": this.props.user.id
      }],
      "job_flow": this.props.data.id,
      "created_by": this.props.user.id,
      "updated_by": this.props.user.id,
    })
  }

  updateStore(){
    let persistentMenu = this.jsonifyPersistentMenu()
    if (this.state.viewCartRemove && this.state.changeEmailRemove && this.state.viewPickupLocationRemove){
      persistentMenu.buttons.splice(1,3)
    } else if (this.state.viewCartRemove && this.state.changeEmailRemove){
      persistentMenu.buttons.splice(1,2)
      if(this.props.taskEditor.deliveryContainer && !this.props.taskEditor.deliveryContainer.pickUpChecked){
        persistentMenu.buttons.splice(1,1)
      }
    } else if (this.state.changeEmailRemove && this.state.viewPickupLocationRemove){
      persistentMenu.buttons.splice(2,2)
    } else if (this.state.viewCartRemove && this.state.viewPickupLocationRemove){
      persistentMenu.buttons.splice(3,1)
      persistentMenu.buttons.splice(1,1)
    } else if (this.state.viewCartRemove){
      persistentMenu.buttons.splice(1,1)
      if(this.props.taskEditor.deliveryContainer && !this.props.taskEditor.deliveryContainer.pickUpChecked){
       persistentMenu.buttons.splice(2,1)
      }
    } else if (this.state.changeEmailRemove){
      persistentMenu.buttons.splice(2,1)
      if(this.props.taskEditor.deliveryContainer && !this.props.taskEditor.deliveryContainer.pickUpChecked){
       persistentMenu.buttons.splice(2,1)
      }
    } else if (this.state.viewPickupLocationRemove){
      persistentMenu.buttons.splice(3,1)
    } 
    // console.log(JSON.stringify(persistentMenu))
    this.props.dispatch({
      type:'UPDATE_OPERATION_CONTAINER', 
      payload:{
        id: this.props.data.id,
        persistentMenu: persistentMenu,
        greetingText: this.jsonifyGreetingText(),
        changeEmailRemove: this.state.changeEmailRemove,
        viewPickupLocationRemove: this.state.viewPickupLocationRemove,
        viewCartRemove: this.state.viewCartRemove,
        welcomeTextError: this.state.welcomeTextError,
        viewProductError: this.state.viewProductError,
        viewCartError: this.state.viewCartError,
        changeEmailError: this.state.changeEmailError,
        viewPickupLocationError: this.state.viewPickupLocationError,
        errorModal:  this.state.errorModal
      }
    })
  }

  componentWillMount() {
    let data = this.props.data
    // console.log(JSON.stringify(data))
    if(this.props.taskEditor.editMode && this.props.taskEditor.operationContainer===null){
      this.setState({welcomeTextId: data.greeting_text[0].id})
      let viewPickupLocation = data.persistent_menu.find(menu=>menu.payload === "/pickup~~locs")

      if(viewPickupLocation){
        this.props.dispatch({type:'PICKUP_MODE_ON', payload: false})
      }else{
        this.props.dispatch({type:'PICKUP_MODE_OFF', payload: true})
      }
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }
  
  handleWelcomeTextChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({welcomeTextError:"error"})
      this.setState({errorModal: "error"})
      
    } else {
      this.setState({welcomeTextError:null})
    }
    this.setState({welcomeText:event.target.value})
  }

  handleProductChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({viewProductError:"error"})
      this.setState({error: "error"})
    } else {
      this.setState({viewProductError:null})
    }
    this.setState({viewProduct:event.target.value})
  }

  handleViewCartChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({viewCartError:"error"})
      this.setState({error: "error"})
    } else {
      this.setState({viewCartError:null})
    }
    this.setState({viewCart:event.target.value})
  }

  handleChangeEmailChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({changeEmailError:"error"})
      this.setState({error: "error"})
    } else {
      this.setState({changeEmailError:null})
    }
    this.setState({changeEmail:event.target.value})
  }

  handleViewPickupLocationChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({viewPickupLocationError:"error"})
      this.setState({error: "error"})
    } else {
      this.setState({viewPickupLocationError:null})
    }
    this.setState({viewPickupLocation:event.target.value})
  }

  save() {
    if(this.props.taskEditor.editMode){
      this.updateStore()
      this.props.dispatch(updateTask(this.props.taskEditor))
    } else {
      this.props.dispatch(saveTask(this.props.taskEditor))
    }
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header save={this.save.bind(this)} {...this.props}/>
        </Row>
        <Row className="task-editor-form">
          <Sidebar selected={'operation'} {...this.state} />
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} {...this.state}/>
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Instruction />
              </Col>
            </Row>
            <Row className="little-padding-top">
              <Col md={6}>
                <EditInterface {...this.state} 
                  handleWelcomeTextChange={this.handleWelcomeTextChange.bind(this)}
                  handleProductChange={this.handleProductChange.bind(this)}
                  handleViewCartChange={this.handleViewCartChange.bind(this)}
                  handleChangeEmailChange={this.handleChangeEmailChange.bind(this)}
                  handleViewPickupLocationChange={this.handleViewPickupLocationChange.bind(this)}
                  toggleChangeEmailRemove={this.toggleChangeEmailRemove.bind(this)}
                  toggleViewPickupLocationRemove={this.toggleViewPickupLocationRemove.bind(this)}
                  toggleViewCartRemove={this.toggleViewCartRemove.bind(this)} />
              </Col>
              <Col md={6}>
                <Row className="form-padding">
                  <Col md={12} className="text-center margin-bottom-top">
                    {this.state.welcomeText}
                  </Col>
                </Row>
                <Row>
                  <Col md={3}>
                  </Col>
                  <Col md={6}>
                    <Preview {...this.state} />
                  </Col>
                  <Col md={3}>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OperationContainer)


