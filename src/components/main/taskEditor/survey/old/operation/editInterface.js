import React from 'react'
import { Row, Form, FormGroup, Col, FormControl, ControlLabel, Glyphicon, Button } from 'react-bootstrap'

const EditInterface = (props) => {
  return (
    <div>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right">
            Welcome Text
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup validationState={props.welcomeTextError}>
              <Col sm={9}>
                <FormControl componentClass="textarea" 
                  placeholder="Welcome! Please select what you want to do." 
                  onChange={props.handleWelcomeTextChange} 
                  value={props.welcomeText} />
              </Col>
              <Col sm={3}>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right">
            View Product Button
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup validationState={props.viewProductError}>
              <Col sm={9}>
                <FormControl type="text" placeholder="View Product"
                  onChange={props.handleProductChange}
                  value={props.viewProduct}
                  maxLength={30} />
              </Col>
              <Col sm={3}>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right">
            View Cart Button
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup validationState={props.viewCartError}>
              <Col sm={9}>
                { !props.viewCartRemove?
                    <FormControl type="text" placeholder="View Cart"
                      onChange={props.handleViewCartChange}
                      value={props.viewCart}
                      maxLength={30} />
                    :
                    null
                }
              </Col>
              <Col sm={3}>
                <Button 
                  onClick={props.toggleViewCartRemove}
                  bsSize="small"
                  bsStyle={props.viewCartRemove?"success":"danger"} >
                  { 
                    !props.viewCartRemove?
                      <Glyphicon glyph="glyphicon glyphicon-remove" />
                      :
                      <Glyphicon glyph="glyphicon glyphicon-plus" />
                  }
                </Button>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right">
            Change Email Button
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup validationState={props.changeEmailError}>
              <Col sm={9}>
                { 
                  !props.changeEmailRemove?
                    <FormControl type="text" placeholder="Change Email"
                      onChange={props.handleChangeEmailChange}
                      value={props.changeEmail}
                      maxLength={30} />
                    :
                    null
                }
              </Col>
              <Col sm={3}>
                <Button 
                  onClick={props.toggleChangeEmailRemove}
                  bsSize="small"
                  bsStyle={props.changeEmailRemove?"success":"danger"} >
                  { 
                    !props.changeEmailRemove?
                      <Glyphicon glyph="glyphicon glyphicon-remove" />
                      :
                      <Glyphicon glyph="glyphicon glyphicon-plus" />
                  }
                </Button>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right">
            View Pickup Locations Button
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup validationState={props.viewPickupLocationError}>
              <Col sm={9}>
                { 
                  !props.viewPickupLocationRemove?
                    <FormControl type="text" placeholder="View Pick-up Location"
                      onChange={props.handleViewPickupLocationChange}
                      value={props.viewPickupLocation}
                      maxLength={30} />
                    :
                  null
                }
              </Col>
              <Col sm={3}>
                 <Button 
                  onClick={props.toggleViewPickupLocationRemove}
                  bsSize="small"
                  bsStyle={props.viewPickupLocationRemove?"success":"danger"} >
                  { 
                    !props.viewPickupLocationRemove?
                      <Glyphicon glyph="glyphicon glyphicon-remove" />
                      :
                      <Glyphicon glyph="glyphicon glyphicon-plus" />
                  }
                </Button>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
    </div>
  )
}

EditInterface.propTypes = {
  viewCartError: React.PropTypes.string,
  handleViewCartChange: React.PropTypes.func.isRequired,
  viewCart: React.PropTypes.string.isRequired,
  toggleViewCartRemove: React.PropTypes.func.isRequired,
  viewCartRemove: React.PropTypes.bool.isRequired,
  changeEmailError: React.PropTypes.string,
  handleChangeEmailChange: React.PropTypes.func.isRequired,
  changeEmail: React.PropTypes.string.isRequired,
  toggleChangeEmailRemove: React.PropTypes.func.isRequired,
  changeEmailRemove: React.PropTypes.bool.isRequired,
  viewPickupLocationError: React.PropTypes.string,
  handleViewPickupLocationChange: React.PropTypes.func.isRequired,
  viewPickupLocation: React.PropTypes.string.isRequired,
  toggleViewPickupLocationRemove: React.PropTypes.func.isRequired,
  viewPickupLocationRemove: React.PropTypes.bool.isRequired,
  welcomeTextError: React.PropTypes.string,
  handleWelcomeTextChange: React.PropTypes.func.isRequired,
  welcomeText: React.PropTypes.string.isRequired,
  viewProductError: React.PropTypes.string,
  handleProductChange: React.PropTypes.func.isRequired,
  viewProduct: React.PropTypes.string.isRequired,
}

export default EditInterface





// <Form horizontal>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           
//         </Col>
//         <Col sm={8}>
//           <FormControl componentClass="textarea" placeholder="Welcome! Please select what you want to do." 
//             onChange={props.handleWelcomeTextChange} value={props.welcomeText} />
//         </Col>
//       </FormGroup>      
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Product Button
//         </Col>
//         <Col sm={8}>

//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Cart Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="View Cart"
//             onChange={props.handleViewCartChange}
//             value={props.viewCart}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           Change Email Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="Change Email"
//             onChange={props.handleChangeEmailChange}
//             value={props.changeEmail}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Pick-up Location Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="View Pick-up Location"
//             onChange={props.handleViewPickupLocationChange}
//             value={props.viewPickupLocation}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//     </Form>