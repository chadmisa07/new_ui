import React from 'react'
import { Link } from 'react-router'

const Instruction = (props) => {
  return (
    <div className="little-padding-left">
      <p>
        This is the main operation menu for your user. 
        It will be displayed in standard menu form. Learn <Link to="">more</Link>.
      </p>
      <p>
        Please edit the welcome message and the menu button title texts.
      </p>
    </div>
  )
}

export default Instruction