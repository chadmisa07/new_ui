import React from 'react'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import Instruction from './instruction'
import FBPreview from './fbPreview'
import CheckoutForm from './checkoutForm'

import { saveTask, updateTask } from '../request/request'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface,
    taskEditor: state.taskEditor,
    taskName: "Order Task",
    processName: "Order Management",
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
    botLink: "Bot Name here (automatically filled) with the Bot created if launched at creation.",
    data: state.task.data
  }
}

class CheckoutContainer extends React.Component{

  constructor(props) {
    super(props)
    if (!this.props.taskEditor.editMode){
      if (props.taskEditor.checkoutContainer===null) {
         this.state = {
          askEmailChoice:true,
          checkout: {
            text: "Proceed with checkout?",
            qr_type: 53,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~proceed_with_checkout",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id,
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          }, 
          askReceipt: {
            text: "Do you want an email receipt?",
            qr_type: 54,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~send_email_receipt",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~do_checkout",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          },
          askEmail: {
            text: "Receiving a receipt requires your email. Provide your email?",
            qr_type: 55,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~wait_email",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          },
          emailError:{
            text_type:  56,
            message: "I'm sorry please input a valid email address.",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          checkoutError: null,
          askReceiptError: null,
          askEmailError: null,
          emailErrorTextError: null,
          provideEmail: {
            text_type:  57,
            message: "Please provide your email address",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          provideEmailError: null,
          saveEmail: {
            text_type:  58,
            message: "Email successfully saved",
            created_by: props.user.id,
            updated_by: props.user.id,
          },
          saveEmailError: null,
          retryEmailError: null,
          retryEmail: {
            text: "Your email is invalid. Do you want to try again?",
            qr_type: 59,
            created_by: props.user.id,
            updated_by: props.user.id,
            quick_replies:[{
              title:  "Yes",
              payload:  "c0mm@nd~~wait_email",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id,
            },{ 
              title:  "No",
              payload:  "c0mm@nd~~whatsnext",
              reply_type: 44,
              created_by: props.user.id,
              updated_by: props.user.id
            }]
          }
        }
      } else {
        this.state = props.taskEditor.checkoutContainer
      }
    }else{
      this.state = props.taskEditor.checkoutContainer
    } 
  }

  updateStore(){
    this.props.dispatch({type:'UPDATE_CHECKOUT_CONTAINER', payload:this.state})
  }

  componentDidMount() {
    this.updateStore()
  }

  componentWillMount(){
    if(!this.state.checkout.text.replace(/\s/g, '').length){
      this.setState({checkoutError:"error"})
    } else if (!this.state.askReceipt.text.replace(/\s/g, '').length){
      this.setState({askReceiptError:"error"})
    } else if (!this.state.askEmail.text.replace(/\s/g, '').length){
      this.setState({askEmailError:"error"})
    } else if (!this.state.emailError.message.replace(/\s/g, '').length){
      this.setState({emailErrorTextError:"error"})
    }
  }
  
  componentWillUnmount(){
    this.updateStore()
  }

  handleAskEmailCheck(){
    this.setState({askEmailChoice:true})
    this.updateStore()
  }

  handleSkipCheck(){
    this.setState({askEmailChoice:false})
    this.updateStore()
  }

  handleCheckoutChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({checkoutError:"error"})
    } else {
      this.setState({checkoutError:null})
    }
    let checkout = this.state.checkout
    checkout.text = event.target.value
    this.setState({ checkout: checkout })
  }

  handleAskReceiptChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({askReceiptError:"error"})
    } else {
      this.setState({askReceiptError:null})
    }
    let askReceipt = this.state.askReceipt
    askReceipt.text = event.target.value
    this.setState({ askReceipt: askReceipt })
  }

  handleAskEmailChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({askEmailError:"error"})
    } else {
      this.setState({askEmailError:null})
    }
    let askEmail = this.state.askEmail
    askEmail.text = event.target.value
    this.setState({ askEmail: askEmail })
  }

  handleEmailErrorChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({emailErrorTextError:"error"})
    } else {
      this.setState({emailErrorTextError:null})
    }
    let emailError = this.state.emailError
    emailError.message = event.target.value
    this.setState({ emailError: emailError })
  }

  handleRetryEmail = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({retryEmailError:"error"})
    } else {
      this.setState({retryEmailError:null})
    }
    let retryEmail = this.state.retryEmail
    retryEmail.text = event.target.value
    this.setState({ retryEmail: retryEmail })
  }

  handleProvideEmail = (event) =>{
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({provideEmailError:"error"})
    } else {
      this.setState({provideEmailError:null})
    }
    let provideEmail = this.state.provideEmail
    provideEmail.message = event.target.value
    this.setState({provideEmail: provideEmail})
  }

  handleSaveEmail = (event) =>{
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({saveEmailError:"error"})
    } else {
      this.setState({saveEmailError:null})
    }
    let saveEmail = this.state.saveEmail
    saveEmail.message = event.target.value
    this.setState({saveEmail: saveEmail})
  }

  save() {
    if(this.props.taskEditor.editMode){
      this.updateStore()
      this.props.dispatch(updateTask(this.props.taskEditor))
    }else{
      this.props.dispatch(saveTask(this.props.taskEditor))
    }
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header save={this.save.bind(this)} {...this.props} />
        </Row>
        <Row  className="task-editor-form">
          <Sidebar selected={'checkout'} {...this.state}/>
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} />
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Instruction />
              </Col>
            </Row>
            <Row>
              <Col md={6} className="form-padding little-padding-left">
                <CheckoutForm {...this.state}
                  handleAskEmailCheck={this.handleAskEmailCheck.bind(this)}
                  handleSkipCheck={this.handleSkipCheck.bind(this)}
                  handleCheckoutChange={this.handleCheckoutChange.bind(this)}
                  handleAskReceiptChange={this.handleAskReceiptChange.bind(this)}
                  handleAskEmailChange={this.handleAskEmailChange.bind(this)}
                  handleEmailErrorChange={this.handleEmailErrorChange.bind(this)} 
                  handleSaveEmail={this.handleSaveEmail.bind(this)}
                  handleProvideEmail={this.handleProvideEmail.bind(this)}
                  handleRetryEmail={this.handleRetryEmail.bind(this)}
                  />
              </Col>
              <Col md={6}>
                <FBPreview {...this.state} />
              </Col>
            </Row>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(CheckoutContainer)