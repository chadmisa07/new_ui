import React from 'react'
import { Form, FormGroup, Col, FormControl, ControlLabel, Checkbox } from 'react-bootstrap'

const EmailQuestions = (props) => {
  return (
    <div>
      <FormGroup validationState={props.askReceiptError}>
        <Col componentClass={ControlLabel} sm={5}>
          Inquire Text
        </Col>
        <Col sm={7}>
          <FormControl type="text" placeholder="Would you like to get a record sent by Email?"
            onChange={props.handleAskReceiptChange}
            value={props.askReceipt.text} />
        </Col>
      </FormGroup>

      <FormGroup validationState={props.askEmailError}>
        <Col componentClass={ControlLabel} sm={5}>
          Input Email Text
        </Col>
        <Col sm={7}>
          <FormControl type="text" placeholder="Please provide a valid email address (ie: aaa@bbb.com)"
            onChange={props.handleAskEmailChange}
            value={props.askEmail.text} />
        </Col>
      </FormGroup>

      <FormGroup validationState={props.provideEmailError}>
        <Col componentClass={ControlLabel} sm={5}>
          Provide Email Text
        </Col>
        <Col sm={7}>
          <FormControl type="text" placeholder="Please provide your email address"
            onChange={props.handleProvideEmail}
            value={props.provideEmail.message} />
        </Col>
      </FormGroup>

      <FormGroup validationState={props.emailErrorTextError}>
        <Col componentClass={ControlLabel} sm={5}>
          Email Error Text
        </Col>
        <Col sm={7}>
          <FormControl componentClass="textarea" 
            placeholder="I'm sorry please input a valid email address."
            onChange={props.handleEmailErrorChange}
            value={props.emailError.message} />
        </Col>
      </FormGroup>
       <FormGroup validationState={props.retryEmailError}>
        <Col componentClass={ControlLabel} sm={5}>
          Retry Input Email
        </Col>
        <Col sm={7}>
          <FormControl type="text" placeholder="Please provide your email again."
          onChange={props.handleRetryEmail}
          value={props.retryEmail.text}/>
        </Col>
      </FormGroup>
      <FormGroup validationState={props.saveEmailError}>
        <Col componentClass={ControlLabel} sm={5}>
          Email Saved Text
        </Col>
        <Col sm={7}>
          <FormControl type="text" placeholder="Email successfully saved." 
          onChange={props.handleSaveEmail}
          value={props.saveEmail.message}/>
        </Col>
      </FormGroup>
    </div>
  )
}

EmailQuestions.propTypes = {
  askReceiptError: React.PropTypes.string,
  askReceipt: React.PropTypes.object.isRequired,
  handleAskReceiptChange: React.PropTypes.func.isRequired,
  askEmailError: React.PropTypes.string,
  askEmail: React.PropTypes.object.isRequired,
  handleAskEmailChange: React.PropTypes.func.isRequired,
  provideEmailError: React.PropTypes.string,
  handleProvideEmail: React.PropTypes.func.isRequired,
  provideEmail: React.PropTypes.object.isRequired,
  emailErrorTextError: React.PropTypes.string,
  handleEmailErrorChange: React.PropTypes.func.isRequired,
  emailError: React.PropTypes.object.isRequired,
  retryEmailError: React.PropTypes.string,
  handleRetryEmail: React.PropTypes.func.isRequired,
  retryEmail: React.PropTypes.object.isRequired,
  saveEmailError: React.PropTypes.string,
  handleSaveEmail: React.PropTypes.func.isRequired,
  saveEmail: React.PropTypes.object.isRequired
}


const CheckoutForm = (props) => {
  return (
    <Form horizontal>
      <FormGroup validationState={props.checkoutError}>
        <Col componentClass={ControlLabel} sm={5}>
          Checkout Response Text
        </Col>
        <Col sm={7}>
          <FormControl componentClass="textarea" placeholder="Proceed to checkout?"
            onChange={props.handleCheckoutChange}
            value={props.checkout.text} />
        </Col>
      </FormGroup>

      <FormGroup className="padding-top-30">
        <Col componentClass={ControlLabel} sm={5}>
          Do you want email?
        </Col>
        <Col sm={7}>
          <Checkbox 
            checked={!props.askEmailChoice}
            onChange={props.handleSkipCheck} >
              Skip asking for Email address
          </Checkbox>
        </Col>
      </FormGroup>
      <FormGroup>
        <Col smOffset={5} sm={7}>
          <Checkbox 
            checked={props.askEmailChoice}
            onChange={props.handleAskEmailCheck} > 
              Ask for Email address
          </Checkbox>
        </Col>
      </FormGroup>
      { props.askEmailChoice? <EmailQuestions {...props} /> : null }
    </Form>
  )
}

CheckoutForm.propTypes = {
  checkoutError: React.PropTypes.string,
  handleCheckoutChange: React.PropTypes.func.isRequired,
  checkout: React.PropTypes.object.isRequired,
  askEmailChoice: React.PropTypes.bool.isRequired,
  handleSkipCheck: React.PropTypes.func.isRequired,
  handleAskEmailCheck: React.PropTypes.func.isRequired,
}

export default CheckoutForm







