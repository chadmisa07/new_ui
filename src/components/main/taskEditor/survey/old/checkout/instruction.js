import React from 'react'
import { Link } from 'react-router'

const Instruction = (props) => {
  return (
    <div className="little-padding-left">
      <p>
        Here we will do the final checkout process for the user.
      </p>
      <p>
       You can opt to ask the user to input a valid e-mail to send the transaction record. Learn <Link to="">more</Link>.
      </p>
    </div>
  )
}

export default Instruction



