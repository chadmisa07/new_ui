import React from 'react'
import { Row, Col } from 'react-bootstrap'

const EmailQuestions = (props) => {
  return (
    <div>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          { props.askReceipt.text }
        </p>
      </Row>
      <Row className="text-center">
        <Col lg={12}>
          <p className="quick-reply-btn">
            Yes
          </p>
          <p className="quick-reply-btn">
            No
          </p>
        </Col>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          { props.askEmail.text }
        </p>
      </Row>
      <Row className="text-center">
        <Col lg={12}>
          <p className="quick-reply-btn">
            Yes
          </p>
          <p className="quick-reply-btn">
            No
          </p>
        </Col>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          {props.provideEmail.message}
        </p>
      </Row>
      <Row>
        <p className="sender-message-bubble text-center pull-right">
          sample@mail
        </p>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          { props.emailError.message }
        </p>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          {props.retryEmail.text}
        </p>
      </Row>
      <Row className="text-center">
        <Col lg={12}>
          <p className="quick-reply-btn">
            Yes
          </p>
          <p className="quick-reply-btn">
            No
          </p>
        </Col>
      </Row>
      <Row>
        <p className="sender-message-bubble text-center pull-right">
          sample@mail.com
        </p>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          {props.saveEmail.message}
        </p>
      </Row>
    </div>
  )
}

EmailQuestions.propTypes = {
  saveEmail: React.PropTypes.object.isRequired,
  retryEmail: React.PropTypes.object.isRequired,
  emailError: React.PropTypes.object.isRequired,
  provideEmail: React.PropTypes.object.isRequired,
  askEmail: React.PropTypes.object.isRequired,
  askReceipt: React.PropTypes.object.isRequired
}


const FBPreview = (props) => {
  return (
    <div className="form-padding chatbox-border">
      <Row>
        <p className="sender-message-bubble text-center pull-right">
          Checkout
        </p>
      </Row>
      <Row>
        <p className="recipient-message-bubble text-center pull-left">
          { props.checkout.text }
        </p>
      </Row>
      <Row className="text-center">
        <Col lg={12}>
          <p className="quick-reply-btn">
            Yes
          </p>
          <p className="quick-reply-btn">
            No
          </p>
        </Col>
      </Row>
      { props.askEmailChoice? <EmailQuestions {...props} /> : null }
    </div>
  )
}
FBPreview.propTypes = {
checkout: React.PropTypes.object.isRequired,
askEmailChoice: React.PropTypes.bool.isRequired
}

export default FBPreview