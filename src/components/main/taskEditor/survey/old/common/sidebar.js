import React from 'react'
import { Link } from 'react-router'
import { toastr } from 'react-redux-toastr'

const SideBar = (props) => {
  function showError(){
    toastr.error('Error', 'Please fill everything.')
  }
  return(


    <div className="flow-left-side-bar">
      <ul className="sidebar">
        <li className="sidebar">
          <Link className="sidebar-header text-center">Bot Task Editor</Link>
        </li>
        <li className="sidebar sidebar-xtra-padding">
          <Link className="sidebar sidebar-red">Menu Settings</Link>
        </li>
        <li className="sidebar">
        {
          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
          props.item_url_error !== "error" && props.image_url_error!== "error" && props.add_button_error !== "error" &&
          props.view_description_error !== "error" && props.locationNameError !== "error" && props.locationDescriptionError !== "error" &&
          props.location_image_url_error !== "error" && props.location_url_error !== "error" && props.contact_info_error !== "error" &&
          props.visit_page_error !== "error" && props.call_btn_error !== "error" && props.responseTextError !== "error" && 
          props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" && props.cartButtonResponseTextError !== "error" && 
          props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" && props.retryEmailError !== "error" && 
          props.saveEmailError !== "error" && props.provideEmailError !== "error"?
          <Link to="/order/operation"
            className={props.selected==='operation'? "sidebar active":"sidebar"}>1. Operation Menu</Link>: 
          <Link className={props.selected==='operation'? "sidebar active":"sidebar"} onClick={showError.bind(this)}>1. Operation Menu</Link>
        }
        </li>
        <li className="sidebar">
        {
          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
          props.item_url_error !== "error" && props.image_url_error!== "error" && props.add_button_error !== "error" &&
          props.view_description_error !== "error" && props.locationNameError !== "error" && props.locationDescriptionError !== "error" &&
          props.location_image_url_error !== "error" && props.location_url_error !== "error" && props.contact_info_error !== "error" &&
          props.visit_page_error !== "error" && props.call_btn_error !== "error" && props.responseTextError !== "error" && 
          props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" && props.cartButtonResponseTextError !== "error" && 
          props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" && props.retryEmailError !== "error" && 
          props.saveEmailError !== "error" && props.provideEmailError !== "error" ?
          <Link to="/order/product"
            className={props.selected==='product'? "sidebar active":"sidebar"}>2. Product Choices</Link>:
          <Link className={props.selected==='product'? "sidebar active":"sidebar"} onClick={showError.bind(this)}>2. Product Choices</Link>
        }
        </li>
        <li className="sidebar">
        {
          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
          props.item_url_error !== "error" && props.image_url_error!== "error" && props.add_button_error !== "error" &&
          props.view_description_error !== "error" && props.locationNameError !== "error" && props.locationDescriptionError !== "error" &&
          props.location_image_url_error !== "error" && props.location_url_error !== "error" && props.contact_info_error !== "error" &&
          props.visit_page_error !== "error" && props.call_btn_error !== "error" && props.responseTextError !== "error" && 
          props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" && props.cartButtonResponseTextError !== "error" && 
          props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" && props.retryEmailError !== "error" && 
          props.saveEmailError !== "error" && props.provideEmailError !== "error" ?
          <Link to="/order/quantity"
            className={props.selected==='quantity'? "sidebar active":"sidebar"}>3. Order Quantity</Link>: 
          <Link className={props.selected==='quantity'? "sidebar active":"sidebar"} onClick={showError.bind(this)}>3. Order Quantity</Link>
        }
        </li>
        <li className="sidebar">
        {
          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
          props.item_url_error !== "error" && props.image_url_error!== "error" && props.add_button_error !== "error" &&
          props.view_description_error !== "error" && props.locationNameError !== "error" && props.locationDescriptionError !== "error" &&
          props.location_image_url_error !== "error" && props.location_url_error !== "error" && props.contact_info_error !== "error" &&
          props.visit_page_error !== "error" && props.call_btn_error !== "error" && props.responseTextError !== "error" && 
          props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" && props.cartButtonResponseTextError !== "error" && 
          props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" && props.retryEmailError !== "error" && 
          props.saveEmailError !== "error" && props.provideEmailError !== "error" ?
          <Link to="/order/cart"
            className={props.selected==='cart'? "sidebar active":"sidebar"}>4. Cart</Link>:
          <Link className={props.selected==='cart'? "sidebar active":"sidebar"} onClick={showError.bind(this)}>4. Cart</Link>
        }
        </li>
        <li className="sidebar">
        {
          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
          props.item_url_error !== "error" && props.image_url_error!== "error" && props.add_button_error !== "error" &&
          props.view_description_error !== "error" && props.locationNameError !== "error" && props.locationDescriptionError !== "error" &&
          props.location_image_url_error !== "error" && props.location_url_error !== "error" && props.contact_info_error !== "error" &&
          props.visit_page_error !== "error" && props.call_btn_error !== "error" && props.responseTextError !== "error" && 
          props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" && props.cartButtonResponseTextError !== "error" && 
          props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" && props.retryEmailError !== "error" && 
          props.saveEmailError !== "error" && props.provideEmailError !== "error" ?
          <Link to="/order/delivery"
            className={props.selected==='delivery'? "sidebar active":"sidebar"}>5. Delivery / Pickup</Link>:
          <Link className={props.selected==='delivery'? "sidebar active":"sidebar"} onClick={showError.bind(this)}>5. Delivery / Pickup</Link>
        }
        </li>
        <li className="sidebar">
        {
          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
          props.item_url_error !== "error" && props.image_url_error!== "error" && props.add_button_error !== "error" &&
          props.view_description_error !== "error" && props.locationNameError !== "error" && props.locationDescriptionError !== "error" &&
          props.location_image_url_error !== "error" && props.location_url_error !== "error" && props.contact_info_error !== "error" &&
          props.visit_page_error !== "error" && props.call_btn_error !== "error" && props.responseTextError !== "error" && 
          props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" && props.cartButtonResponseTextError !== "error" && 
          props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" && props.retryEmailError !== "error" && 
          props.saveEmailError !== "error" && props.provideEmailError !== "error" ?
          <Link to="/order/checkout"
            className={props.selected==='checkout'? "sidebar active":"sidebar"}>6. Checkout</Link>:
          <Link className={props.selected==='checkout'? "sidebar active":"sidebar"} onClick={showError.bind(this)}>6. Checkout</Link>
        }
        </li>
      </ul>
    </div>
  )
}

export default SideBar