import React from 'react'
import { Link } from 'react-router'
import { Grid, Row, Col, Navbar } from 'react-bootstrap'
import { toastr } from 'react-redux-toastr'
import LoadingBar from 'react-redux-loading-bar'

const Header = (props) => {
  function showError(){
    toastr.error('Error', 'Please fill everything.')
  }
  return (
    <div className="header">
     <LoadingBar className="loadingBar"/>
      <Grid fluid>
        <Row>
          <Col lg={12}>
            <Navbar fluid defaultExpanded>
              <Navbar.Header>
                <Link to="/bot" className="pull-left header-orange">
                  Task Editor
                </Link>
              </Navbar.Header>
              <Navbar.Collapse>
                  <Navbar.Text pullLeft className="save header-border-bottom">
                      <a href="#">        
                        {
                          props.welcomeTextError !== "error"  && props.viewProductError !== "error" && props.viewCartError !== "error" && 
                          props.changeEmailError !== "error" && props.viewPickupLocationError !== "error" && props.nameError !== "error" &&
                          props.codeError !== "error" && props.descriptionError !== "error" && props.priceError !== "error" && 
                          props.item_url_error !== "error" && props.add_button_error !== "error" && props.view_description_error !== "error" &&
                          props.responseTextError !== "error" && props.quickReplyTextError !== "error" && props.cartResponseTextError !== "error" &&
                          props.cartButtonResponseTextError !== "error" && props.cartOrderButtonError !== "error" && props.cartViewCartButtonError !== "error" && 
                          props.cartCheckoutButtonError !== "error" && props.checkoutError !== "error" && props.askReceiptError !== "error" &&
                          props.askEmailError !== "error" &&  props.emailErrorTextError !== "error" ?
                          <span className="header-white" onClick={props.save}>Save</span>: 
                          <span className="header-white" onClick={showError.bind(this)}>Save</span>
                        }
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullLeft className="header-border-bottom">
                      <a href="#" className="header-white">
                        Close
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullLeft className="header-border-bottom">
                      <a href="#" className="header-white">
                        Import
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullLeft className="header-border-bottom">
                      <a href="#" className="header-white">
                        Export
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullRight className="header-border-bottom">
                    <a className="header-text-right" href="/logout" >Help</a>
                  </Navbar.Text>
                  <Navbar.Text pullRight>
                    <a className="header-text-right" href="/logout" >Deploy Bot</a>
                </Navbar.Text>
              </Navbar.Collapse>
            </Navbar>
          </Col>
        </Row>
      </Grid>
    </div>
  )
}

export default Header