import React from 'react'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'

const SaveLoader = (props) => (
  <div>
    <Segment>
      <Dimmer active inverted>
        <Loader>Loading</Loader>
      </Dimmer>
    </Segment>
  </div>
)

export default SaveLoader