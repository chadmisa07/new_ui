import React from 'react'
import { Thumbnail, Button, ButtonGroup } from 'react-bootstrap'



const MenuInterface = (props) => {

  return (
    <div>
      <Thumbnail src={props.previewInterface.product.image_url} 
        className="fb-button">
        <h3 className="fb-title">{props.previewInterface.product.name}</h3>
        <p className="fb-description">
          {"₱ " + props.previewInterface.product.price + " " + props.previewInterface.product.description}
        </p>
        <p className="product-url">{props.previewInterface.product.item_url}</p>
          <ButtonGroup vertical block>
            <Button className="fb-button">{props.previewInterface.add_button}</Button>
            <Button className="fb-button">{props.previewInterface.view_description}</Button>
          </ButtonGroup>
      </Thumbnail>
    </div>
  )
}
MenuInterface.propTypes = {
  previewInterface: React.PropTypes.object.isRequired
}

export default MenuInterface