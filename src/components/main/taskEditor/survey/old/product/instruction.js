import React from 'react'
import { Link } from 'react-router'

const Instruction = (props) => {
  return (
    <div className="little-padding-left">
      <p>
        This is where you will show the users product choices.
      </p>
      <p>
        The products will be displayed from left to right in carousel form.
        Learn <Link to="">more</Link>.        
      </p>
      <p>
        You will need to enter the product information for each choice.
        We have prepared 2 products for you to edit.
      </p>
    </div>
  )
}

export default Instruction
