import React from 'react'
import { connect } from 'react-redux'
import { Grid, Row, Col, Button, Glyphicon } from 'react-bootstrap'
import { toastr } from 'react-redux-toastr'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import Instruction from './instruction'
import ProductButton from './productBtn'
import ProductModal from './productModal'
import PreviewModal from './previewModal'
import validator from 'validator'

import { saveTask,updateTask } from '../request/request'

import cloudinary from 'cloudinary'

cloudinary.config({ 
  cloud_name: 'dvarqi8oo', 
  api_key: '356916821992389', 
  api_secret: '8offWzpvLu-bIut3xcBxkLbOlbk' 
})

let count = 2
let previewImage = null
let current_index = -1

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    app: state.app,
    taskEditor: state.taskEditor,
    taskName: "Order Task",
    processName: "Order Management",
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
    data: state.task.data
  }
}

class ProductContainer extends React.Component{

  constructor(props) {
    super(props)
    if(!this.props.taskEditor.editMode){
      if (props.app.createBotMode){
        if(props.taskEditor.productContainer===null) {
          this.state = {
            savingImage: false,
            addMode: true,
            showProductModal: false,
            showPreviewModal: false,
            name: "Add Menu",
            code: "",
            description: "",
            price: 0,
            image_url: "",
            preview_url: "",
            item_url: "",
            add_button: "",
            view_description: "",
            current_index: "",
            previewInterface:{},
            welcomeText: {
              "message": "We always have the best deals.",
              "text_type": 34,
              "created_by": props.user.id,
              "updated_by": props.user.id,
            },
            menuInterfaces: [this.newMenuInterface(), this.newMenuInterface()],
            nameError: null,
            codeError: null,
            descriptionError: null,
            priceError: null,
            image_url_error: null,
            item_url_error: null,
            add_button_error: null,
            view_description_error: null,
            invalidUrl: false
          }
        } else {
          this.state = props.taskEditor.productContainer
          count = props.taskEditor.productContainer.menuInterfaces.length
        }
      }
    } else {
        this.state = props.taskEditor.productContainer
        count = props.taskEditor.productContainer.menuInterfaces.length
    }
  }

  updateStore() {
    this.props.dispatch({type:"UPDATE_PRODUCT_CONTAINER", payload:this.state})
  }

  componentDidMount() {
    if(!this.props.taskEditor.editMode){
      this.updateStore()
    }
  }

  componentWillMount(){
    if(this.props.taskEditor.editMode && this.props.taskEditor.productContainer===null){
      count = this.props.data.menu_interface.length
    }
  }

  componentWillUnmount(){
    this.updateStore()
  }

  handleNameChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({nameError:"error"})
    } else {
      this.setState({nameError:null})
    }
    this.setState({ name: event.target.value })
  }

  handleCodeChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({codeError:"error"})
    } else {
      this.setState({codeError:null})
    }
    this.setState({ code: event.target.value })
  }

  handleDescriptionChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({descriptionError:"error"})
    } else {
      this.setState({descriptionError:null})
    }
    this.setState({ description: event.target.value })
  }

  handlePriceChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({priceError:"error"})
    } else {
      this.setState({priceError:null})
    }
    this.setState({ price: Number(event.target.value) })
  }

  handleImageChange = (event) => {
    this.setState({image_url:event.target.files[0]})
    // let reader = new FileReader();
    // reader.readAsDataURL(event.target.files[0])
    // reader.onloadend = () => {
    //   this.setState({
    //     image_url: reader.result
    //   });
    // }
  }

  handleItemChange = (event) => {
    if(!validator.isURL(event.target.value) || !event.target.value.replace(/\s/g, '').length){
      this.setState({invalidUrl: false})
      this.setState({item_url_error:"error"})
    } else {
      this.setState({invalidUrl: true})
      this.setState({item_url_error:null})
      
    }
    this.setState({ item_url: event.target.value })
  }

  handleAddBtnChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({add_button_error:"error"})
    } else {
      this.setState({add_button_error:null})
    }
    this.setState({ add_button: event.target.value })
  }

  handleViewDescriptionChange = (event) => {
    if(!event.target.value.replace(/\s/g, '').length){
      this.setState({view_description_error:"error"})
    } else {
      this.setState({view_description_error:null})
    }
    this.setState({ view_description: event.target.value })
  }

  mapItem(menuInterface){
    this.setState({
      name: menuInterface.product.name,
      code: menuInterface.product.code,
      description: menuInterface.product.description,
      price: menuInterface.product.price,
      image_url: menuInterface.product.image_url,
      item_url: menuInterface.product.item_url,
      add_button: menuInterface.add_button,
      view_description: menuInterface.view_description,
    })
  }

  newMenuInterface(){
    const userId = this.props.user.id
    return {
      "product": {
        "name": "",
        "code": "",
        "description": "",
        "price": 1,
        "image_url": "",
        "preview_url": "",
        "item_url": "",
        "created_by": userId,
        "updated_by": userId
      },
      "id": "",
      "add_button": "",
      "view_description": "",
      "index": count,
      "created_by": userId,
      "updated_by": userId
    }
  }

  openProductModal(menuInterface, index) {
    current_index = index
    this.setState({addMode:false, current_index:index})
    this.mapItem(menuInterface)
    this.setState({button_id:menuInterface.id})
    this.setState({product_id:menuInterface.product.id})
    this.setState({ showProductModal: true })
    this.setState({invalidUrl: true})
  }

  closeProductModal() {
    this.setState({ showProductModal: false })
    this.setState({nameError: null})
    this.setState({codeError: null})
    this.setState({descriptionError: null})
    this.setState({priceError: null})
    this.setState({image_url_error: null})
    this.setState({item_url_error: null})
    this.setState({add_button_error: null})
    this.setState({view_description_error: null})
    this.setState({invalidUrl: false})
  }

  openPreviewModal(menuInterface) {
    this.setState({ previewInterface: menuInterface })
    this.setState({ showPreviewModal: true })
  }

  closePreviewModal() {
    this.setState({ showPreviewModal: false })
  }

  addProductButton() {
    if (count < 10) {
      let menuInterfaces = this.state.menuInterfaces
      menuInterfaces.splice(count, 0, this.newMenuInterface())
      this.setState({menuInterfaces:menuInterfaces})
      count++
    } else {
      toastr.info('Alert', 'Unable to add more product. \nMaximum of 10 products reached.')
    }
  }

  saveMenuInterface() {
    if(!this.state.name){
      this.setState({nameError: "error"})
      toastr.error('Error', 'Please enter a Product Name')
    }
    else if(!this.state.code){
      this.setState({codeError: "error"})
      toastr.error('Error', 'Please enter a Product Code')
    }
    else if(!this.state.description){
      this.setState({descriptionError: "error"})
      toastr.error('Error', 'Please enter a Product Description')
    }
    else if(!this.state.price){
      this.setState({priceError: "error"})
      toastr.error('Error', 'Please enter a Product Price')
    }
    else if(!this.state.image_url){
      this.setState({image_url_error: "error"})
      toastr.error('Error', 'Please select an image')
    }
    else if(this.state.invalidUrl !== true || !this.state.item_url){
      this.setState({item_url_error: "error"})
      toastr.error('Error', 'Please enter a valid Url')
    }
    else if(!this.state.add_button){
      this.setState({add_button_error: "error"})
      toastr.error('Error', 'Please enter a Button Text')
    }
    else if(!this.state.view_description){
      this.setState({view_description_error: "error"})
      toastr.error('Error', 'Please enter a Button Text')
    }
    else{
      this.setState({savingImage:true})
      let menuInterfaces = this.state.menuInterfaces
      menuInterfaces[current_index] = this.getUserInput()
      this.setState({image_url_error: null})
      try {
        let reader = new FileReader()
        reader.readAsDataURL(menuInterfaces[current_index].product.image_url)
        reader.onloadend = () => {
          this.uploadImage(reader.result, menuInterfaces)
        }
      } catch (error) {
        this.setState({image_url_error: "error"})
        toastr.error('Error', 'Please select an image')
        this.setState({savingImage:false})
      }
    }
  }

  uploadImage(image, menuInterfaces) {
    cloudinary.uploader.upload(image, (result, error) => {
      if(result){
        menuInterfaces[current_index].product.image_url = result.url
        this.setState({menuInterfaces:menuInterfaces})
        this.closeProductModal()
        this.setState({savingImage:false})
        this.updateStore()
      } else {
        toastr.error("Error", "Unable to upload image")
        this.closeProductModal()
        this.setState({savingImage:false})
      }
    })
  }

  deleteInterface() {
    let menuInterfaces = this.state.menuInterfaces
    menuInterfaces.splice(this.state.current_index, 1)
    this.setState({ menuInterfaces: menuInterfaces })
    this.setState({ addMode: true })
    count--
    this.closeProductModal()
    toastr.success('Success', 'Successfully deleted interface')
  }

  deleteInterfaceAtIndex(index) {
    let menuInterfaces = this.state.menuInterfaces
    menuInterfaces.splice(index, 1)
    this.setState({ menuInterfaces: menuInterfaces })
    count--
    toastr.success('Success', 'Successfully deleted interface')
  }

  getUserInput() {
    const userId = this.props.user.id
    return {
      "add_button": this.state.add_button,
      "id": this.state.button_id,
      "view_description": this.state.view_description,
      "created_by": userId,
      "updated_by": userId,
      "index": count,
      "product": {
        "name": this.state.name,
        "id": this.state.product_id,
        "code": this.state.code,
        "description": this.state.description,
        "price": this.state.price,
        "image_url": this.state.image_url,
        "item_url": this.state.item_url,
        "preview_url": this.state.image_url,
        "created_by": userId,
        "updated_by": userId
      }
    }
  }

  save() {
    if(this.props.taskEditor.editMode){
      this.updateStore()
      this.props.dispatch(updateTask(this.props.taskEditor))
    } else {
      this.props.dispatch(saveTask(this.props.taskEditor))
    }
  }

  render() {

    const productButtons = this.state.menuInterfaces.map((menuInterface, index) => {
      return (
        <ProductButton key={index} index={index} menuInterface={menuInterface}
          openProductModal={this.openProductModal.bind(this, menuInterface, index)}
          openPreviewModal={this.openPreviewModal.bind(this, menuInterface)}
          deleteInterface={this.deleteInterfaceAtIndex.bind(this, index)} />
      )
    }) 

    let priceString = "₱ - "+this.state.price+" "

    return (
      <Grid fluid>
        <Row>
          <Header save={this.save.bind(this)} {...this.props}  {...this.state}/>
        </Row>
        <Row  className="task-editor-form">
          <Sidebar selected={'product'} {...this.state}/>
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} />
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Instruction />
              </Col>
            </Row>
            <Row className="little-padding-top little-padding-left">
              {productButtons}
              <Col md={2}>
                <div className="max-height black-box text-center product-btn ">
                  <Button bsStyle="primary" bsSize="small" onClick={this.addProductButton.bind(this)}
                    disabled={count>=10}>
                    <Glyphicon glyph="glyphicon glyphicon-plus"/> Add
                  </Button>
                  <p className="padding-top-45"></p>
                </div>
              </Col>
            </Row>
          </div>
          <ProductModal {...this.state}
            closeProductModal={this.closeProductModal.bind(this)} 
            saveMenuInterface={this.saveMenuInterface.bind(this)}
            handleNameChange={this.handleNameChange.bind(this)}
            handleCodeChange={this.handleCodeChange.bind(this)}
            handleDescriptionChange={this.handleDescriptionChange.bind(this)}
            handlePriceChange={this.handlePriceChange.bind(this)}
            handleImageChange={this.handleImageChange.bind(this)}
            handleItemChange={this.handleItemChange.bind(this)}
            handleAddBtnChange={this.handleAddBtnChange.bind(this)}
            handleViewDescriptionChange={this.handleViewDescriptionChange.bind(this)}
            deleteInterface={this.deleteInterface.bind(this)}
            descriptionLength={priceString.length} />
          <PreviewModal {...this.state} previewImage={previewImage}
            closePreviewModal={this.closePreviewModal.bind(this)} />
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(ProductContainer)