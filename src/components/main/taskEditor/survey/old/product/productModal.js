import React from 'react'
import { Modal, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap'

const ProductModal = (props) => {
  return (
    <Modal show={props.showProductModal} onHide={props.closeProductModal}>
      <Modal.Header closeButton>
        <Modal.Title>Add Menu</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <FormGroup validationState={props.nameError}>
            <ControlLabel>Product Name</ControlLabel>
            <FormControl type="text" placeholder="Product Name" maxLength={80} 
              value={props.name}
              onChange={props.handleNameChange} />
          </FormGroup>
          <FormGroup validationState={props.codeError}>
            <ControlLabel>Product Code</ControlLabel>
            <FormControl type="text" placeholder="Product Code"
              value={props.code}
              onChange={props.handleCodeChange} />
          </FormGroup>
          <FormGroup validationState={props.descriptionError}>
            <ControlLabel>Product Description</ControlLabel>
            <FormControl componentClass="textarea" placeholder="Describe your product here" maxLength={80 - props.descriptionLength}
              value={props.description}
              onChange={props.handleDescriptionChange} />
          </FormGroup>
          <FormGroup validationState={props.priceError}>
            <ControlLabel>Price</ControlLabel>
            <FormControl type="number" min={1}
              value={props.price}
              onChange={props.handlePriceChange} />
          </FormGroup>
          <FormGroup validationState={props.image_url_error}>
            <ControlLabel>Image</ControlLabel>
            <FormControl type="file" placeholder="Upload photo"
              value={props.image}
              onChange={props.handleImageChange} />
          </FormGroup>
          <FormGroup validationState={props.item_url_error}>
            <ControlLabel>Product URL</ControlLabel>
            <FormControl type="url" placeholder="Enter detail product URL to view in View More."
              value={props.item_url}
              onChange={props.handleItemChange} />
          </FormGroup>
          <FormGroup validationState={props.add_button_error}>
            <ControlLabel>"Add to Cart" Button Text</ControlLabel>
            <FormControl type="text" placeholder="Buy Now" maxLength={20}
              value={props.add_button}
              onChange={props.handleAddBtnChange} />
          </FormGroup>
          <FormGroup validationState={props.view_description_error}>
            <ControlLabel>"View More" Button Text.  This jumps to the product URL defined above.</ControlLabel>
            <FormControl type="text" placeholder="View More" maxLength={20}
              value={props.view_description}
              onChange={props.handleViewDescriptionChange} />
          </FormGroup>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="primary" onClick={props.saveMenuInterface} disabled={props.savingImage}>
          {props.savingImage?"Saving":"Save"}
        </Button>
        <Button bsStyle="danger" onClick={props.deleteInterface} disabled={props.savingImage}>
          Delete
        </Button>
        <Button bsStyle="default" onClick={props.closeProductModal.bind(this)} disabled={props.savingImage}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

ProductModal.propTypes = {
  closeProductModal: React.PropTypes.func.isRequired,
  savingImage: React.PropTypes.bool.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  saveMenuInterface: React.PropTypes.func.isRequired,
  view_description_error: React.PropTypes.string,
  view_description: React.PropTypes.string.isRequired,
  handleViewDescriptionChange: React.PropTypes.func.isRequired,
  add_button_error: React.PropTypes.string,
  add_button: React.PropTypes.string.isRequired,
  handleAddBtnChange: React.PropTypes.func.isRequired,
  item_url_error: React.PropTypes.string,
  item_url: React.PropTypes.string.isRequired,
  handleItemChange: React.PropTypes.func.isRequired,
  image_url_error: React.PropTypes.string,
  handleImageChange: React.PropTypes.func.isRequired,
  priceError: React.PropTypes.string,
  price: React.PropTypes.number.isRequired,
  handlePriceChange: React.PropTypes.func.isRequired,
  descriptionError: React.PropTypes.string,
  description: React.PropTypes.string.isRequired,
  handleDescriptionChange: React.PropTypes.func.isRequired,
  codeError: React.PropTypes.string,
  code: React.PropTypes.string.isRequired,
  handleCodeChange: React.PropTypes.func.isRequired,
  nameError: React.PropTypes.string,
  name: React.PropTypes.string.isRequired,
  handleNameChange: React.PropTypes.func.isRequired,
  showProductModal: React.PropTypes.bool.isRequired,
}

export default ProductModal


