import React from 'react'
import { Col, Button, Glyphicon } from 'react-bootstrap'
import { Link } from 'react-router'

const ProductButton = (props) => {
  return (
    <Col md={2}>
      <div className="black-box text-center product-btn">
        <Button bsStyle="primary" bsSize="small" onClick={props.openProductModal}>
          <Glyphicon glyph="glyphicon glyphicon-edit"/> Edit
        </Button>
        <p className="padding-top-15">
          <Button bsStyle="danger" bsSize="small" onClick={props.deleteInterface}>
            <Glyphicon glyph="glyphicon glyphicon-remove"/>
          </Button>
        </p>
        <p className="padding-top-15">{"Product " + (props.index+1).toString()}</p>
      </div>
      <div className="padding-top-15 text-center">
        {
          props.menuInterface.product.name===""? 
            <p>-</p>
            :
            <Link href="#" onClick={props.openPreviewModal}>
              {props.menuInterface.product.name}
            </Link>
        }
      </div>
    </Col>
  )
}

ProductButton.propTypes = {
  openProductModal: React.PropTypes.func.isRequired,
  deleteInterface: React.PropTypes.func.isRequired,
  index: React.PropTypes.number.isRequired,
  openPreviewModal: React.PropTypes.func.isRequired
}

export default ProductButton
