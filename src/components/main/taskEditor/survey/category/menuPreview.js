import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'

const Preview = (props) => {
  return (
    <Grid className="gray-box" fluid>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-preview-text"><b>Menu</b></span>
        </Col>
      </Row>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-blue-text">View Categories</span>
        </Col>
      </Row>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-blue-text">Help</span>
        </Col>
      </Row>
    </Grid>
  )
}

Preview.propTypes = {
  viewProduct: React.PropTypes.string.isRequired,
  viewPickupLocationRemove: React.PropTypes.bool.isRequired,
  viewPickupLocation: React.PropTypes.string.isRequired,
  changeEmailRemove: React.PropTypes.bool.isRequired,
  changeEmail: React.PropTypes.string.isRequired,
  viewCartRemove: React.PropTypes.bool.isRequired,
  viewCart: React.PropTypes.string.isRequired,
}

export default Preview