import React from 'react'
import { connect } from 'react-redux'
import { Col, Grid, Row, Button, Glyphicon } from 'react-bootstrap'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import SurveyForm from './surveyForm'
import PreviewModal from './previewModal'
import SurveyBtn from './surveyBtn'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    taskName: "Survey Task",
    processName: "Survey Management",
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
  }
}

class CategoryContainer extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      surveyForm: false,
      preview: false
    }
  }

  openSurveyForm(){
    this.setState({surveyForm:true})
  }

  closeSurveyForm(){
    this.setState({surveyForm:false})
  }

  openPreview(){
    this.setState({preview:true})
  }

  closePreview(){
    this.setState({preview:false})
  }
  
  render() {
    return (
      <Grid fluid>
        <Row >
          <Header/>
        </Row>
        <Row  className="task-editor-form">
          <Sidebar selected="category"/>
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} {...this.state} />
              </Col>
            </Row>
            <Row className="little-padding-top little-padding-left">
              <SurveyBtn openPreview={this.openPreview.bind(this)} />
              <SurveyBtn openPreview={this.openPreview.bind(this)} />
              <SurveyBtn openPreview={this.openPreview.bind(this)} />
              <Col md={2}>
                <div className="max-height black-box text-center product-btn">
                  <Button 
                    bsStyle="primary" 
                    bsSize="small"
                    onClick={this.openSurveyForm.bind(this)} >
                    <Glyphicon glyph="glyphicon glyphicon-plus"/> Add
                  </Button>
                  <p className="padding-top-45"></p>
                </div>
              </Col>
            </Row>
          </div>
          <SurveyForm 
            {...this.state}
            closeModal={this.closeSurveyForm.bind(this)} />
          <PreviewModal 
            {...this.state}
            closePreview={this.closePreview.bind(this)} />
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(CategoryContainer)