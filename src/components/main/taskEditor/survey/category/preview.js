import React from 'react'
import { Thumbnail, Button, ButtonGroup } from 'react-bootstrap'

const Preview = (props) => {
  return (
    <div>
      <Thumbnail src={"http://www.marginalboundaries.com/wp-content/uploads/Passive-Income.jpg"} 
        className="fb-button">
        <h3 className="fb-title">Income</h3>
        <p className="fb-description">
          This is a survey about your current income.
        </p>
        <ButtonGroup vertical block>
          <Button className="fb-button">Answer</Button>
          <Button className="fb-button">View Description</Button>
        </ButtonGroup>
      </Thumbnail>
    </div>
  )
}

export default Preview