import React from 'react'
import { Link } from 'react-router'
import { Grid, Row, Col, Navbar } from 'react-bootstrap'
import LoadingBar from 'react-redux-loading-bar'

const Header = (props) => {
  return (
    <div className="header">
     <LoadingBar className="loadingBar"/>
      <Grid fluid>
        <Row>
          <Col lg={12}>
            <Navbar fluid defaultExpanded>
              <Navbar.Header>
                <Link to="/bot" className="pull-left header-orange">
                  Task Editor
                </Link>
              </Navbar.Header>
              <Navbar.Collapse>
                  <Navbar.Text pullLeft className="save header-border-bottom">
                    <a href="#">    
                      <span className="header-white">Save</span>
                    </a>
                  </Navbar.Text>
                  <Navbar.Text pullLeft className="header-border-bottom">
                      <a href="#" className="header-white">
                        Close
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullLeft className="header-border-bottom">
                      <a href="#" className="header-white">
                        Import
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullLeft className="header-border-bottom">
                      <a href="#" className="header-white">
                        Export
                      </a>
                  </Navbar.Text>
                  <Navbar.Text pullRight className="header-border-bottom">
                    <a className="header-text-right" href="/logout" >Help</a>
                  </Navbar.Text>
                  <Navbar.Text pullRight>
                    <a className="header-text-right" href="/logout" >Deploy Bot</a>
                </Navbar.Text>
              </Navbar.Collapse>
            </Navbar>
          </Col>
        </Row>
      </Grid>
    </div>
  )
}

export default Header