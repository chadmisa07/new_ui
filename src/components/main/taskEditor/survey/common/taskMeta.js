import React from 'react'
import { Table } from 'react-bootstrap'

const TaskMeta = (props) => {
  return (
    <Table bordered className="form-padding half-width">
      <tbody>
        <tr>
          <td><b>Task Name</b></td>
          <td className="three-fourth red-text">{props.taskName}</td>
        </tr>
        <tr>
          <td><b>Process</b></td>
          <td className="red-text">{props.processName}</td>
        </tr>
        <tr>
          <td><b>Task Description</b></td>
          <td className="red-text">{props.taskDescription}</td>
        </tr>
        <tr>
          <td><b>Link to Bot</b></td>
          <td>{props.bot.name}</td>
        </tr>
      </tbody>
    </Table>
  )
}

export default TaskMeta
  