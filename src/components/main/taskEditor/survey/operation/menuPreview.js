import React from 'react'
import { Grid, Row, Col } from 'react-bootstrap'

const Preview = (props) => {
  const { viewCategories, helpText } = props
  return (
    <Grid className="gray-box" fluid>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-preview-text"><b>Menu</b></span>
        </Col>
      </Row>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-blue-text">{viewCategories}</span>
        </Col>
      </Row>
      <Row>
        <Col sm={12} className="gray-bot-border persistent-menu-text-padding">
          <span className="persistent-menu-blue-text">{helpText}</span>
        </Col>
      </Row>
      
    </Grid>
  )
}

export default Preview