import React from 'react'
import { connect } from 'react-redux'
import { Col, Grid, Row } from 'react-bootstrap'

import Header from '../common/header'
import Sidebar from '../common/sidebar'
import TaskMeta from '../common/taskMeta'
import EditInterface from './editInterface'
import MenuPreview from './menuPreview'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    menuInterface: state.menuInterface,
    taskName: "Survey Task",
    processName: "Survey Management",
    taskDescription: "Task for taking orders - Hello World!  Testing a new task...",
  }
}

class OperationContainer extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      welcomeText:"",
      viewCategories:"",
      helpText:""
    }
  }

  handleInputChange = (e) => {
    this.setState({[e.target.name]:e.target.value})
  }
  
  render() {    
    const preview = <MenuPreview {...this.state}/>
    return (
      <Grid fluid>
        <Row >
          <Header/>
        </Row>
        <Row  className="task-editor-form">
          <Sidebar selected="operation"/>
          <div className="page-content">
            <Row>
              <Col md={12}>
                <TaskMeta {...this.props} {...this.state} />
              </Col>
            </Row>
            <Row className="little-padding-top">
              <Col md={6}>
                <EditInterface 
                  handleInputChange={this.handleInputChange} />
              </Col>
              <Col md={6}>
                <Row className="form-padding">
                  <Col md={12} className="text-center margin-bottom-top">
                    {this.state.welcomeText}
                  </Col>
                </Row>
                <Row>
                  <Col md={3}>
                  </Col>
                  <Col md={6}>
                    { 
                      this.state.helpText!=='' || this.state.viewCategories!==''?
                      preview : null
                    }
                  </Col>
                  <Col md={3}>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OperationContainer)