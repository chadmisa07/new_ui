import React from 'react'
import { Row, Form, FormGroup, Col, FormControl, ControlLabel } from 'react-bootstrap'

const EditInterface = (props) => {
  return (
    <div>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right micro-top-padding">
            Welcome Text
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup>
              <Col sm={9}>
                <FormControl componentClass="textarea" 
                  placeholder="Welcome! Please select what you want to do."
                  name="welcomeText"
                  value={props.welcomeText}
                  onChange={props.handleInputChange.bind(this)} />
              </Col>
              <Col sm={3}>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right micro-top-padding">
            View Categories
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup>
              <Col sm={9}>
                <FormControl 
                  type="text" 
                  placeholder="View Categories"
                  maxLength={30}
                  value={props.viewCategories}
                  name={"viewCategories"}
                  onChange={props.handleInputChange.bind(this)} />
              </Col>
              <Col sm={3}>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
      <Row>
        <Col sm={4}>
          <ControlLabel className="pull-right micro-top-padding">
            Help
          </ControlLabel>
        </Col>
        <Col sm={8}>
          <Form horizontal>
            <FormGroup>
              <Col sm={9}>
                <FormControl 
                  type="text" 
                  placeholder="Help"
                  maxLength={30}
                  value={props.helpText}
                  name={"helpText"}
                  onChange={props.handleInputChange.bind(this)} />
              </Col>
              <Col sm={3}>
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row>
    </div>
  )
}

export default EditInterface





// <Form horizontal>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           
//         </Col>
//         <Col sm={8}>
//           <FormControl componentClass="textarea" placeholder="Welcome! Please select what you want to do." 
//             onChange={props.handleWelcomeTextChange} value={props.welcomeText} />
//         </Col>
//       </FormGroup>      
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Product Button
//         </Col>
//         <Col sm={8}>

//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Cart Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="View Cart"
//             onChange={props.handleViewCartChange}
//             value={props.viewCart}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           Change Email Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="Change Email"
//             onChange={props.handleChangeEmailChange}
//             value={props.changeEmail}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//       <FormGroup>
//         <Col componentClass={ControlLabel} sm={4}>
//           View Pick-up Location Button
//         </Col>
//         <Col sm={8}>
//           <FormControl type="text" placeholder="View Pick-up Location"
//             onChange={props.handleViewPickupLocationChange}
//             value={props.viewPickupLocation}
//             maxLength={30}/>
//         </Col>
//       </FormGroup>
//     </Form>