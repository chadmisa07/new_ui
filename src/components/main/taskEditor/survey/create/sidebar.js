import React from 'react'
import { Link } from 'react-router'

const SideBar = (props) => {
  return(
    <div className="flow-left-side-bar">
      <ul className="sidebar">
        <li className="sidebar">
          <Link className="sidebar-header text-center">Bot Task Editor</Link>
        </li>
        <li className="sidebar sidebar-xtra-padding">
          <Link className="sidebar sidebar-red">Menu Settings</Link>
        </li>
        <li className="sidebar">        
          <Link 
            to="/survey/operation" 
            className="sidebar"
            activeClassName="active" >
            1. Operation Menu
          </Link> 
          <Link 
            to="/survey/category" 
            className="sidebar"
            activeClassName="active" >
            2. Categories
          </Link> 
          <Link 
            to="/survey/question"
            className="sidebar"
            activeClassName="active" >
            3. Question and Answers
          </Link> 
        </li>
      </ul>
    </div>
  )
}

export default SideBar