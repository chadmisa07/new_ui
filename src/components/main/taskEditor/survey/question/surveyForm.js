import React from 'react'
import { Modal, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap'

const SurveyForm = (props) => {
  return (
    <Modal show={props.surveyForm} onHide={props.closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>Create Survey</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <FormGroup >
            <ControlLabel>Name</ControlLabel>
            <FormControl 
              type="text" 
              placeholder="Survey Name" 
              maxLength={80} 
              value={props.name} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Description</ControlLabel>
            <FormControl 
              type="textarea" 
              placeholder="Survey Description"
              value={props.description} />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Image</ControlLabel>
            <FormControl type="file" placeholder="Upload photo"/>
          </FormGroup>
        </form>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="primary">
          Save
        </Button>
        <Button bsStyle="danger">
          Delete
        </Button>
        <Button bsStyle="default" onClick={props.closeModal}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default SurveyForm


