import React from 'react'
import { Modal, Button } from 'react-bootstrap'
import Preview from './preview'

const PreviewModal = (props) => {
  return (
    <Modal show={props.preview} onHide={props.closePreview}>
      <Modal.Header closeButton>
        <Modal.Title>Template Preview</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Preview />
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.closePreview}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

export default PreviewModal


