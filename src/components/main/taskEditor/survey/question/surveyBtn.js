import React from 'react'
import { Col, Button, Glyphicon } from 'react-bootstrap'
import { Link } from 'react-router'

const SurveyButton = (props) => {
  return (
    <Col md={2}>
      <div className="black-box text-center product-btn">
        <Button bsStyle="primary" bsSize="small">
          <Glyphicon glyph="glyphicon glyphicon-edit"/> Edit
        </Button>
        <p className="padding-top-15">
          <Button bsStyle="danger" bsSize="small">
            <Glyphicon glyph="glyphicon glyphicon-remove"/>
          </Button>
        </p>
        <p className="padding-top-15">Product 1</p>
      </div>
      <div className="padding-top-15 text-center">
        <Link href="#" onClick={props.openPreview}>
          Sample Product
        </Link>
      </div>
    </Col>
  )
}

export default SurveyButton
