import axios from 'axios'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'
import * as api from '../utils/api'

export function mapToData(pages, userId, currentPages){
  let finalData = []
  pages.forEach(function(page, index) {
   let currentPage =  currentPages.find(current_page=>current_page.page_id === page.id)
   if(!currentPage){
    let newPage = {
        name : page.name,
        description : page.category,
        page_token : page.access_token,
        page_id : page.id,
        messenger_link : "https://m.me/"+page.id,
        bot: null,
        created_by : userId,
        updated_by : userId,
      }
      finalData.push(newPage)
    }
  })
  return finalData
}

export function getFacebookData(code, user, token, currentPages) {
  if(code){
    return function(dispatch) {
      dispatch({type: "GET_FB_ACCESS_TOKEN_REQUEST" })
      delete axios.defaults.headers.common['Authorization']
      axios.get("https://graph.facebook.com/v2.8/oauth/access_token?"+
                "client_id=1038731512904671&redirect_uri=https://cbot-tooling.herokuapp.com/fb/&"+
                "scope=manage_pages&client_secret=39192908f891a14e6a544110780a7927&code="+code)
        .then(function (response) {
          dispatch({type: "GET_FB_ACCESS_TOKEN_REQUEST_FULFILLED", payload: response.data})

          dispatch({type: "GET_FB_APP_ACCESS_TOKEN_REQUEST" })
          axios.get("https://graph.facebook.com/v2.8/oauth/access_token?client_id=1038731512904671&"+
                     "grant_type=client_credentials&client_secret=39192908f891a14e6a544110780a7927")
          .then(function (appAccessTokenResponse) {
            dispatch({type: "GET_FB_APP_ACCESS_TOKEN_REQUEST_FULFILLED", payload: appAccessTokenResponse.data})
            let appToken = appAccessTokenResponse.data.access_token
            let accessToken = response.data.access_token

              dispatch({type: "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST" })
              axios.get("https://graph.facebook.com/debug_token?input_token="+accessToken+
                         "&access_token="+appToken)
              .then(function (debugTokenResponse) {
                if(debugTokenResponse.data.data.scopes.length >= 5){
                  axios.defaults.headers.common['Authorization'] = 'Bearer ' + accessToken
                  let userId = debugTokenResponse.data.data.user_id
                     axios.get("https://graph.facebook.com/v2.8/"+userId+"/accounts")
                    .then(function (userResponse) {
                      delete axios.defaults.headers.common['Authorization']
                      axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                      axios.post(api.URL+"api/fb-page/", mapToData(userResponse.data.data, user, currentPages))
                        .then(function (response) {
                        // console.log(JSON.stringify(mapToData(userResponse.data.data, user, currentPages)))
                        toastr.success('Success', 'Page/s successfully saved')
                        dispatch({type: "PAGE_SUCESSFULLY_SAVED_FULFILLED" })
                        browserHistory.push("/administration")
                      })
                      .catch(function (error){
                         toastr.error('Error', 'Sorry, an error occured!')
                          dispatch({type: "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST_REJECTED" })
                          browserHistory.push("/administration")
                      })
                  })
                  .catch(function (error) {
                    toastr.error('Error', 'Sorry, an error occured!')
                    dispatch({type: "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST_REJECTED" })
                    browserHistory.push("/administration")
                  })
                } else {
                  toastr.error('Error', 'Please grant all needed permissions.')
                  dispatch({type: "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST_REJECTED" })
                  browserHistory.push("/administration")
                }
              })
              .catch(function (error) {
                toastr.error('Error', 'Sorry, an error occured!')
                dispatch({type: "GET_FB_ACCESS_TOKEN_DEBUG_REQUEST_REJECTED" })
                browserHistory.push("/administration")
              })
          })
          .catch(function (error) {
            toastr.error('Error', 'Sorry, an error occured!')
            dispatch({type: "GET_FB_APP_ACCESS_TOKEN_REQUEST_REJECTED" })
            browserHistory.push("/administration")
          })
        })
        .catch(function (error) {
          toastr.error('Error', 'Sorry, an error occured!')
          dispatch({type: "GET_FB_ACCESS_TOKEN_REQUEST_REJECTED" })
          browserHistory.push("/administration")
        })
     }  
  }else{
    toastr.error('Error', 'Please accept all the permissions.')
    browserHistory.push("/administration")
  }
}