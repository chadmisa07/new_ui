import React from 'react'
import { connect } from 'react-redux'
import logo from '../includes/img/logo.png'
import logo_small from '../includes/img/logo-small.png'
import { Link } from 'react-router'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
  }
}

class CreateBotContainer extends React.Component{
  constructor(props){
    super(props)
    this.state  ={
      sidebar:true
    }
  }
  render() {
    return (
      <div className={this.state.sidebar?"":"sidebar_hidden"}>
          <div className="navbar navbar-fixed-top">
            <div className="navbar-inner">
              <div className="container-fluid">
                <a className="brand tac" href="sched_index_week.html">
                  <img src={logo} className="visible-desktop visible-tablet" alt="NTUITIV" />
                  <img src={logo_small} className="visible-phone" alt="NTUITIV" />
                </a>
                <ul className="nav user_menu nav-items">
                  <li className="hidden-phone hidden-tablet"></li>
                  <li className="dropdown visible-desktop">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="fa fa-user-circle fa-lg icon-blue"></i>&nbsp;&nbsp;
                      Hi, John Appleseed! &nbsp;<i className="fa fa-caret-down icon-blue"></i></a>
                    <ul className="dropdown-menu">
                      <li><a href="login.html">Change Password</a></li>
                      <li><a href="login.html">Logout</a></li>
                    </ul>
                  </li>
                </ul>
                <ul className="nav nav-btns">
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                   <Link to="/bot-create" activeClassName="active">
                    <i className="fa fa-plus-circle icon-blue"></i>
                    <span className="action-btn-label"> Create New Bot</span>
                   </Link>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                    <Link to="/bot-edit" activeClassName="active"><i className="fa fa-pencil icon-blue"></i>
                    <span className="action-btn-label"> Edit Bots</span></Link>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                    <Link to="/home" activeClassName="active"><i className="fa fa-file-code-o icon-blue"></i>
                    <span className="action-btn-label"> Edit Bot Tasks</span></Link>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown">
                    <a href="#"><i className="fa fa-commenting-o icon-blue"></i>
                    <span className="action-btn-label"> Create Survey</span></a>
                  </li>
                  <li className="divider-vertical visible-phone visible-tablet"></li>
                  <li className="dropdown visible-phone visible-tablet pull-right">
                    <a href="#" className="dropdown-toggle" data-toggle="dropdown" data-target=".nav-collapse"><i className="fa fa-caret-down icon-blue"></i></a>
                    <ul className="dropdown-menu">
                      <li><a href="#">Change Password</a></li>
                      <li><a href="login.html">Logout</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div id="contentwrapper">
    <div className="main_content">
      <nav>
        <div className="breadCrumb module">
          <ul>
            <li><a href="index.html"><i className="icon-home"></i></a></li>
            <li>Bot Creation</li>
            <li>Edit Bots</li>
          </ul>
        </div>
      </nav>

    <div className="row-fluid">
      <div className="span12">
        <h3 className="heading">Edit Bots</h3>
        <div className="row-fluid">
          <div className="span12">
            <form className="form-horizontal">
              <div className="well">
                <p>
                  This is where you will <span className="orange-text">edit or delete bots that you created, as well as change its tasks</span>.<br/><br/>
                  The Steps involved are:
                  <ol>
                    <li>Select which bot to edit.</li>
                    <li>You can change its descriptions or change the linked tasks.</li>
                    <li>Save as new bot and relaunch it!</li>
                  </ol>
                  <span>Let's get started!</span>
                  <span className="pull-right">
                    <label htmlFor="checkbox">
                      <input type="checkbox" className="checkbox-position" id="checkbox"/> Don't show this again
                    </label>
                  </span>
                </p>
                <div>
                  
                </div>
              </div>
              <fieldset className="margin-top-20">
                <div className="span12">
                  <div className="well">
                  <p className="blue-text">Select which bot to edit by pressing the Edit button beside it.</p>
                  <br/>
                  <table className="table table-bordered table-striped table-overflow" data-provides="rowlink">
                    <thead>
                      <tr>
                        <th>Bot ID</th>
                        <th>Bot Name</th>
                        <th>Bot Description</th>
                        <th>Jobs Defined</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr className="rowlink">
                        <td>1</td>
                        <td>Sample Bot</td>
                        <td>Sample bot descriptions</td>
                        <td>order bot 1</td>
                        <td>
                          <button className="btn btn-primary btn-small" data-toggle="modal" data-backdrop="static" href="#editBotModal">
                          Edit</button>
                          <button className="btn btn-danger btn-small" data-toggle="modal" data-backdrop="static" href="#deleteBotModal">
                          Delete</button> 
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
         </div>
  </div>

  <a onClick={() => this.setState({sidebar:!this.state.sidebar})} className="sidebar_switch on_switch" title="Hide Menu"></a>
  <div className="sidebar">
    <div className="antiScroll">
      <div className="antiscroll-inner">
        <div className="antiscroll-content">
          <div className="sidebar_inner">
            <div className="timer tac">
              <span className="timer-time">14:09:56</span><br/>
              <span className="timer-date">Friday, August 28, 2015</span>
            </div>
          <div id="side_accordion" className="accordion">
            <div className="accordion-group">
              <div className="accordion-heading">
                <a href="#collapseOne" className="accordion-toggle" onClick="location.href='#'">
                  <i className="fa fa-cogs icon-blue"></i>&nbsp;&nbsp;&nbsp;Bot Creation
                </a>
              </div>
            </div>
            <div className="accordion-group">
              <div className="accordion-heading">
                <a href="#collapseOne" className="accordion-toggle" onClick="location.href='#'">
                  <i className="fa fa-briefcase icon-blue"></i>&nbsp;&nbsp;&nbsp;Bot Administration
                </a>
              </div>
            </div>
            <div className="accordion-group">
              <div className="accordion-heading">
                <a href="#collapseEleven" data-parent="#side_accordion" data-toggle="collapse" className="accordion-toggle">
                  <i className="fa fa-bar-chart icon-blue"></i>&nbsp;&nbsp;&nbsp;Business Process
                </a>
              </div>
              <div className="accordion-body collapse" id="collapseEleven">
                <div className="accordion-inner">
                  <ul className="nav nav-list">
                    <li><a href="#">Order Management</a></li>
                    <li><a href="#">Survey Management</a></li>
                    <li><a href="#">FAQ Management</a></li>
                    <li><a href="#">Custom Task</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="push"></div>
          </div> 
          </div>
        </div>
      </div>
    </div>
  </div>

<div className="modal hide fade" id="editBotModal">
  <div className="modal-header modal-header-new">
    <button className="close" data-dismiss="modal">×</button>
    <h3>Update Bot</h3>
  </div>
  <div className="modal-body">
    <div className="row-fluid">
      <div className="span12">
        <form className="form-horizontal">
          <fieldset>
            <div className="control-group formSep">
              <div className="control-group">
                <label className="">Bot ID</label>
                <input type="text" id="" className="span12" placeholder="Bot ID"  disabled="true" />
              </div>
              <div className="control-group">
                <label className="">Bot Name</label>
                <input type="text" id="" className="span12" placeholder="Bot ID"  value="Order Bot" />
              </div>
              <div className="control-group">
                <label className="">Bot Description</label>
                <input type="text" id="" className="span12" placeholder="Bot ID"  value="This is an order bot" />
              </div>
              <div className="control-group">
                <label className="">Choose Task</label>
                <select className="span12">
                  <option>Select Task</option>
                  <option>Order Bot 1</option>
                  <option>Survey 1</option>
                </select>
              </div>
              <br/>
              <div className="row-fluid">
                <div className="btns-action">
                  <a href="#" className="btn btn-primary">Update</a>
                  <a href="#" className="btn" data-dismiss="modal">Close</a>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div className="modal hide fade" id="deleteBotModal">
  <div className="modal-header modal-header-new">
    <button className="close" data-dismiss="modal">×</button>
    <h3>Delete Bot</h3>
  </div>
  <div className="modal-body">
    <div className="row-fluid">
      <div className="control-group formSep">
        <p>You are about to delete <span className="red-text">Sample</span>, are you sure you want to delete this bot?</p><br/>
        <div className="row-fluid">
          <div className="btns-action">
           <a href="#" className="btn btn-danger">Delete</a>
           <a href="#" className="btn" data-dismiss="modal">Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  </div>
    )
  }
}

export default connect(mapStateToProps)(CreateBotContainer)


