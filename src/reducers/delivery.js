export default function reducer(state=false, action) {
  switch(action.type) {
    case "DELIVERY_OPTION_CHOSEN": return action.payload
    default: return state
  }
}