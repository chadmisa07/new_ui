const defaultState = {
  bots:[],
  purchases:[]
}

export default function reducer(state=defaultState, action){
  switch(action.type) {
    case "FETCHED_BOTS_BUSINESS_PROCESSES": {
      return {...state, bots: action.payload}
    }
    case "FETCHED_PURCHASES_BUSINESS_PROCESSES": {
    	return {...state, purchases: action.payload}	
    }
    case "RESET_BUSINESS_PROCESSES": {
    	return {...state, purchases: [], bots:[]}	
    }
    default: {
      return {...state}
    }
  }
}
