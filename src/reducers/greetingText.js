export default function reducer(state={}, action) {
  switch(action.type) {
    case "GREETING_TEXT_CREATED": return action.payload
    default: return state
  }
}