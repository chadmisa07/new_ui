const default_state = {
	id: null,
	username: null,
	first_name: null,
	last_name: null,
}

export default function reducer(state=default_state, action){

	switch(action.type) {
		case "FETCHED_USER": 
			return {...state, id: action.payload.id, username: action.payload.username, 
				first_name: action.payload.first_name, last_name: action.payload.last_name
			}
		default: return state
	}
	
}