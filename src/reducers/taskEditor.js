let defaultState = {
  savingImage: false,
  cartContainer: null,
  checkoutContainer: null,
  deliveryContainer: null,
  operationContainer: null,
  productContainer: null,
  quantityContainer: null,
  bot: null,
  pickup:true,
  delivery:false,
  skip: false,
  editMode: false,
  taskId: null,
  taskName: "Order Bot",
  taskDescription: "Order Bot Description",
  taskNameError: null,
  taskDescriptionError: null
}

export default function reducer(state, action){
  switch(action.type) {
    case "UPDATE_TASK_NAME_ERROR": {
      return {...state, taskNameError: action.payload.taskNameError}
    }
    case "UPDATE_TASK_DESCRIPTION_ERROR": {
       return {...state, taskDescriptionError: action.payload.taskDescriptionError}
    }
    case "UPDATE_TASK_NAME_AND_DESCRIPTION": {
      return {...state, taskName: action.payload.name, taskDescription: action.payload.description}
    }
    case "UPDATE_CART_CONTAINER": {
      return {...state, cartContainer: action.payload}
    }
    case "UPDATE_CHECKOUT_CONTAINER": {
      return {...state, checkoutContainer: action.payload}
    }
    case "UPDATE_DELIVERY_CONTAINER": {
      return {...state, deliveryContainer: action.payload}
    }
    case "UPDATE_OPERATION_CONTAINER": {
      return {...state, operationContainer: action.payload, taskId: action.payload.id}
    }
    case "UPDATE_PRODUCT_CONTAINER": {
      return {...state, productContainer: action.payload}
    }
    case "UPDATE_QUANTITY_CONTAINER": {
      return {...state, quantityContainer: action.payload}
    }
    case "ASSIGN_BOT_FOR_TASK": {
      return {...state, bot: action.payload}
    }
    case "CREATE_TASK_REQUEST": {
      return {...state, fetching: true}
    }
    case "CREATE_TASK_REQUEST_FULFILLED": {
      return {...state, fetching: false}
    }
    case "CREATE_TASK_REQUEST_REJECTED": {
      return {...state, fetching: false}
    }
    case "SAVING_IMAGE_ON": {
      return {...state, savingImage: true}
    }
    case "SAVING_IMAGE_OFF": {
      return {...state, savingImage: false}
    }
    case "PICKUP_MODE_ON": {
      return {...state, pickup: true, delivery:false, skip:false}
    }
    case "PICKUP_MODE_OFF": {
      return {...state, pickup: false}
    }
    case "SKIP_MODE_ON": {
      return {...state, skip: true, delivery:false, pickup:false}
    }
    case "DELIVERY_MODE_ON": {
      return {...state, skip: false, delivery:true, pickup:false}
    }
    case "EDIT_MODE":{
      return{...state, editMode: action.payload}
    }
    case "RESET_TASK_EDITOR": {
      return defaultState
    }
    default: {
      return {...state}
    }
  }
}