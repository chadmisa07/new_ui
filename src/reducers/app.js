const defaultState = {createBotMode:true}

export default function reducer(state, action) {
  switch(action.type) {
    case "CREATE_BOT_MODE_ON": {
      return {...state, createBotMode: true}
    }
    case "EDIT_BOT_MODE_ON": {
      return {...state, createBotMode: false}
    }
    default: return defaultState
  }
}