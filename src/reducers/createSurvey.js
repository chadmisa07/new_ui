const defaultState = {
  survey:{
    name:"",
    description:"",
    image_url:"",
    id:"",
    survey_type:"qr"
  },
  task:{
    name:"",
    description:"",
  },
  questions:[],
  surveys:[],
  tasks:[]
}

export default function reducer(state=defaultState, action){
  switch(action.type) {
    case "SURVEY_CREATED": {
      return {...state, survey:action.payload}
    }
    case "TASK_CREATED": {
      return {...state, task:action.payload}
    }
    case "CLEAR_SURVEY": {
      return {...state, survey: {}}
    }
    case "FETCHED_SURVEYS":  {
      return {...state, surveys:action.payload}
    }
    case "FETCHED_TASKS":  {
      return {...state, tasks:action.payload}
    }
    default: {
      return {...state}
    }
  }
}