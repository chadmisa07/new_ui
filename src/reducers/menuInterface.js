export default function reducer(state, action) {
  switch(action.type) {
    case "MENU_INTERFACE_CREATED": {
      return {
        ...state,
        arr: [...state.arr, action.payload]
      }
    }
    case "RESET_MENU_INTERFACE": {
      return (
        {arr:[]}
      )
    }
    default: {
      return {...state}
    }
  }
}