export default function reducer(state={}, action) {
  switch(action.type) {

    case "BOTS_FETCH_REQUEST_FULFILLED": {
      return {...state, bots: action.payload}      
    }

    case "FB_PAGES_FETCH_REQUEST_FULFILLED": {
      return {...state, fb_pages: action.payload}
    }

    case "RESET_BOTS": {
    	return {...state, bots: []}  
    }
    default: return state
  }
}