const default_state = {
  pages: [],
  fetching: false,
  fetched: false,
  error: null
}

export default function reducer(state=default_state, action) {
  switch(action.type) {
    case "GET_FB_PAGE_REQUEST": {
      return {...state, fetching: true, error: null}
    }
    case "GET_FB_PAGE_REQUEST_FULFILLED": {
      return {...state, fetching: false, fetched: true, pages: action.payload.results}
    }
    case "GET_FB_PAGE_REQUEST_REJECTED": {
      return {...state, fetching: false, error: "Error fetching FB pages"}
    }
    default: {
      return {...state}
    }
  }
}