import React from 'react'
import { connect } from 'react-redux'

export default function(ComposedComponent) {
  class Authenticate extends React.Component {
    componentWillMount() {
      if (!this.props.is_authenticated) {
        this.context.router.push('/login')
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.is_authenticated) {
        this.context.router.push('/login')
      }
    }

    render() {
      return (
        <ComposedComponent {...this.props} />
      )
    }
  }

  Authenticate.propTypes = {
    is_authenticated: React.PropTypes.bool.isRequired,
  }

  Authenticate.contextTypes = {
    router: React.PropTypes.object.isRequired
  }

  function mapStateToProps(state) {
    return {
      is_authenticated: state.auth.is_authenticated
    };
  }

  return connect(mapStateToProps)(Authenticate);
}
